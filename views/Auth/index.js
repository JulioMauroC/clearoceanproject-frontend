import React from 'react'
import { Animated } from 'react-native'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import styled from 'styled-components/native'

import AuthSwitch from './components/AuthSwitch'
import AuthForm from './components/AuthForm'
import AuthTerms from './components/AuthTerms'

const AuthViewWrapper = styled.ScrollView`
  flex: 1;

`

const LogosFooter = styled.Image`
  width: 100%;
  /* position: absolute;
  bottom: 0;
  left: 0; */
  align-self:flex-end;
  transform:translateY(10px);
  
`

const AuthView = (props) => {
  const { registerRequest } = props
  const [ isRegisterActive, setIsRegisterActive ] = React.useState(true)

  const opacity = React.useState(new Animated.Value(0))[ 0 ]

  function fadeIn() {
    Animated.timing(opacity, {
      toValue: 1,
      duration: 2000,
      useNativeDriver: true,
    }).start()
  }

  React.useEffect(() => {
    fadeIn()
  }, [])

  return (
    <AuthViewWrapper showsVerticalScrollIndicator={ false }>
      <KeyboardAwareScrollView
        behavior="padding"
        enabled
        style={ { flex: 1 } }
        key="key"

      >
        <Animated.View
          style={ {
            flex: 1,
            opacity,
          } }
        >
          <AuthSwitch
            action={ setIsRegisterActive }
            isRegisterActive={ isRegisterActive }
          />
          <AuthForm
            isRegisterActive={ isRegisterActive }
            registerRequest={ registerRequest }
          />
        </Animated.View>
      </KeyboardAwareScrollView>

      <AuthTerms />
      <LogosFooter
        source={ require('assets/images/footer-logos.png') }
        resizeMode="contain"
      />
    </AuthViewWrapper>
  )
}

export default AuthView
