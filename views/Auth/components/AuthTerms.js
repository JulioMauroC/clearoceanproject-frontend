import React from 'react'
import { Text, StyleSheet } from 'react-native'
import { FONT_SIZES } from 'consts'
import styled from 'styled-components/native'
import * as RootNavigation from 'navigation/RootNavigation'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

const AuthTermsWrapper = styled.View`
  width: 100%;
  /* flex: 1; */
  align-self: flex-end;
  justify-content: flex-end;
  padding-left: ${ (props) => props.theme.dimensions.appHorizontalPadding };
  padding-right: ${ (props) => props.theme.dimensions.appHorizontalPadding };
  opacity: 0.85;
  margin-top: ${ heightToDp('7%') };
`

const styles = StyleSheet.create({
  text: {
    fontSize: 11,
    color: 'rgba(2, 2, 2, 0.7)',
  },
  link: {
    textDecorationLine: 'underline',
    fontSize: FONT_SIZES.MIDSMALL,
    color: 'rgba(2, 2, 2, 1)',
  },
})

const AuthTerms = () => (
  <AuthTermsWrapper>
    <Text style={ styles.text }>
      By clicking on Sign up, you agree to
      {' '}
      <Text
        onPress={ () => RootNavigation.navigate('Terms', {
          screen: 'Terms of Use',
        }) }
        style={ styles.link }
      >
        Terms of Use.
        {'\n'}
        {'\n'}
      </Text>
      To learn more about how we collect, use, share and protect your personal
      data please read our
      <Text
        style={ styles.link }
        onPress={ () => RootNavigation.navigate('Terms', {
          screen: 'Privacy Policy',
        }) }
      >
        {' '}
        Privacy Policy.
      </Text>
      {'\n'}
      {'\n'}
      To consult our disclaimer press
      {' '}
      <Text
        onPress={ () => RootNavigation.navigate('Terms', {
          screen: 'Disclaimer',
        }) }
        style={ styles.link }
      >
        {' '}
        here.
      </Text>
    </Text>
  </AuthTermsWrapper>
)

export default AuthTerms
