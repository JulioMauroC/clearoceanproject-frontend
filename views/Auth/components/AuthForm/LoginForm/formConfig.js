import * as yup from 'yup'

export const initialValues = {
  email: '',
  password: '',
}

export const validationSchema = yup.object({
  email: yup.string().email('Invalid email format').required('Required!'),
  password: yup.string().min(8, 'Minimum 8 characters').required('Required!'),
})
