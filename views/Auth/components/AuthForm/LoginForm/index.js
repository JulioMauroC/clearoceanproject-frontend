import React from 'react'
import { TextInput } from 'react-native'

import { Formik } from 'formik'
import { compose } from 'recompose'
import styled from 'styled-components/native'
import LoginContainer from 'containers/Login'
import * as RootNavigation from 'navigation/RootNavigation'
import Button from 'components/Button'

import { COLORS } from 'consts'
import { initialValues, validationSchema } from './formConfig'

const LoginFormWrapper = styled.View`
  padding-left: ${ (props) => props.theme.dimensions.appHorizontalPadding };
  padding-right: ${ (props) => props.theme.dimensions.appHorizontalPadding };
  /* padding: 30px; */
  height: 100%;

  justify-content: space-between;
`

const InputsContainer = styled.View``

const InputWrapper = styled.View`
  padding: 0;
  /* margin-bottom: 15px; */
  border-bottom-width: 1px;
  border-bottom-color: rgba(73, 126, 147, 0.2);
  width: 100%;
`

const ErrorMessage = styled.Text`
  color: ${ COLORS.RED };
  font-size: 11px;
`

const ForgotPasswordButton = styled.TouchableOpacity`
  margin-top: 15px;
  display: flex;
`

const ForgotPasswordText = styled.Text`
  color: ${ COLORS.BLUE };
  font-size: 12px;
  font-family: 'Lato-Bold';
`

const LoginForm = (props) => {
  const { loginRequest, errorMessage } = props

  return (
    <Formik
      initialValues={ initialValues }
      validationSchema={ validationSchema }
      // enableReinitialize
      onSubmit={ async (values, { resetForm }) => {
        loginRequest(values)
      } }
    >
      {({
        handleChange,
        handleBlur,
        handleSubmit,
        values,
        errors,
        touched,
      }) => (
        <LoginFormWrapper>
          <InputsContainer>
            <InputWrapper>
              <TextInput
                name="email"
                style={ { height: 40 } }
                placeholder="email"
                onChangeText={ handleChange('email') }
                onBlur={ handleBlur('email') }
                value={ values.email }
                keyboardType="email-address"
              />
            </InputWrapper>
            {errors.email && touched.email && (
              <ErrorMessage>{errors.email}</ErrorMessage>
            )}
            <InputWrapper>
              <TextInput
                name="password"
                style={ { height: 40 } }
                placeholder="password"
                onChangeText={ handleChange('password') }
                onBlur={ handleBlur('password') }
                value={ values.password }
                secureTextEntry
              />
            </InputWrapper>
            {errors.password && touched.password && (
              <ErrorMessage>{errors.password}</ErrorMessage>
            )}

            {errorMessage && (
              <ErrorMessage style={ { marginTop: 20 } }>
                {errorMessage}
              </ErrorMessage>
            )}

            <ForgotPasswordButton
              onPress={ () => RootNavigation.navigate('ForgotPassword', {
                screen: 'ConfirmEmail',
              }) }
            >
              <ForgotPasswordText>Forgot Password?</ForgotPasswordText>
            </ForgotPasswordButton>
          </InputsContainer>

          <Button
            title="Log In"
            bgColor={ COLORS.LIGHT_BLUE }
            textColor="white"
            action={ (values) => handleSubmit(values) }
          />
        </LoginFormWrapper>
      )}
    </Formik>
  )
}

export default compose(LoginContainer)(LoginForm)
