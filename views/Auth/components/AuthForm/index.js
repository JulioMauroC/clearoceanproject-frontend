import React from 'react'
import styled from 'styled-components/native'
import RegisterForm from './RegisterForm'
import LoginForm from './LoginForm'

const AuthFormWrapper = styled.View`
  flex: 1;
  height: ${ (props) => props.theme.dimensions.authFormHeight };
  justify-content: center;
  /* border:3px solid red; */
  justify-content: flex-start;
  padding-top: ${ (props) => props.theme.dimensions.paddingLarge };

`

const AuthForm = (props) => {
  const { isRegisterActive } = props

  return (
    <AuthFormWrapper>
      {isRegisterActive ? <RegisterForm /> : <LoginForm />}

    </AuthFormWrapper>
  )
}

export default AuthForm
