import * as yup from 'yup'

export const initialValues = {
  firstName: '',
  lastName: '',
  email: '',
  password: '',
}

export const validationSchema = yup.object({
  firstName: yup
    .string()
    .min(2, 'Mininum 2 characters')
    .max(15, 'Maximum 15 characters')
    .required('Required!'),
  lastName: yup
    .string()
    .min(2, 'Mininum 2 characters')
    .max(15, 'Maximum 15 characters')
    .required('Required!'),
  email: yup.string().email('Invalid email format').required('Required!'),
  password: yup.string().min(8, 'Minimum 8 characters').required('Required!'),
})
