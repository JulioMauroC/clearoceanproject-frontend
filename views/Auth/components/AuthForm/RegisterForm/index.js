import React from 'react'
import { TextInput } from 'react-native'
import { Formik } from 'formik'
import { compose } from 'recompose'
import RegisterContainer from 'containers/Register'

import Button from 'components/Button'

import styled from 'styled-components/native'

import { COLORS } from 'consts'
import { initialValues, validationSchema } from './formConfig'

const RegisterFormWrapper = styled.View`
  padding-left: ${ (props) => props.theme.dimensions.appHorizontalPadding };
  padding-right: ${ (props) => props.theme.dimensions.appHorizontalPadding };
  /* padding: 30px; */
  height: 100%;

  justify-content: space-between;
`

const InputsContainer = styled.View``

const InputWrapper = styled.View`
  padding: 0;
  border-bottom-width: 1px;
  border-bottom-color: rgba(73, 126, 147, 0.2);
  width: 100%;
`

const ErrorMessage = styled.Text`
  color: ${ COLORS.RED };
  font-size: 11px;
`

const RegisterForm = (props) => {
  const { registerRequest, errorMessage } = props

  return (
    <Formik
      initialValues={ initialValues }
      validationSchema={ validationSchema }
  // enableReinitialize
      onSubmit={ async (values, { resetForm }) => {
        registerRequest(values)
      } }
    >
      {({
        handleChange, handleBlur, handleSubmit, values, errors, touched,
      }) => (
        <RegisterFormWrapper>
          <InputsContainer>
            <InputWrapper>
              <TextInput
                name="firstName"
                style={ { height: 40 } }
                placeholder="first name"
                onChangeText={ handleChange('firstName') }
                onBlur={ handleBlur('firstName') }
                value={ values.firstName }
              />
            </InputWrapper>
            {errors.firstName && touched.firstName && (
            <ErrorMessage>{errors.firstName}</ErrorMessage>
            )}

            <InputWrapper>
              <TextInput
                name="lastName"
                style={ { height: 40 } }
                placeholder="last name"
                onChangeText={ handleChange('lastName') }
                onBlur={ handleBlur('lastName') }
                value={ values.lastName }
              />
            </InputWrapper>
            {errors.lastName && touched.lastName && (
            <ErrorMessage>{errors.lastName}</ErrorMessage>
            )}

            <InputWrapper>
              <TextInput
                name="email"
                style={ { height: 40 } }
                placeholder="email"
                onChangeText={ handleChange('email') }
                onBlur={ handleBlur('email') }
                value={ values.email }
                keyboardType="email-address"
              />
            </InputWrapper>
            {errors.email && touched.email && (
            <ErrorMessage>{errors.email}</ErrorMessage>
            )}
            <InputWrapper>
              <TextInput
                name="password"
                style={ { height: 40 } }
                placeholder="password"
                onChangeText={ handleChange('password') }
                onBlur={ handleBlur('password') }
                value={ values.password }
                secureTextEntry
              />
            </InputWrapper>
            {errors.password && touched.password && (
            <ErrorMessage>{errors.password}</ErrorMessage>
            )}

            {errorMessage && (
            <ErrorMessage style={ { marginTop: 20 } }>{errorMessage}</ErrorMessage>
            )}
          </InputsContainer>

          <Button
            title="Register"
            bgColor={ COLORS.LIGHT_BLUE }
            textColor="white"
            action={ (values) => handleSubmit(values) }
          />
        </RegisterFormWrapper>
      )}
    </Formik>
  )
}

export default compose(RegisterContainer)(RegisterForm)
