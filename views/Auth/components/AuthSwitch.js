import React, { useState } from 'react'
import { Text, TouchableOpacity, StyleSheet } from 'react-native'
import styled from 'styled-components/native'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'
import { FONT_SIZES, FONTS, COLORS } from 'consts'

const AuthSwitchWrapper = styled.View`
  margin-top: ${ heightToDp('3%') };
  /* width: 100%; */
  align-self: center;
  flex-direction: row;
  justify-content: space-evenly;
  margin-left: ${ (props) => props.theme.dimensions.appHorizontalPadding };
  margin-right: ${ (props) => props.theme.dimensions.appHorizontalPadding };
  border-bottom-color: #abb3c0;
  border-bottom-width: 0.5;
`

const styles = StyleSheet.create({
  selectedLink: {
    borderBottomWidth: 1,
    borderBottomColor: COLORS.BLUE,
    paddingVertical: 0,
    paddingHorizontal: 25,
    marginHorizontal: 25,
  },
  unselectedLink: {
    paddingVertical: 0,
    paddingHorizontal: 25,
    marginHorizontal: 25,
  },
  selectedText: {
    fontSize: FONT_SIZES.MIDLARGE,
    fontFamily: FONTS.DEFAULT_BOLD,
    paddingVertical: 5,
    marginBottom: 5,
  },
  unselectedText: {
    fontSize: FONT_SIZES.MIDLARGE,
    fontFamily: FONTS.DEFAULT,
    paddingVertical: 5,
    opacity: 0.5,
  },
})

const AuthSwitch = (props) => {
  const { action, isRegisterActive } = props

  return (
    <AuthSwitchWrapper>
      <TouchableOpacity
        style={
          isRegisterActive === true
            ? styles.selectedLink
            : styles.unselectedLink
        }
        onPress={ () => action(true) }
      >
        <Text
          style={
            isRegisterActive === true
              ? styles.selectedText
              : styles.unselectedText
          }
        >
          Register
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={
          isRegisterActive === true
            ? styles.unselectedLink
            : styles.selectedLink
        }
        onPress={ () => action(false) }
      >
        <Text
          style={
            isRegisterActive === true
              ? styles.unselectedText
              : styles.selectedText
          }
        >
          Login
        </Text>
      </TouchableOpacity>
    </AuthSwitchWrapper>
  )
}

export default AuthSwitch
