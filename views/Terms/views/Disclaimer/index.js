import * as React from 'react'
import styled from 'styled-components/native'
import PDFReader from 'rn-pdf-reader-js'

const Header = styled.View`
  justify-content: center;
  padding-top:25px;
  padding-bottom:25px;
`

const Title = styled.Text`
text-align:center;
font-size:22px;
`

const DisclaimerView = () => (
  <>
    {/* <Header>
      <Title>Disclaimer</Title>
    </Header> */}

    <PDFReader
      source={ {
        uri: 'https://cop-imgs.s3.eu-west-2.amazonaws.com/pdfs/disclaimer.pdf',
      } }
      withPinchZoom
    />
  </>
)
export default DisclaimerView
