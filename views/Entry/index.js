import React from 'react'

import { View } from 'react-native'
import { COLORS } from 'consts'
import { compose } from 'recompose'
import EntriesContainer from 'containers/Entries'

import styled from 'styled-components/native'

import Icon from 'components/Icon'

import ImagesSwiper from 'components/ImagesSwiper'
import Swiper from 'components/Swiper'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import Button from 'components/Button'
import EntryMap from 'components/EntryMap'

const EntryImageSwiper = styled.View`
  height: ${ heightToDp('30%') };
  width: 100%;
`

const FormattedText = styled.Text`
  font-size: ${ (props) => (props.fontSize ? props.fontSize : 12) }px;
  font-family: ${ (props) => (props.fontFamily ? props.fontFamily : 'Lato-Regular') };
  color: ${ (props) => (props.color ? props.color : '#707070') };
  margin-right: 10;
  margin-top: 5;
  text-align: ${ (props) => (props.textAlign ? props.textAlign : 'left') };
`

const EntryWrapper = styled.ScrollView`
  flex: 1;
  width: 100%;
`

const EntryWrapperBody = styled.View`
  padding: ${ widthToDp('6%') }px;
  background-color: white;
  border-top-left-radius: 20;
  border-top-right-radius: 20;
  margin-top: 50;
`

const EntryWrapperSection = styled.View`
  /* padding: ${ widthToDp('6%') }px; */
  padding-top: ${ heightToDp('4%') };
  padding-bottom: ${ heightToDp('4%') };
  width: 100%;
  position: relative;
  margin-top: 20;
`

const IconWrapper = styled.View`
  background-color: ${ (props) => (props.color ? props.color : props.theme.colors.red) };
  position: absolute;
  top: 0;
  left: 0%;
  width: ${ widthToDp('12%') }px;
  height: ${ widthToDp('6%') }px;
  /* transform: translateX(-${ widthToDp('8%') }px); */
  justify-content: center;
  align-items: center;
  border-radius: 50;
`

const DeleteButtonContainer = styled.View`
  justify-content: flex-end;
  background-color: white;
  border-top-left-radius: 20;
  border-top-right-radius: 20;
  margin-top: 50;
`

const EntryView = (props) => {
  const { deleteEntryRequest } = props

  const {
    authorFirstName,
    authorLastName,
    imageUrl,
    imageUrls,
    latitude,
    longitude,
    types,
    createdAtFormatted,
    isCurrentUserAdmin,
    yacht,
    id,
  } = props.route.params

  console.log('props')
  console.log(props)

  // const isEntryPrivate = yacht?.isPrivateProfile

  return (
    <EntryWrapper>
      <EntryImageSwiper>
        <ImagesSwiper data={ imageUrls } />
      </EntryImageSwiper>
      <EntryWrapperBody>
        <EntryWrapperSection
          style={ {
            // flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'flex-start',
          } }
        >
          <IconWrapper color="#e9f0f8">
            <Icon icon="user" color={ COLORS.MID_GREEN } size={ 16 } />
          </IconWrapper>

          <FormattedText fontSize={ 22 }>
            Uploaded By
            {'\n'}
            <FormattedText color="black" fontFamily="Lato-Bold" fontSize={ 26 }>
              {authorFirstName && authorLastName
                ? `${ authorFirstName } \n${ authorLastName }`
                : 'deleted user'}
            </FormattedText>
          </FormattedText>

          <FormattedText fontSize={ 16 }>{createdAtFormatted}</FormattedText>
        </EntryWrapperSection>

        <EntryWrapperSection>
          <IconWrapper color="#e9f0f8">
            <Icon icon="map-marker-alt" color={ COLORS.RED } size={ 16 } />
          </IconWrapper>
          <FormattedText fontSize={ 22 }>
            Location
            {'\n'}
          </FormattedText>

          <EntryMap
            latitude={ latitude }
            longitude={ longitude }
            authorFirstName={ authorFirstName || null }
            authorLastName={ authorLastName || null }
            types={ types }
          />

          <View style={ { paddingLeft: 5, flexDirection: 'row' } }>
            <FormattedText fontSize={ 13 }>{`lat: ${ latitude }`}</FormattedText>
            <FormattedText fontSize={ 13 }>{`lon: ${ longitude }`}</FormattedText>
          </View>
        </EntryWrapperSection>

        <EntryWrapperSection>
          <IconWrapper color="#e9f0f8">
            <Icon icon="wine-bottle" color={ COLORS.PURPLE } size={ 16 } />
          </IconWrapper>

          {types && types.length > 0 ? (
            <>
              <FormattedText fontSize={ 22 }>
                Types
                {'\n'}
              </FormattedText>
              <Swiper data={ types } />
            </>
          ) : (
            <FormattedText fontSize={ 18 }>
              No types associated with this entry
            </FormattedText>
          )}
        </EntryWrapperSection>

        {isCurrentUserAdmin && (
          <DeleteButtonContainer>
            <Button
              action={ () => deleteEntryRequest(id) }
              title="Delete Entry"
              bgColor={ COLORS.RED }
              textColor="white"
            />
          </DeleteButtonContainer>
        )}
      </EntryWrapperBody>
    </EntryWrapper>
  )
}

export default compose(EntriesContainer)(EntryView)
