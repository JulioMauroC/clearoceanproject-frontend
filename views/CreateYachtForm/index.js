import * as React from 'react'

import {
  Animated,
  View,
  Dimensions,
  KeyboardAvoidingView,
  Platform,
} from 'react-native'
import { Formik } from 'formik'

import { compose } from 'recompose'

import CreateYachtContainer from 'containers/CreateYachtForm'
import PrevNext from 'components/Forms/PrevNext'

import BackgroundAnim from 'components/Forms/BackgroundAnim'
import FormHeader from 'components/Forms/FormHeader'

import ImagesPreview from 'components/Forms/ImagesPreview'
import SubmitButton from 'components/Forms/Button'
import FormResume from 'components/Forms/FormResume'
import FieldsSwitcher from './components/Form/FieldsSwitcher'

import {
  DATA,
  initialValues,
  validationSchema,
} from './components/Form/FormConfig'

const { width } = Dimensions.get('screen')

const CreateYachtFormView = (props) => {
  const { createYachtRequest } = props

  const scrollX = React.useRef(new Animated.Value(0)).current
  const [ previewImage, setPreviewImage ] = React.useState()
  const [ isFormValid, setFormValid ] = React.useState(false)
  const [ hideHeader, setHideHeader ] = React.useState(false)

  const formRef = React.useRef()
  const flatListRef = React.useRef()
  const backgroundColors = DATA.map((item) => item.backgroundColor)

  const scrollToPrev = (currentIndex) => {
    flatListRef.current.scrollToIndex({
      animated: true,
      index: currentIndex - 1,
    })
  }

  const scrollToNext = (currentIndex) => {
    flatListRef.current.scrollToIndex({
      animated: true,
      index: currentIndex + 1,
    })
  }

  const scrollToIndex = (index) => {
    flatListRef.current.scrollToIndex({
      animated: true,
      index,
    })
  }

  const onScrollAction = () => Animated.event(
    [
      {
        nativeEvent: { contentOffset: { x: scrollX } },
      },
    ],
    { useNativeDriver: false },
  )

  return (
    <KeyboardAvoidingView
      style={ {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
      } }
      behavior={ Platform.OS === 'ios' ? 'padding' : 'height' }
      keyboardVerticalOffset={ 30 }
    >
      <Formik
        innerRef={ formRef }
        initialValues={ initialValues }
        validationSchema={ validationSchema }
        enableReinitialize
        onSubmit={ (val, { resetForm }) => {
          createYachtRequest(val)
        } }
      >
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          setFieldValue,
          values,
          errors,
          touched,
          setFieldTouched,
          isValid,
          dirty,
          validateForm,
          resetform,
          isSubmitting,
        }) => {
          // setFormErrors(errors)

          console.log(values)

          if (
            touched.YachtFlagInput
            && touched.YachtNameInput
            && !errors.YachtNameInput
            && !errors.YachtFlagInput
          ) {
            setFormValid(true)
          } else {
            setFormValid(false)
          }

          return (
            <View
              style={ {
                position: 'relative',

              } }
            >
              {/* <ProgressText text="Tell us about the yacht..." /> */}

              <BackgroundAnim
                isFormValid={ isFormValid }
                backgrounds={ backgroundColors }
                scrollX={ scrollX }
              />

              <Animated.FlatList
                removeClippedSubviews={ false }
                data={ DATA }
                ref={ flatListRef }
                keyExtractor={ (item) => item.key }
                horizontal
                scrollEventThrottle={ 32 }
                scrollToNext={ scrollToNext }
                onScroll={ onScrollAction() }
                onScrollBeginDrag={ () => {
                  setHideHeader(false)
                } }
                scrollEnabled={ false }
                contentContainerStyle={ { paddingBottom: 50 } }
                showsHorizontalScrollIndicator={ false }
                pagingEnabled
                renderItem={ ({ item, index }) => {
                  // LOOP EM TODOS OS FIELDS no config file
                  const {
                    isConfirmationScreen,
                    description,
                    backgroundColor,
                    title,
                    fieldName,
                    fieldIcon,
                    hasImagePreview,
                    triggerIcon,
                    buttonText,
                    required,
                    fieldLabel,
                    fieldSubLabel,
                  } = item

                  return (
                    <View
                      style={ {
                        width,
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        position: 'relative',

                        // borderWidth: 3,
                        // borderColor: 'red',

                      } }
                    >
                      <FormHeader
                        isConfirmationScreen={ isConfirmationScreen }
                        isFormValid={ isFormValid }
                        title={ title }
                        description={ description }
                        fieldIcon={ fieldIcon }
                        hideHeader={ hideHeader }
                        backgroundColor={ backgroundColor }
                        data={ DATA }
                        scrollX={ scrollX }
                      />

                      <ImagesPreview
                        previewImage={ previewImage }
                        hasImagePreview={ hasImagePreview }
                        fieldColor={ backgroundColor }
                        action={ () => {
                          setFieldValue(fieldName, null)
                          setPreviewImage(null)
                        } }
                        setFieldValue={ setFieldValue }
                      />

                      {fieldName && (
                        <FieldsSwitcher
                          { ...props }
                          fieldName={ fieldName }
                          fieldLabel={ fieldLabel }
                          fieldSubLabel={ fieldSubLabel }
                          handleChange={ handleChange }
                          handleBlur={ handleBlur }
                          setFieldValue={ setFieldValue }
                          values={ values }
                          value={ values[ fieldName ] }
                          setPreviewImage={ setPreviewImage }
                          previewImage={ previewImage }
                          buttonText={ buttonText }
                          errors={ errors }
                          touched={ touched }
                          setFieldTouched={ setFieldTouched }
                          fieldColor={ backgroundColor }
                          currentIndex={ index }
                          flatListRef={ flatListRef }
                          scrollToNext={ scrollToNext }
                          triggerIcon={ triggerIcon }
                          isConfirmationScreen={ isConfirmationScreen }
                          setHideHeader={ setHideHeader }
                          hideHeader={ hideHeader }
                        />
                      )}

                      <FormResume
                        isConfirmationScreen={ isConfirmationScreen }
                        touched={ touched }
                        errors={ errors }
                        isSubmitting={ isSubmitting }
                        values={ values }
                        formConfigData={ DATA }
                        scrollToIndex={ scrollToIndex }
                      />

                      <SubmitButton
                        text="Create Yacht"
                        handleSubmit={ handleSubmit }
                        isConfirmationScreen={ isConfirmationScreen }
                        isFormValid={ isFormValid }
                      />

                      <PrevNext
                        currentIndex={ index }
                        numOfSteps={ DATA.length }
                        scrollToPrev={ scrollToPrev }
                        scrollToNext={ scrollToNext }
                        fieldColor={ backgroundColor }
                        required={ required }
                        values={ values }
                        fieldName={ fieldName }
                        errors={ errors }
                        touched={ touched }
                      />
                    </View>
                  )
                } }
              />
            </View>
          )
        }}
      </Formik>

    </KeyboardAvoidingView>
  )
}

export default compose(CreateYachtContainer)(CreateYachtFormView)
