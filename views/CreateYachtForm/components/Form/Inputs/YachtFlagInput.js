import React from 'react'

import { Platform, TextInput, Dimensions } from 'react-native'
import PickerModal from 'components/Forms/PickerModal'

import styled from 'styled-components/native'

import SailCodes from 'consts/SailCodes'

import { COLORS } from 'consts'

const flagValues = SailCodes.map((code, index) => code)

const YachtFlagInputWrapper = styled.View``

function YachtFlagInput(props) {
  const {
    values,
    value,
    setFieldValue,
    fieldName,
    currentIndex,
    touched,
    fieldColor,
    scrollToNext,
    setFieldTouched,
    triggerIcon,
    errors,
  } = props

  const [ valid, setValid ] = React.useState(false)

  React.useEffect(() => {
    const wasTouched = touched[ fieldName ] === true
    const areNotErrors = errors[ fieldName ] === undefined

    if (wasTouched !== true) {
      setValid(null)
    } else if (wasTouched === true && !areNotErrors) {
      setValid(false)
    } else if (wasTouched === true && areNotErrors && value !== 'null') {
      setValid(true)
    }
  })

  return (
    <YachtFlagInputWrapper>
      <PickerModal
        pickerValues={ flagValues }
        setFieldValue={ setFieldValue }
        backgroundColor={ fieldColor }
        fieldName={ fieldName }
        setFieldTouched={ setFieldTouched }
        fieldColor={ fieldColor }
        valueFromForm={ values[ fieldName ] }
        scrollToNext={ scrollToNext }
        currentIndex={ currentIndex }
        triggerIcon={ triggerIcon }
        valid={ valid }
        formValues={ values }
      />
    </YachtFlagInputWrapper>
  )
}

export default YachtFlagInput
