import React from 'react'
import styled from 'styled-components/native'
import TextField from 'components/Forms/TextField'

const YachtNameInputWrapper = styled.View`
  width: 100%;
`

function YachtNameInput(props) {
  const {
    value, fieldName, touched, errors,
  } = props

  const [ valid, setValid ] = React.useState(false)

  React.useEffect(() => {
    const wasTouched = touched[ fieldName ] === true
    const areNotErrors = errors[ fieldName ] === undefined

    if (wasTouched !== true) {
      setValid(null)
    } else if (wasTouched === true && !areNotErrors) {
      setValid(false)
    } else if (wasTouched === true && areNotErrors && value !== 'null') {
      setValid(true)
    }
  })

  return (
    <YachtNameInputWrapper>
      <TextField { ...props } valid={ valid } placeholder="yacht's name" />
    </YachtNameInputWrapper>
  )
}

export default YachtNameInput
