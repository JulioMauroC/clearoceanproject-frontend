import * as yup from 'yup'
import { COLORS } from 'consts'

export const initialValues = {
  YachtNameInput: '',
  YachtFlagInput: '',
  YachtOfficialNumberInput: '',
  yachtImagesInput: [],
}

export const DATA = [
  {
    key: '3544572',
    title: 'Yacht Details',
    fieldName: 'YachtNameInput',
    fieldIcon: 'ship',
    backgroundColor: '#353f8f',
    required: true,
    formResumeTitle: "Yacht's name",
    type: 'text',
    fieldLabel: 'Enter Yacht Name',
  },
  {
    key: '917dd72',
    title: 'Yacht Details',
    fieldName: 'YachtFlagInput',
    fieldIcon: 'ship',
    backgroundColor: '#0e427f',
    buttonText: 'Click Here to share your location',
    triggerIcon: 'mousePointer',
    required: true,
    formResumeTitle: "Yacht's flag",
    type: 'picker',
    fieldLabel: 'Select Flag',
    fieldSubLabel: '(Click on the field to find your flag)',
  },
  {
    key: '3gn1747',
    title: 'Yacht Details',
    // description:
    //   'This field is optional, but your profile remain anonymous until official number is provided.',
    fieldName: 'YachtOfficialNumberInput',
    fieldIcon: 'ship',
    backgroundColor: '#145EB3',
    formResumeTitle: "Yacht's official number",
    type: 'text',
    fieldLabel: 'Yacht Official Number',
    fieldSubLabel: '(This is optional)',
  },
  {
    key: '3574lzz',
    title: 'Yacht Details',
    fieldIcon: 'ship',
    fieldName: 'YachtImageInput',
    backgroundColor: '#005E63',
    buttonText: 'Click to confirm',
    triggerIcon: 'upload',
    hasImagePreview: true,
    formResumeTitle: "Yacht's image",
    type: 'image',
    fieldLabel: 'Upload Picture',
  },
  {
    key: '3574lp0',
    title: 'Please confirm',
    fieldIcon: 'check-circle',
    // backgroundColor: '#0aa27f',
    backgroundColor: COLORS.RED,
    buttonText: 'Click to confirm',
    isConfirmationScreen: true,
  },
]

export const validationSchema = yup.object().shape({
  YachtNameInput: yup.string().required('Please enter the name for the yacht'),
  YachtFlagInput: yup.string().min(2).required('You need to select a flag'),

  // YachtImageInput: yup.object().required(),
})
