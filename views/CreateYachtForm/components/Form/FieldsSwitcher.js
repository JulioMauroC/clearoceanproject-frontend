import React from 'react'

import styled from 'styled-components/native'

import FieldLabels from 'components/Forms/FieldLabels'
import YachtNameInput from './Inputs/YachtNameInput'
import YachtFlagInput from './Inputs/YachtFlagInput'
import YachtOfficialNumberInput from './Inputs/YachtOfficialNumberInput'
import YachtImageInput from './Inputs/YachtImageInput'

const FieldsSwitcherWrapper = styled.View`
  bottom: ${ (props) => props.theme.dimensions.switchFieldBottomDistance };
  width: 65%;
  z-index: 50000;
`

export const fieldsReg = {
  YachtNameInput,
  YachtFlagInput,
  YachtOfficialNumberInput,
  YachtImageInput,
}

export const FieldsSwitcher = (props) => {
  const { fieldName, fieldLabel, fieldSubLabel } = props

  if (fieldName) {
    const CurrentField = fieldsReg[ fieldName ]
    return (
      <FieldsSwitcherWrapper>
        <FieldLabels fieldLabel={ fieldLabel } fieldSubLabel={ fieldSubLabel } />
        <CurrentField { ...props } />
      </FieldsSwitcherWrapper>
    )
  }
}

export default FieldsSwitcher
