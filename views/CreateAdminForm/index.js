import * as React from 'react'

import {
  Animated,
  View,
  Dimensions,
  KeyboardAvoidingView,
  Platform,
} from 'react-native'
import { Formik } from 'formik'

import { compose } from 'recompose'

import CreateAdminContainer from 'containers/CreateAdminForm'

import ImagesPreview from 'components/Forms/ImagesPreview'

import SubmitButton from 'components/Forms/Button'

import PrevNext from 'components/Forms/PrevNext'

import BackgroundAnim from 'components/Forms/BackgroundAnim'
import FormHeader from 'components/Forms/FormHeader'
import FormResume from 'components/Forms/FormResume'
import FieldsSwitcher from './components/Form/FieldsSwitcher'

import {
  DATA,
  initialValues,
  validationSchema,
} from './components/Form/FormConfig'

const { width, height } = Dimensions.get('screen')

const CreateAdminFormView = (props) => {
  const { createAdminRequest } = props

  const scrollX = React.useRef(new Animated.Value(0)).current
  const [ previewImage, setPreviewImage ] = React.useState()
  const [ formErrors, setFormErrors ] = React.useState()
  const [ isFormValid, setFormValid ] = React.useState(false)
  const [ hideHeader, setHideHeader ] = React.useState(false)

  const formRef = React.useRef()
  const flatListRef = React.useRef()
  const backgroundColors = DATA.map((item) => item.backgroundColor)

  const scrollToPrev = (currentIndex) => {
    flatListRef.current.scrollToIndex({
      animated: true,
      index: currentIndex - 1,
    })
  }

  const scrollToNext = (currentIndex) => {
    flatListRef.current.scrollToIndex({
      animated: true,
      index: currentIndex + 1,
    })
  }

  const scrollToIndex = (index) => {
    flatListRef.current.scrollToIndex({
      animated: true,
      index,
    })
  }

  return (
    <KeyboardAvoidingView
      style={ {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
      } }
      behavior={ Platform.OS === 'ios' ? 'padding' : 'height' }
      keyboardVerticalOffset={ 30 }
    >
      <Formik
        innerRef={ formRef }
        initialValues={ initialValues }
        validationSchema={ validationSchema }
        enableReinitialize
        onSubmit={ async (val, { resetForm }) => {
          await createAdminRequest(val)
          // await resetForm()
        } }
      >
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          setFieldValue,
          values,
          errors,
          touched,
          setFieldTouched,
          validationSchema,
          isValid,
          dirty,
          validateForm,
          pristine,
          isSubmitting,
        }) => {
          setFormErrors(errors)

          if (
            touched.AdminPositionInput
            && !errors.AdminPositionInput
            && values.AdminPositionInput
          ) {
            setFormValid(true)
          }

          return (
            <View
              style={ {
                position: 'relative',
              } }
            >

              <BackgroundAnim
                isFormValid={ isFormValid }
                backgrounds={ backgroundColors }
                scrollX={ scrollX }
              />

              <Animated.FlatList
                scrollEnabled={ false }
                removeClippedSubviews={ false }
                data={ DATA }
                ref={ flatListRef }
                keyExtractor={ (item) => item.key }
                horizontal
                scrollEventThrottle={ 32 }
                scrollToNext={ scrollToNext }
                onScroll={ Animated.event(
                  [
                    {
                      nativeEvent: { contentOffset: { x: scrollX } },
                    },
                  ],
                  { useNativeDriver: false },
                ) }
                contentContainerStyle={ { paddingBottom: 50 } }
                showsHorizontalScrollIndicator={ false }
                pagingEnabled
                renderItem={ ({ item, index }) => {
                  const {
                    isConfirmationScreen,
                    description,
                    backgroundColor,
                    title,
                    fieldName,
                    fieldIcon,
                    hasImagePreview,
                    triggerIcon,
                    buttonText,
                    required,
                    fieldLabel,
                    fieldSubLabel,
                  } = item

                  return (
                    <View
                      style={ {
                        position: 'relative',
                        width,
                        alignItems: 'center',
                        justifyContent: 'space-between',
                      } }
                    >
                      <FormHeader
                        isConfirmationScreen={ isConfirmationScreen }
                        isFormValid={ isFormValid }
                        title={ title }
                        description={ description }
                        fieldIcon={ fieldIcon }
                        hideHeader={ hideHeader }
                        backgroundColor={ backgroundColor }
                        data={ DATA }
                        scrollX={ scrollX }
                      />

                      <ImagesPreview
                        previewImage={ previewImage }
                        hasImagePreview={ hasImagePreview }
                        fieldColor={ backgroundColor }

                        action={ () => {
                          setFieldValue(fieldName, null)
                          setPreviewImage(null)
                        } }
                      />

                      {item.fieldName && (
                        <FieldsSwitcher
                          { ...props }
                          fieldName={ item.fieldName }
                          fieldLabel={ fieldLabel }
                          fieldSubLabel={ fieldSubLabel }
                          handleChange={ handleChange }
                          handleBlur={ handleBlur }
                          setFieldValue={ setFieldValue }
                          values={ values }
                          value={ values[ item.fieldName ] }
                          setPreviewImage={ setPreviewImage }
                          previewImage={ previewImage }
                          buttonText={ item.buttonText }
                          errors={ errors }
                          touched={ touched }
                          setFieldTouched={ setFieldTouched }
                          fieldColor={ item.backgroundColor }
                          currentIndex={ index }
                          flatListRef={ flatListRef }
                          scrollToNext={ scrollToNext }
                          triggerIcon={ item.triggerIcon }
                          isConfirmationScreen={ item.isConfirmationScreen }
                          setHideHeader={ setHideHeader }
                          hideHeader={ hideHeader }
                        />
                      )}

                      <FormResume
                        isConfirmationScreen={ isConfirmationScreen }
                        touched={ touched }
                        errors={ errors }
                        isSubmitting={ isSubmitting }
                        values={ values }
                        formConfigData={ DATA }
                        scrollToIndex={ scrollToIndex }
                      />

                      <SubmitButton
                        text="Create Profile"
                        handleSubmit={ handleSubmit }
                        isConfirmationScreen={ isConfirmationScreen }
                        isFormValid={ isFormValid }
                      />

                      <PrevNext
                        currentIndex={ index }
                        numOfSteps={ DATA.length }
                        scrollToPrev={ scrollToPrev }
                        scrollToNext={ scrollToNext }
                        fieldColor={ backgroundColor }
                        required={ required }
                        values={ values }
                        fieldName={ fieldName }
                        errors={ errors }
                        touched={ touched }
                      />
                    </View>
                  )
                } }
              />
            </View>
          )
        }}
      </Formik>

    </KeyboardAvoidingView>
  )
}

export default compose(CreateAdminContainer)(CreateAdminFormView)
