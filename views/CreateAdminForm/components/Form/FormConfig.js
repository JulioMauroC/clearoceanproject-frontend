import * as yup from 'yup'
import { COLORS } from 'consts'

export const initialValues = {
  AdminImageInput: '',
  AdminPositionInput: '',

}

export const DATA = [
  {
    key: 'aa44572',
    title: 'Your Details',
    fieldName: 'AdminImageInput',
    fieldIcon: 'user',
    backgroundColor: '#353f8f',
    hasImagePreview: true,
    triggerIcon: 'upload',
    formResumeTitle: 'Your image',
    type: 'image',
    fieldLabel: 'Upload Picture',
  },
  {
    key: '917dd72',
    title: 'Your Details',
    fieldName: 'AdminPositionInput',
    fieldIcon: 'user',
    backgroundColor: '#0e427f',
    // buttonText: 'Click Here to share your location',
    triggerIcon: 'mousePointer',
    required: true,
    formResumeTitle: 'Your role',
    type: 'picker',
    fieldLabel: 'Select your role',
  },

  {
    key: '3574lp0',
    title: 'Your Details',
    fieldIcon: 'check-circle',
    // backgroundColor: '#0aa27f',
    backgroundColor: COLORS.RED,
    buttonText: 'Click to confirm',
    isConfirmationScreen: true,
  },
]

export const validationSchema = yup.object().shape({
  AdminPositionInput: yup
    .string()
    .required('Please select your role at the yacht'),
})
