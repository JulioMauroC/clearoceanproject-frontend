import React from 'react'

import { View } from 'react-native'
import { COLORS } from 'consts'
import styled from 'styled-components/native'
import Avatar from 'components/Avatar'
import Icon from 'components/Icon'

import ProfileHeader from 'components/ProfileHeader'
import ImageBackground from 'components/ImageBackground'
import * as RootNavigation from 'navigation/RootNavigation'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import Button from 'components/Button'

const UserWrapper = styled.View`
  flex: 1;
  align-items: center;
  width: 100%;
`

const CloseButtonWrapper = styled.TouchableOpacity`
  position: absolute;
  right: 15px;
  top: 15px;
  z-index: 10000;
`

const UserCardWrapper = styled.View`
  position: relative;
  width: ${ (props) => props.theme.dimensions.userCardWidth };
  height: ${ (props) => props.theme.dimensions.userCardHeight };
  background-color: white;
  align-self: center;
  margin: auto;
  border-radius: 20px;
  box-shadow: 1px 2px 10px rgba(11, 33, 33, 0.4);
  
`

const UserCardHeader = styled.View`
  flex: 0.25;
  width: ${ (props) => props.theme.dimensions.userCardWidth * 0.8 };
  margin-left: auto;
  margin-right: auto;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  /* border:3px solid red; */
  padding-top: ${ (props) => props.theme.dimensions.paddingMedLarge };
  padding-left: ${ (props) => props.theme.dimensions.paddingMedium };
  padding-right: ${ (props) => props.theme.dimensions.paddingMedium };

`

const FormattedText = styled.Text`
  font-size: ${ (props) => (props.fontSize ? props.fontSize : 12) }px;
  
  font-family:${ (props) => (props.fontFamily ? props.fontFamily : 'Lato-Regular') };
  color: ${ (props) => (props.color ? props.color : '#707070') };
  /* margin-right: 10px; */
  text-align: left;
`

const HeaderTextWrapper = styled.View``

const UserStatusWrapper = styled.View`
  flex: 0.25;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0 40px;

`

const UserInfoWrapper = styled.View`
  flex: 0.1;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  padding: 0 40px;
`

const DeleteButtonContainer = styled.View`
  flex: 0.1;
  justify-content: flex-end;
  padding-left: ${ (props) => props.theme.dimensions.paddingMedLarge };
  padding-right: ${ (props) => props.theme.dimensions.paddingMedLarge };
  margin-top: auto;
  margin-bottom: ${ (props) => props.theme.dimensions.paddingLarge };
`

const UserView = (props) => {
  const {
    id,
    email,
    entries,
    position,
    profileImg,
    firstName,
    lastName,
    deleteUserRequest,
  } = props.route.params

  const numOfUploadedImagesFn = () => {
    let total = 0

    entries.forEach((entry) => {
      total += entry.imageUrl.length
    })

    return total
  }

  const numOfUploadedImages = numOfUploadedImagesFn()

  const numOfUploadedTypesFn = () => {
    const typesArr = []

    entries.forEach((entry) => {
      entry.types.forEach((type) => {
        if (typesArr.indexOf(type) === -1) {
          typesArr.push(type)
        }
      })
    })

    return typesArr.length
  }

  const numOfUploadedTypes = numOfUploadedTypesFn()

  return (
    <UserWrapper>
      <ImageBackground />
      <ProfileHeader />
      <UserCardWrapper>
        <CloseButtonWrapper
          hitSlop={ {
            top: 10,
            bottom: 10,
            left: 10,
            right: 10,
          } }
          onPress={ () => RootNavigation.navigate('Users') }
        >
          <Icon icon="times-circle" color={ COLORS.RED } size={ 16 } />
        </CloseButtonWrapper>

        <UserCardHeader>
          <Avatar
            imgUrl={ profileImg }
            size={ widthToDp('20%') }
            firstName={ firstName }
            lastName={ lastName }
          />
          <HeaderTextWrapper>
            <FormattedText color="black" fontSize={ 18 } fontFamily="Lato-Bold">
              {`${ firstName } ${ lastName }`}
            </FormattedText>
            <FormattedText fontSize={ 14 } fontFamily="Lato-Light">
              {email}
            </FormattedText>
            <FormattedText fontSize={ 14 } fontFamily="Lato-Light">
              {position}
            </FormattedText>
          </HeaderTextWrapper>
        </UserCardHeader>
        <UserStatusWrapper>
          <View>
            <FormattedText color="black" fontSize={ 12 } fontFamily="Lato-Light">
              <FormattedText color="black" fontSize={ 24 } fontFamily="Lato-Bold">
                {entries.length}
                {'\n'}
              </FormattedText>
              {entries.length === 1 ? ' Entry' : ' Entries'}
            </FormattedText>
          </View>
          <View>
            <FormattedText color="black" fontSize={ 12 } fontFamily="Lato-Light">
              <FormattedText color="black" fontSize={ 24 } fontFamily="Lato-Bold">
                {numOfUploadedImages}
                {'\n'}
              </FormattedText>
              {numOfUploadedImages === 1 ? 'Image Upload' : ' Image Uploads'}
            </FormattedText>
          </View>
          <View>
            <FormattedText color="black" fontSize={ 12 } fontFamily="Lato-Light">
              <FormattedText color="black" fontSize={ 24 } fontFamily="Lato-Bold">
                {numOfUploadedTypes}
                {'\n'}
              </FormattedText>
              {numOfUploadedTypes === 1
                ? 'Type found'
                : 'Types found'}
            </FormattedText>
          </View>
        </UserStatusWrapper>
        <UserInfoWrapper>
          <FormattedText fontSize={ 16 }>Role:</FormattedText>
          <FormattedText color="black" fontSize={ 14 }>
            {position}
          </FormattedText>
        </UserInfoWrapper>
        <DeleteButtonContainer>
          <Button
            title="Delete User"
            bgColor={ COLORS.RED }
            textColor="white"
            action={ () => deleteUserRequest(id) }
          />
        </DeleteButtonContainer>
      </UserCardWrapper>
    </UserWrapper>
  )
}

export default UserView
