import React from 'react'
import {
  View, Text, TouchableOpacity, Animated,
} from 'react-native'

import Avatar from 'components/Avatar'
import Icon from 'components/Icon'

import * as RootNavigation from 'navigation/RootNavigation'
import { DIMENSIONS } from 'consts'

const UserItem = (props) => {
  const {
    id,
    firstName,
    lastName,
    email,
    profileImg,
    position,
    isAdmin,
    entries,
    scale,
    opacity,
    deleteUserRequest,
    isCurrentUserAdmin,
  } = props

  return (
    <Animated.View
      style={ {
        marginBottom: DIMENSIONS.USERS_LIST.SPACING,
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        padding: DIMENSIONS.USERS_LIST.SPACING,
        borderRadius: 5,
        shadowColor: 'black',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 5,
        transform: [ { scale } ],
        opacity,
      } }
    >
      <TouchableOpacity
        onPress={ () => RootNavigation.navigate('User', {
          id,
          email,
          firstName,
          lastName,
          entries,
          isAdmin,
          position,
          profileImg,
          isCurrentUserAdmin,
        }) }
        style={ {
          flexDirection: 'row',
          alignItems: 'center',
          flex: 1,
        } }
      >
        <Avatar
          imgUrl={ profileImg }
          size={ DIMENSIONS.USERS_LIST.USER_AVATAR_SIZE }
          firstName={ firstName }
          lastName={ lastName }
        />

        <View
          style={ {
            justifyContent: 'center',
            alignSelf: 'center',
            marginLeft: 10,
          } }
        >
          <View style={ { flexDirection: 'row', alignItems: 'center' } }>
            <Text
              style={ {
                fontSize: 12,
                fontFamily: 'Lato-Bold',
                color: 'black',
                marginRight: 5,
              } }
            >
              {`${ firstName } ${ lastName }`}
            </Text>
            <Text
              style={ {
                fontSize: 10,
                fontFamily: 'Lato-Light',
                color: 'black',
              } }
            >
              {position && `(${ position })`}
            </Text>
          </View>

          <Text
            style={ {
              fontSize: 10,
              fontFamily: 'Lato-Regular',
              color: '#707070',
            } }
          >
            {email}
          </Text>
          <Text
            style={ {
              fontSize: 10,
              fontFamily: 'Lato-Regular',
              color: '#707070',
            } }
          >
            {entries.length === 1
              ? `${ entries.length } entry`
              : `${ entries.length } entries`}
          </Text>
        </View>

        <Icon
          color="rgba(22,22,22,0.6)"
          icon="angle-double-right"
          size={ 16 }
          style={ {
            alignSelf: 'center',
            marginLeft: 'auto',
            opacity: 0.85,
          } }
        />
      </TouchableOpacity>
    </Animated.View>
  )
}

export default UserItem
