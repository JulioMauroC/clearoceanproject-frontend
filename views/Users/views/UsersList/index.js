import React from 'react'
import { Animated } from 'react-native'

import { useSelector } from 'react-redux'

import styled from 'styled-components/native'

import { DIMENSIONS } from 'consts'

import UserItem from './UserItem'

const UsersListWrapper = styled.TouchableOpacity`
  width: 100%;
  flex: 1;
  margin-top: 10px;
`

const UsersListView = (props) => {
  const { usersList, deleteUserRequest, isAdmin } = props

  const scrollY = React.useRef(new Animated.Value(0)).current

  const ITEM_SIZE = DIMENSIONS.USERS_LIST.USER_AVATAR_SIZE + DIMENSIONS.USERS_LIST.SPACING * 3

  return (
    <UsersListWrapper>
      <Animated.FlatList
        data={ usersList }
        onScroll={ Animated.event(
          [
            {
              nativeEvent: { contentOffset: { y: scrollY } },
            },
          ],
          { useNativeDriver: true },
        ) }
        keyExtractor={ (item, index) => `user-list-user-${ item._id }-${ index }` }
        contentContainerStyle={ {
          paddingVertical: DIMENSIONS.USERS_LIST.SPACING,
          paddingHorizontal: DIMENSIONS.USERS_LIST.SPACING * 1.8,
        } }
        renderItem={ ({ item, index }) => {
          const inputRange = [
            -1,
            0,
            ITEM_SIZE * index,
            ITEM_SIZE * (index + 0.5),
          ]

          const scale = scrollY.interpolate({
            inputRange,
            outputRange: [ 1, 1, 1, 0.9 ],
          })

          const opacity = scrollY.interpolate({
            inputRange,
            outputRange: [ 1, 1, 1, 0 ],
          })

          return (
            <UserItem
              isCurrentUserAdmin={ isAdmin }
              id={ item._id }
              firstName={ item.firstName }
              lastName={ item.lastName }
              email={ item.email }
              profileImg={ item.profileImage }
              position={ item.position }
              isAdmin={ item.isAdmin }
              entries={ item.entries }
              scrollY={ scrollY }
              opacity={ opacity }
              scale={ scale }
              deleteUserRequest={ deleteUserRequest }
            />
          )

          // currYachtState._id === item.yacht && (
          //   <UserItem
          //     isCurrentUserAdmin={ isAdmin }
          //     id={ item._id }
          //     firstName={ item.firstName }
          //     lastName={ item.lastName }
          //     email={ item.email }
          //     profileImg={ item.profileImage }
          //     position={ item.position }
          //     isAdmin={ item.isAdmin }
          //     entries={ item.entries }
          //     scrollY={ scrollY }
          //     opacity={ opacity }
          //     scale={ scale }
          //     deleteUserRequest={ deleteUserRequest }
          //   />
          // )
        } }
      />
    </UsersListWrapper>
  )
}

export default UsersListView
