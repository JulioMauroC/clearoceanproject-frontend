import React from 'react'

import styled from 'styled-components/native'

const FormButton = (props) => {
  const { action } = props

  const FormButtonWrapper = styled.TouchableOpacity`
    background-color: ${ (props) => props.bgColor };
    border-radius: 50px;
    height: ${ (props) => props.theme.dimensions.inviteCardHeight / 6 };
    justify-content:center;
  `

  const TextWrapper = styled.Text`
    color: ${ (props) => props.textColor };
    font-size: 16px;
    font-family: 'Lato-Bold';
    text-align: center;
    align-self:center;
  `

  return (
    <FormButtonWrapper { ...props } onPress={ action }>
      <TextWrapper { ...props }>{props.text}</TextWrapper>
    </FormButtonWrapper>
  )
}

export default FormButton
