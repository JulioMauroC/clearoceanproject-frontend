import React from 'react'
import styled from 'styled-components/native'
import { TextInput, Dimensions } from 'react-native'
import { COLORS } from 'consts'

const { width } = Dimensions.get('screen')

const TextFieldWrapper = styled.View`
  background-color: rgba(205, 205, 205, 0.5);
  /* box-shadow: 0px 1px 6px ${ (props) => props.borderColor }; */
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: ${ width * 0.75 }px;
  height: ${ (props) => props.theme.dimensions.inviteCardHeight / 6 };
  border-radius: 50px;
  /* margin-top: ${ (props) => props.theme.dimensions.paddingMedLarge }; */
  margin-bottom: ${ (props) => props.theme.dimensions.paddingMedLarge };
`

const TextField = (props) => {
  const {
    placeholder, handleChange, handleBlur, valid, value,
  } = props

  // const modifiedBackgroundColor = Color(fieldColor).lighten(0.6);

  const color = valid === true
    ? COLORS.GREEN
    : valid === false
      ? COLORS.RED
      : 'transparent'
  return (
    <TextFieldWrapper borderColor={ color }>
      <TextInput
        underlineColorAndroid="transparent"
        placeholder={ placeholder }
        placeholderTextColor="rgba(115, 115, 115, 0.75)"
        onChangeText={ handleChange }
        onBlur={ handleBlur }
        value={ value }
        style={ {
          width: '75%',
          paddingLeft: 20,
          color: 'grey',
          fontFamily: 'Lato-Bold',
        } }
      />
    </TextFieldWrapper>
  )
}

export default TextField
