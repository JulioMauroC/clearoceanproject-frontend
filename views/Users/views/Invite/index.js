import React from 'react'
import { View } from 'react-native'
import { Formik } from 'formik'
import styled from 'styled-components/native'
import { COLORS } from 'consts'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Modal from 'components/Modal'
import FormButton from './components/FormButton'
import InviteTextField from './components/InviteTextField'
import { initialValues, validationSchema } from './formConfig'

const InviteTitle = styled.Text`
  font-size: 18px;
  color: rgba(0, 0, 0, 0.8);
  font-family: 'Lato-Bold';
  margin-bottom: 25px;
`

const FormWrapper = styled.View`
  flex: 1;
  justify-content: space-evenly;
  padding-bottom: 50px;
`

const ErrorMessage = styled.Text`
  color: ${ COLORS.RED };
  font-size: 11px;
`

const InviteView = (props) => {
  const { inviteUserRequest } = props

  const [ modalVisible, setModalVisible ] = React.useState(false)

  return (
    <KeyboardAwareScrollView
      behavior="padding"
      enabled
      style={ { flex: 1 } }
      key="key"
    >
      <View
        style={ {
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 50,
        } }
      >
        <InviteTitle>Invite User</InviteTitle>

        <Formik
          initialValues={ initialValues }
          validationSchema={ validationSchema }
          onSubmit={ (values) => {
            inviteUserRequest(values)
            // setModalVisible(true)
          } }
        >
          {({
            handleChange,
            handleBlur,
            errors,
            handleSubmit,
            touched,
            values,
          }) => (
            <FormWrapper>
              <InviteTextField
                placeholder="first name"
                handleChange={ handleChange('invitedFirstName') }
                fieldName="invitedFirstName"
                value={ values.invitedFirstName }
                handleBlur={ handleBlur('invitedFirstName') }
              />
              {errors.invitedFirstName && touched.invitedFirstName && (
                <ErrorMessage>{errors.invitedFirstName}</ErrorMessage>
              )}
              <InviteTextField
                placeholder="last name"
                handleChange={ handleChange('invitedLastName') }
                fieldName="invitedLastName"
                value={ values.invitedLastName }
                handleBlur={ handleBlur('invitedLastName') }
              />
              {errors.invitedLastName && touched.invitedLastName && (
                <ErrorMessage>{errors.invitedLastName}</ErrorMessage>
              )}
              <InviteTextField
                placeholder="email"
                handleChange={ handleChange('invitedEmail') }
                fieldName="invitedEmail"
                value={ values.invitedEmail }
                handleBlur={ handleBlur('invitedEmail') }
              />
              {errors.invitedEmail && touched.invitedEmail && (
                <ErrorMessage>{errors.invitedEmail}</ErrorMessage>
              )}

              <FormButton
                action={ handleSubmit }
                bgColor={ COLORS.LIGHT_BLUE }
                textColor="white"
                text="Invite"
              />
            </FormWrapper>
          )}
        </Formik>

        <Modal modalVisible={ modalVisible } setModalVisible={ setModalVisible } />
      </View>
    </KeyboardAwareScrollView>
  )
}

export default InviteView
