import * as yup from 'yup'

export const initialValues = {
  invitedFirstName: '',
  invitedLastName: '',
  invitedEmail: '',
}

export const validationSchema = yup.object({
  invitedFirstName: yup
    .string()
    .min(2, 'Minimum 2 characters')
    .required('Required!'),
  invitedLastName: yup
    .string()
    .min(2, 'Minimum 2 characters')
    .required('Required!'),
  invitedEmail: yup
    .string()
    .email('Invalid email format')
    .required('Required!'),
})
