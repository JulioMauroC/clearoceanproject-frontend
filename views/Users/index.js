import React from 'react'

import { compose } from 'recompose'
import UsersContainer from 'containers/Users'

import styled from 'styled-components/native'

import ProfileHeader from 'components/ProfileHeader'

import Tabs from 'components/Tabs'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import UsersList from './views/UsersList'

import Invite from './views/Invite'

const UsersView = (props) => {
  const {
    inviteUserRequest,
    isLoading,
    deleteUserRequest,
    getYachtUsersRequest,
    yachtUsers,
  } = props

  const { isAdmin } = props.currentUserData

  const UsersWrapper = styled.ScrollView`
    flex: 1;
  `
  const UsersListContainer = styled.View`
    flex: 1;
    align-items: center;
    width: 100%;
    margin-top: ${ (props) => props.theme.dimensions.listPaddingTop };
  `

  const [ activeViewIndex, setActiveViewIndex ] = React.useState(0)

  React.useEffect(() => {
    getYachtUsersRequest()
  }, [])

  React.useEffect(() => {
    console.log('yachtUsers')

    console.log(yachtUsers?.length)
  }, [ yachtUsers ])

  const adminTabs = [
    { id: 'users', label: 'Users' },
    { id: 'addUser', label: 'Add User' },
  ]

  const userTabs = [ { id: 'users', label: 'Users' } ]

  return (
    <UsersWrapper>
      <KeyboardAwareScrollView
        behavior="padding"
        enabled
        style={ { flex: 1 } }
        key="key"
      >
        <ProfileHeader />

        <UsersListContainer>
          <Tabs
            tabs={ isAdmin ? adminTabs : userTabs }
            action={ setActiveViewIndex }
            activeViewIndex={ activeViewIndex }
          />

          {activeViewIndex === 0 ? (
            yachtUsers
            && yachtUsers.length > 0 && (
              <UsersList
                usersList={ yachtUsers }
                isAdmin={ isAdmin }
                deleteUserRequest={ deleteUserRequest }
              />
            )
          ) : (
            <Invite inviteUserRequest={ inviteUserRequest } />
          )}
        </UsersListContainer>
      </KeyboardAwareScrollView>
    </UsersWrapper>
  )
}

export default compose(UsersContainer)(UsersView)
