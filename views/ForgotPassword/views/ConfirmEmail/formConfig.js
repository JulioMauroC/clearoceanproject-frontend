import * as yup from 'yup'

export const initialValues = {
  email: '',
}

export const validationSchema = yup.object({
  email: yup.string().email('Invalid email format').required('Required!'),
})
