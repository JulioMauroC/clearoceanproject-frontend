import * as yup from 'yup'

export const initialValues = {
  password: '',
}

export const validationSchema = yup.object({
  password: yup.string().min(8, 'Minimum 8 characters').required('Required!'),
})
