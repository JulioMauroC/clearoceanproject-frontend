import React from 'react'
import { TextInput, TouchableOpacity, Text } from 'react-native'
import styled from 'styled-components/native'
import { Formik } from 'formik'
import { compose } from 'recompose'
import ResetPasswordContainer from 'containers/ResetPassword'
import Button from 'components/Button'

import { FONT_SIZES, FONTS, COLORS } from 'consts'
import { initialValues, validationSchema } from './formConfig'

const ResetPasswordViewWrapper = styled.View`
  flex: 1;
  justify-content: flex-start;
  padding-top: ${ (props) => props.theme.dimensions.paddingLarge };
`

const FormWrapper = styled.View`
  padding-left: ${ (props) => props.theme.dimensions.appHorizontalPadding };
  padding-right: ${ (props) => props.theme.dimensions.appHorizontalPadding };
  height: 50%;
  /* justify-content: space-between; */
`

const FormTitle = styled.Text`
  font-size: ${ FONT_SIZES.MIDLARGE };
  font-family: ${ FONTS.DEFAULT_BOLD };
`

const FormSubTitle = styled.Text`
  font-size: ${ FONT_SIZES.TITLE };
  font-family: ${ FONTS.DEFAULT_LIGHT };
  padding-top: ${ (props) => props.theme.dimensions.paddingMedium };
`

const FormDescription = styled.Text`
  padding-top: ${ (props) => props.theme.dimensions.paddingLarge };
  font-size: ${ FONT_SIZES.MIDREGULAR };
  font-family: ${ FONTS.DEFAULT };
  color: ${ COLORS.DARKEST_GRAY };
`

const InputsContainer = styled.View``

const InputWrapper = styled.View`
  padding: 0;
  padding-top: ${ (props) => props.theme.dimensions.paddingLarge };
  /* margin-bottom: 15px; */
  border-bottom-width: 1px;
  border-bottom-color: rgba(73, 126, 147, 0.2);
  width: 100%;
`

const ErrorMessage = styled.Text`
  color: ${ COLORS.RED };
  font-size: 11px;
`

const ResetPasswordView = (props) => {
  const { errorMessage, changePasswordRequest } = props

  const { email, code } = props.route.params

  return (
    <ResetPasswordViewWrapper>
      <Formik
        initialValues={ initialValues }
        validationSchema={ validationSchema }
        enableReinitialize
        onSubmit={ async (values, { resetForm }) => {
          changePasswordRequest(email, code, values.password)
        } }
      >
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <FormWrapper>
            <FormTitle>Reset Password</FormTitle>
            <FormSubTitle>Create New Password</FormSubTitle>
            <FormDescription>
              Your new password must be different from previous used passwords.
            </FormDescription>
            <InputsContainer>
              <InputWrapper>
                <TextInput
                  name="password"
                  style={ { height: 40 } }
                  placeholder="enter new password"
                  onChangeText={ handleChange('password') }
                  onBlur={ handleBlur('password') }
                  value={ values.password }
                  keyboardType="default"
                  secureTextEntry
                />
              </InputWrapper>
              {errors.password && touched.password && (
                <ErrorMessage>{errors.password}</ErrorMessage>
              )}

              {errorMessage && (
                <ErrorMessage style={ { marginTop: 20 } }>
                  {errorMessage}
                </ErrorMessage>
              )}
            </InputsContainer>

            <Button
              marginTop="auto"
              title="Reset Password"
              bgColor={ COLORS.LIGHT_BLUE }
              textColor="white"
              action={ (values) => handleSubmit(values) }
            />
          </FormWrapper>
        )}
      </Formik>
    </ResetPasswordViewWrapper>
  )
}

export default compose(ResetPasswordContainer)(ResetPasswordView)
