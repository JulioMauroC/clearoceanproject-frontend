import * as yup from 'yup'

export const initialValues = {
  code: '',
}

export const validationSchema = yup.object({
  code: yup.string().min(6, 'Minimum 6 digits').max(6, 'Maximum 6 digits').required('Required!'),
})
