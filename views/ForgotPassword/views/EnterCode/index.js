import React from 'react'
import { TextInput, TouchableOpacity, Text } from 'react-native'
import styled from 'styled-components/native'
import { Formik } from 'formik'
import { compose } from 'recompose'
import ResetPasswordContainer from 'containers/ResetPassword'
import Button from 'components/Button'

import { FONT_SIZES, FONTS, COLORS } from 'consts'
import { initialValues, validationSchema } from './formConfig'

const EnterCodeViewWrapper = styled.View`
  flex: 1;
  justify-content: flex-start;
  padding-top: ${ (props) => props.theme.dimensions.paddingLarge };
`

const FormWrapper = styled.View`
  padding-left: ${ (props) => props.theme.dimensions.appHorizontalPadding };
  padding-right: ${ (props) => props.theme.dimensions.appHorizontalPadding };
  height: 50%;
  /* justify-content: space-between; */
`

const FormTitle = styled.Text`
  font-size: ${ FONT_SIZES.MIDLARGE };
  font-family: ${ FONTS.DEFAULT_BOLD };
`

const FormSubTitle = styled.Text`
  font-size: ${ FONT_SIZES.TITLE };
  font-family: ${ FONTS.DEFAULT_LIGHT };
  padding-top: ${ (props) => props.theme.dimensions.paddingMedium };
`

const FormDescription = styled.Text`
  padding-top: ${ (props) => props.theme.dimensions.paddingLarge };
  font-size: ${ FONT_SIZES.MIDREGULAR };
  font-family: ${ FONTS.DEFAULT };
  color: ${ COLORS.DARKEST_GRAY };
`

const InputsContainer = styled.View``

const InputWrapper = styled.View`
  padding: 0;
  padding-top: ${ (props) => props.theme.dimensions.paddingLarge };
  /* margin-bottom: 15px; */
  border-bottom-width: 1px;
  border-bottom-color: rgba(73, 126, 147, 0.2);
  width: 100%;
`

const ErrorMessage = styled.Text`
  color: ${ COLORS.RED };
  font-size: 11px;
`

const EnterCodeView = (props) => {
  const { errorMessage, confirmCodeRequest } = props

  const { email } = props.route.params

  return (
    <EnterCodeViewWrapper>
      <Formik
        initialValues={ initialValues }
        validationSchema={ validationSchema }
        enableReinitialize
        onSubmit={ async (values, { resetForm }) => {
          confirmCodeRequest(email, values.code)
        } }
      >
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          values,
          errors,
          touched,
        }) => (
          <FormWrapper>
            <FormTitle>Reset Password</FormTitle>
            <FormSubTitle>Verify Code</FormSubTitle>
            <FormDescription>
              Please enter the code sent to your email.
            </FormDescription>
            <InputsContainer>
              <InputWrapper>
                <TextInput
                  name="code"
                  style={ { height: 40 } }
                  placeholder="enter 6 digit code"
                  onChangeText={ handleChange('code') }
                  onBlur={ handleBlur('code') }
                  value={ values.code }
                  keyboardType="number-pad"
                />
              </InputWrapper>
              {errors.code && touched.code && (
                <ErrorMessage>{errors.code}</ErrorMessage>
              )}

              {errorMessage && (
                <ErrorMessage style={ { marginTop: 20 } }>
                  {errorMessage}
                </ErrorMessage>
              )}
            </InputsContainer>

            <Button
              marginTop="auto"
              title="Submit Code"
              bgColor={ COLORS.LIGHT_BLUE }
              textColor="white"
              action={ (values) => handleSubmit(values) }
            />
          </FormWrapper>
        )}
      </Formik>
    </EnterCodeViewWrapper>
  )
}

export default compose(ResetPasswordContainer)(EnterCodeView)
