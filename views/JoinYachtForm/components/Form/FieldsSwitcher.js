import React from 'react'
import {
  View, Text, TextInput, TouchableOpacity,
} from 'react-native'
import styled from 'styled-components/native'
import FieldLabels from 'components/Forms/FieldLabels'

import YachtUniqueNameInput from './Inputs/YachtUniqueNameInput'
import UserPositionInput from './Inputs/UserPositionInput'
import UserImageInput from './Inputs/UserImageInput'

const FieldsSwitcherWrapper = styled.View`
  /* flex: 0.4;
  justify-content: center;
  align-items: center;
  width: 100%; */
  bottom: ${ (props) => props.theme.dimensions.switchFieldBottomDistance };
  width: 65%;
  z-index:50000;
`

export const fieldsReg = {
  YachtUniqueNameInput,
  UserPositionInput,
  UserImageInput,
}

export const FieldsSwitcher = (props) => {
  const { fieldName, fieldLabel, fieldSubLabel } = props

  if (fieldName) {
    const CurrentField = fieldsReg[ fieldName ]
    return (
      <FieldsSwitcherWrapper>
        <FieldLabels
          fieldLabel={ fieldLabel }
          fieldSubLabel={ fieldSubLabel }
        />
        <CurrentField { ...props } />
      </FieldsSwitcherWrapper>
    )
  }
}

export default FieldsSwitcher
