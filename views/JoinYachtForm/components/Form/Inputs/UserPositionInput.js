import React from 'react'

import styled from 'styled-components/native'
import PickerModal from 'components/Forms/PickerModal'

import YachtPositions from 'consts/YachtPositions'

const yachtPositionValues = YachtPositions.map((code, index) => code)

const UserPositionWrapper = styled.View``

function UserPosition(props) {
  const {
    values,
    value,
    setFieldValue,
    fieldName,
    currentIndex,
    touched,
    fieldColor,
    scrollToNext,
    setFieldTouched,
    triggerIcon,
    errors,
  } = props

  const [ valid, setValid ] = React.useState(false)

  React.useEffect(() => {
    const wasTouched = touched[ fieldName ] === true
    const areNotErrors = errors[ fieldName ] === undefined

    if (wasTouched !== true) {
      setValid(null)
    } else if (wasTouched === true && !areNotErrors) {
      setValid(false)
    } else if (wasTouched === true && areNotErrors && value !== 'null') {
      setValid(true)
    }
  })

  return (
    <UserPositionWrapper>
      <PickerModal
        pickerValues={ yachtPositionValues }
        setFieldValue={ setFieldValue }
        backgroundColor={ fieldColor }
        fieldName={ fieldName }
        setFieldTouched={ setFieldTouched }
        fieldColor={ fieldColor }
        valueFromForm={ values[ fieldName ] }
        scrollToNext={ scrollToNext }
        currentIndex={ currentIndex }
        triggerIcon={ triggerIcon }
        valid={ valid }
        formValues={ values }
      />
    </UserPositionWrapper>
  )
}

export default UserPosition
