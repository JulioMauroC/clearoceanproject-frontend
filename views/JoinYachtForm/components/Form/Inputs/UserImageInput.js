import React from 'react'

import styled from 'styled-components/native'

import pickFromCamera from 'utils/pickFromCamera'
import pickFromGallery from 'utils/pickFromGallery'
import ImagePreview from 'components/Forms/ImagePreview'
import AddImageButtons from 'components/Forms/AddImageButtons'

import CameraButtons from 'components/CameraButtons'

const YachtImageInputWrapper = styled.View`
  position: relative;
  justify-content: space-between;
  align-items: center;
`

function YachtImageInput(props) {
  const {
    values,
    value,
    setFieldValue,
    fieldName,
    currentIndex,
    fieldColor,
    scrollToNext,
    triggerIcon,
    setPreviewImage,
    previewImage,
  } = props

  const handleCamera = async () => {
    const img = await pickFromCamera()

    const data = {
      height: img.height,
      width: img.width,
      type: img.type,
      uri: img.uri,
    }

    if (img) {
      setFieldValue('UserImageInput', data)
    }

    setPreviewImage(img.uri)
  }

  const handleGallery = async () => {
    const img = await pickFromGallery()

    const data = {
      height: img.height,
      width: img.width,
      type: img.type,
      uri: img.uri,
    }

    if (img) {
      setFieldValue('UserImageInput', data)
    }

    setPreviewImage(img.uri)
  }

  return (
    <YachtImageInputWrapper>
      <AddImageButtons
        handleCamera={ handleCamera }
        handleGallery={ handleGallery }
        fieldColor={ fieldColor }
        defaultText={
          values[ fieldName ]
            ? 'Press to take another picture'
            : 'Press to take picture'
        }
      />

      {/* <CameraButtons
        fieldColor={ fieldColor }
        handleCamera={ handleCamera }
        handleGallery={ handleGallery }
      /> */}
    </YachtImageInputWrapper>
  )
}

export default YachtImageInput
