import React from 'react'
import styled from 'styled-components/native'
import TextConfirm from 'components/Forms/TextConfirm'

const YachtUniqueNameInputWrapper = styled.View``

function YachtUniqueNameInput(props) {
  const {
    yachtUniqueName, fieldColor, currentIndex, scrollToNext,
  } = props

  return (
    <YachtUniqueNameInputWrapper>
      <TextConfirm
        fieldColor={ fieldColor }
        scrollToNext={ scrollToNext }
        value={ yachtUniqueName }
        currentIndex={ currentIndex }
      />
    </YachtUniqueNameInputWrapper>
  )
}

export default YachtUniqueNameInput
