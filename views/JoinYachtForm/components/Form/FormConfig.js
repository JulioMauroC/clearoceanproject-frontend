import * as yup from 'yup'
import { COLORS } from 'consts'

export const initialValues = {
  YachtUniqueNameInput: '',
  UserPositionInput: '',
  UserImageInput: '',
}

export const DATA = [
  {
    key: '3544572',
    title: 'Join Yacht',
    // description: 'You can find this on the email we sent to you.',
    fieldName: 'YachtUniqueNameInput',
    fieldIcon: 'ship',
    backgroundColor: '#353f8f',
    // required: true,
    formResumeTitle: "Yacht's unique name",
    type: 'text',
    fieldLabel: 'Click to confirm Yacht Unique Name',
  },
  {
    key: '917dd72',
    title: 'Join Yacht',
    fieldName: 'UserPositionInput',
    fieldIcon: 'ship',
    backgroundColor: '#0e427f',
    triggerIcon: 'mousePointer',
    required: true,
    formResumeTitle: 'Your role',
    type: 'picker',
    fieldLabel: 'Select your role',
  },

  {
    key: '3574lzz',
    title: 'Join Yacht',
    fieldIcon: 'ship',
    fieldName: 'UserImageInput',
    backgroundColor: '#005E63',
    buttonText: 'Click to confirm',
    triggerIcon: 'upload',
    hasImagePreview: true,
    formResumeTitle: 'Your profile image',
    type: 'image',
    fieldLabel: 'Upload Your profile image',
  },
  {
    key: '3574lp0',
    title: 'Join Yacht',
    fieldIcon: 'check-circle',
    // backgroundColor: '#0aa27f',
    backgroundColor: COLORS.RED,
    buttonText: 'Click to confirm',
    isConfirmationScreen: true,
  },
]

export const validationSchema = yup.object().shape({
  UserPositionInput: yup.string().required('You need to select a position'),
})
