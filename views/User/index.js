import React from 'react'
import { COLORS } from 'consts'
import styled from 'styled-components/native'
import { compose } from 'recompose'
import UsersContainer from 'containers/Users'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import ImagesSwiper from 'components/ImagesSwiper'
import Button from 'components/Button'

import EditPositionField from './components/EditPositionField'

const UserWrapper = styled.ScrollView`
  flex: 1;
  width: 100%;
`

const EntryImageSwiper = styled.View`
  height: ${ heightToDp('25%') };
  width: 100%;
`
const EntryWrapperBody = styled.View`
  padding: ${ widthToDp('6%') }px;
  background-color: white;
  border-top-left-radius: 20;
  border-top-right-radius: 20;
  margin-top: 30;
`

const EntryWrapperSection = styled.View`
  padding-top: ${ heightToDp('2%') };
  padding-bottom: ${ heightToDp('2%') };
  width: 100%;
  position: relative;
  margin-top: 20;
`

const FormattedText = styled.Text`
  font-size: ${ (props) => (props.fontSize ? props.fontSize : 12) }px;
  margin-right: ${ (props) => (props.marginRight ? props.marginRight : 0) }px;
  font-family: ${ (props) => (props.fontFamily ? props.fontFamily : 'Lato-Regular') };
  color: ${ (props) => (props.color ? props.color : '#707070') };
  text-align: left;
  margin-bottom: 5px;
`

const DeleteButtonContainer = styled.View`
  justify-content: flex-end;
  background-color: white;
  border-top-left-radius: 20;
  border-top-right-radius: 20;
  margin-top: 50;
`

const UserView = (props) => {
  const { id, isCurrentUserAdmin } = props.route.params

  const { deleteUserRequest, getUserRequest, userData } = props

  const [ numOfUploadedImages, setNumOfUploadedImages ] = React.useState(0)
  const [ numOfUploadedTypes, setNumOfUploadedTypes ] = React.useState(0)

  React.useEffect(() => {
    getUserRequest(id)
  }, [])

  React.useEffect(() => {
    if (userData && userData.entries && userData.entries.length > 0) {
      const numOfUploadedImagesFn = () => {
        let total = 0

        userData.entries.forEach((entry) => {
          if (entry && entry.imageUrls) {
            total += entry.imageUrls.length
          } else {
            return 0
          }
        })

        return total
      }

      setNumOfUploadedImages(numOfUploadedImagesFn())

      const numOfUploadedTypesFn = () => {
        const typesArr = []

        userData.entries.forEach((entry) => {
          if (entry && entry.types) {
            entry.types.forEach((type) => {
              if (typesArr.indexOf(type) === -1) {
                typesArr.push(type)
              }
            })
          }
        })

        return typesArr.length
      }

      setNumOfUploadedTypes(numOfUploadedTypesFn())
    }
  }, [ userData ])

  const currentUserId = props.currentUserData._id

  return userData ? (
    <UserWrapper>
      <EntryImageSwiper>
        {userData.profileImage && (
          <ImagesSwiper data={ [ userData.profileImage ] } />
        )}
      </EntryImageSwiper>

      <EntryWrapperBody>
        <EntryWrapperSection
          style={ {
            justifyContent: 'space-between',
            alignItems: 'flex-start',
          } }
        >
          <FormattedText color="black" fontFamily="Lato-Bold" fontSize={ 26 }>
            {`${ userData.firstName } ${ userData.lastName }`}
          </FormattedText>

          {isCurrentUserAdmin ? (
            <EditPositionField { ...props } />
          ) : (
            <FormattedText fontSize={ 18 } fontFamily="Lato-Regular">
              {userData.position}
            </FormattedText>
          )}

          <FormattedText fontSize={ 18 } fontFamily="Lato-Regular">
            {userData.email}
          </FormattedText>
        </EntryWrapperSection>

        <EntryWrapperSection
          style={ {
            justifyContent: 'space-between',
            alignItems: 'flex-start',
          } }
        >
          <FormattedText color="black" fontSize={ 18 } fontFamily="Lato-Light">
            <FormattedText color="black" fontSize={ 24 } fontFamily="Lato-Bold">
              {userData.entries?.length}
            </FormattedText>
            {userData.entries?.length === 1 ? ' Entry' : ' Entries'}
          </FormattedText>

          <FormattedText color="black" fontSize={ 18 } fontFamily="Lato-Light">
            <FormattedText color="black" fontSize={ 24 } fontFamily="Lato-Bold">
              {numOfUploadedImages}
            </FormattedText>
            {numOfUploadedImages === 1 ? 'Image Upload' : ' Image Uploads'}
          </FormattedText>

          <FormattedText color="black" fontSize={ 18 } fontFamily="Lato-Light">
            <FormattedText color="black" fontSize={ 24 } fontFamily="Lato-Bold">
              {numOfUploadedTypes}
            </FormattedText>
            {numOfUploadedTypes === 1 ? 'Type found' : 'Types found'}
          </FormattedText>
        </EntryWrapperSection>

        {isCurrentUserAdmin && currentUserId !== id && (
          <DeleteButtonContainer>
            <Button
              title="Delete User"
              bgColor={ COLORS.RED }
              textColor="white"
              action={ () => deleteUserRequest(id) }
            />
          </DeleteButtonContainer>
        )}
      </EntryWrapperBody>
    </UserWrapper>
  ) : null
}

export default compose(UsersContainer)(UserView)
