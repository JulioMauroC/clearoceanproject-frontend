import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import { COLORS } from 'consts'
import styled from 'styled-components/native'
import Icon from 'components/Icon'
import { Formik, useFormikContext } from 'formik'
import YachtPositions from 'consts/YachtPositions'
import PickerModal from './PickerModal'

const FormattedText = styled.Text`
  font-size: ${ (props) => (props.fontSize ? props.fontSize : 12) }px;
  margin-right: ${ (props) => (props.marginRight ? props.marginRight : 0) }px;
  font-family: ${ (props) => (props.fontFamily ? props.fontFamily : 'Lato-Regular') };
  color: ${ (props) => (props.color ? props.color : '#707070') };
  text-align: left;
  margin-bottom: 5px;
`

const EditPositionField = (props) => {
  const { position, _id } = props.userData

  console.log('position!')
  console.log(position)
  console.log('id!')
  console.log(_id)

  const { editOtherUserRequest } = props

  const [ modalVisible, setModalVisible ] = React.useState(false)

  return (
    <>
      <View
        style={ {
          flexDirection: 'row',
          justifyContent: 'space-between',
          width: '100%',
        } }
      >
        <FormattedText fontSize={ 18 } fontFamily="Lato-Regular">
          {position}
        </FormattedText>

        <TouchableOpacity onPress={ () => setModalVisible(true) }>
          <Icon icon="pen" color={ COLORS.DARKEST_GRAY } size={ 18 } />
        </TouchableOpacity>
      </View>

      <PickerModal
        modalVisible={ modalVisible }
        setModalVisible={ setModalVisible }
        pickerValues={ YachtPositions }
        editOtherUserRequest={ editOtherUserRequest }
        userId={ _id }
        initialValue={ position }
      />
    </>
  )
}

export default EditPositionField
