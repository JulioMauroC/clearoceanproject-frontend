import React from 'react'
import { Platform, View } from 'react-native'
import styled from 'styled-components/native'
import { COLORS } from 'consts'
import ModalSelector from 'react-native-modal-selector'
import PickerModalIOS from './PickerModalIOS'

const PickerModalWrapper = styled.View``

const PickerModal = (props) => {
  const {
    modalVisible,
    pickerValues,
    setModalVisible,
    editOtherUserRequest,
    userId,
    initialValue,
  } = props

  const [ pickerValue, setPickerValue ] = React.useState(initialValue)

  // React.useEffect(() => {
  //   console.log(pickerValue)
  //   editOtherUserRequest({ userId, position: pickerValue })
  // }, [ pickerValue ])

  return (
    <PickerModalWrapper>
      {Platform.OS === 'ios' ? (
        <View style={ { width: '100%' } }>
          <PickerModalIOS
            visible={ modalVisible }
            setModalVisible={ setModalVisible }
            setPickerValue={ setPickerValue }
            pickerValues={ pickerValues }
            pickerValue={ pickerValue }
            modalVisible={ modalVisible }
            backgroundColor={ COLORS.MID_GREEN }
            editOtherUserRequest={ editOtherUserRequest }
            userId={ userId }
          />
        </View>
      ) // <ModalSelector
      //   data={pickerValues}
      //   keyExtractor={(item) => item.label}
      //   initValue="Press to select"
      //   accessible
      //   scrollViewAccessibilityLabel="Scrollable options"
      //   cancelButtonAccessibilityLabel="Cancel Button"
      //   onChange={async (option) => {
      //     await setFieldValue(fieldName, option.value);
      //     await setFieldTouched(fieldName);
      //   }}
      // />
        : null}
    </PickerModalWrapper>
  )
}

export default PickerModal
