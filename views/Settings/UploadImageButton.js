import React from 'react'

import { Image } from 'react-native'
import styled from 'styled-components/native'

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

import { faArrowCircleUp } from '@fortawesome/free-solid-svg-icons'

const UploadImageButtonWrapper = styled.TouchableOpacity`
  transform: translateX(-10px);
`

const UploadImageButton = (props) => {
  const { previewImage, action } = props

  return (
    <UploadImageButtonWrapper onPress={ action }>
      <Image
        source={ { uri: previewImage } }
        style={ {
          width: 26,
          height: 26,
          borderRadius: 21,
        } }
      />

      <FontAwesomeIcon
        icon={ faArrowCircleUp }
        color="#fff"
        size={ 12 }
        style={ {
          position: 'absolute',
          bottom: 0,
          right: 0,
          borderColor: 'black',
          borderRadius: 15,
          borderWidth: 1,
          shadowColor: '#000',
          shadowOffset: {
            width: 3,
            height: 2,
          },
          shadowOpacity: 1,
          shadowRadius: 3.4,
          elevation: 2,
        } }
      />
    </UploadImageButtonWrapper>
  )
}

export default UploadImageButton
