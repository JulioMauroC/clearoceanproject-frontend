import React from 'react'

import { View } from 'react-native'
import { COLORS } from 'consts'
import styled from 'styled-components/native'

import Toggle from 'components/Toggle'

import pickFromGallery from 'utils/pickFromGallery'
import UploadImageButton from './UploadImageButton'

const SettingsYachtWrapper = styled.View`
  flex: 1;
  flex-direction: row;
  margin-top: 20px;
  /* justify-content: space-evenly; */
  align-items: center;
  border-bottom-color: #abb3c0;
  border-bottom-width: 0.2px;
  width: 100%;
`

const FormattedText = styled.Text`
  font-size: ${ (props) => (props.fontSize ? props.fontSize : 12) }px;
  font-family: 'Lato-Bold';
  color: ${ (props) => (props.color ? props.color : '#707070') };
  margin-right: 10px;
  text-align: left;
`

const DescriptionText = styled.Text`
  font-size: ${ (props) => (props.fontSize ? props.fontSize : 12) }px;
  font-family: 'Lato-Bold';
  color: ${ (props) => (props.color ? props.color : '#707070') };
  margin-right: 10px;
  text-align: left;
  width: 70%;
`

const SettingsFieldWrapper = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  /* padding: 10px 15px; */
  width: 100%;
  justify-content: space-between;
  align-items: center;
  padding-top: 25px;
  /* border:1px solid red; */
`

const SettingsTextInput = styled.TextInput``

const SettingsYacht = (props) => {
  const {
    currentYachtData,
    isYachtPrivacyEnabled,
    toggleSwitch,
    yachtImagePreview,
    setYachtImagePreview,
    setYachtOfficialNumber,
    yachtOfficialNumber,
    setFieldValue,
    setFieldTouched,
  } = props

  React.useEffect(() => {
    console.log('yachtOfficialNumber')
    console.log(yachtOfficialNumber)
  }, [ yachtOfficialNumber ])

  const handleYachtOfficialNumberChange = (input) => {
    setFieldTouched('yachtOfficialNumber')
    setFieldValue('yachtOfficialNumber', input)
    setYachtOfficialNumber(input)
  }

  const handleYachtCamera = async () => {
    const img = await pickFromGallery()

    if (!img.cancelled) {
      setYachtImagePreview(img.uri)
      setFieldTouched('yachtImage')
    }
  }
  return (
    <SettingsYachtWrapper>
      <View
        style={ {
          width: '100%',
          paddingBottom: 25,
        } }
      >
        <View>
          <FormattedText fontSize={ 18 } color="black">
            Yacht Settings
          </FormattedText>

          <SettingsFieldWrapper>
            <FormattedText fontSize={ 14 } color={ COLORS.BLUE }>
              Yacht Privacy
            </FormattedText>

            <Toggle
              toggleStatsValues={ [ 'private', 'public' ] }
              toggleSwitch={ toggleSwitch }
              toggleState={ isYachtPrivacyEnabled }
              fieldName="yachtPrivacy"
              setFieldValue={ setFieldValue }
              setFieldTouched={ setFieldTouched }
            />
            <DescriptionText>
              Publically display or hide your yachts entries in Global Entries.
            </DescriptionText>
          </SettingsFieldWrapper>
        </View>

        <View
          style={ {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'space-between',
            width: '100%',
          } }
        >
          <SettingsFieldWrapper>
            <FormattedText fontSize={ 14 } color={ COLORS.BLUE }>
              Change Image
            </FormattedText>

            <View>
              <UploadImageButton
                previewImage={ yachtImagePreview }
                action={ handleYachtCamera }
              />
            </View>
          </SettingsFieldWrapper>
        </View>
        <View
          style={ {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'space-between',
            width: '100%',
            marginTop: 5,
          } }
        >
          <SettingsFieldWrapper>
            <FormattedText fontSize={ 14 } color={ COLORS.BLUE }>
              Yacht's official number
            </FormattedText>

            <View>
              <SettingsTextInput
                placeholder={ yachtOfficialNumber }
                onBlur={ () => setFieldTouched('yachtOfficialNumber') }
                onChangeText={ async (text) => handleYachtOfficialNumberChange(text) }
              />
            </View>
          </SettingsFieldWrapper>
        </View>
      </View>
    </SettingsYachtWrapper>
  )
}

export default SettingsYacht
