import React from 'react'

import { View } from 'react-native'
import { COLORS } from 'consts'
import styled from 'styled-components/native'

import Toggle from 'components/Toggle'

import Button from 'components/Button'

import pickFromCamera from 'utils/pickFromCamera'
import pickFromGallery from 'utils/pickFromGallery'
import UploadImageButton from './UploadImageButton'

const SettingsProfileWrapper = styled.View`
  flex: 1;
  flex-direction: row;
  /* justify-content: space-between; */
  padding-bottom: 20px;
  align-items: center;
  border-bottom-color: #abb3c0;
  border-bottom-width: 0.2px;
  width: 100%;
`

const FormattedText = styled.Text`
  font-size: ${ (props) => (props.fontSize ? props.fontSize : 12) }px;
  font-family: 'Lato-Bold';
  color: ${ (props) => (props.color ? props.color : '#707070') };
  margin-right: 10px;
  text-align: left;
`

const DescriptionText = styled.Text`
  font-size: ${ (props) => (props.fontSize ? props.fontSize : 12) }px;
  font-family: 'Lato-Bold';
  color: ${ (props) => (props.color ? props.color : '#707070') };
  margin-right: 10px;
  text-align: left;
  width: 70%;
`

const SettingsFieldWrapper = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  /* padding: 10px 15px; */
  width: 100%;
  justify-content: space-between;
  align-items: center;
  padding-top: 25px;
  /* border:1px solid red; */
`

const SettingsTextInput = styled.TextInput``

const SettingsProfile = (props) => {
  const {
    isUserPrivacyEnabled,
    toggleSwitch,
    setFieldValue,
    userImagePreview,
    setUserImagePreview,
    userDeleteModalOn,
    setUserDeleteModalOn,
    deleteUserRequest,
    setFieldTouched,
    currentUserData,
    setUserFirstName,
    setUserLastName,
    userFirstName,
    userLastName,
  } = props

  const handleUserCamera = async () => {
    const img = await pickFromGallery()

    if (!img.cancelled) {
      setUserImagePreview(img.uri)
      setFieldTouched('userImage')
    }
  }
  return (
    <SettingsProfileWrapper>
      <View
        style={ {
          width: '100%',
        } }
      >
        <View>
          <FormattedText fontSize={ 18 } color="black">
            Profile Settings
          </FormattedText>

          {/* <SettingsFieldWrapper>
            <FormattedText fontSize={ 14 } color={ COLORS.BLUE }>
              Profile Privacy
            </FormattedText>

            <Toggle
              toggleStatsValues={ [ 'private', 'public' ] }
              toggleSwitch={ toggleSwitch }
              toggleState={ isUserPrivacyEnabled }
              fieldName="isPrivateProfile"
              setFieldValue={ setFieldValue }
              setFieldTouched={ setFieldTouched }
            />

          </SettingsFieldWrapper> */}
        </View>

        <View>
          <SettingsFieldWrapper>
            <FormattedText fontSize={ 14 } color={ COLORS.BLUE }>
              Change Image
            </FormattedText>

            <View>
              <UploadImageButton
                previewImage={ userImagePreview }
                action={ handleUserCamera }
              />
            </View>
          </SettingsFieldWrapper>

          <View
            style={ {
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'space-between',
              width: '100%',
            } }
          >
            <SettingsFieldWrapper>
              <FormattedText fontSize={ 14 } color={ COLORS.BLUE }>
                First Name
              </FormattedText>

              <View>
                <SettingsTextInput
                  placeholder={ currentUserData.firstName }
                  onBlur={ () => setFieldTouched('userFirstName') }
                  // placeholderTextColor="red"
                  value={ userFirstName }
                  onChangeText={ async (text) => setUserFirstName(text) }
                />
              </View>
            </SettingsFieldWrapper>
          </View>
          <View>
            <SettingsFieldWrapper>
              <FormattedText fontSize={ 14 } color={ COLORS.BLUE }>
                Last Name
              </FormattedText>

              <View>
                <SettingsTextInput
                  placeholder={ currentUserData.lastName }
                  onBlur={ () => setFieldTouched('userLastName') }
                  // placeholderTextColor="red"
                  value={ userLastName }
                  // onChangeText={async (text) => console.log(text)}
                  onChangeText={ async (text) => setUserLastName(text) }
                />
              </View>
            </SettingsFieldWrapper>
          </View>
        </View>
        <View
          style={ {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'space-between',
            width: '100%',
          } }
        >
          <SettingsFieldWrapper>
            <FormattedText fontSize={ 14 } color={ COLORS.BLUE }>
              Delete Account
            </FormattedText>

            <View>
              <Button
                action={ () => setUserDeleteModalOn(true) }
                bgColor={ COLORS.RED }
                textColor="white"
                fontSize={ 13 }
                height={ 20 }
                title="delete"
                paddingLeft={ 7 }
                paddingRight={ 7 }
              />
            </View>
          </SettingsFieldWrapper>
        </View>
      </View>
    </SettingsProfileWrapper>
  )
}

export default SettingsProfile
