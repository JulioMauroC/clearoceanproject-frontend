import React from 'react'

import { View } from 'react-native'

import styled from 'styled-components/native'
import { Formik, useFormikContext } from 'formik'

import { useFocusEffect } from '@react-navigation/native'

import UsersContainer from 'containers/Users'
import YachtsContainer from 'containers/Yachts'

import { compose } from 'recompose'

import SettingsHeader from './SettingsHeader'

import SettingsProfile from './SettingsProfile'
import SettingsYacht from './SettingsYacht'
import SettingsButtons from './SettingsButtons'

import ConfirmationModal from './ConfirmationModal'

const SettingsWrapper = styled.ScrollView`
  flex: 1;
  width: 100%;
  height: 100%;
`

const SettingsInnerWrapper = styled.View`
  flex: 1;
  width: 100%;
  height: auto;
  padding-left: ${ (props) => props.theme.dimensions.paddingLarge };
  padding-right: ${ (props) => props.theme.dimensions.paddingLarge };
`

const SettingsView = (props) => {
  const {
    currentUserData,
    currentYachtData,
    editUserRequest,
    editYachtRequest,
    getCurrentUserRequest,
    officialNumber,
    deleteCurrentUserRequest,
  } = props

  useFocusEffect(
    React.useCallback(() => {
      getCurrentUserRequest()
    }, []),
  )

  // useFocusEffect(
  //   React.useCallback(() => {
  //     console.log('currentUserData')
  //     console.log(currentUserData)
  //   }, [ currentUserData ]),
  // )

  // const [ isUserPrivacyEnabled, setIsUserPrivacyEnabled ] = React.useState(
  //   currentUserData?.isPrivateProfile,
  // )

  const [ isUserAdmin, setIsUserAdmin ] = React.useState(
    currentUserData?.isAdmin,
  )

  const [ isYachtPrivacyEnabled, setIsYachtPrivacyEnabled ] = React.useState(
    currentUserData?.yacht?.isPrivateProfile,
  )
  // const toggleUserPrivacySwitch = () => setIsUserPrivacyEnabled((previousState) => !previousState)
  const toggleYachtPrivacySwitch = () => setIsYachtPrivacyEnabled((previousState) => !previousState)

  const [ userFirstName, setUserFirstName ] = React.useState(
    currentUserData?.firstName,
  )

  const [ userLastName, setUserLastName ] = React.useState(
    currentUserData?.lastName,
  )

  // const [ userPosition, setUserPosition ] = React.useState(
  //   currentUserData?.position,
  // )

  const [ yachtImagePreview, setYachtImagePreview ] = React.useState(
    currentUserData?.yacht.yachtImage,
  )

  const [ yachtOfficialNumber, setYachtOfficialNumber ] = React.useState(
    currentYachtData?.officialNumber,
  )

  const [ userImagePreview, setUserImagePreview ] = React.useState(
    currentUserData?.profileImage,
  )

  const [ userDeleteModalOn, setUserDeleteModalOn ] = React.useState(false)

  // const settingsInitialValues = {
  //   // isPrivateProfile: currentUserData.isPrivateProfile,
  //   userImage: currentUserData.profileImage,
  //   userFirstName: currentUserData.firstName,
  //   userLastName: currentUserData.lastName,
  //   yachtPrivacy: currentYachtData?.isPrivateProfile,
  //   yachtImage: currentYachtData.yachtImage,
  //   yachtOfficialNumber: currentYachtData.officialNumber,
  // }

  const resetStateValues = () => {
    // setIsUserPrivacyEnabled(settingsInitialValues.isPrivateProfile)
    setUserImagePreview(currentUserData?.profileImage)
    setIsYachtPrivacyEnabled(currentYachtData?.isPrivateProfile)
    setYachtImagePreview(currentYachtData.yachtImage)
    setYachtOfficialNumber(currentYachtData.officialNumber)
    setUserFirstName(currentUserData.firstName)
    setUserLastName(currentUserData.lastName)
  }

  // const resetStateValues = () => {
  //   // setIsUserPrivacyEnabled(settingsInitialValues.isPrivateProfile)
  //   setUserImagePreview(settingsInitialValues.userImage)
  //   setIsYachtPrivacyEnabled(settingsInitialValues.yachtPrivacy)
  //   setYachtImagePreview(settingsInitialValues.yachtImage)
  //   setYachtOfficialNumber(settingsInitialValues.yachtOfficialNumber)
  //   setUserFirstName(settingsInitialValues.userFirstName)
  //   setUserLastName(settingsInitialValues.userLastName)
  // }

  const FieldUpdater = () => {
    const { setFieldValue, setFieldTouched, touchedFields } = useFormikContext()

    // React.useEffect(() => {
    //   setFieldValue('isPrivateProfile', isUserPrivacyEnabled)
    // }, [ isUserPrivacyEnabled ])

    React.useEffect(() => {
      setFieldValue('yachtPrivacy', isYachtPrivacyEnabled)
    }, [ isYachtPrivacyEnabled ])

    React.useEffect(() => {
      setFieldValue('userFirstName', userFirstName)
    }, [ userFirstName ])

    React.useEffect(() => {
      setFieldValue('userLastName', userLastName)
    }, [ userLastName ])

    React.useEffect(() => {
      setFieldValue('yachtPrivacy', isYachtPrivacyEnabled)
    }, [ isYachtPrivacyEnabled ])

    React.useEffect(() => {
      setFieldValue('userImage', userImagePreview)
    }, [ userImagePreview ])

    React.useEffect(() => {
      setFieldValue('yachtImage', yachtImagePreview)
    }, [ yachtImagePreview ])

    React.useEffect(() => {
      setFieldValue('yachtOfficialNumber', yachtOfficialNumber)
    }, [ yachtOfficialNumber ])
    return null
  }

  return (
    currentUserData && (
      <SettingsWrapper contentContainerStyle={ { flexGrow: 1 } }>
        {/* <ImageBackground /> */}
        {/* <ProfileHeader /> */}

        <Formik
          style={ { flex: 1, heoght: '100%' } }
          initialValues={ {
            // isPrivateProfile: currentUserData.isPrivateProfile,
            userImage: currentUserData.profileImage,
            userFirstName: currentUserData.firstName,
            userLastName: currentUserData.lastName,
            yachtPrivacy: currentUserData.yacht.isPrivateProfile,
            yachtImage: currentYachtData.yachtImage,
            yachtOfficialNumber: currentYachtData.officialNumber,
          } }
          enableReinitialize
          onSubmit={ async (val, formikBag) => {
            console.log('VAL!')
            console.log(val)
            editUserRequest(currentUserData._id, {
              // isPrivateProfile: val.isPrivateProfile,
              userImage: val.userImage,
              firstName: val.userFirstName,
              lastName: val.userLastName,
            })

            editYachtRequest(currentYachtData._id, {
              isPrivateProfile: val.yachtPrivacy,
              yachtImage: val.yachtImage,
              yachtOfficialNumber: val.yachtOfficialNumber,
            })
          } }
        >
          {({
            handleSubmit,
            setFieldValue,
            dirty,
            resetForm,
            touched,
            setFieldTouched,
            values,
          }) => (
            // console.log('values')
            // console.log(values)

            <SettingsInnerWrapper>
              <FieldUpdater />
              <SettingsHeader />

              <View
                style={ {
                  flex: 1,
                } }
              >
                <SettingsProfile
                  currentUserData={ currentUserData }
                  // toggleSwitch={ toggleUserPrivacySwitch }
                  // isUserPrivacyEnabled={ isUserPrivacyEnabled }
                  setFieldValue={ setFieldValue }
                  userImagePreview={ userImagePreview }
                  setUserImagePreview={ setUserImagePreview }
                  userDeleteModalOn={ userDeleteModalOn }
                  setUserDeleteModalOn={ setUserDeleteModalOn }
                  deleteCurrentUserRequest={ deleteCurrentUserRequest }
                  setFieldTouched={ setFieldTouched }
                  userFirstName={ userFirstName }
                  userLastName={ userLastName }
                  setUserFirstName={ setUserFirstName }
                  setUserLastName={ setUserLastName }
                />
                {isUserAdmin ? (
                  <SettingsYacht
                    currentYachtData={ currentYachtData }
                    toggleSwitch={ toggleYachtPrivacySwitch }
                    isYachtPrivacyEnabled={ isYachtPrivacyEnabled }
                    setFieldValue={ setFieldValue }
                    yachtImagePreview={ yachtImagePreview }
                    setYachtImagePreview={ setYachtImagePreview }
                    setYachtOfficialNumber={ setYachtOfficialNumber }
                    yachtOfficialNumber={ yachtOfficialNumber }
                    setFieldTouched={ setFieldTouched }
                  />
                ) : null}

                <SettingsButtons
                  dirty={ dirty }
                  resetForm={ resetForm }
                  resetStateValues={ resetStateValues }
                  handleSubmit={ handleSubmit }
                  touched={ touched }
                />
              </View>
            </SettingsInnerWrapper>
          )}
        </Formik>
        <ConfirmationModal
          userDeleteModalOn={ userDeleteModalOn }
          setUserDeleteModalOn={ setUserDeleteModalOn }
          deleteCurrentUserRequest={ deleteCurrentUserRequest }
        />
      </SettingsWrapper>
    )
  )
}
export default compose(UsersContainer, YachtsContainer)(SettingsView)
