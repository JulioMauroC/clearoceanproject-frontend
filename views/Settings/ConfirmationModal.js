import React, { useState, useEffect } from 'react'
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Modal,
  TouchableHighlight,
} from 'react-native'

import { useFocusEffect } from '@react-navigation/native'

// useFocusEffect(
//   React.useCallback(() => {
//     const unsubscribe = API.subscribe(userId, user => setUser(user));

//     return () => unsubscribe();
//   }, [userId])
// );

import { COLORS } from 'consts'

import * as RootNavigation from 'navigation/RootNavigation'

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    // margin: 20,
    backgroundColor: COLORS.LIGHT_BLUE,
    borderColor: COLORS.BLUE,
    borderWidth: 2,
    borderRadius: 20,
    padding: 35,
    color: 'white',
    alignItems: 'center',
    shadowColor: COLORS.DARKEST_GRAY,
    shadowOffset: {
      width: 0,
      height: 15,
    },
    shadowOpacity: 1,
    shadowRadius: 5,
    elevation: 5,
    width: '80%',
    height: '40%',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  openButton: {
    backgroundColor: '#FFD500',
    borderRadius: 12,
    paddingVertical: 8,
    paddingHorizontal: 15,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    color: COLORS.WHITE,
  },
  modalText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
    // fontWeight: 'bold',
    opacity: 1,
    color: COLORS.WHITE,
  },
  modalTextBold: {
    color: 'white',
    textAlign: 'center',
    fontSize: 18,
    opacity: 1,
  },
  modalSubText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 14,
    opacity: 0.7,
    marginBottom: 10,
  },
  confirmButton: {
    backgroundColor: COLORS.WHITE,
    borderRadius: 12,
    paddingVertical: 8,
    paddingHorizontal: 15,
    elevation: 2,
  },
  cancelButton: {
    backgroundColor: COLORS.WHITE,
    borderRadius: 12,
    paddingVertical: 8,
    paddingHorizontal: 15,
    elevation: 2,
    borderWidth: 1,
  },
  confirmTextStyle: {
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  cancelTextStyle: {
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
  },
})

const ConfirmationModal = (props) => {
  const {
    navigation,
    userDeleteModalOn,
    setUserDeleteModalOn,
    action,
    deleteCurrentUserRequest,
  } = props
  // const [ modalVisible, setModalVisible ] = React.useState(true)

  const goToInvite = () => {
    RootNavigation.navigate('Invite')
    setModalVisible(false)
  }

  const goToDashboard = () => {
    // RootNavigation.navigate('MainTab', { screen: 'Users' })
    navigation.goBack()
    setModalVisible(false)
  }

  return (
    <Modal
      animationType="fade"
      onDismiss={ () => console.log('dismissed') }
      transparent
      visible={ userDeleteModalOn }
      onRequestClose={ () => {
        Alert.alert('Modal has been closed.')
      } }
    >
      <View style={ styles.centeredView }>
        <View style={ styles.modalView }>
          <Text style={ styles.modalText }>
            Are you sure you would like to delete your account?
            {'\n'}
            All records associated with this account will be deleted. Also if
            you are the yacht admin all the accounts associated with the yacht
            will be permanently deleted.
          </Text>

          {/* <TouchableHighlight style={ styles.confirmButton } onPress={ goToInvite }>
            <Text style={ styles.confirmTextStyle }>Yes, Send a new invite</Text>
          </TouchableHighlight> */}

          <TouchableHighlight
            style={ styles.cancelButton }
            onPress={ () => deleteCurrentUserRequest() }
          >
            <Text style={ styles.cancelTextStyle }>Confirm</Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={ styles.cancelButton }
            onPress={ () => setUserDeleteModalOn(false) }
          >
            <Text style={ styles.cancelTextStyle }>Don't delete</Text>
          </TouchableHighlight>
        </View>
      </View>
    </Modal>
  )
}

export default ConfirmationModal
