import React from 'react'
import styled from 'styled-components/native'
import * as RootNavigation from 'navigation/RootNavigation'
import Icon from 'components/Icon'

import { COLORS } from 'consts'

const SettingsHeaderWrapper = styled.View`
  margin-top: 20px;
  margin-bottom: 20px;
  justify-content: center;
  align-items: center;
`

const FormattedText = styled.Text`
  font-size: ${ (props) => (props.fontSize ? props.fontSize : 12) }px;
  font-family: 'Lato-Bold';
  color: ${ (props) => (props.color ? props.color : '#707070') };
  margin-right: 10px;
  text-align: left;
`

const SettingsHeader = (props) => (
  <SettingsHeaderWrapper>
    <FormattedText
      fontSize={ 18 }
      color={ COLORS.BLUE }
      style={ { textAlign: 'center' } }
    >
      SETTINGS
    </FormattedText>
  </SettingsHeaderWrapper>
)

export default SettingsHeader
