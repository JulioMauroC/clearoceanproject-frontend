import React from 'react';
import { View, Dimensions, Text } from 'react-native';
import { COLORS } from 'consts';
import styled from 'styled-components/native';

import howManyFromType from 'utils/howManyFromType';
import howManyOtherTypes from 'utils/howManyOtherTypes';
import { navigate } from 'navigation/RootNavigation';
import ImageUrl from 'assets/images/world.png';

import EntriesContainer from 'containers/Entries';
import { compose } from 'recompose';
import { LinearGradient } from 'expo-linear-gradient';
import Icon from 'components/Icon';

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen';

const { width, height } = Dimensions.get('window');

const ButtonsWrapper = styled.View`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  padding-left: 30px;
  padding-right: 30px;
`;

const ButtonWrapper = styled.TouchableOpacity`
  position: relative;
  width: 40%;
  height: 40px;
  border-radius: 15px;
  justify-content: center;
  align-items: center;
  margin-bottom: 15px;
`;

const ResultsViewTitle = styled.Text`
  color: ${COLORS.GREEN};
  font-family: 'Lato-Bold';
  font-size: 18px;
  padding-left: 30px;
`;

const WorldText = styled.Text`
  font-size: ${(props) => (props.fontSize ? props.fontSize : 22)}px;
  padding: ${(props) => (props.padding ? props.padding : 0)}px;
  font-family: ${(props) =>
    props.fontFamily ? props.fontFamily : 'Lato-Regular'};
  color: ${(props) => (props.color ? props.color : COLORS.GREEN)};
  text-align: left;
`;

const ScrollViewWrapper = styled.ScrollView``;

const WorldWrapper = styled.View`
  width: ${width}px;
  background-color: ${COLORS.PURPLE};
  justify-content: flex-start;
  padding-bottom: 50px;
  padding-top: 50px;
`;

const WorldImage = styled.Image`
  width: 75%;
  align-self: center;
`;

const GlobalResultsView = (props) => {
  const { globalEntriesData, getGlobalEntriesRequest } = props;

  React.useEffect(() => {
    getGlobalEntriesRequest();
  }, []);

  return (
    <ScrollViewWrapper>
      <WorldWrapper>
        <ResultsViewTitle>GLOBAL RESULTS</ResultsViewTitle>
        <WorldImage
          resizeMode="contain"
          source={ImageUrl}
          style={{ transform: [{ translateY: -50 }] }}
        />

        <View style={{ transform: [{ translateY: -100 }] }}>
          <WorldText
            color={COLORS.GREEN}
            fontSize={30}
            fontFamily="Lato-Bold"
            style={{ paddingLeft: 30, paddingBottom: 20 }}
          >
            {globalEntriesData ? globalEntriesData.length : 0}{' '}
            <WorldText fontSize={32} fontFamily="Lato-Light">
              Total Entries
            </WorldText>
          </WorldText>
          <WorldText style={{ paddingTop: 10, paddingLeft: 30 }}>
            Fishing Equipment:{' '}
            {howManyFromType('Fishing eq.', globalEntriesData)}
          </WorldText>
          <WorldText style={{ paddingTop: 10, paddingLeft: 30 }}>
            Plastic Bottles: {howManyFromType('Bottles', globalEntriesData)}
          </WorldText>
          <WorldText style={{ paddingTop: 10, paddingLeft: 30 }}>
            Plastic Packaging: {howManyFromType('Packaging', globalEntriesData)}
          </WorldText>
          <WorldText style={{ paddingTop: 10, paddingLeft: 30 }}>
            Plastic Bags: {howManyFromType('Bags', globalEntriesData)}
          </WorldText>
          <WorldText style={{ paddingTop: 10, paddingLeft: 30 }}>
            Polystyrene: {howManyFromType('Polystyrene', globalEntriesData)}
          </WorldText>
          <WorldText style={{ paddingTop: 10, paddingLeft: 30 }}>
            Rope: {howManyFromType('Rope', globalEntriesData)}
          </WorldText>

          <WorldText style={{ paddingTop: 10, paddingLeft: 30 }}>
            Other: {howManyOtherTypes(globalEntriesData)}
          </WorldText>
        </View>

        {globalEntriesData && (
          <ButtonsWrapper>
            <ButtonWrapper
              onPress={() =>
                navigate('Results', {
                  screen: 'Global Results List',
                  // params: { email: action.payload.email },
                })
              }
            >
              <LinearGradient
                colors={[COLORS.GREEN, COLORS.DARK_GREEN]}
                style={{
                  width: '100%',
                  height: '100%',
                  borderRadius: 12,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                  opacity: 1,
                }}
              >
                <Icon
                  color={COLORS.PURPLE}
                  icon="list"
                  size={widthToDp('4%')}
                />
                <Text
                  style={{
                    textAlign: 'center',
                    padding: 5,
                    color: COLORS.PURPLE,
                    fontFamily: 'Lato-Bold',
                    fontSize: widthToDp('4%'),
                  }}
                >
                  List View
                </Text>
              </LinearGradient>
            </ButtonWrapper>

            <ButtonWrapper
              onPress={() =>
                navigate('Results', {
                  screen: 'Global Results Map',
                  // params: { email: action.payload.email },
                })
              }
            >
              <LinearGradient
                colors={[COLORS.GREEN, COLORS.DARK_GREEN]}
                style={{
                  width: '100%',
                  height: '100%',
                  borderRadius: 12,
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                  opacity: 1,
                }}
              >
                <Icon
                  color={COLORS.PURPLE}
                  icon="globe-europe"
                  size={widthToDp('4%')}
                />
                <Text
                  style={{
                    textAlign: 'center',
                    padding: 5,
                    color: COLORS.PURPLE,
                    fontFamily: 'Lato-Bold',
                    fontSize: widthToDp('4%'),
                  }}
                >
                  Map View
                </Text>
              </LinearGradient>
            </ButtonWrapper>
          </ButtonsWrapper>
        )}
      </WorldWrapper>
    </ScrollViewWrapper>
  );
};

export default compose(EntriesContainer)(GlobalResultsView);
