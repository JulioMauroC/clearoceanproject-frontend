import React from 'react'

import ImagePreview from 'react-native-image-preview'

const ImagePreviewModal = (props) => {
  const { imgUri, showImageModal, setShowImageModal } = props

  return (
    <ImagePreview
      visible={ showImageModal }
      source={ { uri: imgUri } }
      close={ () => setShowImageModal(false) }
    />
  )
}

export default ImagePreviewModal
