import React from 'react'
import styled from 'styled-components/native'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'
import ImageThumbnail from './ImageThumbnail'

const ThumbnailsContainer = styled.View`
  /* position: absolute; */
  width: 100%;
  height: ${ widthToDp('20%') };
  flex-direction: row;
  justify-content: center;
  align-items: center;
  /* bottom: ${ heightToDp('5%') }; */
  /* border:3px solid red; */

`

const EntryImages = (props) => {
  const {

    imageUrls,
  } = props

  const [ thumbnailsSize, setThumbnailsSize ] = React.useState(0)

  // React.useEffect(() => {
  //   setFieldValue('EntryImageInput', previewImages)
  // }, [ previewImages, setFieldValue ])

  React.useEffect(() => {
    switch (imageUrls.length) {
      case 1:
        setThumbnailsSize(widthToDp('20%'))
        break
      case 2:
        setThumbnailsSize(widthToDp('18%'))
        break
      case 3:
        setThumbnailsSize(widthToDp('16%'))
        break
      case 4:
        setThumbnailsSize(widthToDp('15%'))
        break
      case 5:
        setThumbnailsSize(widthToDp('14%'))
        break
      default:
        setThumbnailsSize(widthToDp('14%'))
    }
  }, [ imageUrls.length ])

  return imageUrls.length > 0 ? (
    <ThumbnailsContainer>
      {imageUrls.map((image, index) => (
        <ImageThumbnail
          size={ thumbnailsSize }
          key={ image.uri }
          action={ (index) => {
            // setFieldValue(previewImages)
            // action(index)
            console.log('oh yeh')
          } }
          index={ index }
          previewImage={ image.uri }
        />
      ))}
    </ThumbnailsContainer>
  ) : null
}

export default EntryImages
