import React from 'react'

import { View } from 'react-native'
import { COLORS } from 'consts'
import { compose } from 'recompose'
import EntriesContainer from 'containers/Entries'

import styled from 'styled-components/native'

import Icon from 'components/Icon'

import ImagesSwiper from 'components/ImagesSwiper'
import Swiper from 'components/Swiper'
import EntryMap from 'components/EntryMap'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

const EntryImageSwiper = styled.View`
  height: ${ heightToDp('30%') };
  width: 100%;
`

const FormattedText = styled.Text`
  font-size: ${ (props) => (props.fontSize ? props.fontSize : 12) }px;
  font-family: ${ (props) => (props.fontFamily ? props.fontFamily : 'Lato-Regular') };
  color: ${ (props) => (props.color ? props.color : '#707070') };
  margin-right: 10;
  text-align: ${ (props) => (props.textAlign ? props.textAlign : 'left') };
`

const EntryWrapper = styled.ScrollView`
  flex: 1;
  /* align-items: center; */
  width: 100%;
`

const EntryWrapperBody = styled.View`
  padding: ${ widthToDp('6%') }px;
  background-color: white;
  border-top-left-radius: 20;
  border-top-right-radius: 20;
  margin-top: 50;
`

const EntryWrapperSection = styled.View`
  /* padding: ${ widthToDp('6%') }px; */
  padding-top: ${ heightToDp('4%') };
  padding-bottom: ${ heightToDp('4%') };
  width: 100%;
  position: relative;
  margin-top: 20;
`

const IconWrapper = styled.View`
  background-color: ${ (props) => (props.color ? props.color : props.theme.colors.red) };
  position: absolute;
  top: 0;
  left: 0%;
  width: ${ widthToDp('12%') }px;
  height: ${ widthToDp('6%') }px;
  /* transform: translateX(-${ widthToDp('8%') }px); */
  justify-content: center;
  align-items: center;
  border-radius: 50;
`

const GlobalEntryView = (props) => {
  const {
    authorFirstName,
    authorLastName,
    imageUrl,
    imageUrls,
    latitude,
    longitude,
    types,
    createdAtFormatted,
    isCurrentUserAdmin,
    yacht,
  } = props.route.params

  const isEntryPrivate = yacht.isPrivateProfile

  return (
    <EntryWrapper>
      <EntryImageSwiper>
        <ImagesSwiper data={ imageUrls } />
      </EntryImageSwiper>
      <EntryWrapperBody>
        <EntryWrapperSection
          style={ {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          } }
        >
          <IconWrapper color="#e9f0f8">
            <Icon icon="user" color={ COLORS.MID_GREEN } size={ 16 } />
          </IconWrapper>

          <FormattedText fontSize={ 22 }>
            Uploaded By
            {'\n'}
            <FormattedText color="black" fontFamily="Lato-Bold" fontSize={ 26 }>
              {isEntryPrivate
                ? 'Private'
                : `${ authorFirstName } \n${ authorLastName }`}
            </FormattedText>
          </FormattedText>

          <FormattedText fontSize={ 18 }>{createdAtFormatted}</FormattedText>
        </EntryWrapperSection>

        <EntryWrapperSection
          style={ {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          } }
        >
          <IconWrapper color="#e9f0f8">
            <Icon icon="ship" color={ COLORS.LIGHT_BLUE } size={ 16 } />
          </IconWrapper>

          <FormattedText fontSize={ 22 }>
            Yacht
            {'\n'}
            <FormattedText color="black" fontFamily="Lato-Bold" fontSize={ 26 }>
              {isEntryPrivate ? 'Private' : `${ yacht.name }`}
            </FormattedText>
          </FormattedText>
        </EntryWrapperSection>

        <EntryWrapperSection>
          <IconWrapper color="#e9f0f8">
            <Icon icon="map-marker-alt" color={ COLORS.RED } size={ 16 } />
          </IconWrapper>
          <FormattedText fontSize={ 22 }>
            Location
            {'\n'}
          </FormattedText>

          <EntryMap
            latitude={ latitude }
            longitude={ longitude }
            authorFirstName={ authorFirstName }
            authorLastName={ authorLastName }
            types={ types }
          />

          <View style={ { paddingLeft: 5, flexDirection: 'row' } }>
            <FormattedText fontSize={ 13 }>{`lat: ${ latitude }`}</FormattedText>
            <FormattedText fontSize={ 13 }>{`lon: ${ longitude }`}</FormattedText>
          </View>
        </EntryWrapperSection>

        <EntryWrapperSection>
          <IconWrapper color="#e9f0f8">
            <Icon icon="wine-bottle" color={ COLORS.PURPLE } size={ 16 } />
          </IconWrapper>

          {types && types.length > 0 ? (
            <>
              <FormattedText fontSize={ 22 }>
                Types
                {'\n'}
              </FormattedText>
              <Swiper data={ types } />
            </>
          ) : (
            <FormattedText fontSize={ 18 }>
              No types associated with this entry
            </FormattedText>
          )}
        </EntryWrapperSection>
      </EntryWrapperBody>
    </EntryWrapper>
  )
}

export default compose(EntriesContainer)(GlobalEntryView)
