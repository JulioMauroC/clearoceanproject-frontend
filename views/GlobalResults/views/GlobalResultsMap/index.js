import React from 'react'
import { StyleSheet } from 'react-native'
import { compose } from 'recompose'
import EntriesContainer from 'containers/Entries'
import { COLORS } from 'consts'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import styled from 'styled-components/native'
import MapView, { Marker } from 'react-native-maps'

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
    borderRadius: 5,
  },
})

const GlobalEntriesListTitle = styled.Text`
  color: ${ COLORS.GREEN };
  font-family: 'Lato-Bold';
  font-size: 18px;
  padding-left: 30px;
`

const GlobalEntriesListWrapper = styled.View`
  width: 100%;
  height: 100%;
  background-color: ${ COLORS.PURPLE };
  justify-content: space-evenly;
`

const EntryMapWrapper = styled.View`
  width: 100%;
  height: ${ heightToDp('50%') };
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
`

const GlobalResultsMap = (props) => {
  const { globalEntriesData } = props

  console.log('globalEntriesData')
  console.log(globalEntriesData)

  const cameraSettings = {
    center: {
      latitude: 0,
      longitude: 0,
    },
    pitch: 0,
    heading: 0,

    // Only on iOS MapKit, in meters. The property is ignored by Google Maps.
    altitude: 50000000,

    // Only when using Google Maps.
    zoom: 16,
  }

  return (
    <GlobalEntriesListWrapper>
      <GlobalEntriesListTitle>Global Entries Map</GlobalEntriesListTitle>

      <EntryMapWrapper>
        <MapView style={ styles.map } camera={ cameraSettings }>
          {/* <Marker
          coordinate={ { latitude, longitude } }
          title={ `created by ${ authorFirstName } ${ authorLastName }` }
          description={ `${ types.length } types detected at ${ latitude },${ longitude }` }
        /> */}

          {globalEntriesData
            && globalEntriesData.map((entry) => {
              console.log('entry')
              console.log(entry)

              return (
                <Marker
                  key={ `global-entry-map-${ entry._id }` }
                  coordinate={ {
                    latitude: entry.location.coordinates[ 1 ],
                    longitude: entry.location.coordinates[ 0 ],
                  } }
                  title={ `created by ${ entry?.author?.firstName } ${ entry?.author?.lastName } from yacht ${ entry?.yacht?.name }` }
                  description={ `${ entry?.types?.length } types detected.` }
                />
              )
            })}
        </MapView>
      </EntryMapWrapper>
    </GlobalEntriesListWrapper>
  )
}

export default compose(EntriesContainer)(GlobalResultsMap)
