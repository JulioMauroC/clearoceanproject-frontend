import React from 'react'
import { Animated, View } from 'react-native'
import { compose } from 'recompose'
import EntriesContainer from 'containers/Entries'
import { COLORS, DIMENSIONS } from 'consts'

import styled from 'styled-components/native'

import EntryItem from './EntryItem'

const GlobalEntriesListTitle = styled.Text`
  color: ${ COLORS.GREEN };
  font-family: 'Lato-Bold';
  font-size: 18px;
  padding-left: 30px;
  padding-top: 30px;
`

const GlobalEntriesListWrapper = styled.View`
  width: 100%;
  height: 100%;
  background-color: ${ COLORS.PURPLE };
`

const GlobalEntriesListView = (props) => {
  const { globalEntriesData } = props

  const scrollY = React.useRef(new Animated.Value(0)).current

  const ITEM_SIZE = DIMENSIONS.USERS_LIST.USER_AVATAR_SIZE + DIMENSIONS.USERS_LIST.SPACING * 3

  return (
    <GlobalEntriesListWrapper>
      <GlobalEntriesListTitle>Global Entries List</GlobalEntriesListTitle>
      {globalEntriesData && (
        <Animated.FlatList
          data={ globalEntriesData }
          onScroll={ Animated.event(
            [
              {
                nativeEvent: { contentOffset: { y: scrollY } },
              },
            ],
            { useNativeDriver: true },
          ) }
          keyExtractor={ (item) => item._id }
          contentContainerStyle={ {
            marginTop: 50,
            paddingVertical: DIMENSIONS.USERS_LIST.SPACING,
            paddingHorizontal: DIMENSIONS.USERS_LIST.SPACING * 1.5,
          } }
          renderItem={ ({ item, index }) => {
            const {
              imageUrl, createdAt, _id, location, types,
            } = item

            // const entryImageUrl = item.imageUrl[ 0 ] || null

            const inputRange = [
              -1,
              0,
              ITEM_SIZE * index,
              ITEM_SIZE * (index + 0.5),
            ]

            const scale = scrollY.interpolate({
              inputRange,
              outputRange: [ 1, 1, 1, 0.9 ],
            })

            const opacity = scrollY.interpolate({
              inputRange,
              outputRange: [ 1, 0.95, 1, 0 ],
            })

            return (
              item && item.author && (
              <EntryItem
                id={ _id }
                authorFirstName={ item?.author?.firstName }
                authorLastName={ item?.author?.lastName }
                createdAt={ createdAt }
                imageUrls={ item.imageUrls }
                imageUrl={ item.imageUrls ? item.imageUrls[ 0 ] : null }
                location={ location }
                types={ types }
                userPosition={ item?.author?.position }
                yacht={ item.yacht }
                scrollY={ scrollY }
                opacity={ opacity }
                scale={ scale }
              />
              )
            )
          } }
        />
      )}
    </GlobalEntriesListWrapper>
  )
}

export default compose(EntriesContainer)(GlobalEntriesListView)
