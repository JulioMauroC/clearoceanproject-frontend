import React from 'react'
import styled from 'styled-components/native'
import Icon from 'components/Icon'
import Text from 'components/Text/Text'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

const StatusDisplayWrapper = styled.View`
  background-color: ${ (props) => props.theme.colors.red };
  margin-left: ${ (props) => props.theme.dimensions.appHorizontalPadding };
  margin-right: ${ (props) => props.theme.dimensions.appHorizontalPadding };
  margin-top: auto;
  margin-bottom: auto;
  /* flex: 0.9; */
  flex-direction: row;
  max-height: ${ (props) => props.theme.dimensions.dashboardCardHeight };
  flex-direction: row;
  align-items: center;
  /* padding: 10px 20px; */
  border-radius: 10px;
  padding-top: ${ heightToDp('1%') };
  padding-right: ${ widthToDp('6%') };
  padding-bottom: ${ heightToDp('2%') };
  padding-left: ${ widthToDp('6%') };
`

const StatusDisplayIconWrapper = styled.View`
  flex: 1;
  align-self: flex-start;
`

const StatusDisplayContentWrapper = styled.View`
  flex: 9;
  align-self: flex-start;
  padding-left: ${ (props) => props.theme.dimensions.paddingMedium };
`

const StatusDisplay = (props) => (
  <StatusDisplayWrapper>
    <StatusDisplayIconWrapper>
      <Icon icon="engine-warning" color="white" size={ 26 } />
    </StatusDisplayIconWrapper>

    <StatusDisplayContentWrapper>
      <Text color="white" size={ heightToDp('2.5%') }>

        <Text color="white" size={ heightToDp('3%') } fontFamily="Lato-Black">
          Status:

        </Text>
        {' '}
        Limited features
      </Text>
      <Text color="white" size={ heightToDp('1.6%') } style={ { paddingVertical: 2 } }>
        Waiting for email confirmation
      </Text>
      <Text color="white" size={ heightToDp('1.6%') }>
        Waiting for yachts official number
      </Text>
      {/* <Text
        style={ {
          color: 'white',
          paddingLeft: 15,
          fontSize: 16,
          fontFamily: 'Lato-Regular',
        } }
      />
      <Text style={ { fontFamily: 'Lato-Black' } }>{'Status:\n'}</Text>
      <Text>{'Waiting for email confirmation\n'}</Text>
      <Text>Waiting for yachts official number</Text> */}
    </StatusDisplayContentWrapper>
  </StatusDisplayWrapper>
)

export default StatusDisplay
