import React from 'react'
import { Dimensions } from 'react-native'
import Color from 'color'
import Text from 'components/Text/Text'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import styled from 'styled-components/native'
import { navigate } from 'navigation/RootNavigation'
import { LinearGradient } from 'expo-linear-gradient'
import Icon from 'components/Icon'

const windowWidth = Dimensions.get('window').width

const DashboardPanelButtonWrapper = styled.TouchableOpacity`
  position: relative;
  /* background-color: ${ (props) => (props.disabled ? 'grey' : props.backgroundColor) }; */
  width: ${ (props) => props.theme.dimensions.dashboardCardWidth };
  height: ${ (props) => props.theme.dimensions.dashboardCardHeight };
  border-radius: ${ (props) => props.theme.dimensions.dashboardCardWidth * 0.035 };
  /* margin-bottom: ${ (windowWidth * 0.2) / 3 }px; */
  justify-content: center;
  align-items: center;
  margin-bottom: ${ (props) => props.theme.dimensions.paddingMedLarge };
  /* border: 5px solid red; */
`

const DashboardPanelButton = (props) => {
  const {
    name,
    backgroundColor,
    color,
    icon,
    disabled,
    goTo,
    iconSize,
    isEmailVerified,
  } = props

  const brightenedColor = Color(backgroundColor).lighten(0.2).hex()
  const darkenedColor = Color(backgroundColor).darken(0.5).hex()

  const disabledBrightenedColor = Color(backgroundColor).lighten(0.2).desaturate(0.6).fade(0.5)
    .hex()
  const disabledBackgroundColor = Color(backgroundColor).darken(0.5).desaturate(0.9).fade(0.5)
    .hex()

  return (
    <DashboardPanelButtonWrapper
      // disabled={ !isEmailVerified }
      onPress={ isEmailVerified ? () => navigate(goTo) : () => alert('please confirm your email first') }
    >
      <LinearGradient
        colors={ isEmailVerified ? [ brightenedColor, backgroundColor ] : [ disabledBrightenedColor, disabledBackgroundColor ] }
        style={ {
          width: '100%',
          height: '100%',
          borderRadius: 12,
          alignItems: 'center',
          justifyContent: 'center',
          opacity: isEmailVerified ? 1 : 0.9,
        } }
      >
        <Icon
          color="rgba(256, 256, 256, 0.8)"
          icon={ icon }
          size={ widthToDp('8%') }
        />
        <Text
          size={ widthToDp('5%') }
          color={ isEmailVerified ? 'rgba(256, 256, 256, 0.8)' : 'rgba(256, 256, 256, 0.8)' }
          fontFamily="Lato-Bold"
          style={ {
            textAlign: 'center',
            padding: 5,
          } }
        >
          {name}
        </Text>
      </LinearGradient>
    </DashboardPanelButtonWrapper>
  )
}

export default DashboardPanelButton
