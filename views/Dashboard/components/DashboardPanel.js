import React from 'react'

import styled from 'styled-components/native'

import DashboardPanelButton from './DashboardPanelButton'

const DashboardPanelWrapper = styled.View`
  width: 100%;
  /* flex: 0.6; */
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;

  /* border:3px solid green; */
`

const DashboardPanel = (props) => {
  const { isEmailVerified } = props

  return (
    <DashboardPanelWrapper>
      <DashboardPanelButton
        name="Yacht Users"
        disabled={ false }
        icon="user"
        iconSize={ 32 }
        backgroundColor="#1E75B6"
        color="white"
        goTo="Users"
        isEmailVerified={ isEmailVerified }
      />
      <DashboardPanelButton
        name="Upload"
        disabled={ false }
        icon="cloud-upload-alt"
        iconSize={ 36 }
        backgroundColor="#1E75B6"
        color="white"
        goTo="Upload"
        isEmailVerified={ isEmailVerified }
      />
      <DashboardPanelButton
        name="Yacht Entries"
        disabled={ false }
        icon="ship"
        iconSize={ 36 }
        backgroundColor="#1E75B6"
        color="white"
        goTo="Entries"
        isEmailVerified={ isEmailVerified }
      />
      <DashboardPanelButton
        name="Global Results"
        disabled={ false }
        icon="globe-africa"
        iconSize={ 34 }
        backgroundColor="#1E75B6"
        color="white"
        goTo="Results"
        isEmailVerified={ isEmailVerified }
      />
    </DashboardPanelWrapper>
  )
}

export default DashboardPanel
