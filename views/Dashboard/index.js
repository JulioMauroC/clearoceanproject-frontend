import React from 'react'
import styled from 'styled-components/native'
import ProfileHeader from 'components/ProfileHeader'
import UsersContainer from 'containers/Users'
import YachtsContainer from 'containers/Yachts'
import { compose } from 'recompose'

import { useFocusEffect } from '@react-navigation/native'

import DashboardPanel from './components/DashboardPanel'

const DashboardWrapper = styled.View`
  flex: 1;
  align-items: center;
  width: 100%;
  position: relative;
`
const DashboardContainer = styled.View`
  flex: 1;
  width: 100%;
  margin-top: ${ (props) => props.theme.dimensions.yachtImageHeight
    + props.theme.dimensions.avatarDiameter / 2
    + props.theme.dimensions.paddingLarge };
`

const DashboardView = (props) => {
  const {
    getCurrentUserRequest,
    getCurrentYachtRequest,
    isEmailVerified,
  } = props

  useFocusEffect(
    React.useCallback(() => {
      getCurrentUserRequest()
    }, []),
  )

  React.useEffect(() => {
    if (!isEmailVerified) {
      const interval = setInterval(() => {
        getCurrentUserRequest()
      }, 15000)
      return () => clearInterval(interval)
    }
  }, [ isEmailVerified ])

  React.useEffect(() => {
    async function fetchData() {
      await getCurrentYachtRequest()
    }
    fetchData()
  }, [ getCurrentYachtRequest, getCurrentUserRequest ])

  return (
    <DashboardWrapper>
      <ProfileHeader />
      <DashboardContainer>
        <DashboardPanel isEmailVerified={ isEmailVerified } />
      </DashboardContainer>
    </DashboardWrapper>
  )
}

export default compose(YachtsContainer, UsersContainer)(DashboardView)
