import * as React from 'react'
import {
  Text, View, StyleSheet, SafeAreaView, Alert,
} from 'react-native'
import { AssetsSelector } from 'expo-images-picker'
import * as Permissions from 'expo-permissions'
import * as MediaLibrary from 'expo-media-library'
import { Ionicons } from '@expo/vector-icons'

const ForceInset = {
  top: 'never',
  bottom: 'never',
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textStyle: {
    color: 'black',
    fontWeight: 'bold',
  },
  buttonStyle: {
    backgroundColor: 'white',
    borderWidth: 2,
    borderRadius: 5,
    borderColor: 'black',
    width: 100,
  },
})

export default class ImageBrowseView extends React.Component {
  getPermission = async () => {
    const { granted } = await Permissions.askAsync(Permissions.CAMERA_ROLL)

    return granted
  }

  render() {
    const emptyStayComponent = <Text style={ styles.emptyStay }>Empty =</Text>
    const isGranted = MediaLibrary.requestPermissionsAsync()

    return (isGranted && (
    <View style={ styles.container }>
      <AssetsSelector
        options={ {
          assetsType: [ 'photo' ],
          noAssets: {
            Component: () => <View />,
          },
          maxSelections: 3,
          margin: 2,
          portraitCols: 4,
          landscapeCols: 5,
          widgetWidth: 100,
          widgetBgColor: 'white',
          videoIcon: {
            iconName: 'ios-videocam',
            color: 'green',
            size: 22,
          },
          selectedIcon: {
            iconName: 'ios-checkmark-circle-outline',
            color: 'white',
            bg: '#4fffc880',
            size: 26,
          },
          defaultTopNavigator: {
            continueText: 'DONE ',
            goBackText: 'BACK ',
            textStyle: styles.textStyle,
            buttonStyle: styles.buttonStyle,
            backFunction: () => goBack(),
            doneFunction: (data) => onDone(data),
          },

          /* Test Custom Navigator */

          /* CustomTopNavigator: {
                  Component: CustomNavigator,
                  props: {
                      backFunction: true,
                      text: 'Done',
                      doneFunction: (data) => onDone(data)
                  },
              }
        */
        } }
      />
    </View>
    )

    )
  }
}
