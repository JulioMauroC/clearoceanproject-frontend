import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native'
import * as ImageManipulator from 'expo-image-manipulator'
import { ImageBrowser } from 'expo-image-picker-multiple'

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  container: {
    position: 'relative',
  },
  emptyStay: {
    textAlign: 'center',
  },
  countBadge: {
    paddingHorizontal: 8.6,
    paddingVertical: 5,
    borderRadius: 50,
    position: 'absolute',
    right: 3,
    bottom: 3,
    justifyContent: 'center',
    backgroundColor: '#0580FF',
  },
  countBadgeText: {
    fontWeight: 'bold',
    alignSelf: 'center',
    padding: 'auto',
    color: '#ffffff',
  },
})

// import * as ImagePicker from 'expo-image-picker'
// import * as Permissions from 'expo-permissions'
// import { Alert } from 'react-native'

// const pickFromGallery = async () => {
//   console.log('pickFromGallery started')
//   const { granted } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
//   if (granted) {
//     const data = await ImagePicker.launchImageLibraryAsync({
//       mediaTypes: ImagePicker.MediaTypeOptions.Images,
//       allowsEditing: true,
//       aspect: [ 4, 3 ],
//       quality: 1,
//       // base64: true,
//     })

//     return data
//   }
//   Alert.alert('you need to give permission')
//   console.log('not granted')
// }

export default class ImageBrowseView extends React.Component {
  async componentDidMount() {
    const { granted } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
  }

  async _processImageAsync(uri) {
    const file = await ImageManipulator.manipulateAsync(
      uri,
      [ { resize: { width: 1000 } } ],
      { compress: 0.8, format: ImageManipulator.SaveFormat.JPEG },
    )
    return file
  }

  imagesCallback = (callback) => {
    callback
      .then(async (photos) => {
        const cPhotos = []
        for (const photo of photos) {
          const pPhoto = await this._processImageAsync(photo.uri)
          cPhotos.push({
            uri: pPhoto.uri,
            name: photo.filename,
            type: 'image/jpg',
          })
        }
        navigation.navigate('Upload', { photos: cPhotos })
      })
      .catch((e) => console.log(e))
  };

  _renderDoneButton = (count, onSubmit) => {
    if (!count) return null
    return (
      <TouchableOpacity title="Done" onPress={ onSubmit }>
        <Text onPress={ onSubmit }>Done</Text>
      </TouchableOpacity>
    )
  };

  updateHandler = (count, onSubmit) => {
    this.props.navigation.setOptions({
      title: `Selected ${ count } files`,
      headerRight: () => this._renderDoneButton(count, onSubmit),
    })
  };

  renderSelectedComponent = (number) => (
    <View style={ styles.countBadge }>
      <Text style={ styles.countBadgeText }>{number}</Text>
    </View>
  );

  render() {
    const emptyStayComponent = <Text style={ styles.emptyStay }>Empty =</Text>

    return (
      <View style={ [ styles.flex, styles.container ] }>
        <ImageBrowser
          max={ 4 }
          onChange={ this.updateHandler }
          callback={ this.imagesCallback }
          renderSelectedComponent={ this.renderSelectedComponent }
          emptyStayComponent={ emptyStayComponent }
        />
      </View>
    )
  }
}
