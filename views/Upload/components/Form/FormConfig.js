import * as yup from 'yup'
import { COLORS } from 'consts'

export const initialValues = {
  EntryImageInput: [],
  EntryLocationInput: '',
  EntryTypesInput: [],
}

export const DATA = [
  {
    key: 'vb71572',
    title: 'Add an Entry',
    fieldName: 'EntryImageInput',
    fieldIcon: 'water-lower',
    backgroundColor: '#353f8f',
    buttonText: 'Click Here to start camera',
    triggerIcon: 'upload',
    formResumeTitle: 'Entry image',
    type: 'image',
    required: true,
    hasImagePreview: true,
    fieldLabel: 'Upload Pictures',
    fieldSubLabel: 'max of 5 images',
  },
  {
    key: '1171572',
    title: 'Add an Entry',
    fieldName: 'EntryLocationInput',
    fieldIcon: 'water-lower',
    backgroundColor: '#0e427f',
    buttonText: 'Click Here to share your location',
    formResumeTitle: 'Entry location',
    type: 'picker',
    required: true,
    fieldLabel: 'Press to share location',
  },
  {
    key: 'bvxx747',
    title: 'Add an Entry',
    fieldName: 'EntryTypesInput',
    fieldIcon: 'water-lower',
    backgroundColor: '#145EB3',
    formResumeTitle: 'Entry Types',
    type: 'select',
    // required: true,
    fieldLabel: 'Enter type or select from below',
  },

  {
    key: '3574aaa',
    title: 'Add an Entry',
    fieldIcon: 'check-circle',
    // backgroundColor: '#0aa27f',
    backgroundColor: COLORS.RED,
    buttonText: 'Click to confirm',
    isConfirmationScreen: true,
  },
]

export const validationSchema = yup.object().shape({
  // EntryImageInput: yup.mixed().required('Add an image please'),
  // EntryLocationInput: yup.mixed().required('item location is required'),
  // EntryTypesInput: yup.mixed().required('items are required'),
})
