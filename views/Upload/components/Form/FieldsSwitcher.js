import React from 'react'

import styled from 'styled-components/native'
import FieldLabels from 'components/Forms/FieldLabels'

import EntryTypesInput from './Inputs/EntryTypesInput'
import EntryImageInput from './Inputs/EntryImageInput'
import EntryLocationInput from './Inputs/EntryLocationInput'

const FieldsSwitcherWrapper = styled.View`
  bottom: ${ (props) => props.theme.dimensions.switchFieldBottomDistance };
  width: 65%;
  z-index: 50000;
`

export const fieldsReg = {
  EntryTypesInput,
  EntryImageInput,
  EntryLocationInput,
}

export const FieldsSwitcher = (props) => {
  const { fieldName, fieldLabel, fieldSubLabel } = props

  if (fieldName) {
    const CurrentField = fieldsReg[ fieldName ]
    return (
      <FieldsSwitcherWrapper>
        <FieldLabels fieldLabel={ fieldLabel } fieldSubLabel={ fieldSubLabel } />
        <CurrentField { ...props } />
      </FieldsSwitcherWrapper>
    )
  }
}

export default FieldsSwitcher
