import React from 'react'
import {
  TouchableOpacity,
  Text,
  TextInput,
  View,
  Dimensions,
} from 'react-native'
import styled from 'styled-components/native'
import * as Location from 'expo-location'
import FormButton from 'components/Buttons/FormButton'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faLocationArrow } from '@fortawesome/free-solid-svg-icons'
import { COLORS } from 'consts'

const { width } = Dimensions.get('screen')

const EntryLocationInputWrapper = styled.View`
  /* border: 1px solid red;
  background-color: red; */
`

const TriggerFieldWrapper = styled.TouchableOpacity`
  /* border: 1.5px solid ${ COLORS.GREEN }; */
  background-color: white;
  box-shadow: 0px 1px 2px
    /* ${ (props) => (props.valueFromForm ? COLORS.GREEN : 'transparent') }; */
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
  /* width: ${ width * 0.55 }px; */
  height: 40px;
  border-radius: 10px;
  /* opacity:.75; */
`

function EntryLocationInput(props) {
  const {
    setFieldValue,
    setFieldTouched,
    fieldColor,
    fieldName,
    getLocationRequest,
    location,
  } = props

  const [ locationState, setLocation ] = React.useState({ lat: null, lon: null })
  const [ errorMsg, setErrorMsg ] = React.useState(null)

  React.useEffect(() => {
    if (location) {
      setLocation({
        lat: location.coords.latitude,
        lon: location.coords.longitude,
      })

      setFieldValue(fieldName, [
        location.coords.latitude,
        location.coords.longitude,
      ])
    }
  }, [ location ])

  const handlePosition = async () => {
    setFieldTouched('EntryLocationInput')
    const { status } = await Location.requestPermissionsAsync()
    if (status !== 'granted') {
      setErrorMsg('Permission to access location was denied')
      return
    }

    await getLocationRequest()

    // const location = await Location.getCurrentPositionAsync({})
    // setLocation(location.coords)

    // setLocation({
    //   lat: location.coords.latitude,
    //   lon: location.coords.longitude,
    // })
  }

  return (
    <TriggerFieldWrapper onPress={ handlePosition }>
      {locationState ? (
        <View>
          <Text style={ { fontSize: 12 } }>
            <Text style={ { fontFamily: 'Lato-Bold', fontSize: 12 } }>lat: </Text>

            {locationState.lat}
          </Text>

          <Text style={ { fontSize: 12 } }>
            <Text style={ { fontFamily: 'Lato-Bold', fontSize: 12 } }>lon: </Text>

            {locationState.lon}
          </Text>
        </View>
      ) : (
        <Text
          style={ {
            width: '85%',
            paddingLeft: 20,
            color: fieldColor,
            opacity: 1,
            fontFamily: 'Lato-Bold',
          } }
        >
          Get your location
        </Text>
      )}

      <FontAwesomeIcon
        icon={ faLocationArrow }
        color={ fieldColor }
        size={ 22 }
        secondaryOpacity={ 0.4 }
        style={ { alignSelf: 'center' } }
      />
    </TriggerFieldWrapper>
  )
}

export default EntryLocationInput
