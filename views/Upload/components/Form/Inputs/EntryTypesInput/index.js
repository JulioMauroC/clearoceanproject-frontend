import React from 'react'
import { FlatList, View } from 'react-native'
import styled from 'styled-components/native'
import EntryTypes from 'consts/EntryTypes'
import { FieldArray } from 'formik'

import EntryButton from './EntryButton'

import EntryOtherTypeField from './EntryOtherTypeField'

const EntryTypesInputWrapper = styled.View`
  width: 100%;
  z-index: 99999;
  align-items: center;
  justify-content: flex-end;
  /* position: absolute; */
`

function EntryTypesInput(props) {
  const [ typesFieldsState, setTypesFieldsState ] = React.useState(EntryTypes)

  const { setHideHeader, hideHeader } = props

  return (
    <EntryTypesInputWrapper style={ { transform: [ { translateY: 100 } ] } }>
      <EntryOtherTypeField
        setHideHeader={ setHideHeader }
        typesFieldsState={ typesFieldsState }
        setTypesFieldsState={ setTypesFieldsState }
      />

      <FieldArray
        name="EntryTypesInput"
        render={ (arrayHelpers) => (
          <FlatList
            contentContainerStyle={ {
              marginTop: 25,
            } }
            data={ typesFieldsState }
            keyExtractor={ (item) => item.value }
            numColumns={ 3 }
            ItemSeparatorComponent={ () => <View style={ { height: 4 } } /> }
            pagingEnabled
            renderItem={ ({ item, index }) => (
              <EntryButton
                hideHeader={ hideHeader }
                text={ item.label }
                arrayHelpers={ arrayHelpers }
                index={ index }
                isButtonInitiallySelected={ !!item.isButtonInitiallySelected }
              />
            ) }
          />
        ) }
      />
    </EntryTypesInputWrapper>
  )
}

export default EntryTypesInput
