import React from 'react'
import {
  FlatList,
  View,
  StyleSheet,
  Text,
  TextInput,
  ScrollView,
} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import * as yup from 'yup'
import styled from 'styled-components/native'

import { Formik } from 'formik'

import { COLORS } from 'consts'

export const validationSchema = yup.object().shape({
  otherField: yup.string().required('Please enter a type for the yacht'),
})

const EntryOtherTypeFieldWrapper = styled.View`
  /* border:1px solid red; */

  width: 100%;
  height: 40px;
  /* z-index: 999998; */
  position: relative;
`

const EntryOtherTypeFieldInput = styled.TextInput`
  background-color: #4988d1;
  height: 45px;
  border-radius: 5px;
  padding: 4px;
  flex: 3;
  position: relative;
  z-index: 999999;
  /* width:100%; */
  /* margin-top: 25px; */
`

const EntryOtherTypeField = (props) => {
  const { setTypesFieldsState, typesFieldsState, setHideHeader } = props

  return (
    <Formik
      initialValues={ { otherField: '' } }
      validationSchema={ validationSchema }
      enableReinitialize
      onSubmit={ (values) => {
        const newField = {
          label: values.otherField,
          value: values.otherField,
          isButtonInitiallySelected: true,
        }

        setTypesFieldsState(() => [ ...typesFieldsState, newField ])
      } }
    >
      {({
        handleChange, handleBlur, handleSubmit, values,
      }) => (
        <EntryOtherTypeFieldWrapper>
          <View
            style={ {
              display: 'flex',
              flexDirection: 'row',
              // justifyContent: 'space-between',
            } }
          >
            <EntryOtherTypeFieldInput
              placeholderTextColor="white"
              placeholder="Add Category"
              value={ values.otherField }
              onChangeText={ handleChange('otherField') }
              keyboardType="default"
              // keyboardAppearance="light"
              onBlur={ () => {
                setHideHeader(false)
                handleBlur('otherField')
              } }
              onEndEditing={ () => {
                setHideHeader(false)
              } }
            />

            <TouchableOpacity
              style={ {
                display: 'flex',
                flex: 2,
                alignItems: 'center',
                justifyContent: 'center',
                paddingLeft: 10,
              } }
              onPress={ handleSubmit }
            >
              <Text
                style={ {
                  color: 'white',
                  fontSize: 32,
                } }
              >
                +
              </Text>
            </TouchableOpacity>
          </View>
        </EntryOtherTypeFieldWrapper>
      )}
    </Formik>
  )
}

export default EntryOtherTypeField
