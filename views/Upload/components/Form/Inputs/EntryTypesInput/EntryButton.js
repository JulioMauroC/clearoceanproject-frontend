import React from 'react';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { StyleSheet } from 'react-native';
import styled from 'styled-components';
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  buttonWrapper: {
    width: widthToDp('14%'),
    height: 28,
    textAlign: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    marginVertical: 0,
    marginTop: 10,
    marginHorizontal: 10,
    zIndex: 9999999,
  },
});

const ButtonText = styled.Text`
  color: ${(props) => (props.isButtonSelected ? '#1564bf' : 'white')};
  text-align: center;
  font-weight: 700;
  font-size: 9px;
`;

const EntryButton = (props) => {
  const { text, index, isButtonInitiallySelected, hideHeader } = props;
  const { values } = props.arrayHelpers.form;

  const [isButtonSelected, switchButtonSelected] = React.useState(
    isButtonInitiallySelected
  );

  React.useEffect(() => {
    if (isButtonInitiallySelected === true) {
      props.arrayHelpers.push(text);
    }
  }, [isButtonInitiallySelected]);

  return (
    <TouchableOpacity
      style={[
        styles.buttonWrapper,
        {
          backgroundColor: isButtonSelected ? 'white' : '#1564bf',
          display: hideHeader ? 'none' : 'flex',
        },
      ]}
      onPress={() => {
        switchButtonSelected(!isButtonSelected);

        if (isButtonSelected) {
          props.arrayHelpers.remove(text);
        } else {
          props.arrayHelpers.push(text);
        }
      }}
      isButtonSelected={isButtonSelected}
    >
      <ButtonText numberOfLines={1} isButtonSelected={isButtonSelected}>
        {text}
      </ButtonText>
    </TouchableOpacity>
  );
};

export default EntryButton;
