import React from 'react'
import styled from 'styled-components/native'
import { AsyncStorage } from 'react-native'
import * as RootNavigation from 'navigation/RootNavigation'
import pickFromCamera from 'utils/pickFromCamera'
import pickFromGallery from 'utils/pickFromGallery'
import { FieldArray } from 'formik'

import AddImageButtons from 'components/Forms/AddImageButtons'

import { ImageBrowser } from 'expo-image-picker-multiple'

const EntryImageInputWrapper = styled.View`
  position: relative;
  justify-content: space-between;
  align-items: center;
`

function EntryImageInput(props) {
  const {
    values,
    value,
    setFieldValue,
    fieldName,
    currentIndex,
    fieldColor,
    scrollToNext,
    triggerIcon,
    setPreviewImages,
    previewImages,
    setFieldTouched,
  } = props

  const handleCamera = async () => {
    const img = await pickFromCamera()
    if (!img.cancelled) {
      const data = {
        height: img.height,
        width: img.width,
        type: img.type,
        uri: img.uri,
      }

      const updatedFieldArray = await previewImages.concat({ ...data })

      if (img) {
        await setFieldTouched('EntryImageInput')
        await setFieldValue(fieldName, updatedFieldArray)
      }

      await setPreviewImages(updatedFieldArray)
    }
  }

  const handleGallery = async () => {
    const img = await pickFromGallery()

    if (!img.cancelled) {
      const data = {
        height: img.height,
        width: img.width,
        type: img.type,
        uri: img.uri,
      }

      const updatedFieldArray = await previewImages.concat(data)

      if (img) {
        await setFieldTouched('EntryImageInput')
        // await setFieldValue(fieldName, [])
        await setFieldValue(fieldName, updatedFieldArray)
      }

      await setPreviewImages(updatedFieldArray)
    }
  }

  return (
    <EntryImageInputWrapper>
      <AddImageButtons
        handleCamera={ handleCamera }
        handleGallery={ handleGallery }
        fieldColor={ fieldColor }
        defaultText={
          values[ fieldName ]
            ? 'Press to take another picture'
            : 'Press to take picture'
        }
      />
      {/* <FieldArray
        name={ fieldName }
        render={ (arrayHelpers) => (
          <AddImageButtons
            handleCamera={ handleCamera }
            handleGallery={ handleGallery }
            fieldColor={ fieldColor }
            defaultText={
              values[ fieldName ]
                ? 'Press to take another picture'
                : 'Press to take picture'
            }
          />
        ) }
      /> */}
    </EntryImageInputWrapper>
  )
}

export default EntryImageInput
