import * as React from 'react'

import {
  Animated,
  View,
  Dimensions,
  KeyboardAvoidingView,
  Platform,
} from 'react-native'

import { Formik } from 'formik'

import { compose } from 'recompose'

import EntriesContainer from 'containers/Entries'

import ImagesPreview from 'components/Forms/ImagesPreview'

import SubmitButton from 'components/Forms/Button'

import PrevNext from 'components/Forms/PrevNext'

import BackgroundAnim from 'components/Forms/BackgroundAnim'
import FormHeader from 'components/Forms/FormHeader'
import FormResume from 'components/Forms/FormResume'
import FieldsSwitcher from './components/Form/FieldsSwitcher'

import {
  DATA,
  initialValues,
  validationSchema,
} from './components/Form/FormConfig'

const { width } = Dimensions.get('screen')

const UploadView = (props) => {
  const { createEntryRequest } = props

  const scrollX = React.useRef(new Animated.Value(0)).current
  const [ previewImages, setPreviewImages ] = React.useState([])
  const [ formErrors, setFormErrors ] = React.useState()
  const [ isFormValid, setFormValid ] = React.useState(false)
  const [ hideHeader, setHideHeader ] = React.useState(false)

  const formRef = React.useRef()
  const flatListRef = React.useRef()
  const backgroundColors = DATA.map((item) => item.backgroundColor)

  // console.log('previewImages')
  // console.log(previewImages)

  const removePreviewImage = (index) => {
    const previewImagesArray = [ ...previewImages ]
    previewImagesArray.splice(index, 1)
    setPreviewImages(previewImagesArray)
  }

  const scrollToPrev = (currentIndex) => {
    flatListRef.current.scrollToIndex({
      animated: true,
      index: currentIndex - 1,
    })
  }

  const scrollToNext = (currentIndex) => {
    flatListRef.current.scrollToIndex({
      animated: true,
      index: currentIndex + 1,
    })
  }

  const scrollToIndex = (index) => {
    flatListRef.current.scrollToIndex({
      animated: true,
      index,
    })
  }

  return (
    <KeyboardAvoidingView
      style={ {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
      } }
      behavior={ Platform.OS === 'ios' ? 'padding' : 'height' }
      keyboardVerticalOffset={ 75 }
    >
      <Formik
        innerRef={ formRef }
        initialValues={ initialValues }
        validationSchema={ validationSchema }
        enableReinitialize
        onSubmit={ async (values, { resetForm }) => {
          await createEntryRequest(values)
        } }
      >
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          setFieldValue,
          values,
          errors,
          touched,
          setFieldTouched,
          isSubmitting,
        }) => {
          setFormErrors(errors)

          if (touched.EntryLocationInput && !errors.EntryLocationInput) {
            setFormValid(true)
          }

          return (
            <View
              style={ {
                position: 'relative',
              } }
            >
              {/* <ProgressText text="Add an entry..." /> */}
              <BackgroundAnim
                isFormValid={ isFormValid }
                backgrounds={ backgroundColors }
                scrollX={ scrollX }
              />

              <Animated.FlatList
                scrollEnabled={ false }
                removeClippedSubviews={ false }
                data={ DATA }
                ref={ flatListRef }
                keyExtractor={ (item) => item.key }
                horizontal
                scrollEventThrottle={ 32 }
                scrollToNext={ scrollToNext }
                onScroll={ Animated.event(
                  [
                    {
                      nativeEvent: { contentOffset: { x: scrollX } },
                    },
                  ],
                  { useNativeDriver: false },
                ) }
                onScrollBeginDrag={ () => {
                  setHideHeader(false)
                } }
                // contentContainerStyle={ { paddingBottom: 50 } }
                showsHorizontalScrollIndicator={ false }
                pagingEnabled
                renderItem={ ({ item, index }) => {
                  const {
                    isConfirmationScreen,
                    description,
                    backgroundColor,
                    title,
                    fieldName,
                    fieldIcon,
                    hasImagePreview,
                    required,
                    fieldLabel,
                    fieldSubLabel,
                  } = item

                  return (
                    <View
                      style={ {
                        width,
                        // height: heightToDp('80%'),
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        position: 'relative',
                      } }
                    >
                      <FormHeader
                        isConfirmationScreen={ isConfirmationScreen }
                        isFormValid={ isFormValid }
                        title={ title }
                        description={ description }
                        fieldIcon={ fieldIcon }
                        hideHeader={ hideHeader }
                        backgroundColor={ backgroundColor }
                        data={ DATA }
                        scrollX={ scrollX }

                      />

                      <ImagesPreview
                        previewImages={ previewImages }
                        setPreviewImages={ setPreviewImages }
                        hasImagePreview={ hasImagePreview }
                        fieldColor={ backgroundColor }
                        multiple
                        action={ removePreviewImage }
                        setFieldValue={ setFieldValue }
                      />

                      {item.fieldName && (
                        <FieldsSwitcher
                          { ...props }
                          fieldName={ item.fieldName }
                          fieldLabel={ fieldLabel }
                          fieldSubLabel={ fieldSubLabel }
                          handleChange={ handleChange }
                          handleBlur={ handleBlur }
                          setFieldValue={ setFieldValue }
                          values={ values }
                          value={ values[ item.fieldName ] }
                          setPreviewImages={ setPreviewImages }
                          previewImages={ previewImages }
                          buttonText={ item.buttonText }
                          errors={ errors }
                          touched={ touched }
                          setFieldTouched={ setFieldTouched }
                          fieldColor={ item.backgroundColor }
                          currentIndex={ index }
                          flatListRef={ flatListRef }
                          scrollToNext={ scrollToNext }
                          triggerIcon={ item.triggerIcon }
                          isConfirmationScreen={ item.isConfirmationScreen }
                          setHideHeader={ setHideHeader }
                          hideHeader={ hideHeader }
                        />
                      )}

                      <FormResume
                        isConfirmationScreen={ isConfirmationScreen }
                        touched={ touched }
                        errors={ errors }
                        isSubmitting={ isSubmitting }
                        values={ values }
                        formConfigData={ DATA }
                        scrollToIndex={ scrollToIndex }
                      />

                      <SubmitButton
                        text="Save Entry"
                        handleSubmit={ handleSubmit }
                        isConfirmationScreen={ isConfirmationScreen }
                        isFormValid={ isFormValid }
                      />
                      <PrevNext
                        currentIndex={ index }
                        numOfSteps={ DATA.length }
                        scrollToPrev={ scrollToPrev }
                        scrollToNext={ scrollToNext }
                        fieldColor={ backgroundColor }
                        required={ required }
                        values={ values }
                        fieldName={ fieldName }
                        errors={ errors }
                        touched={ touched }
                      />
                    </View>
                  )
                } }
              />
            </View>
          )
        }}
      </Formik>
    </KeyboardAvoidingView>
  )
}

export default compose(EntriesContainer)(UploadView)
