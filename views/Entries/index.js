import React from 'react'
import { compose } from 'recompose'
import EntriesContainer from 'containers/Entries'
import styled from 'styled-components/native'
import ProfileHeader from 'components/ProfileHeader'
import { useSelector } from 'react-redux'
import Tabs from 'components/Tabs'

import EntriesList from './views/EntriesList'

const EntriesWrapper = styled.View`
  flex: 1;
  align-items: center;
  width: 100%;
`

const EntriesListContainer = styled.View`
  flex: 1;
  align-items: center;
  width: 100%;
  margin-top: ${ (props) => props.theme.dimensions.listPaddingTop };
`

const EntriesView = (props) => {
  const {
    entriesData,
    getYachtEntriesRequest,
    getGlobalEntriesRequest,
    deleteEntryRequest,
  } = props

  const [ entriesList, setEntriesList ] = React.useState(entriesData)

  const [ activeViewIndex, setActiveViewIndex ] = React.useState(0)

  const isCurrentUserAdmin = useSelector((state) => state.USERS.currentUserData.isAdmin)

  console.log('isCurrentUserAdmin')
  console.log(isCurrentUserAdmin)

  // useFocusEffect(
  //   React.useCallback(() => {
  //     getYachtEntriesRequest()
  //   }, []),
  // )
  React.useEffect(() => {
    getYachtEntriesRequest()
  }, [])

  React.useEffect(() => {
    setEntriesList(entriesData)
  }, [ entriesData ])

  const tabs = [
    { id: 'Yacht', label: 'Yacht Entries' },
    // { id: 'Global', label: 'Global' },
  ]

  return (
    <EntriesWrapper>
      {/* <ImageBackground /> */}
      <ProfileHeader />

      <EntriesListContainer>
        <Tabs
          tabs={ tabs }
          action={ setActiveViewIndex }
          activeViewIndex={ activeViewIndex }
        />

        {activeViewIndex === 0 ? (
          <EntriesList
            entriesList={ entriesList }
            deleteEntryRequest={ deleteEntryRequest }
            isCurrentUserAdmin={ isCurrentUserAdmin }
          />
        ) : null}
      </EntriesListContainer>
    </EntriesWrapper>
  )
}

export default compose(EntriesContainer)(EntriesView)
