import React from 'react'

import { View, Dimensions } from 'react-native'
import { COLORS } from 'consts'
import styled from 'styled-components/native'
import Avatar from 'components/Avatar'
import Icon from 'components/Icon'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import ProfileHeader from 'components/ProfileHeader'
import ImageBackground from 'components/ImageBackground'
import * as RootNavigation from 'navigation/RootNavigation'

import Button from 'components/Button'
import EntryImages from './EntryImages'

const EntryWrapper = styled.View`
  flex: 1;
  align-items: center;
  width: 100%;
`

const CloseButtonWrapper = styled.TouchableOpacity`
  position: absolute;
  right: 20px;
  top: 15px;
  z-index: 10000;
`

const EntryCardWrapper = styled.View`
  position: relative;
  width: ${ (props) => props.theme.dimensions.entryCardWidth };
  height: ${ (props) => props.theme.dimensions.entryCardHeight };
  background-color: white;
  /* justify-content: center; */
  padding: 20px 0;
  margin: auto;
  border-radius: 20px;
  box-shadow: 1px 2px 10px rgba(11, 33, 33, 0.4);
`

const EntryCardHeader = styled.View`
  /* flex: 0.35; */
  flex-direction: row;
  width: ${ (props) => props.theme.dimensions.userCardWidth * 0.75 };
  margin-left: auto;
  margin-right: auto;
  justify-content: center;
  align-items: flex-start;
  border-bottom-color: #abb3c0;
  border-bottom-width: 0.4px;
  padding: 20px 0;
`

const FormattedText = styled.Text`
  font-size: ${ (props) => (props.fontSize ? props.fontSize : 12) }px;
  font-family: ${ (props) => (props.fontFamily ? props.fontFamily : 'Lato-Regular') };
  color: ${ (props) => (props.color ? props.color : '#707070') };
  margin-right: 10;
  text-align: ${ (props) => (props.textAlign ? props.textAlign : 'left') };
`

const TypesWrapper = styled.View`
  /* border-bottom-color: #abb3c0;
  border-bottom-width: 0.4px; */
  width: 80%;
  text-align: left;
  align-self: center;
  align-items: flex-start;
  /* border:1px solid red; */
  margin: 20px 0;
`

const DeleteButtonContainer = styled.View`
  /* flex: 0.1; */
  justify-content: flex-end;
  padding-left: ${ (props) => props.theme.dimensions.paddingMedLarge };
  padding-right: ${ (props) => props.theme.dimensions.paddingMedLarge };
  margin-top: auto;
  /* margin-bottom: ${ (props) => props.theme.dimensions.paddingLarge }; */
`

const EntryView = (props) => {
  const {
    id,
    authorFirstName,
    authorLastName,
    imageUrl,
    imageUrls,
    latitude,
    longitude,
    types,
    userName,
    createdAtFormatted,
    deleteEntryRequest,
  } = props.route.params

  return (
    <EntryWrapper>

      <ImageBackground />
      <ProfileHeader />
      <EntryCardWrapper>
        <CloseButtonWrapper
          hitSlop={ {
            top: 10,
            bottom: 10,
            left: 10,
            right: 10,
          } }
          onPress={ () => RootNavigation.navigate('Entries') }
        >
          <Icon icon="times-circle" color={ COLORS.RED } size={ 16 } />
        </CloseButtonWrapper>

        <EntryCardHeader>
          <View style={ { flex: 1 } }>
            <Avatar imgUrl={ imageUrl } size={ widthToDp('20%') } />
            <EntryImages imageUrls={ imageUrls } />
            <View
              style={ {
                flexDirection: 'row',
                alignItems: 'center',
                paddingTop: 10,
              } }
            >
              <Icon icon="map-marker-alt" color={ COLORS.RED } size={ 18 } />

              <View style={ { paddingLeft: 5, flexDirection: 'row' } }>
                <FormattedText fontSize={ 13 }>
                  {`lat: ${ latitude }`}
                </FormattedText>
                <FormattedText fontSize={ 13 }>
                  {`lon: ${ longitude }`}
                </FormattedText>
              </View>
            </View>
          </View>
          <View
            style={ {
              flex: 1,
              paddingLeft: 10,
            } }
          >
            <FormattedText fontSize={ 16 } fontFamily="Lato-Regular">
              Uploaded by
              <FormattedText fontSize={ 16 } fontFamily="Lato-Bold" color="black">
                {` ${ authorFirstName } ${ authorLastName }`}
              </FormattedText>
            </FormattedText>
          </View>
        </EntryCardHeader>
        <TypesWrapper>
          <FormattedText
            fontSize={ 18 }
            color="black"
            style={ { paddingBottom: 10 } }
          >
            Types
          </FormattedText>

          <View style={ { flexDirection: 'row', flexWrap: 'wrap' } }>
            {types && types.length > 0 ? (
              types.map((type, index) => (
                <View style={ { alignSelf: 'center' } }>
                  <FormattedText
                    key={ index }
                    fontSize={ 12 }
                    color={ COLORS.BLUE }
                    style={ { paddingBottom: 5, alignSelf: 'center' } }
                    textAlign="left"
                  >
                    {type}
                  </FormattedText>
                </View>
              ))
            ) : (
              <FormattedText color={ COLORS.RED } fontSize={ 18 }>
                No types associated with this entry
              </FormattedText>
            )}
          </View>
        </TypesWrapper>
        <DeleteButtonContainer>
          <Button
            title="Delete Entry"
            bgColor={ COLORS.RED }
            textColor="white"

          />
        </DeleteButtonContainer>
      </EntryCardWrapper>
    </EntryWrapper>
  )
}

export default EntryView
