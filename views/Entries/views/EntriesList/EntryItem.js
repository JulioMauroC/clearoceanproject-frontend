import React from 'react'
import {
  View, Text, Image, TouchableOpacity, Animated,
} from 'react-native'
import Avatar from 'components/Avatar'
import moment from 'moment'
// import UsersContainer from 'containers/Users'

import styled from 'styled-components/native'
import * as RootNavigation from 'navigation/RootNavigation'
import { DUMMY, DIMENSIONS, COLORS } from 'consts'
import Icon from 'components/Icon'

const EntryItem = (props) => {
  const {
    id,
    authorFirstName,
    authorLastName,
    createdAt,
    imageUrl,
    imageUrls,
    location,
    types,
    userPosition,
    // yacht,
    scrollY,
    opacity,
    scale,
    deleteEntryRequest,
    isCurrentUserAdmin,
  } = props

  const createdAtFormatted = moment(createdAt).fromNow()

  const longitude = location.coordinates[ 0 ]
  const latitude = location.coordinates[ 1 ]

  return (
    <Animated.View
      style={ {
        marginBottom: DIMENSIONS.USERS_LIST.SPACING,
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        padding: DIMENSIONS.USERS_LIST.SPACING,
        borderRadius: 12,
        shadowColor: 'black',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 5,
        transform: [ { scale } ],
        opacity,
      } }
    >
      <TouchableOpacity
        onPress={ () => RootNavigation.navigate('Entry', {
          id,
          authorFirstName,
          authorLastName,
          createdAtFormatted,
          latitude,
          longitude,
          // createdAt,
          imageUrl,
          imageUrls,
          location,
          types,
          deleteEntryRequest,
          isCurrentUserAdmin,
          // userPosition
        }) }
        style={ {
          flexDirection: 'row',
          alignItems: 'center',
          flex: 1,
        } }
      >
        <Avatar
          imgUrl={ imageUrl }
          size={ DIMENSIONS.USERS_LIST.USER_AVATAR_SIZE }
        />

        <View
          style={ {
            alignItems: 'flex-start',
            alignSelf: 'center',
            paddingLeft: 10,
          } }
        >
          <View style={ { alignItems: 'flex-start' } }>
            <Text
              style={ {
                fontSize: 12,
                fontFamily: 'Lato-Regular',
                color: '#707070',
                marginRight: 10,
                paddingVertical: 2,
              } }
            >
              Uploaded by
              <Text
                style={ {
                  fontSize: 12,
                  fontFamily: 'Lato-Black',
                  color: '#707070',
                  marginRight: 10,
                } }
              >
                {authorFirstName && authorLastName
                  ? ` ${ authorFirstName } ${ authorLastName }`
                  : 'deleted user'}
              </Text>
            </Text>
          </View>

          <View>
            <Text
              style={ {
                fontSize: 10,
                fontFamily: 'Lato-Regular',
                color: '#707070',
                marginRight: 10,
              } }
            >
              {createdAtFormatted}
            </Text>
          </View>

          <View style={ { flexDirection: 'row' } }>
            <Icon
              color={ COLORS.BLUE }
              icon="images"
              size={ 12 }
              style={ {
                alignSelf: 'center',
                marginLeft: 'auto',
                opacity: 0.85,
              } }
            />
            <Text
              style={ {
                fontSize: 10,
                fontFamily: 'Lato-Regular',
                color: '#707070',
                marginLeft: 5,
                paddingVertical: 2,
              } }
            >
              {imageUrls
                ? imageUrls.length > 1
                  ? `${ imageUrls.length } images`
                  : '1 image'
                : 'no images'}
            </Text>
          </View>

          <View style={ { flexDirection: 'row' } }>
            <Icon
              color={ COLORS.RED }
              icon="map-marker-alt"
              size={ 12 }
              style={ {
                alignSelf: 'center',
                marginLeft: 'auto',
                opacity: 0.85,
              } }
            />
            <Text
              style={ {
                fontSize: 10,
                fontFamily: 'Lato-Regular',
                color: '#707070',
                marginLeft: 5,
                paddingVertical: 2,
              } }
            >
              {`lat: ${ latitude }, lon: ${ longitude }`}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </Animated.View>
  )
}

export default EntryItem
