import React from 'react'
import styled from 'styled-components/native'
import { COLORS } from 'consts'
import Button from 'components/Button'

const NoEntriesMessageWrapper = styled.View`
  padding: ${ (props) => props.theme.dimensions.paddingMedLarge }px;
`

const FormattedText = styled.Text`
  font-size: ${ (props) => (props.fontSize ? props.fontSize : 12) }px;
  font-family: ${ (props) => (props.fontFamily ? props.fontFamily : 'Lato-Regular') };
  color: ${ (props) => (props.color ? props.color : '#707070') };
  margin-right: 10;
  text-align: ${ (props) => (props.textAlign ? props.textAlign : 'left') };
`

const NoEntriesMessage = (props) => (
  <NoEntriesMessageWrapper>
    <FormattedText fontSize={ 22 }>
      No entries associated with this yacht
    </FormattedText>

    {/* <Button
      title="Click here to add new entry"
      bgColor={ COLORS.RED }
      textColor="white"
    /> */}
  </NoEntriesMessageWrapper>
)

export default NoEntriesMessage
