import React from 'react'
import { Animated } from 'react-native'

// import UsersContainer from 'containers/Users'

import styled from 'styled-components/native'
import * as RootNavigation from 'navigation/RootNavigation'
import { DIMENSIONS } from 'consts'
import NoEntriesMessage from './NoEntriesMessage'

import EntryItem from './EntryItem'

const EntriesListWrapper = styled.View`
  width: 100%;
  flex: 1;
  margin-top: 10px;
`

const EntriesListView = (props) => {
  const { entriesList, deleteEntryRequest, isCurrentUserAdmin } = props

  const scrollY = React.useRef(new Animated.Value(0)).current

  const ITEM_SIZE = DIMENSIONS.USERS_LIST.USER_AVATAR_SIZE + DIMENSIONS.USERS_LIST.SPACING * 3

  return (
    <EntriesListWrapper>
      {entriesList && entriesList.length > 0 ? (
        <Animated.FlatList
          data={ entriesList }
          onScroll={ Animated.event(
            [
              {
                nativeEvent: { contentOffset: { y: scrollY } },
              },
            ],
            { useNativeDriver: true },
          ) }
          keyExtractor={ (item) => item._id }
          contentContainerStyle={ {
            paddingVertical: DIMENSIONS.USERS_LIST.SPACING,
            paddingHorizontal: DIMENSIONS.USERS_LIST.SPACING * 1.5,
          } }
          renderItem={ ({ item, index }) => {
            const position = item?.author?.position
            const name = item?.author?.name

            const {
              imageUrl, createdAt, _id, location, types,
            } = item

            // const entryImageUrl = item.imageUrl[ 0 ] || null

            const inputRange = [
              -1,
              0,
              ITEM_SIZE * index,
              ITEM_SIZE * (index + 0.5),
            ]

            const scale = scrollY.interpolate({
              inputRange,
              outputRange: [ 1, 1, 1, 0.9 ],
            })

            const opacity = scrollY.interpolate({
              inputRange,
              outputRange: [ 1, 1, 1, 0 ],
            })

            return (
              <EntryItem
                id={ _id }
                authorFirstName={ item?.author?.firstName }
                authorLastName={ item?.author?.lastName }
                createdAt={ createdAt }
                imageUrls={ item.imageUrls }
                imageUrl={ item.imageUrls ? item.imageUrls[ 0 ] : null }
                location={ location }
                types={ types }
                userPosition={ position }
                // yacht={item.yacht}
                scrollY={ scrollY }
                opacity={ opacity }
                scale={ scale }
                deleteEntryRequest={ deleteEntryRequest }
                isCurrentUserAdmin={ isCurrentUserAdmin }
              />
            )
          } }
        />
      ) : <NoEntriesMessage />}
    </EntriesListWrapper>
  )
}

export default EntriesListView
