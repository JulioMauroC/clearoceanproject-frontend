import React from 'react'
import {
  View, Text, TouchableOpacity, StyleSheet,
} from 'react-native'

const styles = StyleSheet.create({
  container: {
    // backgroundColor: 'red',
    width: '100%',
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    fontSize: 32,
  },
  text: {
    fontSize: 22,
    color: 'white',
  },
})
const NavButtons = (props) => {
  const { navToEntry } = props

  return (
    <View style={ styles.container }>
      <TouchableOpacity onPress={ navToEntry }>
        <Text style={ styles.text }>Add Entry</Text>
      </TouchableOpacity>
    </View>
  )
}

export default NavButtons
