import React from 'react'
import { View, Text } from 'react-native'

const LoadingView = (props) => <View><Text>Loading...</Text></View>

export default LoadingView
