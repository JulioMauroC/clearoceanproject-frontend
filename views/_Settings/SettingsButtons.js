import React from 'react'
import styled from 'styled-components/native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faUndo, faCheckCircle } from '@fortawesome/free-solid-svg-icons'

import { COLORS } from 'consts'

const SettingsButtonsWrapper = styled.View`
  flex-direction: row;
  justify-content: space-evenly;
  align-items:flex-end;
  margin-top: auto;
  margin-bottom: ${ (props) => props.theme.dimensions.paddingMedLarge };
  width: 100%;
  flex:0.2;

`

const ButtonWrapper = styled.TouchableOpacity`
  width: 30px;
  height: 30px;
  border-radius: 25px;
  background-color: #faf2f2;
  justify-content: center;
  align-items: center;
  box-shadow: 1px 1px 4px rgba(15, 13, 20, 0.35);
`

const SettingsButtons = (props) => {
  const {
    dirty, resetForm, resetStateValues, handleSubmit,
  } = props

  const handleReset = () => {
    resetForm()
    resetStateValues()
  }

  return (
    <SettingsButtonsWrapper>
      <ButtonWrapper disabled={ !dirty } onPress={ handleReset }>
        <FontAwesomeIcon
          icon={ faUndo }
          color={ dirty ? COLORS.RED : '#ABB3C0' }
          size={ 15 }
        />
      </ButtonWrapper>
      <ButtonWrapper disabled={ !dirty } onPress={ handleSubmit }>
        <FontAwesomeIcon
          icon={ faCheckCircle }
          color={ dirty ? COLORS.MID_GREEN : '#ABB3C0' }
          size={ 16 }
        />
      </ButtonWrapper>
    </SettingsButtonsWrapper>
  )
}

export default SettingsButtons
