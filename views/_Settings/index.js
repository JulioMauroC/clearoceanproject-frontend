import React from 'react'

import { View, Dimensions } from 'react-native'
import { COLORS } from 'consts'
import styled from 'styled-components/native'
import { Formik, useFormikContext } from 'formik'
import * as RootNavigation from 'navigation/RootNavigation'
import { useFocusEffect, useIsFocused } from '@react-navigation/native'
import { useSelector } from 'react-redux'
import ProfileHeader from 'components/ProfileHeader'
import ImageBackground from 'components/ImageBackground'

import UsersContainer from 'containers/Users'
import YachtsContainer from 'containers/Yachts'

import { compose } from 'recompose'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import SettingsHeader from './SettingsHeader'

import SettingsProfile from './SettingsProfile'
import SettingsYacht from './SettingsYacht'
import SettingsButtons from './SettingsButtons'

const SettingsWrapper = styled.View`
  flex: 1;
  align-items: center;
  width: 100%;
`

const SettingsCardWrapper = styled.View`
  position: relative;
  width: ${ (props) => props.theme.dimensions.settingsCardWidth };
  height: ${ (props) => props.theme.dimensions.settingsCardHeight };
  max-height:${ heightToDp('80%') };
  background-color: white;
  align-self: center;
  justify-content: flex-start;
  padding-top:${ heightToDp('1%') };
  padding-bottom:${ heightToDp('1%') };
  padding-left:${ (props) => props.theme.dimensions.paddingMedLarge };
  padding-right:${ (props) => props.theme.dimensions.paddingMedLarge };
  margin: auto;
  border-radius: 15px;
  box-shadow: 1px 2px 10px rgba(11, 33, 33, 0.4);
};
  
`

const SettingsView = (props) => {
  const {
    currentUserData,
    currentYachtData,
    editUserRequest,
    editYachtRequest,
    getCurrentUserRequest,
  } = props

  useFocusEffect(
    React.useCallback(() => {
      getCurrentUserRequest()
    }, []),
  )

  const [ isUserPrivacyEnabled, setIsUserPrivacyEnabled ] = React.useState(
    currentUserData?.isPrivateProfile,
  )

  const [ isUserAdmin, setIsUserAdmin ] = React.useState(
    currentUserData?.isAdmin,
  )

  const [ isYachtPrivacyEnabled, setIsYachtPrivacyEnabled ] = React.useState(
    currentUserData?.yacht.isPrivateProfile,
  )
  const toggleUserPrivacySwitch = () => setIsUserPrivacyEnabled((previousState) => !previousState)
  const toggleYachtPrivacySwitch = () => setIsYachtPrivacyEnabled((previousState) => !previousState)

  const [ userImagePreview, setUserImagePreview ] = React.useState(
    currentUserData?.profileImage,
  )

  const [ yachtImagePreview, setYachtImagePreview ] = React.useState(
    currentUserData?.yacht.yachtImage,
  )

  const settingsInitialValues = {
    isPrivateProfile: currentUserData.isPrivateProfile,
    userImage: currentUserData.profileImage,
    yachtPrivacy: currentYachtData.isPrivateProfile,
    yachtImage: currentYachtData.yachtImage,
  }

  const FieldUpdater = () => {
    const { setFieldValue } = useFormikContext()

    React.useEffect(() => {
      setFieldValue('isPrivateProfile', isUserPrivacyEnabled)
    }, [ isUserPrivacyEnabled ])

    React.useEffect(() => {
      setFieldValue('yachtPrivacy', isYachtPrivacyEnabled)
    }, [ isYachtPrivacyEnabled ])

    React.useEffect(() => {
      setFieldValue('userImage', userImagePreview)
    }, [ userImagePreview ])

    React.useEffect(() => {
      setFieldValue('yachtImage', yachtImagePreview)
    }, [ yachtImagePreview ])
    return null
  }

  const resetStateValues = () => {
    setIsUserPrivacyEnabled(settingsInitialValues.isPrivateProfile)
    setUserImagePreview(settingsInitialValues.userImage)
    setIsYachtPrivacyEnabled(settingsInitialValues.yachtPrivacy)
    setYachtImagePreview(settingsInitialValues.yachtImage)
  }

  return (
    <SettingsWrapper>
      {/* <ImageBackground /> */}
      <ProfileHeader />

      <Formik
        initialValues={ settingsInitialValues }
        enableReinitialize
        onSubmit={ async (val, { resetForm, touched }) => {
          await editUserRequest(currentUserData._id, {
            isPrivateProfile: val.isPrivateProfile,
            userImage: val.userImage,
          })

          await editYachtRequest(currentYachtData._id, {
            isPrivateProfile: val.isPrivateProfile,
            yachtImage: val.yachtImage,
          })
        } }
      >
        {({
          handleSubmit, setFieldValue, dirty, resetForm,
        }) => (
          <SettingsCardWrapper>
            <FieldUpdater />
            <SettingsHeader />

            <View style={ {
              flex: 0.9,
            } }
            >

              <SettingsProfile
                currentUserData={ currentUserData }
                toggleSwitch={ toggleUserPrivacySwitch }
                isUserPrivacyEnabled={ isUserPrivacyEnabled }
                setFieldValue={ setFieldValue }
                userImagePreview={ userImagePreview }
                setUserImagePreview={ setUserImagePreview }
              />
              {isUserAdmin ? (
                <SettingsYacht
                  currentYachtData={ currentYachtData }
                  toggleSwitch={ toggleYachtPrivacySwitch }
                  isYachtPrivacyEnabled={ isYachtPrivacyEnabled }
                  setFieldValue={ setFieldValue }
                  yachtImagePreview={ yachtImagePreview }
                  setYachtImagePreview={ setYachtImagePreview }
                />
              ) : null}

              <SettingsButtons
                dirty={ dirty }
                resetForm={ resetForm }
                resetStateValues={ resetStateValues }
                handleSubmit={ handleSubmit }
              />

            </View>

          </SettingsCardWrapper>
        )}
      </Formik>
    </SettingsWrapper>
  )
}
export default compose(UsersContainer, YachtsContainer)(SettingsView)
