import React from 'react'
import styled from 'styled-components/native'
import * as RootNavigation from 'navigation/RootNavigation'
import Icon from 'components/Icon'

import { COLORS } from 'consts'

const SettingsHeaderWrapper = styled.View`
  flex: 0.1;

`

const CloseButtonWrapper = styled.TouchableOpacity`
  position: absolute;
  right: 0px;
  top: 15px;
  z-index: 10000;
`

const FormattedText = styled.Text`
  font-size: ${ (props) => (props.fontSize ? props.fontSize : 12) }px;
  font-family: 'Lato-Bold';
  color: ${ (props) => (props.color ? props.color : '#707070') };
  margin-right: 10px;
  text-align: left;
`

const SettingsHeader = (props) => (
  <SettingsHeaderWrapper>
    <CloseButtonWrapper
      hitSlop={ {
        top: 10,
        bottom: 10,
        left: 10,
        right: 10,
      } }
      onPress={ () => RootNavigation.navigate('Dashboard') }
    >
      <Icon icon="times-circle" color={ COLORS.RED } size={ 18 } />
    </CloseButtonWrapper>

    <FormattedText
      fontSize={ 18 }
      color={ COLORS.BLUE }
      style={ { textAlign: 'center', paddingTop: 15 } }
    >
      SETTINGS
    </FormattedText>
  </SettingsHeaderWrapper>
)

export default SettingsHeader
