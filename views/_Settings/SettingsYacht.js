import React from 'react'

import { View } from 'react-native'
import { COLORS } from 'consts'
import styled from 'styled-components/native'

import Toggle from 'components/Toggle'

import pickFromCamera from 'utils/pickFromCamera'
import pickFromGallery from 'utils/pickFromGallery'
import UploadImageButton from './UploadImageButton'

const SettingsYachtWrapper = styled.View`
  flex: 0.4;
  flex-direction: row;
  /* justify-content: space-evenly; */
  align-items: center;
  border-bottom-color: #abb3c0;
  border-bottom-width: 0.2px;
  width: 100%;
`

const FormattedText = styled.Text`
  font-size: ${ (props) => (props.fontSize ? props.fontSize : 12) }px;
  font-family: 'Lato-Bold';
  color: ${ (props) => (props.color ? props.color : '#707070') };
  margin-right: 10px;
  text-align: left;
`

const SettingsFieldWrapper = styled.View`
  flex-direction: row;
  /* padding: 10px 15px; */
  width: 100%;
  justify-content: space-between;
  align-items: center;
  /* border:1px solid red; */
`

const SettingsYacht = (props) => {
  const {
    currentYachtData,
    isYachtPrivacyEnabled,
    toggleSwitch,
    yachtImagePreview,
    setYachtImagePreview,
  } = props

  const handleYachtCamera = async () => {
    const img = await pickFromGallery()

    setYachtImagePreview(img.uri)
  }
  return (
    <SettingsYachtWrapper>
      <View
        style={ {
          width: '100%',
        } }
      >
        <View>
          <FormattedText
            fontSize={ 16 }
            color="black"
            style={ { paddingBottom: 5 } }

          >
            Yacht Settings
          </FormattedText>

          <SettingsFieldWrapper>
            <FormattedText fontSize={ 14 } color={ COLORS.BLUE }>
              Yacht Privacy
            </FormattedText>

            <Toggle
              toggleStatsValues={ [ 'private', 'public' ] }
              toggleSwitch={ toggleSwitch }
              toggleState={ isYachtPrivacyEnabled }
            />
          </SettingsFieldWrapper>
        </View>

        <View
          style={ {
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'space-between',
            width: '100%',
          } }
        >
          <SettingsFieldWrapper>
            <FormattedText fontSize={ 14 } color={ COLORS.BLUE }>
              Change Image
            </FormattedText>

            <View>
              <UploadImageButton
                previewImage={ yachtImagePreview }
                action={ handleYachtCamera }
              />
            </View>
          </SettingsFieldWrapper>
        </View>
      </View>
      <View
        style={ {
          flex: 1,
        } }
      />
    </SettingsYachtWrapper>
  )
}

export default SettingsYacht
