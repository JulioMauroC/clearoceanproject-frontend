import React from 'react'
import { View, StyleSheet } from 'react-native'
import JoinYachtForm from 'containers/JoinYachtForm'
import styled from 'styled-components/native'

import * as RootNavigation from 'navigation/RootNavigation'
import LargeButton from './components/LargeButton'

const RegisterLobbyWrapper = styled.View`
  flex: 1;
  justify-content: center;
  height: 100%;
  width: 85%;
  align-self: center;
`

const RegisterLobby = (props) => {
  const { userInvitedRequest, isUserInvited } = props

  React.useEffect(() => {
    userInvitedRequest()
  }, [])

  return (
    <RegisterLobbyWrapper>
      <LargeButton
        isUserAllowed
        header="Create"
        subheader=" a new yacht account"
        backgroundColor="#27256E"
        borderBottomLeftRadius={ 30 }
        action={ () => RootNavigation.navigate('CreateYacht') }
      />
      <LargeButton
        isUserAllowed={ isUserInvited }
        header="Join"
        subheader=" a yacht account"
        backgroundColor="#234F7B"
        borderTopRightRadius={ 30 }
        action={ () => RootNavigation.navigate('JoinYacht') }
      />
    </RegisterLobbyWrapper>
  )
}

export default JoinYachtForm(RegisterLobby)
