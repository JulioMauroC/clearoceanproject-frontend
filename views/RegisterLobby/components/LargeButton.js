import React from 'react'
import { View, Text, TouchableOpacity } from 'react-native'
import { COLORS, FONTS, FONT_SIZES } from 'consts'

const styles = {
  header: {
    color: 'white',
    fontFamily: FONTS.DEFAULT_BLACK,
    fontSize: FONT_SIZES.MIDLARGE,
    textAlign: 'center',
  },
  subheader: {
    color: 'white',
    fontFamily: FONTS.DEFAULT,
    fontSize: FONT_SIZES.MIDLARGE,
    textAlign: 'center',
  },
}

const LargeButton = (props) => {
  const { isUserAllowed } = props

  return (
    <TouchableOpacity
      disabled={ !isUserAllowed }
      style={ {
        marginVertical: 35,
        backgroundColor: isUserAllowed ? props.backgroundColor : 'grey',
        opacity: isUserAllowed ? 1 : 0.65,
        height: 185,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomLeftRadius: props.borderBottomLeftRadius,
        borderTopRightRadius: props.borderTopRightRadius,
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
      } }
      onPress={ props.action }
    >
      <View>
        <Text style={ styles.header }>{props.header}</Text>
        <Text style={ styles.subheader }>{props.subheader}</Text>
      </View>
    </TouchableOpacity>
  )
}

export default LargeButton
