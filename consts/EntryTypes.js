const EntryTypes = [
  { label: 'Fishing eq.', value: 'fishingEquipment' },
  { label: 'Bottles', value: 'bottles' },
  { label: 'Packaging', value: 'packaging' },
  { label: 'Bags', value: 'bags' },
  { label: 'Polystyrene', value: 'polystyrene' },
  { label: 'Rope', value: 'rope' },

  // { label: 'Other', value: 'other' },
]

export default EntryTypes.sort()
