import React from 'react'
import { View, Text } from 'react-native'
import Flag from 'react-native-flags'

const SailCodeItem = (props) => {
  const {
    label, iso, flagSize, emojiCodes,
  } = props
  return (
    <View
      style={ {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
      } }
    >
      <Text style={ { fontSize: 18 } }>{label}</Text>
      {/* <Flag code={ iso } size={ 32 } /> */}
      {emojiCodes && (
        <Text>{String.fromCodePoint(emojiCodes[ 0 ], emojiCodes[ 1 ])}</Text>
      )}
    </View>
  )
}

const SailCodes = [
  // {
  //   label: 'BEL',
  //   value: 'BEL',
  //   component: (
  //     <SailCodeItem label="BEL" iso="BE" emojiCodes={ [ 0x1f1e7, 0x1f1ea ] } />
  //   ),
  //   emojiCodes: [ 0x1f1e7, 0x1f1ea ],
  // },
  // {
  //   label: 'BAH',
  //   value: 'BAH',
  //   component: (
  //     <SailCodeItem label="BAH" iso="BS" emojiCodes={ [ 0x1f1e7, 0x1f1f8 ] } />
  //   ),
  //   emojiCodes: [ 0x1f1e7, 0x1f1f8 ],
  // },
  // {
  //   label: 'BRA',
  //   value: 'BRA',
  //   component: (
  //     <SailCodeItem label="BRA" iso="BR" emojiCodes={ [ 0x1f1e7, 0x1f1f7 ] } />
  //   ),
  //   emojiCodes: [ 0x1f1e7, 0x1f1f7 ],
  // },
  // {
  //   label: 'SRI',
  //   value: 'SRI',
  //   component: (
  //     <SailCodeItem label="SRI" iso="LK" emojiCodes={ [ 0x1f1f1, 0x1f1f0 ] } />
  //   ),
  //   emojiCodes: [ 0x1f1f1, 0x1f1f0 ],
  // },
  // {
  //   label: 'DEN',
  //   value: 'DEN',
  //   component: (
  //     <SailCodeItem label="DEN" iso="DK" emojiCodes={ [ 0x1f1e9, 0x1f1f0 ] } />
  //   ),
  //   emojiCodes: [ 0x1f1e9, 0x1f1f0 ],
  // },
  // {
  //   label: 'ESP',
  //   value: 'ESP',
  //   component: (
  //     <SailCodeItem label="ESP" iso="ES" emojiCodes={ [ 0x1f1ea, 0x1f1f8 ] } />
  //   ),
  //   emojiCodes: [ 0x1f1ea, 0x1f1f8 ],
  // },
  {
    label: 'FRA',
    value: 'FRA',
    component: (
      <SailCodeItem label="FRA" iso="FR" emojiCodes={ [ 0x1f1eb, 0x1f1f7 ] } />
    ),
    emojiCodes: [ 0x1f1eb, 0x1f1f7 ],
  },
  // {
  //   label: 'GER',
  //   value: 'GER',
  //   component: (
  //     <SailCodeItem label="GER" iso="GER" emojiCodes={ [ 0x1f1e9, 0x1f1ea ] } />
  //   ),
  //   emojiCodes: [ 0x1f1e9, 0x1f1ea ],
  // },
  // {
  //   label: 'GDR',
  //   value: 'GDR',
  //   component: (
  //     <SailCodeItem label="GDR" iso="GDR" emojiCodes={ [ 0x1f1e9, 0x1f1ea ] } />
  //   ),
  //   emojiCodes: [ 0x1f1e9, 0x1f1ea ],
  // },
  // {
  //   label: 'GRE',
  //   value: 'GRE',
  //   component: (
  //     <SailCodeItem label="GRE" iso="GRE" emojiCodes={ [ 0x1f1ec, 0x1f1f7 ] } />
  //   ),
  //   emojiCodes: [ 0x1f1ec, 0x1f1f7 ],
  // },
  // {
  //   label: 'NED',
  //   value: 'NED',
  //   component: (
  //     <SailCodeItem label="NED" iso="NED" emojiCodes={ [ 0x1f1f3, 0x1f1f1 ] } />
  //   ),
  //   emojiCodes: [ 0x1f1f3, 0x1f1f1 ],
  // },
  {
    label: 'ITA',
    value: 'ITA',
    component: (
      <SailCodeItem label="ITA" iso="ITA" emojiCodes={ [ 0x1f1ee, 0x1f1f9 ] } />
    ),
    emojiCodes: [ 0x1f1ee, 0x1f1f9 ],
  },
  // {
  //   label: 'IRL',
  //   value: 'IRL',
  //   component: (
  //     <SailCodeItem label="IRL" iso="IRL" emojiCodes={ [ 0x1f1ee, 0x1f1ea ] } />
  //   ),
  //   emojiCodes: [ 0x1f1ee, 0x1f1ea ],
  // },
  {
    label: 'UK',
    value: 'UK',
    component: (
      <SailCodeItem label="UK" iso="UK" emojiCodes={ [ 0x1f3f4, 0xe0067 ] } />
    ),
    emojiCodes: [ 0x1f3f4, 0xe0067 ],
  },
  // {
  //   label: 'AUS',
  //   value: 'AUS',
  //   component: (
  //     <SailCodeItem label="AUS" iso="AUS" emojiCodes={ [ 0x1f1e6, 0x1f1fa ] } />
  //   ),
  //   emojiCodes: [ 0x1f1e6, 0x1f1fa ],
  // },
  {
    label: 'BER',
    value: 'BER',
    component: (
      <SailCodeItem label="BER" iso="BER" emojiCodes={ [ 0x1f1e7, 0x1f1f2 ] } />
    ),
    emojiCodes: [ 0x1f1e7, 0x1f1f2 ],
  },
  {
    label: 'CAN',
    value: 'CAN',
    component: (
      <SailCodeItem label="CAN" iso="CAN" emojiCodes={ [ 0x1f1e8, 0x1f1e6 ] } />
    ),
    emojiCodes: [ 0x1f1e8, 0x1f1e6 ],
  },
  {
    label: 'HKG',
    value: 'HKG',
    component: (
      <SailCodeItem label="HKG" iso="HKG" emojiCodes={ [ 0x1f1ed, 0x1f1f0 ] } />
    ),
    emojiCodes: [ 0x1f1ed, 0x1f1f0 ],
  },
  {
    label: 'JAM',
    value: 'JAM',
    component: (
      <SailCodeItem label="JAM" iso="JAM" emojiCodes={ [ 0x1f1ef, 0x1f1f2 ] } />
    ),
    emojiCodes: [ 0x1f1ef, 0x1f1f2 ],
  },
  {
    label: 'KEN',
    value: 'KEN',
    component: (
      <SailCodeItem label="KEN" iso="KEN" emojiCodes={ [ 0x1f1f0, 0x1f1ea ] } />
    ),
    emojiCodes: [ 0x1f1f0, 0x1f1ea ],
  },
  {
    label: 'SIN',
    value: 'SIN',
    component: (
      <SailCodeItem label="SIN" iso="SIN" emojiCodes={ [ 0x1f1f8, 0x1f1ec ] } />
    ),
    emojiCodes: [ 0x1f1f8, 0x1f1ec ],
  },
  {
    label: 'NZL',
    value: 'NZL',
    component: (
      <SailCodeItem label="NZL" iso="NZL" emojiCodes={ [ 0x1f1f3, 0x1f1ff ] } />
    ),
    emojiCodes: [ 0x1f1f3, 0x1f1ff ],
  },
  {
    label: 'RSA',
    value: 'RSA',
    component: (
      <SailCodeItem label="RSA" iso="RSA" emojiCodes={ [ 0x1f1ff, 0x1f1e6 ] } />
    ),
    emojiCodes: [ 0x1f1ff, 0x1f1e6 ],
  },
  {
    label: 'FIN',
    value: 'FIN',
    component: (
      <SailCodeItem label="FIN" iso="FIN" emojiCodes={ [ 0x1f1eb, 0x1f1ee ] } />
    ),
    emojiCodes: [ 0x1f1eb, 0x1f1ee ],
  },
  {
    label: 'HUN',
    value: 'HUN',
    component: (
      <SailCodeItem label="FRA" iso="FR" emojiCodes={ [ 0x1f1eb, 0x1f1f7 ] } />
    ),
    emojiCodes: [ 0x1f1ed, 0x1f1fa ],
  },
  {
    label: 'MEX',
    value: 'MEX',
    component: (
      <SailCodeItem label="MEX" iso="MEX" emojiCodes={ [ 0x1f1f2, 0x1f1fd ] } />
    ),
    emojiCodes: [ 0x1f1f2, 0x1f1fd ],
  },
  {
    label: 'NOR',
    value: 'NOR',
    component: (
      <SailCodeItem label="NOR" iso="NOR" emojiCodes={ [ 0x1f1f3, 0x1f1f4 ] } />
    ),
    emojiCodes: [ 0x1f1f3, 0x1f1f4 ],
  },
  {
    label: 'AUT',
    value: 'AUT',
    component: (
      <SailCodeItem label="AUT" iso="AUT" emojiCodes={ [ 0x1f1e6, 0x1f1f9 ] } />
    ),
    emojiCodes: [ 0x1f1e6, 0x1f1f9 ],
  },
  {
    label: 'POR',
    value: 'POR',
    component: (
      <SailCodeItem label="POR" iso="POR" emojiCodes={ [ 0x1f1f5, 0x1f1f9 ] } />
    ),
    emojiCodes: [ 0x1f1f5, 0x1f1f9 ],
  },
  {
    label: 'PHI',
    value: 'PHI',
    component: (
      <SailCodeItem label="PHI" iso="PHI" emojiCodes={ [ 0x1f1f5, 0x1f1ed ] } />
    ),
    emojiCodes: [ 0x1f1f5, 0x1f1ed ],
  },
  {
    label: 'PUR',
    value: 'PUR',
    component: (
      <SailCodeItem label="PUR" iso="PUR" emojiCodes={ [ 0x1f1f5, 0x1f1f7 ] } />
    ),
    emojiCodes: [ 0x1f1f5, 0x1f1f7 ],
  },
  {
    label: 'PER',
    value: 'PER',
    component: (
      <SailCodeItem label="PER" iso="PER" emojiCodes={ [ 0x1f1f5, 0x1f1ea ] } />
    ),
    emojiCodes: [ 0x1f1f5, 0x1f1ea ],
  },
  {
    label: 'POL',
    value: 'POL',
    component: (
      <SailCodeItem label="POL" iso="POL" emojiCodes={ [ 0x1f1f5, 0x1f1f1 ] } />
    ),
    emojiCodes: [ 0x1f1f5, 0x1f1f1 ],
  },
  {
    label: 'INA',
    value: 'INA',
    component: (
      <SailCodeItem label="INA" iso="INA" emojiCodes={ [ 0x1f1ee, 0x1f1e9 ] } />
    ),
    emojiCodes: [ 0x1f1ee, 0x1f1e9 ],
  },
  {
    label: 'SWE',
    value: 'SWE',
    component: (
      <SailCodeItem label="SWE" iso="SWE" emojiCodes={ [ 0x1f1f8, 0x1f1ea ] } />
    ),
    emojiCodes: [ 0x1f1f8, 0x1f1ea ],
  },
  {
    label: 'ESA',
    value: 'ESA',
    component: (
      <SailCodeItem label="ESA" iso="ESA" emojiCodes={ [ 0x1f1f8, 0x1f1fb ] } />
    ),
    emojiCodes: [ 0x1f1f8, 0x1f1fb ],
  },
  {
    label: 'URS',
    value: 'URS',
    component: (
      <SailCodeItem label="URS" iso="URS" emojiCodes={ [ 0x1f1f7, 0x1f1fa ] } />
    ),
    emojiCodes: [ 0x1f1f7, 0x1f1fa ],
  },
  {
    label: 'THA',
    value: 'THA',
    component: (
      <SailCodeItem label="THA" iso="THA" emojiCodes={ [ 0x1f1f9, 0x1f1ed ] } />
    ),
    emojiCodes: [ 0x1f1f9, 0x1f1ed ],
  },
  {
    label: 'USA',
    value: 'USA',
    component: (
      <SailCodeItem label="USA" iso="USA" emojiCodes={ [ 0x1f1fa, 0x1f1f8 ] } />
    ),
    emojiCodes: [ 0x1f1fa, 0x1f1f8 ],
  },
  {
    label: 'VEN',
    value: 'VEN',
    component: (
      <SailCodeItem label="VEN" iso="VEN" emojiCodes={ [ 0x1f1fb, 0x1f1ea ] } />
    ),
    emojiCodes: [ 0x1f1fb, 0x1f1ea ],
  },
  {
    label: 'ISV',
    value: 'ISV',
    component: (
      <SailCodeItem label="ISV" iso="ISV" emojiCodes={ [ 0x1f1fb, 0x1f1ec ] } />
    ),
    emojiCodes: [ 0x1f1fb, 0x1f1ec ],
  },
  {
    label: 'CHI',
    value: 'CHI',
    component: (
      <SailCodeItem label="CHI" iso="CHI" emojiCodes={ [ 0x1f1e8, 0x1f1f3 ] } />
    ),
    emojiCodes: [ 0x1f1e8, 0x1f1f3 ],
  },
]

const arrangedArray = SailCodes.sort((a, b) => a.label.localeCompare(b.label))
arrangedArray.unshift({ label: 'Select a Flag', value: 'Select a Flag' })

export default arrangedArray
