import { Dimensions } from 'react-native'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'
import getEnvVars from '../config/environment'

const { width, height } = Dimensions.get('screen')

export const SPLASH_TIMEOUT = 1500

export const FONTS = {
  DEFAULT: 'Lato-Regular',
  DEFAULT_LIGHT: 'Lato-Light',
  DEFAULT_BOLD: 'Lato-Bold',
  DEFAULT_BLACK: 'Lato-Black',
}

export const FONT_SIZES = {
  MAXLARGE: 40,
  SUPERLARGE: 35,
  EXTRALARGE: 32,
  LARGE: 28,
  MIDLARGE: 24,
  SEMILARGE: 22,
  TITLE: 20,
  HEADER: 18,
  ITEMHEADER: 17,
  MEDIUM: 16,
  NORMAL: 15,
  MIDREGULAR: 14,
  SMALLMEDIUM: 13,
  SMALL: 12,
  MIDSMALL: 10.5,
  SEMISMALL: 9,
  TINY: 7,
}

export const COLORS = {
  WHITE: '#FFFFFF',
  LIGHT_GRAY: '#fbfbfb',
  MID_GRAY: '#d9d9d9',
  DARK_GRAY: '#abb3c0',
  DARKEST_GRAY: '#5b5b5b',
  PURPLE: '#353f8f',
  GREEN: '#00ffab',
  MID_GREEN: '#00CC88',
  DARK_GREEN: '#09b57c',
  RED: '#d95b5d',
  LIGHT_BLUE: '#497e93',
  BLUE: '#0e427f',
}

export const DUMMY = {
  YACHT_IMG:
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRNukWmwdpmWhTJCcJTGqxiEtbCYaEnYojXqA&usqp=CAU',
  USER_IMG:
    'https://p7.hiclipart.com/preview/1016/429/148/computer-icons-download-avatar-avatar.jpg',
}

export const DIMENSIONS = {
  PROFILE_HEADER_DISTANCE: width * 0.35,
  USERS_LIST: {
    USER_ITEM_HEIGHT: 65,
    USER_AVATAR_SIZE: heightToDp('6.5%'),
    SPACING: heightToDp('1.6%'),
  },
}

export const { API_URL } = getEnvVars()
