const YachtPositions = [
  { label: 'Captain', value: 'Captain' },
  { label: 'First Officer', value: 'First Officer' },
  { label: 'Second Officer', value: 'Second Officer' },
  { label: 'Chief Engineer', value: 'Chief Engineer' },
  { label: 'Second Engineer', value: 'Second Engineer' },
  { label: 'Third Engineer', value: 'Third Engineer' },
  { label: 'ETO', value: 'ETO' },
  { label: 'AV IT Engineer', value: 'AV IT Engineer' },
  { label: 'Electrician', value: 'Electrician' },
  { label: 'Bosun', value: 'Bosun' },
  { label: 'Deckhand', value: 'Deckhand' },
  { label: 'Purser', value: 'Purser' },
  { label: 'Chief Stewardess', value: 'Chief Stewardess' },
  { label: 'Stewardess', value: 'Stewardess' },
  { label: 'Head Chef', value: 'Head Chef' },
  { label: 'Second Chef', value: 'Second Chef' },
  { label: 'Crew Chef', value: 'Crew Chef' },

]

// const arrangedArray = YachtPositions.sort((a, b) => a.label.localeCompare(b.label))
YachtPositions.unshift({ label: 'Select your role', value: 'Select your role' })
export default YachtPositions
