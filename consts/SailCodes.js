import React from 'react'
import { View, Text } from 'react-native'
import Flag from 'react-native-flags'

const SailCodeItem = (props) => {
  const {
    label, iso, flagSize, emojiCodes,
  } = props
  return (
    <View
      style={ {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
      } }
    >
      <Text style={ { fontSize: 18 } }>{label}</Text>
      {/* <Flag code={ iso } size={ 32 } /> */}
      {emojiCodes && (
        <Text>{String.fromCodePoint(emojiCodes[ 0 ], emojiCodes[ 1 ])}</Text>
      )}
    </View>
  )
}

const SailCodes = [
  {
    label: 'FRANCE',
    value: 'FRANCE',
  },
  {
    label: 'Italy',
    value: 'Italy',
  },
  {
    label: 'UK',
    value: 'UK',
  },
  {
    label: 'Malta',
    value: 'Malta',
  },
  {
    label: 'Madeira',
    value: 'Madeira',
  },
  {
    label: 'Cyprus',
    value: 'Cyprus',
  },
  {
    label: 'Monaco',
    value: 'Monaco',
  },
  {
    label: 'Isle of Man',
    value: 'Isle of Man',
  },
  {
    label: 'BVI',
    value: 'BVI',
  },
  {
    label: 'Cayman Island',
    value: 'Cayman Island',
  },
  {
    label: 'Hong Kong',
    value: 'Hong Kong',
  },
  {
    label: 'Marshall Island',
    value: 'Marshall Island',
  },
  {
    label: 'Panama',
    value: 'Panama',
  },
  {
    label: 'St Vincent',
    value: 'St Vincent',
  },
  {
    label: 'Jersey',
    value: 'Jersey',
  },
  {
    label: 'Guernsey',
    value: 'Guernsey',
  },
]

const arrangedArray = SailCodes.sort((a, b) => a.label.localeCompare(b.label))
arrangedArray.unshift({ label: 'Select a Flag', value: 'Select a Flag' })

export default arrangedArray
