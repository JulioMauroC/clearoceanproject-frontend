import { all, fork } from 'redux-saga/effects'
// import loginSaga from '../containers/Login/sagas'
import registerSaga from 'containers/Register/sagas'
import loginSaga from 'containers/Login/sagas'
import createYachtSaga from 'containers/CreateYachtForm/sagas'
import createAdminSaga from 'containers/CreateAdminForm/sagas'
import joinYachtSaga from 'containers/JoinYachtForm/sagas'
import usersSaga from 'containers/Users/sagas'
import yachtsSaga from 'containers/Yachts/sagas'
import entriesSagas from 'containers/Entries/sagas'
import resetPasswordSagas from 'containers/ResetPassword/sagas'

export default function* rootSaga() {
  yield all([
    fork(registerSaga),
    fork(loginSaga),
    fork(createYachtSaga),
    fork(createAdminSaga),
    fork(joinYachtSaga),
    fork(usersSaga),
    fork(yachtsSaga),
    fork(entriesSagas),
    fork(resetPasswordSagas),
  ])
}
