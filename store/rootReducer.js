import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'

import { REDUCER_NAME as registerReducerName } from 'containers/Register/consts'
import registerReducer from 'containers/Register/reducer'

import { REDUCER_NAME as loginReducerName } from 'containers/Login/consts'
import loginReducer from 'containers/Login/reducer'

import { REDUCER_NAME as joinYachtName } from 'containers/JoinYachtForm/consts'
import joinYachtReducer from 'containers/JoinYachtForm/reducer'

import { REDUCER_NAME as createYachtName } from 'containers/CreateYachtForm/consts'
import createYachtReducer from 'containers/CreateYachtForm/reducer'

import { REDUCER_NAME as createAdminName } from 'containers/CreateAdminForm/consts'
import createAdminReducer from 'containers/CreateAdminForm/reducer'

import { REDUCER_NAME as usersReducerName } from 'containers/Users/consts'
import usersReducer from 'containers/Users/reducer'

import { REDUCER_NAME as yachtsReducerName } from 'containers/Yachts/consts'
import yachtsReducer from 'containers/Yachts/reducer'

import { REDUCER_NAME as entriesReducerName } from 'containers/Entries/consts'
import entriesReducer from 'containers/Entries/reducer'

import { REDUCER_NAME as resetPasswordReducerName } from 'containers/ResetPassword/consts'
import resetPasswordReducer from 'containers/ResetPassword/reducer'

const rootReducer = combineReducers({
  form: formReducer,
  [ loginReducerName ]: loginReducer,
  [ registerReducerName ]: registerReducer,
  [ joinYachtName ]: joinYachtReducer,
  [ createYachtName ]: createYachtReducer,
  [ createAdminName ]: createAdminReducer,
  [ usersReducerName ]: usersReducer,
  [ yachtsReducerName ]: yachtsReducer,
  [ entriesReducerName ]: entriesReducer,
  [ resetPasswordReducerName ]: resetPasswordReducer,
})

export default rootReducer
