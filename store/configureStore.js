import { reducer as formReducer } from 'redux-form'
import {
  combineReducers, createStore, applyMiddleware, compose,
} from 'redux'
import createSagaMiddleware from 'redux-saga'
import navigationDebouncer from 'react-navigation-redux-debouncer'
import rootSaga from './rootSaga'
import rootReducer from './rootReducer'

const sagaMiddleware = createSagaMiddleware()

export default function configureStore(initialState = {}) {
  const middlewares = [
    sagaMiddleware,
    navigationDebouncer(500),
  ]

  const enhancers = [ applyMiddleware(...middlewares) ]

  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

  const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(...enhancers),
  )

  // store.runSaga = sagaMiddleware.run
  sagaMiddleware.run(rootSaga)

  return store
}
