import 'react-native-gesture-handler'
import React from 'react'
import * as Font from 'expo-font'
import {
  View, Text, Image, StatusBar, LogBox,
} from 'react-native'
import { Provider } from 'react-redux'
import { PortalProvider, WhitePortal } from 'react-native-portal'
import * as SplashScreen from 'expo-splash-screen'
import { enableScreens } from 'react-native-screens'
import { ThemeProvider } from 'styled-components'
import Theme from 'components/Theme'
import store from 'store/store'

import AppNavigator from 'navigation/AppNavigator'
import { Asset } from 'expo-asset'

import AppView from './components/AppView'

enableScreens()

class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      fontsLoaded: false,
      canAppRun: false,
    }
  }

  async componentDidMount() {
    await SplashScreen.preventAutoHideAsync()

    await Font.loadAsync({
      'Lato-Regular': require('./assets/fonts/Lato/Lato-Regular.ttf'),
      'Lato-Light': require('./assets/fonts/Lato/Lato-Light.ttf'),
      'Lato-Bold': require('./assets/fonts/Lato/Lato-Bold.ttf'),
      'Lato-Black': require('./assets/fonts/Lato/Lato-Black.ttf'),
    })

    // const gif = require('./assets/images/splash.gif');
    const logo = require('./assets/logos/logo.png')
    const splashImage = require('./assets/images/new-splash.png')

    // await Asset.fromModule(gif).downloadAsync();
    await Asset.fromModule(logo).downloadAsync()
    await Asset.fromModule(splashImage).downloadAsync()

    this.setState(
      {
        fontsLoaded: true,
      },
      async () => {
        await SplashScreen.hideAsync() // hide native splash
      },
    )

    setTimeout(() => {
      this.setState({ canAppRun: true })
    }, 2000)

    await LogBox.ignoreAllLogs()
  }

  render() {
    if (!this.state.canAppRun) {
      // LogBox.ignoreAllLogs()
      return (
        <View
          style={ { flex: 1, justifyContent: 'flex-end', alignItems: 'center' } }
        >
          <Image
            source={ require('./assets/images/new-splash.png') }
            onLoad={ this._cacheResourcesAsync }
            resizeMode="contain"
            style={ { height: '90%' } }
          />
        </View>
      )
    }

    return (
      // <PortalProvider>
      <Provider store={ store }>
        <ThemeProvider theme={ Theme }>
          <AppView>
            <AppNavigator />
          </AppView>
        </ThemeProvider>

        {/* <WhitePortal name="portal" /> */}
      </Provider>
      // </PortalProvider>
    )
  }
}

export default App
