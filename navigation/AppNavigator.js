import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import NavHeaderLogo from './components/NavHeaderLogo'

import { navigationRef } from './RootNavigation'
import AuthStack from './Auth'
import MainTab from './MainTab'
import LoadingStack from './Loading'

const Stack = createStackNavigator()

const AppNavigator = (props) => {
  const { onNavChange } = props

  const [ isWithSettings, checkIsWithSettings ] = React.useState(false)

  return (
    <NavigationContainer
      ref={ navigationRef }
      onNavigationStateChange={ onNavChange }
      onReady={ () => {
        const options = navigationRef.current?.getCurrentOptions()
        checkIsWithSettings(options.withSettings)
      } }
      onStateChange={ async () => {
        const options = navigationRef.current?.getCurrentOptions()
        checkIsWithSettings(options.withSettings)
      } }
    >
      <Stack.Navigator
        initialRouteName="Auth"
        screenOptions={ {
          headerTitle: () => <NavHeaderLogo isWithSettings={ isWithSettings } />,
          headerStyle: { height: 65 },
        } }
        options={ { headerLeft: null } }
        headerMode="float"
      >
        <Stack.Screen name="Auth" component={ AuthStack } />
        <Stack.Screen
          name="MainTab"
          options={ { headerLeft: null, gesturesEnabled: false } }
          component={ MainTab }
        />
        <Stack.Screen name="Loading" component={ LoadingStack } />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default AppNavigator
