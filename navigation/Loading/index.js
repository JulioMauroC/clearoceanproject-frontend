import React from 'react'

import { createStackNavigator } from '@react-navigation/stack'

import LoadingView from 'views/Loading'

const Stack = createStackNavigator()

const LoadingStack = () => (
  <Stack.Navigator initialRouteName="Loading" headerMode="none">
    <Stack.Screen name="Loading" component={ LoadingView } />
  </Stack.Navigator>
)

export default LoadingStack
