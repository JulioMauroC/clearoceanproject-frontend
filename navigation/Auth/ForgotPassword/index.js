import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import ConfirmEmailView from 'views/ForgotPassword/views/ConfirmEmail'
import EnterCodeView from 'views/ForgotPassword/views/EnterCode'
import ResetPasswordView from 'views/ForgotPassword/views/ResetPassword'

const Stack = createStackNavigator()

const ForgotPasswordStack = () => (
  <Stack.Navigator initialRouteName="ConfirmEmail" headerMode="none">
    <Stack.Screen name="ConfirmEmail" component={ ConfirmEmailView } />
    <Stack.Screen name="EnterCode" component={ EnterCodeView } />
    <Stack.Screen name="ResetPassword" component={ ResetPasswordView } />
  </Stack.Navigator>
)

export default ForgotPasswordStack
