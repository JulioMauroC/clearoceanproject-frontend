import React from 'react'

import {
  createStackNavigator,
  TransitionPresets,
} from '@react-navigation/stack'

import AuthView from 'views/Auth'
import RegisterStack from './Register'
import ForgotPasswordStack from './ForgotPassword'
import TermsStack from './Terms'
// import LoginStack from './LoginStack'

const Stack = createStackNavigator()

const AuthStack = () => (
  <Stack.Navigator initialRouteName="Auth" headerMode="none">
    <Stack.Screen name="Auth" component={ AuthView } />
    <Stack.Screen name="Register" component={ RegisterStack } />
    <Stack.Screen name="ForgotPassword" component={ ForgotPasswordStack } />
    <Stack.Screen
      options={ {
        ...TransitionPresets.ModalSlideFromBottomIOS,
      } }
      name="Terms"
      component={ TermsStack }
    />
    {/* <Stack.Screen name="Login" component={ LoginStack } /> */}
  </Stack.Navigator>
)

export default AuthStack
