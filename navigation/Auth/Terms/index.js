import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import TermsOfUseView from 'views/Terms/views/TermsOfUse'
import PrivacyPolicyView from 'views/Terms/views/PrivacyPolicy'
import DisclaimerView from 'views/Terms/views/Disclaimer'

const Stack = createStackNavigator()

const TermsStack = () => (
  <Stack.Navigator
    initialRouteName="Terms of Use"
    screenOptions={ {
      headerBackTitleVisible: false,
    } }
  >
    <Stack.Screen name="Terms of Use" component={ TermsOfUseView } />
    <Stack.Screen name="Privacy Policy" component={ PrivacyPolicyView } />
    <Stack.Screen name="Disclaimer" component={ DisclaimerView } />
  </Stack.Navigator>
)

export default TermsStack
