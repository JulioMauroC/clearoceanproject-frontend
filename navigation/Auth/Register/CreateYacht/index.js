import React from 'react'

import { createStackNavigator } from '@react-navigation/stack'

// import CreateYachtStep1 from 'views/CreateYachtForm/Steps/Step1'
// import CreateYachtStep2 from 'views/CreateYachtForm/Steps/Step2'
// import CreateYachtStep3 from 'views/CreateYachtForm/Steps/Step3'

import CreateYachtFormView from 'views/CreateYachtForm'

const Stack = createStackNavigator()

const CreateYachtStack = () => (
  <Stack.Navigator initialRouteName="Create Yacht Form" headerMode="none">
    <Stack.Screen name="Create Yacht Form" component={ CreateYachtFormView } />
    {/* <Stack.Screen name="Create Yacht Step 2" component={ CreateYachtStep2 } />
    <Stack.Screen name="Create Yacht Step 3" component={ CreateYachtStep3 } /> */}
  </Stack.Navigator>
)

export default CreateYachtStack
