import React from 'react'

import { createStackNavigator } from '@react-navigation/stack'

import JoinYachtFormView from 'views/JoinYachtForm'
// import JoinYachtStep1 from 'views/JoinYachtForm/Steps/Step1'
// import JoinYachtStep2 from 'views/JoinYachtForm/Steps/Step2'
// import JoinYachtStep3 from 'views/JoinYachtForm/Steps/Step3'

const Stack = createStackNavigator()

const JoinYachtStack = () => (
  <Stack.Navigator initialRouteName="JoinYachtForm" headerMode="none">
    <Stack.Screen name="JoinYachtForm" component={ JoinYachtFormView } />
    {/* <Stack.Screen name="Join Yacht Step 1" component={ JoinYachtStep1 } />
    <Stack.Screen name="Join Yacht Step 2" component={ JoinYachtStep2 } />
    <Stack.Screen name="Join Yacht Step 3" component={ JoinYachtStep3 } /> */}
  </Stack.Navigator>
)

export default JoinYachtStack
