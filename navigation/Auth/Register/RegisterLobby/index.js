import React from 'react'

import { createStackNavigator } from '@react-navigation/stack'

import RegisterLobbyView from 'views/RegisterLobby/'

const Stack = createStackNavigator()

const RegisterLobbyStack = () => (
  <Stack.Navigator initialRouteName="RegisterLobby" headerMode="none">
    <Stack.Screen name="RegisterLobby" component={ RegisterLobbyView } />
  </Stack.Navigator>
)

export default RegisterLobbyStack
