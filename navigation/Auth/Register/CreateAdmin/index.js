import React from 'react'
// import CreateAdminStep1 from 'views/CreateAdminForm/Steps/Step1'
// import CreateAdminStep2 from 'views/CreateAdminForm/Steps/Step2'
import CreateAdminView from 'views/CreateAdminForm'
import { createStackNavigator } from '@react-navigation/stack'

const Stack = createStackNavigator()

const CreateAdminStack = () => (
  <Stack.Navigator initialRouteName="Create Admin Form" headerMode="none">
    <Stack.Screen name="Create Admin" component={ CreateAdminView } />
    {/* <Stack.Screen name="Create Admin Step 1" component={ CreateAdminStep1 } />
    <Stack.Screen name="Create Admin Step 2" component={ CreateAdminStep2 } /> */}
  </Stack.Navigator>
)

export default CreateAdminStack
