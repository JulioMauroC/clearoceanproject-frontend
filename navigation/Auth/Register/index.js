import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import CreateAdminStack from './CreateAdmin'
import CreateYachtStack from './CreateYacht'
import JoinYachtStack from './JoinYacht'
import RegisterLobbyStack from './RegisterLobby'

const Stack = createStackNavigator()

const RegisterStack = () => (
  <Stack.Navigator initialRouteName="RegisterLobby" headerMode="none">
    <Stack.Screen name="RegisterLobby" component={ RegisterLobbyStack } />
    <Stack.Screen name="CreateYacht" component={ CreateYachtStack } />
    <Stack.Screen name="CreateAdmin" component={ CreateAdminStack } />
    <Stack.Screen name="JoinYacht" component={ JoinYachtStack } />
  </Stack.Navigator>
)

export default RegisterStack
