import React from 'react'
import * as RootNavigation from 'navigation/RootNavigation'
import { View, TouchableOpacity } from 'react-native'
import { COLORS } from 'consts'
import { useSelector } from 'react-redux'
import { selectIsEmailVerified } from 'containers/Users/selectors'
import styled from 'styled-components/native'
import Icon from 'components/Icon'
import getNavigationCurrentRoute from 'utils/getNavigationCurrentRoute'

const NavBottomTabsWrapper = styled.View`
  height: ${ (props) => props.theme.dimensions.navBottomHeight };
  background-color: ${ (props) => props.theme.colors.logoGreen };
  justify-content: space-evenly;
  align-items: center;
`

const LabelText = styled.Text`
  font-family: 'Lato-Regular';
  font-size: ${ (props) => (props.isMidIndex ? 12 : 12) };
  text-align: center;
  padding-top: ${ (props) => (props.isMidIndex ? 0 : 2) }px;
`

const NavBottomTabs = (props) => {
  const { state, descriptors } = props
  const IsEmailVerified = useSelector(selectIsEmailVerified)
  const currentRoute = getNavigationCurrentRoute(state.routes)

  return (
    <NavBottomTabsWrapper style={ { flexDirection: 'row' } }>
      {state.routes.map((route, index) => {
        const { options } = descriptors[ route.key ]

        const isMidIndex = index === Math.floor(state.routes.length / 2)

        const isFocused = options.name === currentRoute

        const color = options.color ? options.color : 'white'

        return (
          <TouchableOpacity
            key={ route.key }
            accessibilityRole="button"
            accessibilityState={ isFocused ? { selected: true } : {} }
            accessibilityLabel={ options.tabBarAccessibilityLabel }
            testID={ options.tabBarTestID }
            onPress={
              IsEmailVerified
                ? () => RootNavigation.navigate(options.name)
                : () => alert('please confirm your email first')
            }
            style={ {
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
            } }
          >
            <View
              style={ [
                {
                  alignItems: 'center',
                  justifyContent: 'center',
                },
                isMidIndex && {
                  width: options.size + options.size / 2,
                  height: options.size + options.size / 2,
                  transform: [ { translateY: -2 } ],
                  alignItems: 'center',
                },
              ] }
            >
              <Icon
                size={ options.size }
                icon={ options.icon }
                color="white"
              />
              {!isMidIndex && (
                <LabelText
                  isMidIndex={ isMidIndex }
                  style={ { color: 'white' } }
                >
                  {options.label}
                </LabelText>
              )}
            </View>
          </TouchableOpacity>
        )
      })}
    </NavBottomTabsWrapper>
  )
}

export default NavBottomTabs
