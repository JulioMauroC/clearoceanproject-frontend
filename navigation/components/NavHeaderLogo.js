import React from 'react'
import {
  View, Image, Dimensions, TouchableOpacity,
} from 'react-native'
import { useNavigation } from '@react-navigation/native'
import { useSelector } from 'react-redux'
import { selectIsEmailVerified } from 'containers/Users/selectors'
import Logo from 'assets/logos/logo.png'
import Icon from 'components/Icon'

const { width } = Dimensions.get('screen')

function NavHeaderLogo(props) {
  const navigation = useNavigation()
  const { isWithSettings } = props

  const IsEmailVerified = useSelector(selectIsEmailVerified)

  return (
    <View
      style={ {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: width * 0.9,
        // height: '100%',
      } }
    >
      <Image source={ Logo } resizeMode="contain" style={ { width: '50%' } } />

      {isWithSettings && (
        <TouchableOpacity onPress={ IsEmailVerified ? () => navigation.navigate('Settings') : () => alert('please confirm your email first') }>
          <Icon icon="cog" size={ 17 } color="grey" />
        </TouchableOpacity>
      )}
    </View>
  )
}

export default NavHeaderLogo
