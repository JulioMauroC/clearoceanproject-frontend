import React from 'react'

import { View, Text, TouchableOpacity } from 'react-native'
import { COLORS } from 'consts'
import styled from 'styled-components/native'
import Icon from 'components/Icon'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

const NavBottomTabsWrapper = styled.View`
  height: ${ (props) => props.theme.dimensions.navBottomHeight };
  background-color: white;
  justify-content: space-evenly;
  align-items: center;
  /* border: 2px solid red; */
  border-top-left-radius: ${ widthToDp('5%') };
  border-top-right-radius: ${ widthToDp('5%') };
`

const LabelText = styled.Text`
  font-family: 'Lato-Light';
  font-size: ${ (props) => (props.isMidIndex ? 10 : 10) };
  text-align: center;
  padding-top: ${ (props) => (props.isMidIndex ? 0 : 2) }px;
`

const NavBottomTabs = (props) => {
  const { state, descriptors, navigation } = props

  return (
    <NavBottomTabsWrapper style={ { flexDirection: 'row' } }>
      {state.routes.map((route, index) => {
        const { options } = descriptors[ route.key ]

        const isMidIndex = index === Math.floor(state.routes.length / 2)

        const isFocused = state.index === index

        const color = options.color ? options.color : 'grey'

        // const midIndex = (Math.floor(state.routes.length / 2))

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
          })

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name)
          }
        }

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          })
        }

        return (
          <TouchableOpacity
            key={ route.key }
            accessibilityRole="button"
            accessibilityState={ isFocused ? { selected: true } : {} }
            accessibilityLabel={ options.tabBarAccessibilityLabel }
            testID={ options.tabBarTestID }
            onPress={ onPress }
            onLongPress={ onLongPress }
            style={ {
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              alignSelf: 'center',
            } }
          >
            <View
              style={ [
                {
                  alignItems: 'center',
                  justifyContent: 'center',
                },
                isMidIndex && {
                  width: options.size + options.size / 2,
                  height: options.size + options.size / 2,
                  backgroundColor: COLORS.PURPLE,
                  borderRadius: options.size / 2 + options.size / 3,
                  transform: [ { translateY: -(options.size / 2) } ],
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                },
              ] }
            >
              <Icon
                size={ options.size }
                icon={ options.icon }
                color={ isFocused && !isMidIndex ? COLORS.BLUE : color }
              />
              <LabelText
                isMidIndex={ isMidIndex }
                style={ { color: isFocused ? COLORS.BLUE : 'grey' } }
              >
                {options.label}
              </LabelText>
            </View>
          </TouchableOpacity>
        )
      })}
    </NavBottomTabsWrapper>
  )
}

export default NavBottomTabs
