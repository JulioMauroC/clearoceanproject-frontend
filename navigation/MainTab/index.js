import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import NavBottomTabs from 'navigation/components/NavBottomTabs'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'
import DashboardStack from './Dashboard'

import EntriesStack from './Dashboard/Entries'
import UsersStack from './Dashboard/Users'
import ResultsStack from './Dashboard/Results'
import UploadStack from './Dashboard/Upload'

const Tab = createBottomTabNavigator()

const MainTab = () => (
  <Tab.Navigator
    // options={ { headerLeft: null } }
    navigationOptions={ {
      title: 'Title',
      headerLeft: null,
      gesturesEnabled: false,
    } }
    tabBar={ ({ ...props }) => <NavBottomTabs { ...props } /> }
    initialRouteName="Dashboard"
  >
    <Tab.Screen
      name="Dashboard"
      options={ {
        icon: 'home',
        size: heightToDp('3.5%'),
        name: 'Dashboard',
        label: `My${ '\n' }Dashboard`,
      } }
      component={ DashboardStack }
    />
    <Tab.Screen
      name="Users"
      options={ {
        icon: 'users',
        size: heightToDp('3.5%'),
        name: 'Users',
        label: `Yacht${ '\n' }Users`,
      } }
      component={ UsersStack }
    />
    <Tab.Screen
      name="Upload"
      options={ {
        icon: 'camera',
        size: heightToDp('6%'),
        color: 'white',
        label: 'Upload',
        name: 'Upload',
      } }
      component={ UploadStack }
    />
    <Tab.Screen
      name="Entries"
      options={ {
        icon: 'water-lower',
        name: 'Entries',
        size: heightToDp('3.5%'),
        label: `Yacht${ '\n' }Entries`,
      } }
      component={ EntriesStack }
    />
    <Tab.Screen
      name="Result"
      options={ {
        icon: 'globe-europe',
        name: 'Result',
        size: heightToDp('3%'),
        label: `Global${ '\n' }Results`,
      } }
      component={ ResultsStack }
    />
  </Tab.Navigator>
)

export default MainTab
