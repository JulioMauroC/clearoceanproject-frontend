import React from 'react'

import {
  createStackNavigator,
  TransitionPresets,
} from '@react-navigation/stack'

import SettingsView from 'views/Settings'

const Stack = createStackNavigator()

const SettingsStack = () => (
  <Stack.Navigator initialRouteName="Settings" headerMode="none">
    <Stack.Screen
      options={ {
        ...TransitionPresets.ModalSlideFromBottomIOS,

      } }
      name="Settings"
      component={ SettingsView }
    />
  </Stack.Navigator>
)

export default SettingsStack
