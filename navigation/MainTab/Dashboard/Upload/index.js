import React from 'react'

import { createStackNavigator } from '@react-navigation/stack'

import UploadView from 'views/Upload'
// import ImagesBrowseView from 'views/Upload/components/ImagesBrowseView'

const Stack = createStackNavigator()

const UploadStack = () => (
  <Stack.Navigator initialRouteName="Upload" headerMode="none">
    <Stack.Screen name="Upload" component={ UploadView } options={ { withSettings: true } } />
    {/* <Stack.Screen name="Images Browse" component={ ImagesBrowseView } options={ { withSettings: true } } /> */}
  </Stack.Navigator>
)

export default UploadStack
