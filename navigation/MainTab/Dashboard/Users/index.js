import React from 'react'
import { Button } from 'react-native'
import {
  createStackNavigator,
  TransitionPresets,
} from '@react-navigation/stack'

import UsersView from 'views/Users'
import UserView from 'views/User'
import InviteView from 'views/Users/views/Invite'
import InviteModal from 'views/Users/views/InviteModal'

const Stack = createStackNavigator()

const UsersStack = () => (
  <Stack.Navigator initialRouteName="Users" headerMode="none">
    <Stack.Screen
      name="Users"
      component={ UsersView }
      options={ { withSettings: true } }
    />
    <Stack.Screen
      options={ {
        ...TransitionPresets.ModalSlideFromBottomIOS,
        withSettings: true,
      } }
      name="User"
      component={ UserView }
    />
    <Stack.Screen
      options={ {
        ...TransitionPresets.ModalSlideFromBottomIOS,
        withSettings: true,
      } }
      name="Invite"
      component={ InviteView }
    />
    <Stack.Screen
      options={ {
        ...TransitionPresets.ModalSlideFromBottomIOS,
        withSettings: true,
      } }
      name="Invite Modal"
      component={ InviteModal }
    />
  </Stack.Navigator>
)

export default UsersStack
