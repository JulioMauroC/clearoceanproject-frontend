import React from 'react'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createStackNavigator } from '@react-navigation/stack'

import DashboardView from 'views/Dashboard'
// import EntriesView from 'views/Entries'
// import UsersView from 'views/Users'
import UploadView from 'views/Upload'
// import ResultsView from 'views/Results'
import UsersStack from './Users'
import EntriesStack from './Entries'
import ResultsStack from './Results'
import SettingsStack from './Settings'
// import UploadStack from './Upload'

const Stack = createStackNavigator()
// const Tab = createBottomTabNavigator()

const DashboardStack = () => (
  <Stack.Navigator initialRouteName="Dashboard" headerMode="none">
    <Stack.Screen
      name="Dashboard"
      component={ DashboardView }
      options={ { gestureEnabled: false, withSettings: true } }
    />
    <Stack.Screen
      name="Entries"
      component={ EntriesStack }
      options={ { gestureEnabled: false } }
    />
    <Stack.Screen
      name="Users"
      component={ UsersStack }
      options={ { gestureEnabled: false } }
    />
    <Stack.Screen
      name="Upload"
      component={ UploadView }
      options={ { gestureEnabled: false } }
    />
    <Stack.Screen
      name="Results"
      component={ ResultsStack }
      options={ { gestureEnabled: false } }
    />
    <Stack.Screen
      name="Settings"
      component={ SettingsStack }
      options={ { gestureEnabled: false } }
    />
  </Stack.Navigator>
)

export default DashboardStack
