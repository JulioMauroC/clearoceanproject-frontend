import React from 'react'

import {
  createStackNavigator,
  TransitionPresets,
} from '@react-navigation/stack'

import EntriesView from 'views/Entries'
// import EntryView from 'views/Entries/views/Entry'
import EntryView from 'views/Entry'

const Stack = createStackNavigator()

const EntriesStack = () => (
  <Stack.Navigator initialRouteName="Entries" headerMode="none">
    <Stack.Screen
      name="Entries"
      component={ EntriesView }
      options={ {
        withSettings: true,
      } }
    />
    <Stack.Screen
      options={ {
        ...TransitionPresets.ModalSlideFromBottomIOS,
        withSettings: true,
      } }
      name="Entry"
      component={ EntryView }
    />
  </Stack.Navigator>
)

export default EntriesStack
