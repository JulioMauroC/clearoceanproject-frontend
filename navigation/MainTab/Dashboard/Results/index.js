import React from 'react'

import {
  createStackNavigator,
  TransitionPresets,
} from '@react-navigation/stack'

import GlobalResultsView from 'views/GlobalResults'
import GlobalResultsListView from 'views/GlobalResults/views/GlobalResultsList'
import GlobalResultsMapView from 'views/GlobalResults/views/GlobalResultsMap'
import GlobalEntryView from 'views/GlobalResults/views/GlobalEntry'

const Stack = createStackNavigator()

const ResultsStack = () => (
  <Stack.Navigator initialRouteName="Global Results" headerMode="none">
    <Stack.Screen
      name="Global Results"
      component={ GlobalResultsView }
      options={ {
        withSettings: true,
      } }
    />
    <Stack.Screen
      name="Global Results List"
      component={ GlobalResultsListView }
      options={ {
        withSettings: true,
      } }
    />
    <Stack.Screen
      name="Global Results Map"
      component={ GlobalResultsMapView }
      options={ {
        withSettings: true,
      } }
    />
    <Stack.Screen
      options={ {
        ...TransitionPresets.ModalSlideFromBottomIOS,
        withSettings: true,
      } }
      name="Global Entry View"
      component={ GlobalEntryView }
    />
  </Stack.Navigator>
)

export default ResultsStack
