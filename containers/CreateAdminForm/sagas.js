import { takeLatest, call, put } from 'redux-saga/effects'

import { AsyncStorage } from 'react-native'
import * as RootNavigation from 'navigation/RootNavigation'

import {
  CREATE_ADMIN_REQUEST,
  createAdminFailure,
  createAdminSuccess,
} from './actions'

import { apiCreateAdmin } from './api'

function* registerWorker(action) {
  try {
    const token = yield AsyncStorage.getItem('token')

    action.payload.token = token

    const apiResult = yield call(apiCreateAdmin, action.payload)

    yield put(createAdminSuccess(apiResult.data))

    RootNavigation.navigate('MainTab')
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    console.log(error)
    console.log(errorMessage)
    yield put(createAdminFailure(errorMessage))
  }
}

export default function* watcher() {
  yield takeLatest(CREATE_ADMIN_REQUEST, registerWorker)
}
