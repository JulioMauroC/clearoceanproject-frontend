export const CREATE_ADMIN_REQUEST = 'CREATE_ADMIN_REQUEST'
export const CREATE_ADMIN_FAILURE = 'CREATE_ADMIN_FAILURE'
export const CREATE_ADMIN_SUCCESS = 'CREATE_ADMIN_SUCCESS'

// export const UPDATE_POSITION_REQUEST = 'UPDATE_POSITION_REQUEST'
// export const UPDATE_POSITION_FAILURE = 'UPDATE_POSITION_FAILURE'
// export const UPDATE_POSITION_SUCCESS = 'UPDATE_POSITION_SUCCESS'

//
export const createAdminRequest = (adminData) => ({
  type: CREATE_ADMIN_REQUEST,
  payload: adminData,
})

export const createAdminFailure = (errorMessage) => ({
  type: CREATE_ADMIN_FAILURE,
  payload: errorMessage,
})

export const createAdminSuccess = (adminData) => ({
  type: CREATE_ADMIN_SUCCESS,
  payload: adminData,
})
