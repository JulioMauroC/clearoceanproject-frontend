import axios from 'axios'

// eslint-disable-next-line import/prefer-default-export
export const apiCreateAdmin = async (adminData) => {
  const { AdminImageInput, AdminPositionInput, token } = adminData

  let uri
  const formData = new FormData()

  if (AdminImageInput) {
    uri = AdminImageInput.uri

    const fileType = uri.substring(uri.lastIndexOf('.') + 1)
    formData.append('profileImage', {
      uri,
      name: `photo.${ fileType }`,
      type: `image/${ fileType }`,
    })
  }

  formData.append('position', AdminPositionInput)
  formData.append('token', token)

  // await formData.append('name', Date.now().toString()) // other param

  const config = {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
      Authorization: `Bearer ${ token }`,
    },
  }

  return axios.post('/users/updateAdmin', formData, config)
}
