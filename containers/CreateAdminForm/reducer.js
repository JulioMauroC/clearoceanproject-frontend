import { REDUCER_NAME } from './consts'

import {
  CREATE_ADMIN_REQUEST,
  CREATE_ADMIN_FAILURE,
  CREATE_ADMIN_SUCCESS,
} from './actions'

const initialState = {
  loading: false,
  errorMessage: null,
  adminData: {
    profilePic: null,
    position: null,
  },
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_ADMIN_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        adminData: {
          profilePic: null,
          position: null,
        },
      }
    }

    case CREATE_ADMIN_FAILURE: {
      return {
        ...state,
        loading: false,
        errorMessage: action.payload.errorMessage,
        adminData: {
          profilePic: null,
          position: null,
        },
      }
    }

    case CREATE_ADMIN_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        adminData: {
          profilePic: action.payload.profilePic,
          position: action.payload.position,
        },
      }
    }

    default:
      return state
  }
}
