import { connect, useStore } from 'react-redux'
import { compose, lifecycle } from 'recompose'
import { bindActionCreators } from 'redux'
import { reduxForm } from 'redux-form'
import { createStructuredSelector } from 'reselect'

import {
  createAdminRequest,
  createAdminFailure,
  createAdminSuccess,
} from './actions'

import { FORM_NAME, REDUCER_NAME } from './consts'

import { selectIsLoading, selectAdminData } from './selectors'

export const mapStateToProps = createStructuredSelector({
  isLoading: selectIsLoading,
  adminData: selectAdminData,
})

export const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    createAdminRequest,
    createAdminFailure,
    createAdminSuccess,
  },
  dispatch,
)

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: FORM_NAME,
    submitAsSideEffect: true,
    onSubmit: (values) => createAdminRequest(values),

  }),
)
