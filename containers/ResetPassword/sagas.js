import { takeLatest, call, put } from 'redux-saga/effects'

import * as RootNavigation from 'navigation/RootNavigation'

import {
  SEND_CODE_REQUEST,
  CONFIRM_CODE_REQUEST,
  CHANGE_PASSWORD_REQUEST,
  sendCodeFailure,
  sendCodeSuccess,
  confirmCodeFailure,
  confirmCodeSuccess,
  changePasswordFailure,
  changePasswordSuccess,
} from './actions'

import { apiSendCode, apiConfirmCode, apiChangePassword } from './api'

function* sendCodeWorker(action) {
  try {
    const apiResult = yield call(apiSendCode, action.payload)

    yield put(sendCodeSuccess(apiResult.data))

    RootNavigation.navigate('ForgotPassword', {
      screen: 'EnterCode',
      params: { email: action.payload.email },
    })
  } catch (error) {
    const errorMessage = yield error.response.data.msg
    yield put(sendCodeFailure(errorMessage))
  }
}

function* confirmCodeWorker(action) {
  try {
    const apiResult = yield call(apiConfirmCode, action.payload)

    yield put(confirmCodeSuccess(apiResult.data))

    RootNavigation.navigate('ForgotPassword', {
      screen: 'ResetPassword',
      params: {
        email: apiResult.data.user.email,
        code: apiResult.data.user.resetPasswordCode,
      },
    })
  } catch (error) {
    const errorMessage = yield error.response.data.msg
    console.log(errorMessage)
    yield put(confirmCodeFailure(errorMessage))
  }
}

function* changePasswordWorker(action) {
  try {
    const apiResult = yield call(apiChangePassword, action.payload)

    yield put(changePasswordSuccess(apiResult.data))

    RootNavigation.navigate('Auth', {
      screen: 'Auth',
    })
  } catch (error) {
    const errorMessage = yield error.response.data.msg
    yield put(changePasswordFailure(errorMessage))
  }
}

export default function* watcher() {
  yield takeLatest(SEND_CODE_REQUEST, sendCodeWorker)
  yield takeLatest(CONFIRM_CODE_REQUEST, confirmCodeWorker)
  yield takeLatest(CHANGE_PASSWORD_REQUEST, changePasswordWorker)
}
