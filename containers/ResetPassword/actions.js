export const SEND_CODE_REQUEST = 'SEND_CODE_REQUEST'
export const SEND_CODE_FAILURE = 'SEND_CODE_FAILURE'
export const SEND_CODE_SUCCESS = 'SEND_CODE_SUCCESS'

export const CONFIRM_CODE_REQUEST = 'CONFIRM_CODE_REQUEST'
export const CONFIRM_CODE_FAILURE = 'CONFIRM_CODE_FAILURE'
export const CONFIRM_CODE_SUCCESS = 'CONFIRM_CODE_SUCCESS'

export const CHANGE_PASSWORD_REQUEST = 'CHANGE_PASSWORD_REQUEST'
export const CHANGE_PASSWORD_FAILURE = 'CHANGE_PASSWORD_FAILURE'
export const CHANGE_PASSWORD_SUCCESS = 'CHANGE_PASSWORD_SUCCESS'

export const sendCodeRequest = (email) => ({
  type: SEND_CODE_REQUEST,
  payload: email,
})

export const sendCodeFailure = (errorMessage) => ({
  type: SEND_CODE_FAILURE,
  payload: errorMessage,
})

export const sendCodeSuccess = (userData) => ({
  type: SEND_CODE_SUCCESS,
  payload: userData,
})

//---

export const confirmCodeRequest = (email, code) => ({
  type: CONFIRM_CODE_REQUEST,
  payload: { email, code },
})

export const confirmCodeFailure = (errorMessage) => ({
  type: CONFIRM_CODE_FAILURE,
  payload: errorMessage,
})

export const confirmCodeSuccess = (userData) => ({
  type: CONFIRM_CODE_SUCCESS,
  payload: userData,
})

//---

export const changePasswordRequest = (email, code, newPassword) => ({
  type: CHANGE_PASSWORD_REQUEST,
  payload: { email, code, newPassword },
})

export const changePasswordFailure = (errorMessage) => ({
  type: CHANGE_PASSWORD_FAILURE,
  payload: errorMessage,
})

export const changePasswordSuccess = (userData) => ({
  type: CHANGE_PASSWORD_SUCCESS,
  payload: userData,
})
