import { connect } from 'react-redux'
import { compose } from 'recompose'
import { bindActionCreators } from 'redux'

import { createStructuredSelector } from 'reselect'

import {
  sendCodeRequest,
  confirmCodeRequest,
  changePasswordRequest,
} from './actions'

import {
  selectIsLoading,
  selectUserData,
  selectErrorMessage,
  selectEmail,
  selectCode,
} from './selectors'

export const mapStateToProps = createStructuredSelector({
  isLoading: selectIsLoading,
  userData: selectUserData,
  errorMessage: selectErrorMessage,
  email: selectEmail,
  code: selectCode,
})

export const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    sendCodeRequest,
    confirmCodeRequest,
    changePasswordRequest,
  },
  dispatch,
)

export default compose(connect(mapStateToProps, mapDispatchToProps))
