import {
  SEND_CODE_REQUEST,
  SEND_CODE_FAILURE,
  SEND_CODE_SUCCESS,
  CONFIRM_CODE_REQUEST,
  CONFIRM_CODE_FAILURE,
  CONFIRM_CODE_SUCCESS,
  CHANGE_PASSWORD_REQUEST,
  CHANGE_PASSWORD_FAILURE,
  CHANGE_PASSWORD_SUCCESS,
} from './actions'

const initialState = {
  loading: false,
  errorMessage: null,
  userData: null,
  email: null,
  code: null,
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SEND_CODE_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        email: action.payload.email,
        userData: null,
        code: null,
      }
    }

    case SEND_CODE_FAILURE: {
      return {
        ...state,
        loading: false,
        errorMessage: action.payload,
        userData: null,
        code: null,
      }
    }

    case SEND_CODE_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        userData: action.payload.userData,
      }
    }

    //

    case CONFIRM_CODE_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        email: action.payload.email,
        code: action.payload.code,
      }
    }

    case CONFIRM_CODE_FAILURE: {
      return {
        ...state,
        loading: false,
        errorMessage: action.payload,
        userData: null,
        code: null,
      }
    }

    case CONFIRM_CODE_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        userData: action.payload.userData,
      }
    }

    //

    case CHANGE_PASSWORD_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
      }
    }

    case CHANGE_PASSWORD_FAILURE: {
      return {
        ...state,
        loading: false,
        errorMessage: action.payload,
      }
    }

    case CHANGE_PASSWORD_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        userData: action.payload.userData,
      }
    }

    default:
      return state
  }
}
