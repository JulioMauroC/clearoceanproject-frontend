import axios from 'axios'

// eslint-disable-next-line import/prefer-default-export
export const apiSendCode = (data) => axios.post('/users/password-reset-send-code', data)

export const apiConfirmCode = (data) => {
  console.log('data from api')
  console.log(data)

  return axios.post('/users/password-reset-confirm-code', data)
}

export const apiChangePassword = (data) => {
  console.log('data from api')
  console.log(data)

  return axios.post('/users/password-reset', data)
}
