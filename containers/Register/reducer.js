import { REDUCER_NAME } from './consts'

import {
  REGISTER_REQUEST,
  REGISTER_FAILURE,
  REGISTER_SUCCESS,
  SEND_EMAIL_REQUEST,
  SEND_EMAIL_FAILURE,
  SEND_EMAIL_SUCCESS,
} from './actions'

const initialState = {
  loading: false,
  errorMessage: null,
  userData: null,
  // email: null,

}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case REGISTER_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        userData: action.payload.userData,
      }
    }

    case REGISTER_FAILURE: {
      return {
        ...state,
        loading: false,
        errorMessage: action.payload,
        userData: null,
      }
    }

    case REGISTER_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        userData: action.payload.userData,
      }
    }

    // case SEND_EMAIL_REQUEST: {
    //   return {
    //     ...state,
    //     loading: true,
    //     errorMessage: null,
    //     userData: null,
    //     email: action.payload.email,
    //   };
    // }

    // case SEND_EMAIL_FAILURE: {
    //   return {
    //     ...state,
    //     loading: false,
    //     errorMessage: action.payload.errorMessage,
    //     userData: null,

    //   };
    // }

    // case SEND_EMAIL_SUCCESS: {
    //   return {
    //     ...state,
    //     loading: false,
    //     errorMessage: null,
    //     userData: null,
    //     email: action.payload.email,
    //   };
    // }

    default:
      return state
  }
}
