import { takeLatest, call, put } from 'redux-saga/effects'

import { AsyncStorage } from 'react-native'
import * as RootNavigation from 'navigation/RootNavigation'

import {
  REGISTER_REQUEST,
  SEND_EMAIL_REQUEST,
  registerFailure,
  registerSuccess,
  sendEmailFailure,
  sendEmailSuccess,
} from './actions'

import { apiRegister } from './api'

function* registerWorker(action) {
  try {
    const apiResult = yield call(apiRegister, action.payload)

    yield put(registerSuccess(apiResult.data))

    yield AsyncStorage.setItem('token', apiResult.data.token)

    RootNavigation.navigate('Register')
  } catch (error) {
    // const errorMessage = yield error.toJSON().message

    const errorMessage = yield error.response.data.msg

    yield put(registerFailure(errorMessage))
  }
}

function* sendEmailWorker(action) {
  try {
  } catch (error) {
    console.log(error)
  }
}

export default function* watcher() {
  yield takeLatest(REGISTER_REQUEST, registerWorker)
  yield takeLatest(SEND_EMAIL_REQUEST, sendEmailWorker)
}
