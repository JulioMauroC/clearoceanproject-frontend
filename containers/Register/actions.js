export const REGISTER_REQUEST = 'REGISTER_REQUEST'
export const REGISTER_FAILURE = 'REGISTER_FAILURE'
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'

export const SEND_EMAIL_REQUEST = 'SEND_EMAIL_REQUEST'
export const SEND_EMAIL_FAILURE = 'SEND_EMAIL_FAILURE'
export const SEND_EMAIL_SUCCESS = 'SEND_EMAIL_SUCCESS'

// Register actions
export const registerRequest = (userData) => ({
  type: REGISTER_REQUEST,
  payload: userData,
})

export const registerFailure = (errorMessage) => ({
  type: REGISTER_FAILURE,
  payload: errorMessage,
})

export const registerSuccess = (userData) => ({
  type: REGISTER_SUCCESS,
  payload: userData,
})

// sendEmail actions
export const sendEmailRequest = (email) => ({
  type: SEND_EMAIL_REQUEST,
  payload: email,
})

export const sendEmailFailure = (errorMessage) => ({
  type: SEND_EMAIL_FAILURE,
  payload: errorMessage,
})

export const sendEmailSuccess = (email) => ({
  type: SEND_EMAIL_SUCCESS,
  payload: email,
})
