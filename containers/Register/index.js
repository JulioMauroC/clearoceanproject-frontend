import { connect } from 'react-redux'
import { compose } from 'recompose'
import { bindActionCreators } from 'redux'

import { createStructuredSelector } from 'reselect'

import {
  registerRequest,
  registerFailure,
  registerSuccess,
  sendEmailRequest,
  sendEmailFailure,
  sendEmailSuccess,
} from './actions'

import { selectIsLoading, selectUserData, selectErrorMessage } from './selectors'

export const mapStateToProps = createStructuredSelector({
  isLoading: selectIsLoading,
  userData: selectUserData,
  errorMessage: selectErrorMessage,
})

export const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    registerRequest,
    registerFailure,
    registerSuccess,
    sendEmailRequest,
    sendEmailFailure,
    sendEmailSuccess,
  },
  dispatch,
)

export default compose(
  connect(mapStateToProps, mapDispatchToProps),

)
