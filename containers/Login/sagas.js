import { takeLatest, call, put } from 'redux-saga/effects'

import { AsyncStorage } from 'react-native'
import * as RootNavigation from 'navigation/RootNavigation'

import {
  LOGIN_REQUEST,
  SEND_EMAIL_REQUEST,
  loginFailure,
  loginSuccess,
  sendEmailFailure,
  sendEmailSuccess,
} from './actions'

import { apiLogin } from './api'

function* loginWorker(action) {
  try {
    const apiResult = yield call(apiLogin, action.payload)

    yield put(loginSuccess(apiResult.data))

    yield AsyncStorage.setItem('token', apiResult.data.token)

    RootNavigation.navigate('MainTab')
  } catch (error) {
    const errorMessage = yield error.response.data.msg
    yield put(loginFailure(errorMessage))
  }
}

function* sendEmailWorker(action) {
  try {
  } catch (error) {
    console.log(error)
  }
}

export default function* watcher() {
  yield takeLatest(LOGIN_REQUEST, loginWorker)
  yield takeLatest(SEND_EMAIL_REQUEST, sendEmailWorker)
}
