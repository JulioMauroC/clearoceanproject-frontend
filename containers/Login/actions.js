export const LOGIN_REQUEST = 'LOGIN_REQUEST'
export const LOGIN_FAILURE = 'LOGIN_FAILURE'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'

export const SEND_EMAIL_REQUEST = 'SEND_EMAIL_REQUEST'
export const SEND_EMAIL_FAILURE = 'SEND_EMAIL_FAILURE'
export const SEND_EMAIL_SUCCESS = 'SEND_EMAIL_SUCCESS'

// login actions
export const loginRequest = (userData) => ({
  type: LOGIN_REQUEST,
  payload: userData,
})

export const loginFailure = (errorMessage) => ({
  type: LOGIN_FAILURE,
  payload: errorMessage,
})

export const loginSuccess = (userData) => ({
  type: LOGIN_SUCCESS,
  payload: userData,
})

export const sendEmailRequest = (email) => ({
  type: SEND_EMAIL_REQUEST,
  payload: email,
})

export const sendEmailFailure = (errorMessage) => ({
  type: SEND_EMAIL_FAILURE,
  payload: errorMessage,
})

export const sendEmailSuccess = (email) => ({
  type: SEND_EMAIL_SUCCESS,
  payload: email,
})
