import axios from 'axios'

// axios.defaults.baseURL = 'http://localhost:8080/api'

// eslint-disable-next-line import/prefer-default-export
export const apiLogin = (userData) => axios.post('/users/login', userData)
