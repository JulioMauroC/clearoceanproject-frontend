import { createSelector } from 'reselect'
import _get from 'lodash/fp/get'
import { REDUCER_NAME } from './consts'

export const selectIsLoading = createSelector(
  (state) => state[ REDUCER_NAME ],
  _get('loading'),
)

export const selectErrorMessage = createSelector(
  (state) => state[ REDUCER_NAME ],
  _get('errorMessage'),
)

export const selectEntryData = createSelector(
  (state) => state[ REDUCER_NAME ],
  _get('entryData'),
)

export const selectEntriesData = createSelector(
  (state) => state[ REDUCER_NAME ],
  _get('entriesData'),
)

export const selectGlobalEntriesData = createSelector(
  (state) => state[ REDUCER_NAME ],
  _get('globalEntriesData'),
)

export const selectLocationData = createSelector(
  (state) => state[ REDUCER_NAME ],
  _get('location'),
)
