import { takeLatest, call, put } from 'redux-saga/effects'
import { startSubmit, stopSubmit } from 'redux-form'
import { AsyncStorage } from 'react-native'
import * as RootNavigation from 'navigation/RootNavigation'
import * as Location from 'expo-location'
import {
  apiCreateEntry,
  apiGetYachtEntries,
  apiGetGlobalEntries,
  apiEditEntry,
  apiDeleteEntry,
} from './api'

import {
  CREATE_ENTRY_REQUEST,
  GET_YACHT_ENTRIES_REQUEST,
  GET_GLOBAL_ENTRIES_REQUEST,
  EDIT_ENTRY_REQUEST,
  DELETE_ENTRY_REQUEST,
  GET_LOCATION_REQUEST,
  createEntryFailure,
  createEntrySuccess,
  getYachtEntriesFailure,
  getYachtEntriesSuccess,
  getGlobalEntriesFailure,
  getGlobalEntriesSuccess,
  editEntryFailure,
  editEntrySuccess,
  deleteEntryFailure,
  deleteEntrySuccess,
  getLocationFailure,
  getLocationSuccess,
} from './actions'

function* createEntryWorker(action) {
  try {
    const token = yield AsyncStorage.getItem('token')

    const apiResult = yield call(apiCreateEntry, action.payload, token)

    yield put(createEntrySuccess(apiResult.data))

    RootNavigation.navigate('Dashboard')
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(createEntryFailure(errorMessage))
  }
}

function* getYachtEntriesWorker() {
  try {
    const token = yield AsyncStorage.getItem('token')

    const apiResult = yield call(apiGetYachtEntries, token)

    yield put(getYachtEntriesSuccess(apiResult.data))
  } catch (error) {
    const errorMessage = yield error.toJSON().message

    yield put(getYachtEntriesFailure(errorMessage))
  }
}

function* getGlobalEntriesWorker(action) {
  try {
    const token = yield AsyncStorage.getItem('token')

    const apiResult = yield call(apiGetGlobalEntries, token)

    yield put(getGlobalEntriesSuccess(apiResult.data))
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(getGlobalEntriesFailure(errorMessage))
    // yield put(stopSubmit(FORM_NAME));
  }
}

function* deleteEntryWorker(action) {
  try {
    const apiResult = yield call(apiDeleteEntry, action.payload)

    yield put(deleteEntrySuccess(apiResult.data))
    RootNavigation.navigate('Dashboard')
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(deleteEntryFailure(errorMessage))
  }
}

function* editEntryWorker(action) {
  try {
    const apiResult = yield call(apiEditEntry, action.payload)

    yield put(editEntrySuccess(apiResult.data))
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(editEntryFailure(errorMessage))
  }
}

function* getLocationWorker(action) {
  try {
    // const apiResult = yield call(apiEditEntry, action.payload)
    const location = yield Location.getCurrentPositionAsync({})

    yield put(getLocationSuccess(location))
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(getLocationFailure(errorMessage))
  }
}

export default function* watcher() {
  yield takeLatest(CREATE_ENTRY_REQUEST, createEntryWorker)
  yield takeLatest(GET_YACHT_ENTRIES_REQUEST, getYachtEntriesWorker)
  yield takeLatest(GET_GLOBAL_ENTRIES_REQUEST, getGlobalEntriesWorker)
  yield takeLatest(EDIT_ENTRY_REQUEST, editEntryWorker)
  yield takeLatest(DELETE_ENTRY_REQUEST, deleteEntryWorker)
  yield takeLatest(GET_LOCATION_REQUEST, getLocationWorker)
}
