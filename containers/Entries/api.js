import axios from 'axios'

export const apiGetGlobalEntries = (token) => {
  const config = {
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${ token }`,
    },
  }

  return axios.get('/entries/all/global', config)
}

export const apiGetYachtEntries = (token) => {
  const config = {
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${ token }`,
    },
  }

  return axios.get('/entries/all/yacht', config)
}

export const apiEditEntry = () => 'test'

export const apiDeleteEntry = (id) => axios.delete(`/entries/delete/${ id }`)

export const apiCreateEntry = (entryData, token) => {
  const { EntryImageInput, EntryLocationInput, EntryTypesInput } = entryData

  const config = {
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${ token }`,
    },
  }

  const formData = new FormData()

  if (EntryImageInput) {
    if (Array.isArray(EntryImageInput)) {
      EntryImageInput.forEach((item, index) => {
        formData.append('entryImage', {
          uri: item.uri,
          name: `photo.${ item.fileType }`,
          type: `image/${ item.fileType }`,
        })
      })
    } else {
      let uri
      uri = EntryImageInput.uri

      const fileType = uri.substring(uri.lastIndexOf('.') + 1)
      formData.append('entryImage', {
        uri,
        name: `photo.${ fileType }`,
        type: `image/${ fileType }`,
      })
    }
  }

  formData.append('longitude', EntryLocationInput[ 1 ])
  formData.append('latitude', EntryLocationInput[ 0 ])
  formData.append('types', JSON.stringify(EntryTypesInput))
  formData.append('token', token)

  return axios.post('/entries/create', formData, config)
}
