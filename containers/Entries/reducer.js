import {
  CREATE_ENTRY_REQUEST,
  CREATE_ENTRY_FAILURE,
  CREATE_ENTRY_SUCCESS,
  EDIT_ENTRY_REQUEST,
  EDIT_ENTRY_FAILURE,
  EDIT_ENTRY_SUCCESS,
  DELETE_ENTRY_REQUEST,
  DELETE_ENTRY_FAILURE,
  DELETE_ENTRY_SUCCESS,
  GET_YACHT_ENTRIES_REQUEST,
  GET_YACHT_ENTRIES_FAILURE,
  GET_YACHT_ENTRIES_SUCCESS,
  GET_GLOBAL_ENTRIES_REQUEST,
  GET_GLOBAL_ENTRIES_FAILURE,
  GET_GLOBAL_ENTRIES_SUCCESS,
  GET_LOCATION_REQUEST,
  GET_LOCATION_FAILURE,
  GET_LOCATION_SUCCESS,
} from './actions'

const initialState = {
  loading: false,
  errorMessage: null,
  entriesData: null,
  globalEntriesData: null,
  entryData: null,
  location: null,
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_YACHT_ENTRIES_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        entriesData: action.payload,
      }
    }

    case GET_GLOBAL_ENTRIES_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        globalEntriesData: action.payload,
      }
    }

    case GET_YACHT_ENTRIES_FAILURE: {
      return {
        ...state,
        loading: false,
        errorMessage: action.payload,
        entriesData: null,
      }
    }

    case GET_GLOBAL_ENTRIES_FAILURE: {
      return {
        ...state,
        loading: false,
        errorMessage: action.payload,
        globalEntriesData: null,
      }
    }

    case GET_YACHT_ENTRIES_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        entriesData: action.payload,
      }
    }

    case GET_GLOBAL_ENTRIES_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        globalEntriesData: action.payload,
      }
    }

    case CREATE_ENTRY_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
      }
    }

    case CREATE_ENTRY_FAILURE: {
      return {
        ...state,
        loading: false,
        entryData: null,
        errorMessage: action.payload.errorMessage,
      }
    }

    case CREATE_ENTRY_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        entryData: action.payload,
      }
    }

    case EDIT_ENTRY_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        entryData: action.payload.entryData,
      }
    }

    case EDIT_ENTRY_FAILURE: {
      return {
        ...state,
        loading: false,
        entryData: null,
        errorMessage: action.payload.errorMessage,
      }
    }

    case EDIT_ENTRY_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        entryData: action.payload.entryData,
      }
    }

    case DELETE_ENTRY_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
      }
    }

    case DELETE_ENTRY_FAILURE: {
      return {
        ...state,
        loading: false,
        entryData: null,
        errorMessage: action.payload.errorMessage,
      }
    }

    case DELETE_ENTRY_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        // entriesData: action.payload,
      }
    }
    case GET_LOCATION_REQUEST: {
      return {
        ...state,
        loading: true,
        location: null,
      }
    }
    case GET_LOCATION_FAILURE: {
      return {
        ...state,
        loading: false,
        errorMessage: action.payload.errorMessage,
        location: null,
      }
    }
    case GET_LOCATION_SUCCESS: {
      return {
        ...state,
        loading: false,
        location: action.payload,
      }
    }

    default:
      return state
  }
}
