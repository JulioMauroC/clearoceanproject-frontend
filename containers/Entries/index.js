import { connect, useStore } from 'react-redux'
import { compose, lifecycle } from 'recompose'
import { bindActionCreators } from 'redux'
import { reduxForm } from 'redux-form'
import { createStructuredSelector } from 'reselect'

import {
  createEntryRequest,
  createEntryFailure,
  createEntrySuccess,
  getYachtEntriesRequest,
  getYachtEntriesFailure,
  getYachtEntriesSuccess,
  getGlobalEntriesRequest,
  getGlobalEntriesFailure,
  getGlobalEntriesSuccess,
  editEntryRequest,
  editEntryFailure,
  editEntrySuccess,
  deleteEntryRequest,
  deleteEntryFailure,
  deleteEntrySuccess,
  getLocationRequest,
  getLocationFailure,
  getLocationSuccess,
} from './actions'

import {
  selectIsLoading,
  selectErrorMessage,
  selectEntryData,
  selectEntriesData,
  selectGlobalEntriesData,
  selectLocationData,
} from './selectors'

export const mapStateToProps = createStructuredSelector({
  isLoading: selectIsLoading,
  errorMessage: selectErrorMessage,
  entryData: selectEntryData,
  entriesData: selectEntriesData,
  globalEntriesData: selectGlobalEntriesData,
  location: selectLocationData,
})

export const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    createEntryRequest,
    createEntryFailure,
    createEntrySuccess,
    getYachtEntriesRequest,
    getYachtEntriesFailure,
    getYachtEntriesSuccess,
    getGlobalEntriesRequest,
    getGlobalEntriesFailure,
    getGlobalEntriesSuccess,
    editEntryRequest,
    editEntryFailure,
    editEntrySuccess,
    deleteEntryRequest,
    deleteEntryFailure,
    deleteEntrySuccess,
    getLocationRequest,
    getLocationFailure,
    getLocationSuccess,
  },
  dispatch,
)

export default compose(connect(mapStateToProps, mapDispatchToProps))
