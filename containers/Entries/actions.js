// export const GET_ENTRY_REQUEST = 'GET_ENTRY_REQUEST'
// export const GET_ENTRY_FAILURE = 'GET_ENTRY_FAILURE'
// export const GET_ENTRY_SUCCESS = 'GET_ENTRY_SUCCESS'

export const CREATE_ENTRY_REQUEST = 'CREATE_ENTRY_REQUEST'
export const CREATE_ENTRY_FAILURE = 'CREATE_ENTRY_FAILURE'
export const CREATE_ENTRY_SUCCESS = 'CREATE_ENTRY_SUCCESS'

export const GET_YACHT_ENTRIES_REQUEST = 'GET_YACHT_ENTRIES_REQUEST'
export const GET_YACHT_ENTRIES_FAILURE = 'GET_YACHT_ENTRIES_FAILURE'
export const GET_YACHT_ENTRIES_SUCCESS = 'GET_YACHT_ENTRIES_SUCCESS'

export const GET_GLOBAL_ENTRIES_REQUEST = 'GET_GLOBAL_ENTRIES_REQUEST'
export const GET_GLOBAL_ENTRIES_FAILURE = 'GET_GLOBAL_ENTRIES_FAILURE'
export const GET_GLOBAL_ENTRIES_SUCCESS = 'GET_GLOBAL_ENTRIES_SUCCESS'

export const EDIT_ENTRY_REQUEST = 'EDIT_ENTRY_REQUEST'
export const EDIT_ENTRY_FAILURE = 'EDIT_ENTRY_FAILURE'
export const EDIT_ENTRY_SUCCESS = 'EDIT_ENTRY_SUCCESS'

export const DELETE_ENTRY_REQUEST = 'DELETE_ENTRY_REQUEST'
export const DELETE_ENTRY_FAILURE = 'DELETE_ENTRY_FAILURE'
export const DELETE_ENTRY_SUCCESS = 'DELETE_ENTRY_SUCCESS'

export const GET_LOCATION_REQUEST = 'GET_LOCATION_REQUEST'
export const GET_LOCATION_FAILURE = 'GET_LOCATION_FAILURE'
export const GET_LOCATION_SUCCESS = 'GET_LOCATION_SUCCESS'

// --- GET ENTRY

// export const getEntryRequest = (entryId) => ({
//   type: GET_ENTRY_REQUEST,
//   payload: entryId,
// });

// export const getEntryFailure = (errorMessage) => ({
//   type: GET_ENTRY_FAILURE,
//   payload: errorMessage,
// });

// export const getEntrySuccess = (entryData) => ({
//   type: GET_ENTRY_SUCCESS,
//   payload: entryData,
// });

// --- CREATE ENTRY

export const createEntryRequest = (entryData) => ({
  type: CREATE_ENTRY_REQUEST,
  payload: entryData,
})

export const createEntryFailure = (errorMessage) => ({
  type: CREATE_ENTRY_FAILURE,
  payload: errorMessage,
})

export const createEntrySuccess = (entriesData) => ({
  type: CREATE_ENTRY_SUCCESS,
  payload: entriesData,
})

// --- GET YACHT ENTRIES

export const getYachtEntriesRequest = () => ({
  type: GET_YACHT_ENTRIES_REQUEST,
})

export const getYachtEntriesFailure = (errorMessage) => ({
  type: GET_YACHT_ENTRIES_FAILURE,
  payload: errorMessage,
})

export const getYachtEntriesSuccess = (entriesData) => ({
  type: GET_YACHT_ENTRIES_SUCCESS,
  payload: entriesData,
})

// --- GET GLOBAL ENTRIES

export const getGlobalEntriesRequest = () => ({
  type: GET_GLOBAL_ENTRIES_REQUEST,
})

export const getGlobalEntriesFailure = (errorMessage) => ({
  type: GET_GLOBAL_ENTRIES_FAILURE,
  payload: errorMessage,
})

export const getGlobalEntriesSuccess = (entriesData) => ({
  type: GET_GLOBAL_ENTRIES_SUCCESS,
  payload: entriesData,
})

// --- EDIT ENTRY

export const editEntryRequest = (entryId, updatedFields) => ({
  type: EDIT_ENTRY_REQUEST,
  payload: { entryId, updatedFields },
})

export const editEntryFailure = (errorMessage) => ({
  type: EDIT_ENTRY_FAILURE,
  payload: errorMessage,
})

export const editEntrySuccess = (updatedEntry) => ({
  type: EDIT_ENTRY_SUCCESS,
  payload: updatedEntry,
})

// --- DELETE ENTRY

export const deleteEntryRequest = (entryId) => ({
  type: DELETE_ENTRY_REQUEST,
  payload: entryId,
})

export const deleteEntryFailure = (errorMessage) => ({
  type: DELETE_ENTRY_FAILURE,
  payload: errorMessage,
})

export const deleteEntrySuccess = () => ({
  type: DELETE_ENTRY_SUCCESS,
  // payload: entriesData,
})

// --- GET LOCATION

export const getLocationRequest = () => ({
  type: GET_LOCATION_REQUEST,

})

export const getLocationFailure = (errorMessage) => ({
  type: GET_LOCATION_FAILURE,
  payload: errorMessage,
})

export const getLocationSuccess = (location) => ({
  type: GET_LOCATION_SUCCESS,
  payload: location,
})
