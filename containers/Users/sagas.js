import { takeLatest, call, put } from 'redux-saga/effects'

import { AsyncStorage } from 'react-native'
import * as RootNavigation from 'navigation/RootNavigation'

import {
  apiGetUser,
  apiGetUsers,
  apiEditUser,
  apiEditOtherUser,
  apiDeleteCurrentUser,
  apiInviteUser,
  apiGetCurrentUser,
  apiGetYachtUsers,
  apiDeleteUser,
} from './api'

import {
  GET_USER_REQUEST,
  GET_CURRENT_USER_REQUEST,
  GET_USERS_REQUEST,
  GET_YACHT_USERS_REQUEST,
  EDIT_USER_REQUEST,
  EDIT_OTHER_USER_REQUEST,
  DELETE_USER_REQUEST,
  DELETE_CURRENT_USER_REQUEST,
  INVITE_USER_REQUEST,
  LOGOUT_USER_REQUEST,
  getCurrentUserRequest,
  getCurrentUserFailure,
  getCurrentUserSuccess,
  getUserFailure,
  getUserSuccess,
  getUsersFailure,
  getUsersSuccess,
  getYachtUsersFailure,
  getYachtUsersSuccess,
  editUserFailure,
  editUserSuccess,
  editOtherUserFailure,
  editOtherUserSuccess,
  deleteCurrentUserFailure,
  deleteCurrentUserSuccess,
  deleteUserFailure,
  deleteUserSuccess,
  inviteUserFailure,
  inviteUserSuccess,
  logoutUserFailure,
  logoutUserSuccess,
} from './actions'

function* getCurrentUserWorker(action) {
  try {
    const token = yield AsyncStorage.getItem('token')

    const apiResult = yield call(apiGetCurrentUser, token)

    yield put(getCurrentUserSuccess(apiResult.data))
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(getCurrentUserFailure(errorMessage))
  }
}

function* getUserWorker(action) {
  try {
    const apiResult = yield call(apiGetUser, action.payload)

    yield put(getUserSuccess(apiResult.data))
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(getUserFailure(errorMessage))
  }
}

function* getUsersWorker() {
  try {
    const apiResult = yield call(apiGetUsers)

    yield put(getUsersSuccess(apiResult.data))
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    console.log(errorMessage)
    yield put(getUsersFailure(errorMessage))
  }
}

function* getYachtUsersWorker() {
  try {
    const token = yield AsyncStorage.getItem('token')
    const apiResult = yield call(apiGetYachtUsers, token)

    yield put(getYachtUsersSuccess(apiResult.data))
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    console.log(errorMessage)
    yield put(getYachtUsersFailure(errorMessage))
  }
}

function* editUserWorker(action) {
  try {
    const token = yield AsyncStorage.getItem('token')

    const apiResult = yield call(apiEditUser, action.payload, token)

    yield put(editUserSuccess(apiResult.data))
    yield put(getCurrentUserRequest())
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(editUserFailure(errorMessage))
  }
}

function* editOtherUserWorker(action) {
  try {
    const token = yield AsyncStorage.getItem('token')

    const apiResult = yield call(apiEditOtherUser, action.payload, token)

    yield put(editOtherUserSuccess(apiResult.data))
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(editOtherUserFailure(errorMessage))
  }
}

function* deleteCurrentUserWorker(action) {
  try {
    const token = yield AsyncStorage.getItem('token')

    const apiResult = yield call(apiDeleteCurrentUser, token)
    // const apiResult = yield call(apiDeleteUser, action.payload)

    yield put(deleteCurrentUserSuccess(apiResult.data))
    RootNavigation.navigate('Auth', { screen: 'Auth' }) // sign out
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(deleteCurrentUserFailure(errorMessage))
  }
}

function* deleteUserWorker(action) {
  try {
    const apiResult = yield call(apiDeleteUser, action.payload)
    // const apiResult = yield call(apiDeleteUser, action.payload)

    yield put(deleteUserSuccess(apiResult.data))
    RootNavigation.navigate('Dashboard') // sign out
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(deleteUserFailure(errorMessage))
  }
}

function* inviteUserWorker(action) {
  try {
    const token = yield AsyncStorage.getItem('token')
    const apiResult = yield call(apiInviteUser, action.payload, token)

    yield put(inviteUserSuccess(apiResult.data))
    console.log('triggered saga')
    RootNavigation.navigate('Invite Modal')
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(inviteUserFailure(errorMessage))
  }
}

function* logoutUserWorker() {
  try {
    yield AsyncStorage.removeItem('token')
    yield put(logoutUserSuccess())
    RootNavigation.navigate('Auth', { screen: 'Auth' })
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(logoutUserFailure(errorMessage))
  }
}

export default function* watcher() {
  yield takeLatest(GET_CURRENT_USER_REQUEST, getCurrentUserWorker)
  yield takeLatest(GET_USER_REQUEST, getUserWorker)
  yield takeLatest(GET_USERS_REQUEST, getUsersWorker)
  yield takeLatest(GET_YACHT_USERS_REQUEST, getYachtUsersWorker)
  yield takeLatest(EDIT_USER_REQUEST, editUserWorker)
  yield takeLatest(EDIT_OTHER_USER_REQUEST, editOtherUserWorker)
  yield takeLatest(DELETE_USER_REQUEST, deleteUserWorker)
  yield takeLatest(DELETE_CURRENT_USER_REQUEST, deleteCurrentUserWorker)
  yield takeLatest(INVITE_USER_REQUEST, inviteUserWorker)
  yield takeLatest(LOGOUT_USER_REQUEST, logoutUserWorker)
}
