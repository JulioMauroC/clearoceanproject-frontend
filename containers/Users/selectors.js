import { createSelector } from 'reselect'
import _get from 'lodash/fp/get'
import { REDUCER_NAME } from './consts'

export const selectIsLoading = createSelector(
  (state) => state[ REDUCER_NAME ],
  _get('loading'),
)

export const selectErrorMessage = createSelector(
  (state) => state[ REDUCER_NAME ],
  _get('errorMessage'),
)

export const selectUserData = createSelector(
  (state) => state[ REDUCER_NAME ],
  _get('userData'),
)

export const selectUsersData = createSelector(
  (state) => state[ REDUCER_NAME ],
  _get('usersData'),
)

export const selectYachtUsersData = createSelector(
  (state) => state[ REDUCER_NAME ],
  _get('yachtUsersData'),
)

export const selectCurrentUserData = createSelector(
  (state) => state[ REDUCER_NAME ],
  _get('currentUserData'),
)

export const selectIsEmailVerified = createSelector(
  selectCurrentUserData,
  (userData) => userData?.isEmailVerified,
)
