import { connect } from 'react-redux'
import { compose } from 'recompose'
import { bindActionCreators } from 'redux'
import { createStructuredSelector } from 'reselect'

import {
  getCurrentUserRequest,
  getCurrentUserFailure,
  getCurrentUserSuccess,
  getUserRequest,
  getUserFailure,
  getUserSuccess,
  getUsersRequest,
  getUsersFailure,
  getUsersSuccess,
  editUserRequest,
  editUserFailure,
  editUserSuccess,
  editOtherUserRequest,
  deleteUserRequest,
  deleteCurrentUserRequest,
  inviteUserRequest,
  inviteUserFailure,
  inviteUserSuccess,
  getYachtUsersRequest,
} from './actions'

import {
  selectIsLoading,
  selectErrorMessage,
  selectUserData,
  selectUsersData,
  selectCurrentUserData,
  selectIsEmailVerified,
  selectYachtUsersData,
} from './selectors'

export const mapStateToProps = createStructuredSelector({
  isLoading: selectIsLoading,
  userData: selectUserData,
  yachtUsers: selectYachtUsersData,
  errorMessage: selectErrorMessage,
  usersData: selectUsersData,
  currentUserData: selectCurrentUserData,
  isEmailVerified: selectIsEmailVerified,
})

export const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    getCurrentUserRequest,
    getCurrentUserFailure,
    getCurrentUserSuccess,
    getUserRequest,
    getUserFailure,
    getUserSuccess,
    getUsersRequest,
    getUsersFailure,
    getUsersSuccess,
    editUserRequest,
    editUserFailure,
    editUserSuccess,
    editOtherUserRequest,
    deleteCurrentUserRequest,
    deleteUserRequest,
    inviteUserRequest,
    inviteUserFailure,
    inviteUserSuccess,
    getYachtUsersRequest,
  },
  dispatch,
)

export default compose(connect(mapStateToProps, mapDispatchToProps))
