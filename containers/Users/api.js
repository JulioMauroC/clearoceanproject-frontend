import axios from 'axios'

// axios.defaults.baseURL = 'http://localhost:8080/api'

// eslint-disable-next-line import/prefer-default-export
export const apiGetCurrentUser = (token) => {
  const config = {
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${ token }`,
    },
  }

  return axios.get('/users/current', config)
}

export const apiGetUsers = () => axios.get('/users/all')

export const apiGetYachtUsers = (token) => {
  const config = {
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${ token }`,
    },
  }

  return axios.get('/yacht/users', config)
}

export const apiDeleteUser = (id) => axios.delete(`/users/${ id }`)

export const apiDeleteCurrentUser = (token) => {
  const config = {
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${ token }`,
    },
  }

  return axios.delete('/users/current', config)
}

export const apiGetUser = (id) => axios.get(`/users/user/${ id }`)

export const apiInviteUser = (userData, token) => {
  const config = {
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${ token }`,
    },
  }
  return axios.post('/users/invite', userData, config)
}

export const apiEditUser = (userData, token) => {
  const dataToUpdate = { ...userData.updatedFields }
  dataToUpdate.userId = userData.userId

  const config = {
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${ token }`,
    },
  }

  let uri
  const formData = new FormData()

  if (dataToUpdate.userImage) {
    uri = dataToUpdate.userImage

    const fileType = uri.substring(uri.lastIndexOf('.') + 1)
    formData.append('profileImage', {
      uri,
      name: `photo.${ fileType }`,
      type: `image/${ fileType }`,
    })
  }

  if ('userId' in dataToUpdate) {
    formData.append('userId', dataToUpdate.userId)
  }

  if ('position' in dataToUpdate) {
    formData.append('position', dataToUpdate.position)
  }

  if ('isPrivateProfile' in dataToUpdate) {
    formData.append('isPrivateProfile', dataToUpdate.isPrivateProfile)
  }
  if ('firstName' in dataToUpdate) {
    formData.append('firstName', dataToUpdate.firstName)
  }
  if ('lastName' in dataToUpdate) {
    formData.append('lastName', dataToUpdate.lastName)
  }

  // formData.append('firstName', dataToUpdate.firstName)
  // formData.append('lastName', dataToUpdate.lastName)

  return axios.post('/users/updateUser', formData, config)
}

export const apiEditOtherUser = (userData, token) => {
  const dataToUpdate = { ...userData.updatedFields }
  dataToUpdate.userId = userData.userId

  const config = {
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${ token }`,
    },
  }

  let uri
  const formData = new FormData()

  if (dataToUpdate.userImage) {
    uri = dataToUpdate.userImage

    const fileType = uri.substring(uri.lastIndexOf('.') + 1)
    formData.append('profileImage', {
      uri,
      name: `photo.${ fileType }`,
      type: `image/${ fileType }`,
    })
  }

  if ('userId' in dataToUpdate) {
    formData.append('userId', dataToUpdate.userId)
  }

  if ('position' in dataToUpdate) {
    formData.append('position', dataToUpdate.position)
  }

  if ('isPrivateProfile' in dataToUpdate) {
    formData.append('isPrivateProfile', dataToUpdate.isPrivateProfile)
  }
  if ('firstName' in dataToUpdate) {
    formData.append('firstName', dataToUpdate.firstName)
  }
  if ('lastName' in dataToUpdate) {
    formData.append('lastName', dataToUpdate.lastName)
  }

  return axios.post('/users/updateOtherUser', formData, config)
}
