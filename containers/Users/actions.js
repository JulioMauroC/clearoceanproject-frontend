export const GET_CURRENT_USER_REQUEST = 'GET_CURRENT_USER_REQUEST'
export const GET_CURRENT_USER_FAILURE = 'GET_CURRENT_USER_FAILURE'
export const GET_CURRENT_USER_SUCCESS = 'GET_CURRENT_USER_SUCCESS'

export const GET_USER_REQUEST = 'GET_USER_REQUEST'
export const GET_USER_FAILURE = 'GET_USER_FAILURE'
export const GET_USER_SUCCESS = 'GET_USER_SUCCESS'

export const GET_USERS_REQUEST = 'GET_USERS_REQUEST'
export const GET_USERS_FAILURE = 'GET_USERS_FAILURE'
export const GET_USERS_SUCCESS = 'GET_USERS_SUCCESS'

export const GET_YACHT_USERS_REQUEST = 'GET_YACHT_USERS_REQUEST'
export const GET_YACHT_USERS_FAILURE = 'GET_YACHT_USERS_FAILURE'
export const GET_YACHT_USERS_SUCCESS = 'GET_YACHT_USERS_SUCCESS'

export const EDIT_USER_REQUEST = 'EDIT_USER_REQUEST'
export const EDIT_USER_FAILURE = 'EDIT_USER_FAILURE'
export const EDIT_USER_SUCCESS = 'EDIT_USER_SUCCESS'

export const EDIT_OTHER_USER_REQUEST = 'EDIT_OTHER_USER_REQUEST'
export const EDIT_OTHER_USER_FAILURE = 'EDIT_OTHER_USER_FAILURE'
export const EDIT_OTHER_USER_SUCCESS = 'EDIT_OTHER_USER_SUCCESS'

export const DELETE_CURRENT_USER_REQUEST = 'DELETE_CURRENT_USER_REQUEST'
export const DELETE_CURRENT_USER_FAILURE = 'DELETE_CURRENT_USER_FAILURE'
export const DELETE_CURRENT_USER_SUCCESS = 'DELETE_CURRENT_USER_SUCCESS'

export const DELETE_USER_REQUEST = 'DELETE_USER_REQUEST'
export const DELETE_USER_FAILURE = 'DELETE_USER_FAILURE'
export const DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS'

export const INVITE_USER_REQUEST = 'INVITE_USER_REQUEST'
export const INVITE_USER_FAILURE = 'INVITE_USER_FAILURE'
export const INVITE_USER_SUCCESS = 'INVITE_USER_SUCCESS'

export const LOGOUT_USER_REQUEST = 'LOGOUT_USER_REQUEST'
export const LOGOUT_USER_FAILURE = 'LOGOUT_USER_FAILURE'
export const LOGOUT_USER_SUCCESS = 'LOGOUT_USER_SUCCESS'

// --- GET CURRENT USER

export const getCurrentUserRequest = () => ({
  type: GET_CURRENT_USER_REQUEST,
})

export const getCurrentUserFailure = (errorMessage) => ({
  type: GET_CURRENT_USER_FAILURE,
  payload: errorMessage,
})

export const getCurrentUserSuccess = (userData) => ({
  type: GET_CURRENT_USER_SUCCESS,
  payload: userData,
})

// --- GET USER

export const getUserRequest = (userId) => ({
  type: GET_USER_REQUEST,
  payload: userId,
})

export const getUserFailure = (errorMessage) => ({
  type: GET_USER_FAILURE,
  payload: errorMessage,
})

export const getUserSuccess = (userData) => ({
  type: GET_USER_SUCCESS,
  payload: userData,
})

// --- GET USERS

export const getUsersRequest = () => ({
  type: GET_USERS_REQUEST,
})

export const getUsersFailure = (errorMessage) => ({
  type: GET_USERS_FAILURE,
  payload: errorMessage,
})

export const getUsersSuccess = (usersData) => ({
  type: GET_USERS_SUCCESS,
  payload: usersData,
})

// --- GET YACHT USERS

export const getYachtUsersRequest = () => ({
  type: GET_YACHT_USERS_REQUEST,
})

export const getYachtUsersFailure = (errorMessage) => ({
  type: GET_YACHT_USERS_FAILURE,
  payload: errorMessage,
})

export const getYachtUsersSuccess = (yachtUsersData) => ({
  type: GET_YACHT_USERS_SUCCESS,
  payload: yachtUsersData,
})

// --- EDIT USER

export const editUserRequest = (userId, updatedFields) => ({
  type: EDIT_USER_REQUEST,
  payload: { userId, updatedFields },
})

export const editUserFailure = (errorMessage) => ({
  type: EDIT_USER_FAILURE,
  payload: errorMessage,
})

export const editUserSuccess = (updatedUser) => ({
  type: EDIT_USER_SUCCESS,
  payload: updatedUser,
})

// --- EDIT OTHER USER

export const editOtherUserRequest = (userId, updatedFields) => ({
  type: EDIT_OTHER_USER_REQUEST,
  payload: { userId, updatedFields },
})

export const editOtherUserFailure = (errorMessage) => ({
  type: EDIT_OTHER_USER_FAILURE,
  payload: errorMessage,
})

export const editOtherUserSuccess = (updatedUser) => ({
  type: EDIT_OTHER_USER_SUCCESS,
  payload: updatedUser,
})

// --- DELETE CURRENT USER

export const deleteCurrentUserRequest = (userId) => ({
  type: DELETE_CURRENT_USER_REQUEST,
  payload: userId,
})

export const deleteCurrentUserFailure = (errorMessage) => ({
  type: DELETE_CURRENT_USER_FAILURE,
  payload: errorMessage,
})

export const deleteCurrentUserSuccess = (deletedUser, updatedUsers) => ({
  type: DELETE_CURRENT_USER_SUCCESS,
  payload: { deletedUser, updatedUsers },
})

// --- DELETE  USER

export const deleteUserRequest = (userId) => ({
  type: DELETE_USER_REQUEST,
  payload: userId,
})

export const deleteUserFailure = (errorMessage) => ({
  type: DELETE_USER_FAILURE,
  payload: errorMessage,
})

export const deleteUserSuccess = (deletedUser, updatedUsers) => ({
  type: DELETE_USER_SUCCESS,
  payload: { deletedUser, updatedUsers },
})

// --- INVITE USER

export const inviteUserRequest = (invitedUserData) => ({
  type: INVITE_USER_REQUEST,
  payload: invitedUserData,
})

export const inviteUserFailure = (errorMessage) => ({
  type: INVITE_USER_FAILURE,
  payload: errorMessage,
})

export const inviteUserSuccess = (invitedUserData) => ({
  type: INVITE_USER_SUCCESS,
  payload: invitedUserData,
})

// --- LOGOUT USER

export const logoutUserRequest = () => ({
  type: LOGOUT_USER_REQUEST,
})

export const logoutUserFailure = (errorMessage) => ({
  type: LOGOUT_USER_FAILURE,
  payload: errorMessage,
})

export const logoutUserSuccess = () => ({
  type: LOGOUT_USER_SUCCESS,
})
