import {
  GET_USER_REQUEST,
  GET_USER_FAILURE,
  GET_USER_SUCCESS,
  GET_CURRENT_USER_REQUEST,
  GET_CURRENT_USER_FAILURE,
  GET_CURRENT_USER_SUCCESS,
  GET_USERS_REQUEST,
  GET_USERS_FAILURE,
  GET_USERS_SUCCESS,
  GET_YACHT_USERS_REQUEST,
  GET_YACHT_USERS_FAILURE,
  GET_YACHT_USERS_SUCCESS,
  EDIT_USER_REQUEST,
  EDIT_USER_FAILURE,
  EDIT_USER_SUCCESS,
  EDIT_OTHER_USER_REQUEST,
  EDIT_OTHER_USER_FAILURE,
  EDIT_OTHER_USER_SUCCESS,
  DELETE_CURRENT_USER_REQUEST,
  DELETE_CURRENT_USER_FAILURE,
  DELETE_CURRENT_USER_SUCCESS,
  DELETE_USER_REQUEST,
  DELETE_USER_FAILURE,
  DELETE_USER_SUCCESS,
  INVITE_USER_REQUEST,
  INVITE_USER_FAILURE,
  INVITE_USER_SUCCESS,
  LOGOUT_USER_REQUEST,
  LOGOUT_USER_FAILURE,
  LOGOUT_USER_SUCCESS,
} from './actions'

const initialState = {
  loading: false,
  errorMessage: null,
  userData: null,
  yachtUsersData: null,
  currentUserData: null,
  usersData: null,
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    // --- GET CURRENT USER

    case GET_CURRENT_USER_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
      }
    }

    case GET_CURRENT_USER_FAILURE: {
      return {
        ...state,
        loading: false,
        currentUserData: null,
        errorMessage: action.payload.errorMessage,
      }
    }

    case GET_CURRENT_USER_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        currentUserData: action.payload,
      }
    }
    // --- GET USER

    case GET_USER_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        userData: action.payload.userData,
      }
    }

    case GET_USER_FAILURE: {
      return {
        ...state,
        loading: false,
        userData: null,
        errorMessage: action.payload.errorMessage,
      }
    }

    case GET_USER_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        userData: action.payload,
      }
    }
    // --- GET YACHT USERS

    case GET_YACHT_USERS_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        yachtUsersData: null,
      }
    }

    case GET_YACHT_USERS_FAILURE: {
      return {
        ...state,
        loading: false,
        usersData: null,
        errorMessage: action.payload.errorMessage,
      }
    }

    case GET_YACHT_USERS_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        yachtUsersData: action.payload,
      }
    }

    // --- GET USERS

    case GET_USERS_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        usersData: null,
      }
    }

    case GET_USERS_FAILURE: {
      return {
        ...state,
        loading: false,
        usersData: null,
        errorMessage: action.payload.errorMessage,
      }
    }

    case GET_USERS_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        usersData: action.payload,
      }
    }

    // --- EDIT USER

    case EDIT_USER_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        currentUserData: null,
      }
    }

    case EDIT_OTHER_USER_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        userData: null,
      }
    }

    case EDIT_USER_FAILURE: {
      return {
        ...state,
        loading: false,
        currentUserData: null,
        errorMessage: action.payload.errorMessage,
      }
    }

    case EDIT_OTHER_USER_FAILURE: {
      return {
        ...state,
        loading: false,
        userData: null,
        errorMessage: action.payload.errorMessage,
      }
    }

    case EDIT_OTHER_USER_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        userData: action.payload,
      }
    }

    case EDIT_USER_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        currentUserData: action.payload,
      }
    }

    // --- DELETE USER

    case DELETE_USER_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
      }
    }

    case DELETE_USER_FAILURE: {
      return {
        ...state,
        loading: false,
        usersData: null,
        errorMessage: action.payload.errorMessage,
      }
    }

    case DELETE_USER_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        usersData: action.payload,
      }
    }

    // --- DELETE CURRENT USER

    case DELETE_CURRENT_USER_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
      }
    }

    case DELETE_CURRENT_USER_FAILURE: {
      return {
        ...state,
        loading: false,
        usersData: null,
        errorMessage: action.payload.errorMessage,
      }
    }

    case DELETE_CURRENT_USER_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        usersData: action.payload,
      }
    }

    // --- INVITE USER

    case INVITE_USER_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        userData: action.payload.invitedUserData,
      }
    }

    case INVITE_USER_FAILURE: {
      return {
        ...state,
        loading: false,
        userData: null,
        errorMessage: action.payload.errorMessage,
      }
    }

    case INVITE_USER_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        userData: action.payload.invitedUserData,
      }
    }

    // --- LOGOUT USER

    case LOGOUT_USER_REQUEST: {
      return {
        ...state,
        loading: true,
      }
    }

    case LOGOUT_USER_FAILURE: {
      return {
        ...state,
        loading: false,
        errorMessage: action.payload.errorMessage,
      }
    }

    case LOGOUT_USER_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        userData: null,
        yachtUsersData: null,
        currentUserData: null,
        usersData: null,
      }
    }

    default:
      return state
  }
}
