import { REDUCER_NAME } from './consts'

import {
  CREATE_YACHT_REQUEST,
  CREATE_YACHT_FAILURE,
  CREATE_YACHT_SUCCESS,

} from './actions'

const initialState = {
  loading: false,
  errorMessage: null,
  yachtData: null,

}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_YACHT_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        yachtData: null,
      }
    }

    case CREATE_YACHT_FAILURE: {
      return {
        ...state,
        loading: false,
        errorMessage: action.payload.errorMessage,
        yachtData: action.payload,
      }
    }

    case CREATE_YACHT_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        yachtData: action.payload,
      }
    }

    default:
      return state
  }
}
