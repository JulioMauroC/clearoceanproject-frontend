import axios from 'axios'
import loadToken from 'utils/loadToken'
import setAuthToken from 'utils/setAuthToken'
import Base64 from 'utils/Base64'

// eslint-disable-next-line import/prefer-default-export
export const apiCreateYacht = async (yachtData) => {
  const {
    YachtFlagInput,
    YachtOfficialNumberInput,
    YachtImageInput,
    YachtNameInput,
    token,
  } = yachtData

  console.log('yachtData')
  console.log(yachtData)
  let uri
  const formData = new FormData()

  if (YachtImageInput) {
    uri = YachtImageInput.uri

    const fileType = uri.substring(uri.lastIndexOf('.') + 1)
    formData.append('yachtPhoto', {
      uri,
      name: `photo.${ fileType }`,
      type: `image/${ fileType }`,
    })
  }

  if (YachtOfficialNumberInput) {
    formData.append('officialNumber', YachtOfficialNumberInput)
  }

  formData.append('yachtName', YachtNameInput)
  formData.append('flag', YachtFlagInput)
  formData.append('token', token)

  // await formData.append('name', Date.now().toString()) // other param

  const config = {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
      Authorization: `Bearer ${ token }`,
    },
  }

  return axios.post('/yacht/new', formData, config)
}
