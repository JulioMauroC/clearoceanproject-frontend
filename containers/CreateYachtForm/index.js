import { connect, useStore } from 'react-redux'
import { compose, lifecycle } from 'recompose'
import { bindActionCreators } from 'redux'
import { reduxForm } from 'redux-form'
import { createStructuredSelector } from 'reselect'

import {
  createYachtRequest,
  createYachtFailure,
  createYachtSuccess,

} from './actions'

import { FORM_NAME, REDUCER_NAME } from './consts'

import { selectIsLoading, selectYachtData } from './selectors'

export const mapStateToProps = createStructuredSelector({
  isLoading: selectIsLoading,
  yachtData: selectYachtData,
})

export const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    createYachtRequest,
    createYachtFailure,
    createYachtSuccess,
  },
  dispatch,
)

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  // reduxForm({
  //   form: FORM_NAME,
  //   submitAsSideEffect: true,

  //   onSubmit: (values) => createYachtRequest(values),
  // }),

)
