import { takeLatest, call, put } from 'redux-saga/effects'
import { AsyncStorage } from 'react-native'
import * as RootNavigation from 'navigation/RootNavigation'

import {
  CREATE_YACHT_REQUEST,
  createYachtFailure,
  createYachtSuccess,
} from './actions'

import { apiCreateYacht } from './api'

function* registerWorker(action) {
  try {
    let token

    token = yield AsyncStorage.getItem('token')

    action.payload.token = token

    const apiResult = yield call(apiCreateYacht, action.payload)

    yield put(createYachtSuccess(apiResult.data))

    RootNavigation.navigate('CreateAdmin')
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(createYachtFailure(errorMessage))
  }
}

export default function* watcher() {
  yield takeLatest(CREATE_YACHT_REQUEST, registerWorker)
}
