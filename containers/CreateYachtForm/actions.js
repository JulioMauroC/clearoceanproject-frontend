export const CREATE_YACHT_REQUEST = 'CREATE_YACHT_REQUEST'
export const CREATE_YACHT_FAILURE = 'CREATE_YACHT_FAILURE'
export const CREATE_YACHT_SUCCESS = 'CREATE_YACHT_SUCCESS'

// Register actions
export const createYachtRequest = (yachtData) => ({
  type: CREATE_YACHT_REQUEST,
  payload: yachtData,
})

export const createYachtFailure = (errorMessage) => ({
  type: CREATE_YACHT_FAILURE,
  payload: errorMessage,
})

export const createYachtSuccess = (yachtData) => ({
  type: CREATE_YACHT_SUCCESS,
  payload: yachtData,
})
