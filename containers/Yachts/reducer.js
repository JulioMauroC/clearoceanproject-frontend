import {
  GET_YACHT_REQUEST,
  GET_YACHT_FAILURE,
  GET_YACHT_SUCCESS,
  GET_CURRENT_YACHT_REQUEST,
  GET_CURRENT_YACHT_FAILURE,
  GET_CURRENT_YACHT_SUCCESS,
  EDIT_YACHT_REQUEST,
  EDIT_YACHT_FAILURE,
  EDIT_YACHT_SUCCESS,
} from './actions'

const initialState = {
  loading: false,
  errorMessage: null,
  yachtData: null,
  currentYachtData: null,
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    // --- GET CURRENT YACHT

    case GET_CURRENT_YACHT_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
      }
    }

    case GET_CURRENT_YACHT_FAILURE: {
      return {
        ...state,
        loading: false,
        currentYachtData: null,
        errorMessage: action.payload.errorMessage,
      }
    }

    case GET_CURRENT_YACHT_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        currentYachtData: action.payload,
      }
    }
    // --- GET YACHT

    case GET_YACHT_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
      }
    }

    case GET_YACHT_FAILURE: {
      return {
        ...state,
        loading: false,
        yachtData: null,
        errorMessage: action.payload.errorMessage,
      }
    }

    case GET_YACHT_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        yachtData: action.payload,
      }
    }

    // --- EDIT YACHT

    case EDIT_YACHT_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        yachtData: action.payload,
      }
    }

    case EDIT_YACHT_FAILURE: {
      return {
        ...state,
        loading: false,
        yachtData: null,
        errorMessage: action.payload.errorMessage,
      }
    }

    case EDIT_YACHT_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        yachtData: action.payload,
      }
    }

    default:
      return state
  }
}
