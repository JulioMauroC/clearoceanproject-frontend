import axios from 'axios'

export const apiGetYacht = (token) => axios.get(`/yacht/${ token }`)

export const apiGetCurrentYacht = (token) => {
  const config = {
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${ token }`,
    },
  }

  return axios.get('/yacht/current', config)
}

export const apiUpdateYacht = (yachtData, token) => {
  const dataToUpdate = { ...yachtData.updatedFields }

  const config = {
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${ token }`,
    },
  }

  let uri
  const formData = new FormData()

  formData.append('yachtId', yachtData.yachtId)

  if (dataToUpdate.yachtImage) {
    uri = dataToUpdate.yachtImage

    const fileType = uri.substring(uri.lastIndexOf('.') + 1)
    formData.append('yachtPhoto', {
      uri,
      name: `photo.${ fileType }`,
      type: `image/${ fileType }`,
    })
  }
  if ('isPrivateProfile' in dataToUpdate) {
    formData.append('isPrivateProfile', dataToUpdate.isPrivateProfile)
  }

  if ('yachtOfficialNumber' in dataToUpdate) {
    formData.append('officialNumber', dataToUpdate.yachtOfficialNumber)
  }

  return axios.post('/yacht/updateYacht', formData, config)
}
