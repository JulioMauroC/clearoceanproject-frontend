import { takeLatest, call, put } from 'redux-saga/effects'
import { AsyncStorage } from 'react-native'
import * as RootNavigation from 'navigation/RootNavigation'

import { apiGetYacht, apiGetCurrentYacht, apiUpdateYacht } from './api'

import {
  GET_YACHT_REQUEST,
  GET_CURRENT_YACHT_REQUEST,
  EDIT_YACHT_REQUEST,
  getCurrentYachtRequest,
  getCurrentYachtFailure,
  getCurrentYachtSuccess,
  getYachtFailure,
  getYachtSuccess,
  editYachtFailure,
  editYachtSuccess,
} from './actions'

function* getCurrentYachtWorker(action) {
  try {
    const token = yield AsyncStorage.getItem('token')

    const apiResult = yield call(apiGetCurrentYacht, token)

    yield put(getCurrentYachtSuccess(apiResult.data))
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(getCurrentYachtFailure(errorMessage))
  }
}

function* getYachtWorker(action) {
  try {
    const token = yield AsyncStorage.getItem('token')

    const apiResult = yield call(apiGetYacht, token)

    yield put(getYachtSuccess(apiResult.data))
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(getYachtFailure(errorMessage))
  }
}

function* editYachtWorker(action) {
  try {
    const token = yield AsyncStorage.getItem('token')
    const apiResult = yield call(apiUpdateYacht, action.payload, token)

    yield put(editYachtSuccess(apiResult.data))
    yield put(getCurrentYachtRequest())
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(editYachtFailure(errorMessage))
  }
}

export default function* watcher() {
  yield takeLatest(GET_CURRENT_YACHT_REQUEST, getCurrentYachtWorker)
  yield takeLatest(GET_YACHT_REQUEST, getYachtWorker)
  yield takeLatest(EDIT_YACHT_REQUEST, editYachtWorker)
}
