export const GET_YACHT_REQUEST = 'GET_YACHT_REQUEST'
export const GET_YACHT_FAILURE = 'GET_YACHT_FAILURE'
export const GET_YACHT_SUCCESS = 'GET_YACHT_SUCCESS'

export const GET_CURRENT_YACHT_REQUEST = 'GET_CURRENT_YACHT_REQUEST'
export const GET_CURRENT_YACHT_FAILURE = 'GET_CURRENT_YACHT_FAILURE'
export const GET_CURRENT_YACHT_SUCCESS = 'GET_CURRENT_YACHT_SUCCESS'

export const EDIT_YACHT_REQUEST = 'EDIT_YACHT_REQUEST'
export const EDIT_YACHT_FAILURE = 'EDIT_YACHT_FAILURE'
export const EDIT_YACHT_SUCCESS = 'EDIT_YACHT_SUCCESS'

// --- GET CURRENT YACHT

export const getCurrentYachtRequest = () => ({
  type: GET_CURRENT_YACHT_REQUEST,

})

export const getCurrentYachtFailure = (errorMessage) => ({
  type: GET_CURRENT_YACHT_FAILURE,
  payload: errorMessage,
})

export const getCurrentYachtSuccess = (yachtData) => ({
  type: GET_CURRENT_YACHT_SUCCESS,
  payload: yachtData,
})

// --- GET YACHT

export const getYachtRequest = (yachtId) => ({
  type: GET_YACHT_REQUEST,
  payload: yachtId,
})

export const getYachtFailure = (errorMessage) => ({
  type: GET_YACHT_FAILURE,
  payload: errorMessage,
})

export const getYachtSuccess = (yachtData) => ({
  type: GET_YACHT_SUCCESS,
  payload: yachtData,
})

// --- EDIT YACHT

export const editYachtRequest = (yachtId, updatedFields) => ({
  type: EDIT_YACHT_REQUEST,
  payload: { yachtId, updatedFields },
})

export const editYachtFailure = (errorMessage) => ({
  type: EDIT_YACHT_FAILURE,
  payload: errorMessage,
})

export const editYachtSuccess = (updatedYacht) => ({
  type: EDIT_YACHT_SUCCESS,
  payload: updatedYacht,
})
