import { connect } from 'react-redux'
import { compose } from 'recompose'
import { bindActionCreators } from 'redux'
import { createStructuredSelector } from 'reselect'

import {
  getCurrentYachtRequest,
  getCurrentYachtFailure,
  getCurrentYachtSuccess,
  getYachtRequest,
  getYachtFailure,
  getYachtSuccess,
  editYachtRequest,
  editYachtFailure,
  editYachtSuccess,
} from './actions'

import {
  selectIsLoading,
  selectErrorMessage,
  selectYachtData,
  selectCurrentYachtData,
} from './selectors'

export const mapStateToProps = createStructuredSelector({
  isLoading: selectIsLoading,
  errorMessage: selectErrorMessage,
  yachtData: selectYachtData,
  currentYachtData: selectCurrentYachtData,
})

export const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    getCurrentYachtRequest,
    getCurrentYachtFailure,
    getCurrentYachtSuccess,
    getYachtRequest,
    getYachtFailure,
    getYachtSuccess,
    editYachtRequest,
    editYachtFailure,
    editYachtSuccess,
  },
  dispatch,
)

export default compose(connect(mapStateToProps, mapDispatchToProps))
