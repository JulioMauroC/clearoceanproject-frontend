import {
  JOIN_YACHT_REQUEST,
  JOIN_YACHT_FAILURE,
  JOIN_YACHT_SUCCESS,
  USER_INVITED_REQUEST,
  USER_INVITED_FAILURE,
  USER_INVITED_SUCCESS,
} from './actions'

const initialState = {
  loading: false,
  errorMessage: null,
  isUserInvited: null,
  yachtUniqueName: null,
  userData: null,
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case JOIN_YACHT_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        userData: null,
      }
    }

    case JOIN_YACHT_FAILURE: {
      return {
        ...state,
        loading: false,
        errorMessage: action.payload.errorMessage,
        userData: null,
      }
    }

    case JOIN_YACHT_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        // userData: action.payload,
      }
    }

    //---

    case USER_INVITED_REQUEST: {
      return {
        ...state,
        loading: true,
        errorMessage: null,
        isUserInvited: null,
        yachtUniqueName: null,
      }
    }

    case USER_INVITED_FAILURE: {
      return {
        ...state,
        loading: false,
        errorMessage: action.payload.errorMessage,
        isUserInvited: null,
        yachtUniqueName: null,
      }
    }

    case USER_INVITED_SUCCESS: {
      return {
        ...state,
        loading: false,
        errorMessage: null,
        isUserInvited: action.payload.isUserInvited,
        yachtUniqueName: action.payload.yachtUniqueName,
      }
    }

    default:
      return state
  }
}
