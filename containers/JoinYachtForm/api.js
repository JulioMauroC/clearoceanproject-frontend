import axios from 'axios'
import { AsyncStorage } from 'react-native'

// eslint-disable-next-line import/prefer-default-export
export const apiUserInvited = async (token) => {
  const config = {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
      Authorization: `Bearer ${ token }`,
    },
  }

  return axios.get('/users/isUserInvited', config)
}

// eslint-disable-next-line import/prefer-default-export
export const apijoinYacht = async (userData, token) => {
  const { UserImageInput, UserPositionInput, yachtUniqueName } = userData

  let uri
  const formData = new FormData()

  if (UserImageInput) {
    uri = UserImageInput.uri

    const fileType = uri.substring(uri.lastIndexOf('.') + 1)
    formData.append('profileImage', {
      uri,
      name: `photo.${ fileType }`,
      type: `image/${ fileType }`,
    })
  }

  formData.append('position', UserPositionInput)
  formData.append('yachtUniqueName', yachtUniqueName)

  // await formData.append('name', Date.now().toString()) // other param

  const config = {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
      Authorization: `Bearer ${ token }`,
    },
  }

  axios.post('/users/joinYacht', formData, config)
}
