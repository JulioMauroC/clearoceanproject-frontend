export const JOIN_YACHT_REQUEST = 'JOIN_YACHT_REQUEST'
export const JOIN_YACHT_FAILURE = 'JOIN_YACHT_FAILURE'
export const JOIN_YACHT_SUCCESS = 'JOIN_YACHT_SUCCESS'

export const USER_INVITED_REQUEST = 'USER_INVITED_REQUEST'
export const USER_INVITED_FAILURE = 'USER_INVITED_FAILURE'
export const USER_INVITED_SUCCESS = 'USER_INVITED_SUCCESS'

export const userInvitedRequest = () => ({
  type: USER_INVITED_REQUEST,
  // payload: email,
})

export const userInvitedFailure = (errorMessage) => ({
  type: USER_INVITED_FAILURE,
  payload: errorMessage,
})

export const userInvitedSuccess = (userData) => ({
  type: USER_INVITED_SUCCESS,
  payload: userData,
})

//
export const joinYachtRequest = (userData) => ({
  type: JOIN_YACHT_REQUEST,
  payload: userData,
})

export const joinYachtFailure = (errorMessage) => ({
  type: JOIN_YACHT_FAILURE,
  payload: errorMessage,
})

export const joinYachtSuccess = (userData) => ({
  type: JOIN_YACHT_SUCCESS,
  // payload: userData,
})
