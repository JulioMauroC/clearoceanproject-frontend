import {
  takeLatest, call, put, delay,
} from 'redux-saga/effects'
import { AsyncStorage } from 'react-native'
import * as RootNavigation from 'navigation/RootNavigation'

import getCurrentUserRequest from 'containers/Users/actions'
import {
  JOIN_YACHT_REQUEST,
  USER_INVITED_REQUEST,
  userInvitedSuccess,
  userInvitedFailure,
  joinYachtFailure,
  joinYachtSuccess,
} from './actions'

import { apijoinYacht, apiUserInvited } from './api'

function* userInvitedWorker() {
  try {
    const token = yield AsyncStorage.getItem('token').then((res) => res)

    const apiResult = yield call(apiUserInvited, token)
    console.log('from saga!!!')
    console.log(apiResult)

    yield put(userInvitedSuccess(apiResult.data))

    // ? for now save on uniquename on redux?would be better on params?
    // yachtUniqueName: apiResult.data,
    // yachtUniqueName: 'HugoBEL',
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(userInvitedFailure(errorMessage))
  }
}

//---

function* joinYachtWorker(action) {
  try {
    const token = yield AsyncStorage.getItem('token')

    const apiResult = yield call(apijoinYacht, action.payload, token)
    yield delay(2000)
    yield put(joinYachtSuccess(apiResult))
    // yield put(getCurrentUserRequest)

    RootNavigation.navigate('MainTab')
  } catch (error) {
    const errorMessage = yield error.toJSON().message
    yield put(joinYachtFailure(errorMessage))
  }
}

export default function* watcher() {
  yield takeLatest(USER_INVITED_REQUEST, userInvitedWorker)
  yield takeLatest(JOIN_YACHT_REQUEST, joinYachtWorker)
}
