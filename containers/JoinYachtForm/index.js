import { connect } from 'react-redux'
import { compose } from 'recompose'
import { bindActionCreators } from 'redux'

import { createStructuredSelector } from 'reselect'

import {
  joinYachtRequest,
  joinYachtFailure,
  joinYachtSuccess,
  userInvitedRequest,
  userInvitedFailure,
  userInvitedSuccess,
} from './actions'

import {
  selectIsLoading,
  selectAdminData,
  selectIsUserInvited,
  selectYachtUniqueName,
} from './selectors'

export const mapStateToProps = createStructuredSelector({
  isLoading: selectIsLoading,
  adminData: selectAdminData,
  isUserInvited: selectIsUserInvited,
  yachtUniqueName: selectYachtUniqueName,
})

export const mapDispatchToProps = (dispatch) => bindActionCreators(
  {
    joinYachtRequest,
    joinYachtFailure,
    joinYachtSuccess,
    userInvitedRequest,
    userInvitedFailure,
    userInvitedSuccess,
  },
  dispatch,
)

export default compose(connect(mapStateToProps, mapDispatchToProps))
