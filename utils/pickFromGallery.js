import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'
import { Alert } from 'react-native'

const pickFromGallery = async () => {
  console.log('pickFromGallery started')
  const { granted } = await Permissions.askAsync(Permissions.CAMERA)
  if (granted) {
    const data = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [ 4, 3 ],
      quality: 1,
      // base64: true,
    })

    return data
  }
  Alert.alert('you need to give permission')
  console.log('not granted')
}

export default pickFromGallery
