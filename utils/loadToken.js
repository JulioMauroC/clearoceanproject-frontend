import { AsyncStorage } from 'react-native'

const loadToken = async () => {
  try {
    const token = await AsyncStorage.getItem('token')

    return token
  } catch (error) {
    console.log('Load token error: ', error)
  }
  return token
}

export default loadToken
