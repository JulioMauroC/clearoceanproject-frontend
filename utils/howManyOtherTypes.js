import EntryTypes from 'consts/EntryTypes'

const howManyOtherTypes = (entries) => {
  if (!entries) {
    return 0
  }
  let counter = 0
  entries.forEach((entry) => {
    entry.types.forEach((type) => {
      if (!EntryTypes.map((item) => item.label).includes(type)) {
        counter++
      }
    })
  })
  return counter
}

export default howManyOtherTypes
