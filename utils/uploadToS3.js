export const getBlob = async (fileUri) => {
  const resp = await fetch(fileUri)
  const imageBody = await resp.blob()
  return imageBody
}
