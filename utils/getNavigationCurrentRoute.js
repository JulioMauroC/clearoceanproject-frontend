const getNavigationCurrentRoute = (route) => {
  if (route[ 0 ].state) {
    return route[ 0 ].state.routes[ route[ 0 ].state.routes.length - 1 ].name
  }

  return route[ 0 ].name
}

export default getNavigationCurrentRoute
