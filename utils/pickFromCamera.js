import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'
import { Camera } from 'expo-camera'
import { Alert } from 'react-native'

const pickFromCamera = async () => {
  const { granted } = await Permissions.askAsync(Permissions.CAMERA)
  if (granted) {
    const data = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [ 1, 1 ],
      quality: 0.5,
    })

    return data
  }
  Alert.alert('you need to give permission')
  console.log('not granted')
}

export default pickFromCamera
