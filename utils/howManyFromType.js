const howManyFromType = (entryName, entries) => {
  if (!entries) {
    return 0
  }
  const entryNameLength = entries.filter((item) => item.types.includes(entryName))

  return entryNameLength.length
}

export default howManyFromType
