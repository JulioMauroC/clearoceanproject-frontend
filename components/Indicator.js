import * as React from 'react'
import { Animated, Dimensions } from 'react-native'
import styled from 'styled-components/native'

const { width } = Dimensions.get('screen')

const IndicatorWrapper = styled.View`
  /* position: absolute; */
  bottom: 5px;
  flex-direction: row;
  /* border:3px solid red; */
  z-index:99999;
  padding-top:${ (props) => props.theme.dimensions.paddingLarge };


`

const Indicator = ({ data, scrollX }) => (
  <IndicatorWrapper>

    {data.map((_, i) => {
      const inputRange = [ (i - 1) * width, i * width, (i + 1) * width ]
      const scale = scrollX.interpolate({
        inputRange,
        outputRange: [ 0.8, 1.4, 0.8 ],
        extrapolate: 'clamp',
      })

      const opacity = scrollX.interpolate({
        inputRange,
        outputRange: [ 0.4, 0.9, 0.6 ],
        extrapolate: 'clamp',
      })

      return (
        <Animated.View
          key={ `indicator-${ i }` }
          style={ {
            height: 10,
            width: 10,
            borderRadius: 5,
            backgroundColor: 'grey',
            opacity,
            margin: 10,
            transform: [ { scale } ],
          } }
        />
      )
    })}
  </IndicatorWrapper>
)

export default Indicator
