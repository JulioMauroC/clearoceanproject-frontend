import React from 'react'
import styled from 'styled-components/native'

const FieldLabelsWrapper = styled.View`
  margin-bottom: ${ (props) => (props.fieldSubLabel ? '20px' : '0') };
`

const FieldLabel = styled.Text`
  color: white;
  font-size: 16px;
  text-align: left;
  opacity: 0.9;
`

const FieldSubLabel = styled.Text`
  color: white;
  font-size: 12px;
  text-align: left;
  opacity: 0.7;
  padding-top: 5px;
`

const FieldLabels = (props) => {
  const { fieldLabel, fieldSubLabel } = props
  return (
    <FieldLabelsWrapper fieldSubLabel={ fieldSubLabel }>
      <FieldLabel>{fieldLabel}</FieldLabel>
      <FieldSubLabel>{fieldSubLabel}</FieldSubLabel>
    </FieldLabelsWrapper>
  )
}

export default FieldLabels
