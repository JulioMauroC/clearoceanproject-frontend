import React from 'react'
import styled from 'styled-components/native'
import Icon from 'components/Icon'
import Color from 'color'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

const PrevNextWrapper = styled.View`
  width: 100%;
  position: absolute;
  flex-direction: row;
  justify-content: space-between;
  bottom: ${ (props) => props.theme.dimensions.prevNextBottomDistance };
  padding-left: ${ (props) => props.theme.dimensions.paddingMedLarge };
  padding-right: ${ (props) => props.theme.dimensions.paddingMedLarge };
  z-index: 0;
`

const PrevNextText = styled.Text`
  color: ${ (props) => props.color };
  font-family: 'Lato-Regular';
  font-size: 14;
`

const ButtonWrapper = styled.TouchableOpacity``

const ButtonContainer = styled.View`
  opacity: ${ (props) => (props.hidden ? 0 : 1) };
`

const PrevNext = (props) => {
  const {
    currentIndex,
    numOfSteps,
    scrollToPrev,
    scrollToNext,
    fieldColor,
    required,
    values,
    fieldName,
    errors,
    touched,
  } = props

  const [ isNextDisabled, setIsNextDisabled ] = React.useState(required)

  React.useEffect(() => {
    if (required) {
      !touched[ fieldName ] || errors[ fieldName ]
        ? setIsNextDisabled(true)
        : setIsNextDisabled(false)
    }
  }, [ values, touched, errors ])

  const lastIndex = numOfSteps - 1

  const disabledArrowColor = Color(fieldColor).lighten(0.6).hex()
  const enabledArrowColor = Color(fieldColor).lighten(2).hex()

  return (
    <PrevNextWrapper>
      <ButtonWrapper
        disabled={ currentIndex === 0 }
        onPress={ () => scrollToPrev(currentIndex) }
      >
        <ButtonContainer hidden={ currentIndex === 0 }>
          <Icon
            icon="long-arrow-alt-right"
            color={ enabledArrowColor }
            size={ widthToDp('6%') }
            transform={ { rotate: 180 } }
          />

          <PrevNextText color={ enabledArrowColor }>Back</PrevNextText>
        </ButtonContainer>
      </ButtonWrapper>

      <ButtonWrapper
        disabled={ isNextDisabled }
        onPress={ () => scrollToNext(currentIndex) }
      >
        <ButtonContainer hidden={ currentIndex === lastIndex }>
          <Icon
            icon="long-arrow-alt-right"
            color={ isNextDisabled ? disabledArrowColor : enabledArrowColor }
            size={ widthToDp('6%') }
          />

          <PrevNextText
            color={ isNextDisabled ? disabledArrowColor : enabledArrowColor }
          >
            Next
          </PrevNextText>
        </ButtonContainer>
      </ButtonWrapper>
    </PrevNextWrapper>
  )
}

export default PrevNext
