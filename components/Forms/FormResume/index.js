import React from 'react'

import styled from 'styled-components/native'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import ResumeDisplay from './ResumeDisplay'

const FormResumeWrapper = styled.View`
  width: ${ widthToDp('60%') };
  height: ${ widthToDp('20%') };
  position: absolute;
  top: ${ heightToDp('40%') };
  z-index: 1;
`
const FormResume = (props) => {
  const {
    isConfirmationScreen,
    touched,
    errors,
    isSubmitting,
    values,
    formConfigData,
    scrollToIndex,
  } = props

  return isConfirmationScreen ? (
    <FormResumeWrapper>
      <ResumeDisplay
        values={ values }
        formConfigData={ formConfigData }
        scrollToIndex={ scrollToIndex }
      />
    </FormResumeWrapper>
  ) : null
}

export default FormResume
