import React from 'react'
import styled from 'styled-components/native'
import Avatar from 'components/Avatar'
import Icon from 'components/Icon'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

const AvatarWrapper = styled.View`
  justify-content: flex-start;
  align-items: flex-start;
`

const FieldText = styled.Text`
  text-align: left;
  font-size: 18px;
  font-family: 'Lato-Bold';
  color: white;
  /* opacity: 0.65; */
`

const FormResumeImage = (props) => {
  const {
    type, isArrayField, values, fieldName,
  } = props

  return !isArrayField ? (
    <AvatarWrapper>
      {values[ fieldName ] ? (
        <Avatar
          size={ widthToDp('8%') }
          imgUrl={ values[ fieldName ].uri }
        />
      ) : (
        <Icon
          icon="empty-set"
          size={ widthToDp('6%') }
          color="black"
          style={ { opacity: 0.75 } }
        />
      )}
    </AvatarWrapper>
  ) : (
    <FieldText>
      {values[ fieldName ].length}
      {' '}
      images uploaded
    </FieldText>
  )
}

export default FormResumeImage
