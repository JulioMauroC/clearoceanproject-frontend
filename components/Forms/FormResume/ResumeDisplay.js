import React from 'react'
import styled from 'styled-components/native'
import { TouchableOpacity } from 'react-native-gesture-handler'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import Icon from 'components/Icon'
import { View, Text } from 'react-native'

import FormResumeImage from './FormResumeImage'

const ResumeDisplayWrapper = styled.View`
  /* flex: 0.3; */
  position: relative;
  align-items: flex-start;
  justify-content: flex-start;
  padding-top: 10px;
  width: 100%;
`

const FieldTitleText = styled.Text`
  text-align: left;
  font-size: 14px;
  padding: 2px;
  color: white;
`

const FieldText = styled.Text`
  text-align: left;
  font-size: 16px;
  font-family: 'Lato-Bold';
  color: white;
  /* opacity: 0.65; */
`

const FieldWrapper = styled.View`
  /* border:1px solid red; */
  width: 100%;
  margin-bottom: 10px;
  padding-bottom: 5px;
  /* border: 3px solid red; */
  position: relative;
`

const ResumeDisplay = (props) => {
  const { values, formConfigData, scrollToIndex } = props

  return (
    <ResumeDisplayWrapper>
      {formConfigData.map((field, index) => {
        const { fieldName, isConfirmationScreen, type } = field
        const isArrayField = Array.isArray(values[ fieldName ])

        console.log('values[ fieldName ]')
        console.log(fieldName)

        return (
          !isConfirmationScreen
          && values[ fieldName ] !== undefined > 0 && (
            <FieldWrapper
              key={ fieldName }
              style={ {
                borderBottomColor: 'white',
                borderBottomWidth: 1,
              } }
            >
              <FieldTitleText>{field.formResumeTitle}</FieldTitleText>

              {type === 'image' ? (
                <FormResumeImage
                  type={ type }
                  isArrayField={ isArrayField }
                  values={ values }
                  fieldName={ fieldName }
                />
              ) : (
                <View style={ { alignItems: 'flex-start' } }>
                  {values[ fieldName ] ? (
                    isArrayField ? (
                      fieldName === 'EntryTypesInput' ? (
                        <FieldText>
                          {values[ fieldName ].length < 4
                            ? values[ fieldName ].map((item) => `${ item }, `)
                            : `${ values[ fieldName ]
                              .slice(0, 3)
                              .map((item) => `${ item }, `) } and ${
                              values[ fieldName ].length - 3
                            } more`}
                        </FieldText>
                      ) : (
                        <FieldText>
                          {values[ fieldName ].map((item) => `${ item }, `)}
                        </FieldText>
                      )
                    ) : (
                      <FieldText>{values[ fieldName ]}</FieldText>
                    )
                  ) : (
                    <Icon
                      icon="empty-set"
                      size={ widthToDp('6%') }
                      color="black"
                      style={ { opacity: 0.75 } }
                    />
                  )}
                </View>
              )}

              <TouchableOpacity
                containerStyle={ {
                  zIndex: 99999999,
                  opacity: 0.95,
                  position: 'absolute',

                  top: '50%',
                  right: '0%',
                } }
                onPress={ () => scrollToIndex(index) }
              >
                <Icon icon="pen" size={ 14 } color="white" />
              </TouchableOpacity>
            </FieldWrapper>
          )
        )
      })}
    </ResumeDisplayWrapper>
  )
}

export default ResumeDisplay
