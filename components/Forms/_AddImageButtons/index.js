import React from 'react'
import {
  StyleSheet,
  Animated,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Text,
} from 'react-native'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import styled from 'styled-components/native'

import Color from 'color'

import MainButton from './MainButton'
import TakePictureButton from './TakePictureButton'

const referenceDimension = heightToDp('8%')

const AddImageButtonsWrapper = styled.View`
  /* flex: 1; */
  position: absolute;
  align-items: center;
  justify-content:center;
  flex-direction:row;
  /* border: 3px solid white; */
  bottom: ${ heightToDp('5%') };
  z-index:99999;
  border:3px solid red;
`

const styles = StyleSheet.create({
  mainButton: {

    position: 'absolute',
    zIndex: 999999,
    bottom: -heightToDp('5%'),
    alignItems: 'center',
    justifyContent: 'center',
    width: referenceDimension,
    height: referenceDimension,
    borderRadius: referenceDimension / 2,
    shadowRadius: 10,
    shadowColor: 'black',
    shadowOpacity: 0.3,
    shadowOffset: { height: 10 },
  },
})

const CameraButtonsText = styled.Text`
  /* position: absolute; */
  color: white;
  font-size: 11px;
  padding: 12px;
  background-color: transparent;
  opacity: 0.75;
`

const AddImageButtons = (props) => {
  const {
    fieldColor, defaultText, handleCamera, handleGallery,
  } = props
  const animation = React.useMemo(() => new Animated.Value(0), [])

  const iconBackgroundColor = Color(fieldColor).lighten(0.4).hex()

  const [ showButtons, toggleShowButtons ] = React.useState(false)

  const toggleMenu = () => {
    toggleShowButtons(!showButtons)
  }

  React.useEffect(() => {
    const toValue = showButtons ? 1 : 0

    Animated.spring(animation, {
      toValue,
      friction: 5,
      useNativeDriver: true,
    }).start()
  }, [ showButtons, animation ])

  const takePictureIconStyle = {
    transform: [
      { scale: animation },
      {
        translateY: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ 0, -referenceDimension / 10 ],
        }),
      },
      {
        translateX: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ 0, -referenceDimension / 1.5 ],
        }),
      },
    ],
  }

  const uploadPictureIconStyle = {
    transform: [
      { scale: animation },
      {
        translateY: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ 0, -referenceDimension / 10 ],
        }),
      },
      {
        translateX: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ 0, referenceDimension / 1.5 ],
        }),
      },
    ],
  }

  const rotation = {
    transform: [
      {
        rotate: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ '0deg', '45deg' ],
        }),
      },
    ],
  }

  return (
    <AddImageButtonsWrapper>
      <TouchableOpacity
        onPress={ handleGallery }
        style={ {
          width: referenceDimension,
          height: referenceDimension,
        } }
      >
        <Animated.View
          style={ [
            {
              alignItems: 'center',
              justifyContent: 'center',
              // position: 'absolute',
              // left: '50%',
            },
            takePictureIconStyle,
          ] }
        >
          <TakePictureButton fieldColor={ fieldColor } buttonIcon="arrow-up" />
          <CameraButtonsText>Upload Picture</CameraButtonsText>
        </Animated.View>
      </TouchableOpacity>
      <TouchableOpacity onPress={ handleCamera }>
        <Animated.View
          style={ [
            {
              alignItems: 'center',
              justifyContent: 'center',
              // position: 'absolute',
              // left: '50%',
            },
            uploadPictureIconStyle,
          ] }
        >
          <TakePictureButton fieldColor={ fieldColor } buttonIcon="camera-home" />
          <CameraButtonsText>Take Picture</CameraButtonsText>
        </Animated.View>
      </TouchableOpacity>

      <TouchableOpacity
        hitSlop={ {
          top: 20, bottom: 20, left: 50, right: 50,
        } }
        onPress={ toggleMenu }
        style={ {
          borderColor: 'red',
          borderWidth: 4,
          padding: 20,
          position: 'absolute',
        } }
      >
        <Animated.View
          style={ [
            styles.mainButton,
            { backgroundColor: iconBackgroundColor },
            {
              borderColor: 'green',
              borderWidth: 1,
              padding: 20,
            },

            rotation,
          ] }
        >
          <MainButton buttonIcon="plus" buttonSize={ referenceDimension / 2 } />
        </Animated.View>
      </TouchableOpacity>
    </AddImageButtonsWrapper>
  )
}

export default AddImageButtons
