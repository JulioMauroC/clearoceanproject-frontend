import React from 'react'
import styled from 'styled-components/native'
import { COLORS } from 'consts'
import Icon from 'components/Icon'

const CameraButtonsIconWrapper = styled.View`
  width: ${ (props) => (props.size ? props.size : 45) };
  height: ${ (props) => (props.size ? props.size : 45) };
  border-radius: 25px;
  /* background-color: ${ (props) => (props.iconBackgroundColor ? props.iconBackgroundColor : COLORS.PURPLE) }; */
  justify-content: center;
  align-items: center;

`

const MainButton = (props) => {
  const { buttonIcon, buttonSize } = props

  return (
    <CameraButtonsIconWrapper
      // iconBackgroundColor="green"
      // iconOutlineColor="green"
      size={ buttonSize }
      style={ {
        shadowColor: 'black',
        shadowOffset: {
          width: 0,
          height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,

        elevation: 7,
      } }
    >
      <Icon icon={ buttonIcon } color="white" size={ buttonSize } />
    </CameraButtonsIconWrapper>
  )
}

export default MainButton
