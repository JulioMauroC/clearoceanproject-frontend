import React from 'react'
import styled from 'styled-components'
import { COLORS } from 'consts'
import Icon from 'components/Icon'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

const ImagePreviewWrapper = styled.View`
  width:${ widthToDp('25%') };
  height: ${ widthToDp('25%') };
  border-radius: 20px;
  box-shadow: 1px 2px 10px rgba(115, 73, 0, 0.4);
  position: relative;
`

const IconButton = styled.TouchableOpacity`
  position: absolute;
  top: 0;
  right: 0;
  z-index: 20000;
  background-color: white;
  border-radius: 10px;
  transform: translate(5px, -5px);
`

const ImagePreviewImage = styled.Image`
  width: 100%;
  height: 100%;
  border-radius: 20px;
  z-index: 10000;
`

const ImagePreview = (props) => {
  const { previewImage, fieldColor, action } = props

  return (
    <ImagePreviewWrapper>
      <IconButton onPress={ action }>
        <Icon icon="times-circle" color={ COLORS.RED } size={ 16 } />
      </IconButton>

      <ImagePreviewImage source={ { uri: previewImage } } resizeMode="cover" />
    </ImagePreviewWrapper>
  )
}

export default ImagePreview
