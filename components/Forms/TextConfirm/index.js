import React from 'react'
import styled from 'styled-components/native'
import {

  Dimensions,
  Text,

} from 'react-native'
import { COLORS } from 'consts'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faCheck } from '@fortawesome/free-solid-svg-icons'
import Color from 'color'

const { width } = Dimensions.get('screen')

const TextConfirmWrapper = styled.View`

  background-color: ${ (props) => (props.backgroundColor ? props.backgroundColor : 'white') };
  box-shadow: 0px 1px 6px ${ (props) => props.borderColor };
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  /* width: ${ width * 0.75 }px; */
  width:auto;
  height: 40px;
  border-radius: 5px;
`
const IconWrapper = styled.TouchableOpacity`
  border: 1px solid ${ (props) => props.borderColor };
  height: 44px;
  width: 44px;
  border-radius: 24px;
  background-color: ${ (props) => (props.valid === null ? 'transparent' : 'white') };
  justify-content: center;
  align-items: center;
`

const TextConfirm = (props) => {
  const {

    fieldColor,
    currentIndex,
    scrollToNext,
    valid,
    value,
  } = props

  const modifiedBackgroundColor = Color(fieldColor).lighten(0.6)

  return (
    <TextConfirmWrapper
      borderColor={ COLORS.GREEN }
      backgroundColor={ modifiedBackgroundColor }
    >
      <Text
        underlineColorAndroid="transparent"
        style={ {
          width: '75%',
          paddingLeft: 20,
          color: 'white',
          opacity: 1,
          fontFamily: 'Lato-Bold',
          fontSize: 20,
        } }
      >
        {value}
      </Text>
      {/* <IconWrapper
        borderColor={ COLORS.GREEN }
        valid={ valid }
        onPress={ () => {
          scrollToNext(currentIndex )
        } }
      >
        <FontAwesomeIcon
          icon={ faCheck }
          color={ COLORS.GREEN }
          size={ 20 }

        />
      </IconWrapper> */}
    </TextConfirmWrapper>
  )
}

export default TextConfirm
