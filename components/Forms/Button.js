import React from 'react'

import { Dimensions } from 'react-native'

import styled from 'styled-components'

const { width } = Dimensions.get('screen')

const ButtonWrapper = styled.TouchableOpacity`
  border: 1.5px solid green;
  background-color: white;
  box-shadow: 0px 1px 6px rgba(211, 23, 44, 0.2);
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: ${ width * 0.5 }px;
  height: 40px;
  border-radius: 50px;
  justify-content: center;
  align-items: center;
  /* margin-top: auto; */
  margin-bottom:10px;
  
`

const ButtonText = styled.Text`
  color: green;
  font-size: 18px;
  font-family: 'Lato-Bold';
  text-align: center;
`

const Button = (props) => {
  const {
    text, action, handleSubmit, isConfirmationScreen, isFormValid,
  } = props

  return isConfirmationScreen && isFormValid ? (
    <ButtonWrapper onPress={ handleSubmit }>
      <ButtonText>{text}</ButtonText>
    </ButtonWrapper>
  ) : null
}

export default Button
