import React from 'react'
import { Modal, Text, TouchableOpacity } from 'react-native'
import styled from 'styled-components/native'
import Icon from 'components/Icon'
import Color from 'color'
import { Picker } from '@react-native-picker/picker'

const InnerModal = styled.View`
  flex: 1;
  align-items: center;
  justify-content: flex-end;
  border-radius: 30px;
`

const PickerWrapper = styled.View`
  height: 400px;
  width: 100%;
  background-color: ${ (props) => props.backgroundColor };
  border-top-left-radius: 60px;
  border-top-right-radius: 60px;
`
const Header = styled.View`
  justify-content: space-around;
  flex-direction: row;
  align-items: center;
  background-color: transparent;
  padding-top: 40px;
  border-radius: 100px;
`

const PickerModalIOS = ({

  values,
  title,
  backgroundColor,
  setPickerValue,
  pickerValue,
  setFieldValue,
  fieldName,
  setFieldTouched,
  modalVisible,
  setModalVisible,
}) => {
  const iconColor = Color(backgroundColor).lighten(2).hex()
  const modalBackgroundColor = Color(backgroundColor).lighten(0.08).hex()

  return (
    <>
      <Modal animated transparent visible={ modalVisible } animationType="slide">
        <InnerModal>
          <PickerWrapper backgroundColor={ modalBackgroundColor }>
            <Header backgroundColor={ modalBackgroundColor }>
              <TouchableOpacity onPress={ () => setModalVisible(false) }>
                <Icon icon="times" color={ iconColor } size={ 26 } />
              </TouchableOpacity>

              <Text>{title || ''}</Text>
              <TouchableOpacity
                onPress={ async () => {
                  await setFieldTouched(fieldName)
                  await setFieldValue(fieldName, pickerValue)
                  await setModalVisible(false)
                } }
              >
                <Icon
                  icon="check"
                  color={ iconColor }
                  size={ 26 }
                  opacity={ pickerValue === '' ? 0.4 : 1 }
                />
              </TouchableOpacity>
            </Header>
            <Picker
              selectedValue={ pickerValue }
              onValueChange={ (itemValue, index) => {
                if (index !== 0) {
                  setPickerValue(itemValue)
                }
              } }
              itemStyle={ { color: 'white' } }
            >
              {values.map((value, index) => (
                <Picker.Item
                  key={ value.label }
                  value={ value.label }
                  label={
                    `${ value.label } ${ value.emojiCodes ? String.fromCodePoint(
                      value.emojiCodes[ 0 ],
                      value.emojiCodes[ 1 ],
                    ) : '' }`
                  }
                  color={ index === 0 ? 'orange' : 'white' }
                />
              ))}
            </Picker>
          </PickerWrapper>
        </InnerModal>
      </Modal>
    </>
  )
}

export default PickerModalIOS
