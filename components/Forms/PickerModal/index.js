import React from 'react'
import {
  Platform, Dimensions, Text, View,
} from 'react-native'
import styled from 'styled-components/native'
import TriggerField from 'components/Forms/TriggerField'
import ModalSelector from 'react-native-modal-selector'
import PickerModalIOS from './PickerModalIOS'
// import PickerModalAndroid from './PickerModalAndroid'

const PickerModalWrapper = styled.View``

const PickerModal = (props) => {
  const {
    formValues,
    pickerValues,
    backgroundColor,
    fieldName,
    fieldColor,
    setFieldValue,
    setFieldTouched,
    scrollToNext,
    currentIndex,
    triggerIcon,
  } = props

  const [ modalVisible, setModalVisible ] = React.useState(false)
  const [ pickerValue, setPickerValue ] = React.useState(formValues[ fieldName ])
  const [ valid, setValid ] = React.useState(false)

  const handleModal = () => setModalVisible(true)

  return (
    <PickerModalWrapper>
      {Platform.OS === 'ios' ? (
        <View style={ { width: '100%' } }>
          <TriggerField
            backgroundColor={ fieldColor }
            setModalVisible={ setModalVisible }
            defaultText={
              formValues[ fieldName ]
                ? formValues[ fieldName ]
                : 'Press to select'
            }
            fieldColor={ fieldColor }
            valueFromForm={ formValues[ fieldName ] }
            scrollToNext={ scrollToNext }
            currentIndex={ currentIndex }
            triggerIcon={ triggerIcon }
            valid={ valid }
            action={ handleModal }
          />
          <PickerModalIOS
            visible={ modalVisible }
            values={ pickerValues }
            backgroundColor={ fieldColor }
            setModalVisible={ setModalVisible }
            setPickerValue={ setPickerValue }
            pickerValue={ pickerValue }
            setFieldValue={ setFieldValue }
            fieldName={ fieldName }
            valueOnForm={ formValues[ fieldName ] }
            setFieldTouched={ setFieldTouched }
            modalVisible={ modalVisible }
          />
        </View>

      ) : (
        <ModalSelector
          data={ pickerValues }
          keyExtractor={ (item) => item.label }
          initValue="Press to select"
          accessible
          scrollViewAccessibilityLabel="Scrollable options"
          cancelButtonAccessibilityLabel="Cancel Button"
          onChange={ async (option) => {
            await setFieldValue(fieldName, option.value)
            await setFieldTouched(fieldName)
          } }
        >
          <TriggerField
            backgroundColor={ fieldColor }
            setModalVisible={ setModalVisible }
            defaultText={
              formValues[ fieldName ]
                ? formValues[ fieldName ]
                : 'Press to select'
            }
            fieldColor={ fieldColor }
            valueFromForm={ formValues[ fieldName ] }
            scrollToNext={ scrollToNext }
            currentIndex={ currentIndex }
            triggerIcon={ triggerIcon }
            valid={ valid }
            action={ handleModal }
          />
        </ModalSelector>
      )}
    </PickerModalWrapper>
  )
}

export default PickerModal
