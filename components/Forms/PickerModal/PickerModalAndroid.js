import React from 'react'
import { Modal } from 'react-native'
import styled from 'styled-components/native'
import Color from 'color'
import ModalSelector from 'react-native-modal-selector'

const InnerModal = styled.View`
  flex: 1;
  align-items: center;
  justify-content: flex-end;
  border-radius: 30px;
`

const PickerWrapper = styled.View`
  height: 400px;
  background-color: ${ (props) => props.backgroundColor };
  border-top-left-radius: 60px;
  border-top-right-radius: 60px;
`

const PickerModalAndroid = ({
  visible,
  values,
  backgroundColor,
  setFieldValue,
  fieldName,
  setFieldTouched,
}) => {
  const modalBackgroundColor = Color(backgroundColor).lighten(0.08).hex()

  return (
    <Modal animated transparent visible={ visible } animationType="slide">
      <InnerModal>
        <PickerWrapper backgroundColor={ modalBackgroundColor }>
          <ModalSelector
            data={ values }
            initValue="Select yacht's flag"
            accessible
            scrollViewAccessibilityLabel="Scrollable options"
            cancelButtonAccessibilityLabel="Cancel Button"
            onChange={ (option, index) => {
              if (index !== 0) {
                setFieldValue(fieldName, option.value)
                setFieldTouched(fieldName)
              }
            } }
          />
        </PickerWrapper>
      </InnerModal>
    </Modal>
  )
}

export default PickerModalAndroid
