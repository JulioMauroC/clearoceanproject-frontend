import React from 'react'

import Backdrop from './Backdrop'
import Wave from './Wave'

const BackgroundAnim = (props) => {
  const { isFormValid, backgrounds, scrollX } = props
  return (
    <>
      <Backdrop
        isFormValid={ isFormValid }
        backgrounds={ backgrounds }
        scrollX={ scrollX }
      />
      <Wave scrollX={ scrollX } />
    </>
  )
}

export default BackgroundAnim
