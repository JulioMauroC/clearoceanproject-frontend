import React from 'react'
import { Animated, Dimensions, StyleSheet } from 'react-native'
import { COLORS } from 'consts'

const { width, height } = Dimensions.get('screen')

const Backdrop = (props) => {
  const { scrollX, backgrounds, isFormValid } = props

  if (isFormValid) {
    backgrounds[ backgrounds.length - 1 ] = '#2DAD65'
  } else {
    backgrounds[ backgrounds.length - 1 ] = COLORS.RED
  }

  const backgroundColor = scrollX.interpolate({
    inputRange: backgrounds.map((_, i) => i * width),
    outputRange: backgrounds.map((bg) => bg),
  })

  return (
    <Animated.View
      style={ [
        StyleSheet.absoluteFillObject,
        {
          backgroundColor,
        },
      ] }
    />
  )
}

export default Backdrop
