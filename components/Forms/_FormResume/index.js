import React from 'react'

import styled from 'styled-components/native'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import ResumeDisplay from './ResumeDisplay'
import ErrorsDisplay from './ErrorsDisplay'

const FormResumeWrapper = styled.View`
  width: ${ widthToDp('50%') };
  height: ${ widthToDp('20%') };
  position: absolute;
  top: ${ heightToDp('40%') };
  z-index: 1;
`
const FormResume = (props) => {
  const {
    isConfirmationScreen, touched, errors, isSubmitting, values, formConfigData,
  } = props

  return isConfirmationScreen ? (
    <FormResumeWrapper>
      {Object.keys(errors).length > 0 ? (
        <ErrorsDisplay
          isConfirmationScreen={ isConfirmationScreen }
          touched={ touched }
          errors={ errors }
          isSubmitting={ isSubmitting }
        />
      ) : (
        <ResumeDisplay values={ values } formConfigData={ formConfigData } />
      )}
    </FormResumeWrapper>
  ) : null
}

export default FormResume
