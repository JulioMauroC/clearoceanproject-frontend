import React from 'react'
import styled from 'styled-components/native'
import Avatar from 'components/Avatar'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import Icon from 'components/Icon'
import { View, Text } from 'react-native'

const ResumeDisplayWrapper = styled.View`
  /* flex: 0.3; */
  align-items: center;
  padding-top: 10px;
`

const FieldTitleText = styled.Text`
  text-align: center;
  font-size: ${ widthToDp('4%') };
  padding: 2px;
  color: white;
`

const FieldText = styled.Text`
  text-align: center;
  font-size: ${ widthToDp('4%') };
  font-family: 'Lato-Bold';
  color: black;
  opacity: 0.65;
`

const AvatarWrapper = styled.View`
  justify-content: center;
  align-items: center;
`

const ResumeDisplay = (props) => {
  const { values, formConfigData } = props

  return (
    <ResumeDisplayWrapper>
      {formConfigData.map((field, index) => {
        const { fieldName, isConfirmationScreen, type } = field
        const isArrayField = Array.isArray(values[ fieldName ])

        return (
          !isConfirmationScreen
          && values[ fieldName ] !== undefined > 0 && (
            <View key={ fieldName }>
              <FieldTitleText>{field.formResumeTitle}</FieldTitleText>
              {type === 'image' ? (
                type === 'image' && !isArrayField ? (
                  <AvatarWrapper>
                    {values[ fieldName ] ? (
                      <Avatar
                        size={ widthToDp('10%') }
                        imgUrl={ values[ fieldName ].uri }
                      />
                    ) : (
                      <Icon
                        icon="empty-set"
                        size={ widthToDp('6%') }
                        color="black"
                        style={ { opacity: 0.75 } }
                      />
                    )}
                  </AvatarWrapper>
                ) : (
                  <FieldText>
                    {values[ fieldName ].length}
                    images uploaded
                  </FieldText>
                )
              ) : (
                <View style={ { alignItems: 'center' } }>
                  {values[ fieldName ] ? (
                    isArrayField ? (
                      <FieldText>
                        {values[ fieldName ].map((item) => `${ item }, `)}
                      </FieldText>
                    ) : (
                      <FieldText>{values[ fieldName ]}</FieldText>
                    )
                  ) : (
                    <Icon
                      icon="empty-set"
                      size={ widthToDp('6%') }
                      color="black"
                      style={ { opacity: 0.75 } }
                    />
                  )}
                </View>
              )}
            </View>
          )
        )
      })}
    </ResumeDisplayWrapper>
  )
}

export default ResumeDisplay
