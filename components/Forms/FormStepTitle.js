import React from 'react'
import { View, Text } from 'react-native'
import styled from 'styled-components/native'
import { COLORS } from 'consts'

const FormStepTitleWrapper = styled.View`
  /* flex: ${ (props) => (props.description ? 0.35 : 0.35) }; */
  width: 100%;
  padding: 10px;
`

const FormStepTitle = (props) => {
  const { stepTitle, description } = props

  return (
    <FormStepTitleWrapper description={ description }>
      <Text
        style={ {
          fontFamily: 'Lato-Bold',
          color: 'black',
          fontSize: 26,
          marginBottom: 10,
          textAlign: 'center',
        } }
      >
        {stepTitle}
      </Text>
    </FormStepTitleWrapper>
  )
}

export default FormStepTitle
