import React from 'react'
import styled from 'styled-components/native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons'

const DescriptionWrapper = styled.View`
  padding: 10px;
  align-items: center;
  flex-direction: row;
  flex:0.3 ;
`

const DescriptionText = styled.Text`
  font-family: 'Lato-Bold';
  font-size: 12px;
  text-align: left;
  width:70%;
  padding: 10px;
`

const Description = (props) => {
  const { description, fieldColor } = props
  return (
    <DescriptionWrapper>
      <FontAwesomeIcon
        icon={ faInfoCircle }
        color={ fieldColor }
        size={ 22 }

      />
      <DescriptionText>{description}</DescriptionText>
    </DescriptionWrapper>
  )
}

export default Description
