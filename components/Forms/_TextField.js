import React from 'react'
import styled from 'styled-components/native'
import { TextInput, Dimensions } from 'react-native'
import { COLORS } from 'consts'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faCheck, faTimes } from '@fortawesome/free-solid-svg-icons'
import Color from 'color'

const { width } = Dimensions.get('screen')

const TextFieldWrapper = styled.View`
  background-color: ${ (props) => (props.backgroundColor ? props.backgroundColor : 'white') };
  box-shadow: 0px 1px 6px ${ (props) => props.borderColor };
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: ${ width * 0.75 }px;
  height: 48px;
  border-radius: 50px;
`
const IconWrapper = styled.TouchableOpacity`
  border: 1px solid ${ (props) => props.borderColor };
  height: 44px;
  width: 44px;
  border-radius: 24px;
  background-color: ${ (props) => (props.valid === null ? 'transparent' : 'white') };
  justify-content: center;
  align-items: center;
`

const TextField = (props) => {
  const {
    placeholder,
    handleChange,
    fieldName,
    setFieldTouched,
    fieldColor,
    currentIndex,
    scrollToNext,
    valid,
    setHideHeader,
    hideHeader,
  } = props

  const modifiedBackgroundColor = Color(fieldColor).lighten(0.6)

  const color = valid === true
    ? COLORS.GREEN
    : valid === false
      ? COLORS.RED
      : 'transparent'
  return (
    <TextFieldWrapper
      borderColor={ color }
      backgroundColor={ modifiedBackgroundColor }
    >
      <TextInput
        underlineColorAndroid="transparent"
        placeholder={ placeholder }
        placeholderTextColor="white"
        // value={ value }
        onChangeText={ handleChange(fieldName) }
        onBlur={ () => {
          setFieldTouched(fieldName)
          setHideHeader(false)
        } }
        onEndEditing={ () => {
          setFieldTouched(fieldName)
          setHideHeader(false)
        } }
        onFocus={ () => setHideHeader(true) }
        style={ {
          width: '75%',
          paddingLeft: 20,
          color: 'white',
          opacity: 0.7,
          fontFamily: 'Lato-Bold',
        } }
      />
      <IconWrapper
        borderColor={ color }
        valid={ valid }
        onPress={ () => {
          valid && scrollToNext(currentIndex)
        } }
      >
        {valid === true ? (
          <FontAwesomeIcon icon={ faCheck } color={ color } size={ 20 } />
        ) : valid === false ? (
          <FontAwesomeIcon icon={ faTimes } color={ color } size={ 20 } />
        ) : null}
      </IconWrapper>
    </TextFieldWrapper>
  )
}

export default TextField
