import React from 'react'
import { View, Text } from 'react-native'
import styled from 'styled-components/native'
import { COLORS } from 'consts'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

const FormStepTitleWrapper = styled.View`
  /* flex: ${ (props) => (props.description ? 0.35 : 0.35) }; */
  width: 90%;
  padding-top: ${ heightToDp('1%') };

`
const FormStepTitleText = styled.Text`
  font-family: 'Lato-Regular';
  color: black;
  font-size: ${ widthToDp('6%') }px;
  margin-bottom: 10;
  text-align: center;
`

const FormStepTitle = (props) => {
  const { stepTitle, description } = props

  return (
    <FormStepTitleWrapper description={ description }>
      <FormStepTitleText>{stepTitle}</FormStepTitleText>
    </FormStepTitleWrapper>
  )
}

export default FormStepTitle
