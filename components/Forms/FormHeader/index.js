import React from 'react'
import styled from 'styled-components'
import Icon from 'components/Icon'
import { COLORS } from 'consts'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'
import Indicator from 'components/Indicator'
import FormStepTitle from './FormStepTitle'
import Description from './Description'

const FormHeaderWrapper = styled.View`
  height: ${ (props) => props.theme.dimensions.headerHeight };
  justify-content: flex-start;
  align-items: center;
  padding-top: ${ (props) => props.theme.dimensions.paddingLarge };
  
`

const FormHeader = (props) => {
  const {
    isConfirmationScreen,
    isFormValid,
    title,
    description,
    fieldIcon,
    hideHeader,
    fieldColor,
    backgroundColor,
    data,
    scrollX,
    moveY,
  } = props

  return (
    <FormHeaderWrapper moveY={ moveY }>
      {isConfirmationScreen ? (
        <>
          <FormStepTitle
            stepTitle={ isFormValid ? title : 'Oops' }
            description={ description }
            isConfirmationScreen={ isConfirmationScreen }
          />
          <Icon
            color={ isFormValid ? COLORS.MID_GREEN : COLORS.RED }
            size={ heightToDp('7%') }
            icon={ fieldIcon }
            style={ { marginTop: heightToDp('1%') } }
          />
          <Indicator data={ data } scrollX={ scrollX } />
        </>
      ) : (
        !hideHeader && (
          <>
            <FormStepTitle description={ description } stepTitle={ title } />
            <Icon
              color={ backgroundColor }
              size={ heightToDp('7%') }
              icon={ fieldIcon }
              style={ { marginTop: heightToDp('1%') } }
            />
            <Indicator data={ data } scrollX={ scrollX } />
          </>
        )
      )}

      {description && !hideHeader && (
        <Description fieldColor={ backgroundColor } description={ description } />
      )}
    </FormHeaderWrapper>
  )
}

export default FormHeader
