import React from 'react'
import styled from 'styled-components/native'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

const ProgressTextWrapper = styled.View`
  position: absolute;
  top: ${ heightToDp('2%') };
  left: 20;
  z-index: 99999;
  opacity: 0.95;
  /* border: 1px solid red; */
`

const ProgressTextContent = styled.Text`
  font-family: 'Lato-Light';
`

const ProgressText = (props) => {
  const { text } = props
  return (
    <ProgressTextWrapper>
      <ProgressTextContent>{text}</ProgressTextContent>
    </ProgressTextWrapper>
  )
}

export default ProgressText
