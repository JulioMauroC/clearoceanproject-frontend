import React from 'react'
import {
  StyleSheet,
  Animated,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Text,
  View,
} from 'react-native'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import styled from 'styled-components/native'

import Color from 'color'

const referenceDimension = heightToDp('8%')

const AddImageButtonsWrapper = styled.View`
  flex-direction: row;
  justify-content: space-evenly;
  width: ${ widthToDp('80%') };
`

const ButtonWrapper = styled.TouchableOpacity`
  width: ${ widthToDp('30%') };
  height: auto;
  min-height: 40px;
  text-align: center;
  justify-content: center;
  border-radius: 10px;
  background-color: transparent;
  border-color: white;
  border-width: 1;
`

const ButtonText = styled.Text`
  color: white;
  text-align: center;
  font-weight: 500;
  font-size: 13px;
`

const AddImageButtons = (props) => {
  const {
    fieldColor, defaultText, handleCamera, handleGallery,
  } = props
  const animation = React.useMemo(() => new Animated.Value(0), [])

  const iconBackgroundColor = Color(fieldColor).lighten(0.4).hex()

  return (
    <AddImageButtonsWrapper>
      <ButtonWrapper onPress={ handleGallery }>
        <ButtonText>From Library</ButtonText>
      </ButtonWrapper>

      <ButtonWrapper>
        <ButtonText onPress={ handleCamera }>From Camera</ButtonText>
      </ButtonWrapper>
    </AddImageButtonsWrapper>
  )
}

export default AddImageButtons
