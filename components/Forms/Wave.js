import React from 'react'
import {
  View, Text, Animated, Dimensions,
} from 'react-native'

import styled from 'styled-components/native'

const { width, height } = Dimensions.get('screen')

const Wave = (props) => {
  const { scrollX } = props

  const YOLO = Animated.modulo(
    Animated.divide(Animated.modulo(scrollX, width), new Animated.Value(width)),
    1,
  )

  const rotate = YOLO.interpolate({
    inputRange: [ 0, 0.5, 1 ],
    outputRange: [ '35deg', '0deg', '35deg' ],
  })

  const translateX = YOLO.interpolate({
    inputRange: [ 0, 0.5, 1 ],
    outputRange: [ 0, -height / 10, 0 ],
  })
  return (
    <Animated.View
      style={ {
        width: height,
        height,
        backgroundColor: 'white',
        borderRadius: 560,
        position: 'absolute',
        top: -height * 0.6,
        left: -height * 0.3,
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.0,

        elevation: 24,
        transform: [
          {
            rotate,
          },
          {
            translateX,
          },
        ],
      } }
    />
  )
}

export default Wave
