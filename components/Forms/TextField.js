import React from 'react'
import styled from 'styled-components/native'
import { TextInput, Dimensions } from 'react-native'
import { COLORS } from 'consts'
import Color from 'color'

const TextFieldWrapper = styled.View`
  background-color: ${ (props) => (props.backgroundColor ? props.backgroundColor : 'white') };
  box-shadow: 0px 1px 6px ${ (props) => props.borderColor };
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: auto;
  height: 40px;
  border-radius: 5px;
`

const TextField = (props) => {
  const {
    placeholder,
    handleChange,
    fieldName,
    setFieldTouched,
    fieldColor,
    valid,
    setHideHeader,
  } = props

  const modifiedBackgroundColor = Color(fieldColor).lighten(0.6)

  const color = valid === true
    ? COLORS.GREEN
    : valid === false
      ? COLORS.RED
      : 'transparent'
  return (
    <TextFieldWrapper
      borderColor={ color }
      backgroundColor={ modifiedBackgroundColor }
    >
      <TextInput
        underlineColorAndroid="transparent"
        placeholder={ placeholder }
        placeholderTextColor="white"
        // value={ value }
        onChangeText={ handleChange(fieldName) }
        onBlur={ () => {
          setFieldTouched(fieldName)
          setHideHeader(false)
        } }
        onEndEditing={ () => {
          setFieldTouched(fieldName)
          setHideHeader(false)
        } }
        onFocus={ () => setHideHeader(true) }
        style={ {
          width: '75%',
          paddingLeft: 20,
          color: 'white',
          opacity: 0.5,
          fontFamily: 'Lato-Bold',
        } }
      />
    </TextFieldWrapper>
  )
}

export default TextField
