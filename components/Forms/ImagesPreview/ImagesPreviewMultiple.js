import React from 'react'
import styled from 'styled-components/native'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'
import ImageThumbnail from './ImageThumbnail'

const ThumbnailsContainer = styled.View`

  position: absolute;
  width: 100%;
  height: ${ widthToDp('20%') };
  flex-direction: row;
  justify-content: center;
  align-items: center;
bottom:${ heightToDp('30%') };
  /* border:3px solid red; */


`

const ImagesPreviewMultiple = (props) => {
  const {
    previewImages,
    hasImagePreview,
    fieldColor,
    action,
    multiple,
    setFieldValue,

  } = props

  const [ thumbnailsSize, setThumbnailsSize ] = React.useState(0)

  // React.useEffect(() => {
  //   setFieldValue('EntryImageInput', previewImages)
  // }, [ previewImages, setFieldValue ])

  React.useEffect(() => {
    switch (previewImages.length) {
      case 1:
        setThumbnailsSize(widthToDp('20%'))
        break
      case 2:
        setThumbnailsSize(widthToDp('18%'))
        break
      case 3:
        setThumbnailsSize(widthToDp('16%'))
        break
      case 4:
        setThumbnailsSize(widthToDp('15%'))
        break
      case 5:
        setThumbnailsSize(widthToDp('14%'))
        break
      default:
        setThumbnailsSize(widthToDp('14%'))
    }
  }, [ previewImages.length ])

  return previewImages && previewImages.length > 0 && hasImagePreview ? (
    <ThumbnailsContainer>
      {previewImages.map((image, index) => (
        <ImageThumbnail
          size={ thumbnailsSize }
          multiple={ multiple }
          key={ image.uri }
          action={ (index) => {
            // setFieldValue(previewImages)
            action(index)
          } }
          index={ index }
          previewImage={ image.uri }
        />
      ))}
    </ThumbnailsContainer>
  ) : null
}

export default ImagesPreviewMultiple
