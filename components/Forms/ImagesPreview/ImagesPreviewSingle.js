import React from 'react'

import styled from 'styled-components/native'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import ImageThumbnail from './ImageThumbnail'

const ImagesPreviewSingle = (props) => {
  const {
    previewImage, hasImagePreview, fieldColor, action, multiple,
  } = props

  return previewImage && hasImagePreview ? <ImageThumbnail { ...props } /> : null
}

export default ImagesPreviewSingle
