import React from 'react'

import ImagesPreviewSingle from './ImagesPreviewSingle'
import ImagesPreviewMultiple from './ImagesPreviewMultiple'

const ImagesPreview = (props) => {
  const { multiple } = props

  return multiple ? (
    <ImagesPreviewMultiple { ...props } />
  ) : (
    <ImagesPreviewSingle { ...props } />
  )
}

export default ImagesPreview
