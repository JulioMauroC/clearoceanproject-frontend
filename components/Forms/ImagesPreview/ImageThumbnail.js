import React from 'react'
import { TouchableOpacity } from 'react-native'
import styled from 'styled-components/native'

import Icon from 'components/Icon'
import { COLORS } from 'consts'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'
import ImagePreviewModal from './ImagePreviewModal'

const ImagePreviewWrapper = styled.View`

  width:${ (props) => (props.size ? props.size : widthToDp('16%')) };
  height:${ (props) => (props.size ? props.size : widthToDp('16%')) };
  margin-left:${ widthToDp('2%') };
  margin-right:${ widthToDp('2%') };
  box-shadow: 1px 2px 10px rgba(115, 73, 0, 0.3);
  /* position:${ (props) => (props.multiple ? 'relative' : 'absolute') };
  top:${ (props) => (props.multiple ? 0 : heightToDp('65%')) }; */
  z-index: 999;
  /* top:${ (props) => (props.multiple ? 0 : 140) };  */
  top:140;

`

const IconButton = styled.TouchableOpacity`
  position: absolute;
  top: 0;
  right: 0;
  z-index: 20000;
  background-color: white;
  border-radius: 10px;
  transform: translate(-2px, 0px);
  z-index: 1;
`

const ImagePreviewImage = styled.Image`
  width: 100%;
  height: 100%;
  /* border-radius: ${ widthToDp('10%') }; */
  border-radius:10px;
  z-index: 9999999;
  /* border:3px solid yellow; */
`

const ImageThumbnail = (props) => {
  const {
    previewImage, action, multiple, size,
  } = props

  const [ showImageModal, setShowImageModal ] = React.useState(false)

  return (
    <ImagePreviewWrapper size={ size } multiple={ multiple }>
      <ImagePreviewModal
        showImageModal={ showImageModal }
        setShowImageModal={ setShowImageModal }
        imgUri={ previewImage }
      />
      <IconButton onPress={ action }>
        <Icon icon="times-circle" color={ COLORS.RED } size={ 18 } />
      </IconButton>
      <TouchableOpacity onPress={ () => setShowImageModal(true) }>
        <ImagePreviewImage source={ { uri: previewImage } } resizeMode="cover" />
      </TouchableOpacity>
    </ImagePreviewWrapper>
  )
}

export default ImageThumbnail
