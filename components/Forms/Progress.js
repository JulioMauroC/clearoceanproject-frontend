import React from 'react'
import { Text, View, FlatList } from 'react-native'
import styled from 'styled-components/native'

const ProgressWrapper = styled.View``

const Progress = (props) => {
  const { values } = props

  return (
    <ProgressWrapper>
      <FlatList
        data={ Object.values(values) }
        horizontal
        renderItem={ (item) => {
          <View>
            <Text>value</Text>
          </View>
        } }
      />
    </ProgressWrapper>
  )
}

export default Progress
