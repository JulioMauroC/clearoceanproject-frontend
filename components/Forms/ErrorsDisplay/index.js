import React from 'react'
import { View, Text, Dimensions } from 'react-native'
import styled from 'styled-components/native'
import Color from 'color'
import { COLORS } from 'consts'

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faExclamation } from '@fortawesome/free-solid-svg-icons'

const { width } = Dimensions.get('screen')

const modifiedBackgroundColor = Color(COLORS.RED).lighten(0.15)

const ErrorsDisplayWrapper = styled.View`
  flex: 0.3;
`

const ErrorsItem = styled.View`
  background-color: ${ modifiedBackgroundColor };
  /* box-shadow: 0px 1px 6px brown; */
  min-width: ${ width * 0.75 }px;
  /* height: 48px; */
  padding-left: 20px;
  border-radius: 50px;
  /* flex: 0.3; */
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  margin-bottom: 14px;
`

const ErrorsTextItem = styled.Text`
  color: white;
  font-family: 'Lato-Bold';
  font-size: 14px;
  padding: 7px 5px;
`

const ErrorItem = styled.View``

const ErrorsDisplay = (props) => {
  const {
    errors, touched, isConfirmationScreen, isSubmitting,
  } = props
  const showErrors = isConfirmationScreen && errors && !isSubmitting

  return showErrors ? (
    <ErrorsDisplayWrapper>
      {Object.keys(touched).length < 1 ? (
        <ErrorsItem>
          <FontAwesomeIcon icon={ faExclamation } color="white" size={ 14 } />
          <ErrorsTextItem>Plase fill the form</ErrorsTextItem>
        </ErrorsItem>
      ) : (
        Object.values(errors).map((error, index) => (
          <ErrorsItem key={ error + index }>
            <FontAwesomeIcon icon={ faExclamation } color="white" size={ 14 } />
            <ErrorsTextItem>{error}</ErrorsTextItem>
          </ErrorsItem>
        ))
      )}
    </ErrorsDisplayWrapper>
  ) : null
}

export default ErrorsDisplay
