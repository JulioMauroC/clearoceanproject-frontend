import React from 'react'
import { View } from 'react-native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import {
  faGlobeEurope,
  faArrowCircleUp,
  faMousePointer,
} from '@fortawesome/free-solid-svg-icons'

export function mousePointer(props) {
  return (
    <FontAwesomeIcon
      icon={ faMousePointer }
      color={ props.iconColor }
      size={ 20 }
      secondaryOpacity={ 0.4 }
    />
  )
}

export function globe(props) {
  return (
    <FontAwesomeIcon
      icon={ faGlobeEurope }
      color={ props.iconColor }
      size={ 20 }
      secondaryOpacity={ 0.4 }
    />
  )
}

export function upload(props) {
  return (
    <FontAwesomeIcon
      icon={ faArrowCircleUp }
      color={ props.iconColor }
      size={ 20 }
      secondaryOpacity={ 0.4 }
    />
  )
}

export const iconsReg = {
  globe,
  upload,
  mousePointer,
}

const IconSwitcher = (props) => {
  const { triggerIcon, iconColor } = props

  const CurrentIcon = iconsReg[ triggerIcon ]

  return (
    <View style={ { flex: 0.2, alignItems: 'center', justifyContent: 'center' } }>
      <CurrentIcon iconColor={ iconColor } />
    </View>
  )
}

export default IconSwitcher
