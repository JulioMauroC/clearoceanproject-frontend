import React from 'react'
import styled from 'styled-components/native'
import { Dimensions, Text } from 'react-native'
import { COLORS } from 'consts'

import Color from 'color'
import Icon from 'components/Icon'
import IconSwitcher from './IconSwitcher'

const { width } = Dimensions.get('screen')

const TriggerFieldWrapper = styled.TouchableOpacity`
  border: 1.5px solid ${ (props) => props.borderColor };
  background-color: ${ (props) => (props.backgroundColor ? props.backgroundColor : 'white') };
  box-shadow: 0px 1px 6px
    ${ (props) => (props.valueFromForm ? COLORS.GREEN : 'transparent') };
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  /* width: ${ width * 0.55 }px; */
  width: 100%;
  height: 40px;
  border-radius: 20px;
`
const IconWrapper = styled.TouchableOpacity`
  /* border: 1px solid ${ (props) => props.borderColor }; */
  height: 40px;
  width: 40px;
  border-radius: 20px;
  background-color: white;
  justify-content: center;
  align-items: center;
`

const TriggerField = (props) => {
  const {
    fieldColor,
    currentIndex,
    valueFromForm,
    scrollToNext,
    defaultText,
    triggerIcon,
    valid,
    action,
  } = props

  // const [ valid, setValid ] = React.useState(false)

  const modifiedBackgroundColor = Color(fieldColor).lighten(0.4).hex()
  const modifiedIconColor = Color(fieldColor).darken(0.7).hex()

  return (
    <TriggerFieldWrapper
      onPress={ action }
      borderColor={ modifiedBackgroundColor }
      backgroundColor={ modifiedBackgroundColor }
      valueFromForm={ valueFromForm }
    >
      <Text
        style={ {
          width: '75%',
          paddingLeft: 20,
          color: 'white',
          opacity: 0.7,
          fontFamily: 'Lato-Bold',
        } }
      >
        {defaultText}
      </Text>

      <IconWrapper
        borderColor={ modifiedIconColor }
        valid={ valid }
        onPress={ () => {
          valueFromForm && scrollToNext(currentIndex)
        } }
      >

        <IconSwitcher
          triggerIcon={ triggerIcon }
          iconColor={ modifiedIconColor }
        />

      </IconWrapper>
    </TriggerFieldWrapper>
  )
}

export default TriggerField
