import React from 'react'
import { Switch, View, Text } from 'react-native'
import styled from 'styled-components/native'
import { COLORS } from 'consts'

const ToggleWrapper = styled.View`
  align-items: center;
  justify-content: flex-end;
  flex-direction: row;
  /* border: 3px solid orange; */
`

const Toggle = (props) => {
  const {
    toggleSwitch, toggleState, toggleStatsValues, setFieldTouched, fieldName,
  } = props

  return (
    <ToggleWrapper>
      <View>
        <Text style={ { fontSize: 8, color: '#3e3e3e', opacity: 0.75 } }>
          {toggleState ? toggleStatsValues[ 0 ] : toggleStatsValues[ 1 ]}
        </Text>
      </View>
      <Switch
        trackColor={ { false: COLORS.RED, true: COLORS.MID_GREEN } }
        thumbColor={ toggleState ? '#fff' : '#E8E8E8' }
        ios_backgroundColor="#3e3e3e"
        onValueChange={ () => {
          setFieldTouched(fieldName)
          toggleSwitch()
          // setFieldValue(fieldName, toggleState)
        } }
        value={ toggleState }
        style={ {
          transform: [ { scaleX: 0.7 }, { scaleY: 0.7 } ],
          marginRight: 'auto',

        } }
      />
    </ToggleWrapper>
  )
}

export default Toggle
