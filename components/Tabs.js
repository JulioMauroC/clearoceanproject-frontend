import React from 'react'
import { View, TouchableOpacity, StyleSheet } from 'react-native'

import { DIMENSIONS } from 'consts'
import styled from 'styled-components/native'

const TabText = styled.Text`
  color: ${ (props) => (props.color ? props.color : '#434343') };
  font-family: ${ (props) => (props.isActive ? 'Lato-Bold' : 'Lato-Regular') };
  font-size: 18px;
  opacity: ${ (props) => (props.isActive ? 1 : 0.8) };
`

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    fontSize: 26,
    paddingTop: DIMENSIONS.USERS_LIST.SPACING * 0.6,
    paddingHorizontal: DIMENSIONS.USERS_LIST.SPACING * 2,
  },
  textContainer: {
    marginRight: DIMENSIONS.USERS_LIST.SPACING,
  },
})
const Tabs = (props) => {
  const {
    tabs, action, activeViewIndex, color,
  } = props

  const handlePress = (tab, index) => {
    action(index)
  }

  return (
    <View style={ styles.container }>
      {tabs.map((tab, index) => {
        const isActive = activeViewIndex === index
        return (
          <TouchableOpacity
            key={ tab.label }
            onPress={ () => handlePress(tab, index) }
          >
            <View style={ styles.textContainer }>
              <TabText color={ color } isActive={ isActive }>
                {tab.label}
              </TabText>
            </View>
          </TouchableOpacity>
        )
      })}
    </View>
  )
}

export default Tabs
