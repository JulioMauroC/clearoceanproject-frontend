import React from 'react'
import { Image, StyleSheet } from 'react-native'

import LogoBackgroundImage from 'assets/logos/logo-background-mobile.png'

import { DIMENSIONS } from 'consts'

const LogoBackgroundImageUrl = Image.resolveAssetSource(LogoBackgroundImage)
  .uri

const ImageBackground = (props) => (
  <Image
    source={ { uri: LogoBackgroundImageUrl } }
    style={ [
      StyleSheet.absoluteFillObject,
      {
        marginTop: DIMENSIONS.PROFILE_HEADER_DISTANCE * 1.3,
        opacity: 0.4,
        width: '100%',
      },
    ] }
    resizeMode="cover"
    blurRadius={ 30 }
  />
)

export default ImageBackground
