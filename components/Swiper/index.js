import React, { useState, useEffect } from 'react'
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  FlatList,
  Animated,
} from 'react-native'
import SwiperItem from './SwiperItem'

const { width, heigth } = Dimensions.get('window')

const Swiper = ({ data }) => {
  const scrollX = new Animated.Value(0)
  const position = Animated.divide(scrollX, width)
  const [ dataList, setDataList ] = useState(data)

  useEffect(() => {
    setDataList(data)
  })

  if (data && data.length) {
    return (
      <View>
        <Animated.FlatList
          removeClippedSubviews={ false }
          data={ data }
          // ref={ (flatList) => { flatList = flatList } }
          // ref={ flatListRef }
          keyExtractor={ (item, index) => `key${ index }` }
          horizontal
          pagingEnabled
          scrollEnabled
          snapToAlignment="center"
          scrollEventThrottle={ 32 }
          decelerationRate="fast"
          showsHorizontalScrollIndicator={ false }
          renderItem={ ({ item }) => <SwiperItem item={ item } /> }
          onScroll={ Animated.event(
            [
              {
                nativeEvent: { contentOffset: { x: scrollX } },
              },
            ],
            { useNativeDriver: false },
          ) }
        />
      </View>
    )
  }

  return null
}

export default Swiper
