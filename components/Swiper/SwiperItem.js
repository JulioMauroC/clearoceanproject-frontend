import React from 'react'

import styled from 'styled-components/native'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import { COLORS } from 'consts'

const ITEM_MARGIN = widthToDp('2%')

const SwiperItemWrapper = styled.View`
  flex: 1;
  padding-left: 10px;
  padding-right: 10px;
  height: 40;
  background-color: ${ COLORS.PURPLE };
  margin: ${ ITEM_MARGIN }px;
  border-radius: 10;
  overflow: hidden;
  align-self: center;
  justify-content: center;
`

const SwiperItemText = styled.Text`
  color: white;
  font-family: 'Lato-Black';
`

const SwiperItem = ({ item }) => (
  <SwiperItemWrapper>
    <SwiperItemText>{item}</SwiperItemText>
  </SwiperItemWrapper>
)

export default SwiperItem
