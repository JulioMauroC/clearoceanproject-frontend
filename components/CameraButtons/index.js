import React from 'react'
import styled from 'styled-components/native'
import { Dimensions } from 'react-native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faCamera, faCloudUploadAlt } from '@fortawesome/free-solid-svg-icons'
import { COLORS } from 'consts'

import Color from 'color'

const { width } = Dimensions.get('screen')

const CameraButtonsWrapper = styled.View`
  width: ${ width * 0.68 }px;
  flex-direction: row;
  justify-content: space-between;
`

const CameraButtonsItem = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
`

const CameraButtonsIconWrapper = styled.View`
  width: 45px;
  height: 45px;
  border-radius: 25px;
  background-color: ${ (props) => (props.iconBackgroundColor ? props.iconBackgroundColor : COLORS.PURPLE) };
  justify-content: center;
  align-items: center;
  border: 0.7px solid ${ (props) => props.iconOutlineColor };
`

const CameraButtonsText = styled.Text`
  color: white;
  font-size: 11px;
  padding: 12px;
  opacity: 0.75;
`

const CameraButtons = (props) => {
  const { fieldColor, handleGallery, handleCamera } = props
  const iconBackgroundColor = Color(fieldColor).lighten(0.4).hex()
  const iconOutlineColor = Color(fieldColor).lighten(1).hex()

  return (
    <CameraButtonsWrapper>
      <CameraButtonsItem onPress={ handleCamera }>
        <CameraButtonsIconWrapper
          iconBackgroundColor={ iconBackgroundColor }
          iconOutlineColor={ iconOutlineColor }
          style={ {
            shadowColor: 'black',
            shadowOffset: {
              width: 0,
              height: 3,
            },
            shadowOpacity: 0.29,
            shadowRadius: 4.65,

            elevation: 7,
          } }
        >
          <FontAwesomeIcon icon={ faCamera } color="white" size={ 17 } />
        </CameraButtonsIconWrapper>

        <CameraButtonsText>Take Picture</CameraButtonsText>
      </CameraButtonsItem>

      <CameraButtonsItem onPress={ handleGallery }>
        <CameraButtonsIconWrapper
          iconBackgroundColor={ iconBackgroundColor }
          iconOutlineColor={ iconOutlineColor }
          style={ {
            shadowColor: 'black',
            shadowOffset: {
              width: 0,
              height: 3,
            },
            shadowOpacity: 0.29,
            shadowRadius: 4.65,

            elevation: 7,
          } }
        >
          <FontAwesomeIcon icon={ faCloudUploadAlt } color="white" size={ 21 } />
        </CameraButtonsIconWrapper>
        <CameraButtonsText>Upload Image</CameraButtonsText>
      </CameraButtonsItem>
    </CameraButtonsWrapper>
  )
}

export default CameraButtons
