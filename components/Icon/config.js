export {
  faCheck,
  faCheckCircle,
  faUser,
  faCalendar,
  faCalendarAlt,
  faArrowLeft,
  faArrowRight,
  faArrowUp,
  faCameraRetro,
  faShip,
  faFlag,
  faPassport,
  faAddressCard,
  faMapPin,
  faWineBottle,
  faHome,
  faUsers,
  faPlusCircle,
  faPlus,
  faWaterLower,
  faGlobeEurope,
  faGlobeAfrica,
  faCog,
  faCloudUploadAlt,
  faEngineWarning,
  faCameraHome,
  faTimesCircle,
  faAngleDoubleRight,
  faMapMarkerAlt,
  faLongArrowAltRight,
  faTimes,
  faEmptySet,
  faPen,
  faCamera,
  faImages,
  faStarChristmas,
  faSignOutAlt,
  faList,
} from '@fortawesome/pro-regular-svg-icons'
