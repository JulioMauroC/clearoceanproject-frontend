import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import * as IconImports from './config'

library.add(IconImports)

const Icon = ({
  icon,
  color,
  secondaryColor,
  secondaryOpacity,
  activeColor,
  transitionDuration,
  size,
  prefix,
  ...props
}) => {
  const iconName = prefix ? [ prefix, icon ] : [ 'far', icon ]

  return (
    <FontAwesomeIcon
      { ...props }
      icon={ iconName }
      size={ size }
      color={ color }
      secondaryColor={ secondaryColor }
      secondaryOpacity={ secondaryOpacity }
    />
  )
}

export default Icon
