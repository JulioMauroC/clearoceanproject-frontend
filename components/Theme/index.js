import { Platform } from 'react-native';

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen';

const goldenRatio = 1.618;

const isAndroid = Platform.OS === 'android';

const Theme = {
  colors: {
    red: '#d95b5d',
    lightBlue: '#497e93',
    green: '#00ffab',
    midGreen: '#00CC88',
    darkGray: '#abb3c0',
    blue: '#0e427f',
    purple: '#353f8f',
    logoGreen: '#47BB52',
    logoBlue: '#1E75B6',
  },
  text: {
    defaultTextColor: '#707070',
    defaultTextSize: 14,
    defaultFontFamily: 'Lato-Regular',
  },
  dimensions: {
    // GLOBAL
    appHorizontalPadding: widthToDp('8%'),
    paddingSmall: widthToDp('1%'),
    paddingMedium: widthToDp('3%'),
    paddingMedLarge: widthToDp('5%'),
    paddingLarge: widthToDp('8%'),
    // AUTH
    authFormHeight: heightToDp('45%'),

    // DASHBOARD
    yachtImageHeight: heightToDp('20%'),
    avatarDiameter: widthToDp('28%'),
    dashboardHeight: heightToDp('35%'),
    dashboardCardWidth: widthToDp('40%'),
    dashboardCardHeight: widthToDp('40%') / goldenRatio,
    dashboardCardIcon: widthToDp('21%') / goldenRatio,
    // SETTINGS
    // settingsCardHeight: heightToDp('70%'),
    // settingsCardWidth: heightToDp('70%') / goldenRatio,
    settingsCardWidth: widthToDp('80%'),
    settingsCardHeight: heightToDp('64%'),

    // NAV
    navBottomHeight: heightToDp('10%'),
    // LISTS
    // listContainerHeight: heightToDp('45%'),
    listPaddingTop: heightToDp('32%'),
    // INVITE
    inviteCardWidth: widthToDp('85%'),
    inviteCardHeight: widthToDp('100%') / goldenRatio,
    // USER
    userCardWidth: widthToDp('80%'),
    userCardHeight: heightToDp('60%'),
    // ENTRY
    entryCardWidth: widthToDp('80%'),
    entryCardHeight: heightToDp('70%'),
    // FORM
    prevNextBottomDistance: isAndroid ? heightToDp('30%') : heightToDp('25%'),
    switchFieldBottomDistance: isAndroid
      ? heightToDp('30%')
      : heightToDp('25%'),
    headerHeight: heightToDp('35%'),
    inputPaddingBottom: heightToDp('4.5%'),
    // MISC
    entryTypesButtonsWidth: widthToDp('22%'),
  },
  shadows: {
    light: '1px 2px 10px rgba(115, 73, 222, 0.5)',
  },
};

export default Theme;
