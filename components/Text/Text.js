import styled from 'styled-components/native'

const Text = styled.Text`
  color: ${ (props) => (props.color ? props.color : props.theme.text.defaultTextColor) };
  font-size: ${ (props) => (props.size ? props.size : props.theme.text.defaultTextSize) };
  font-family: ${ (props) => (props.fontFamily ? props.fontFamily : props.theme.text.defaultFontFamily) };
`

export default Text
