import React from 'react'
import { Text, TouchableOpacity } from 'react-native'

const SkipButton = (props) => {
  const { action } = props
  return (
    <TouchableOpacity onPress={ action }>
      <Text>Skip</Text>
    </TouchableOpacity>
  )
}

export default SkipButton
