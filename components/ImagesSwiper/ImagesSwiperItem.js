import React from 'react'
import {
  View, StyleSheet, Image, Dimensions,
} from 'react-native'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

const { width, height } = Dimensions.get('window')

const IMAGES_MARGIN = widthToDp('6%')

const CarouselItem = ({ item }) => {
  console.log('item')
  console.log(item)
  return (
    <View style={ styles.cardView }>
      <Image style={ styles.image } source={ { uri: item } } resizeMode="cover" />
    </View>
  )
}

const styles = StyleSheet.create({
  cardView: {
    flex: 1,
    width: width - IMAGES_MARGIN * 2,
    height: '95%',
    backgroundColor: 'white',
    margin: IMAGES_MARGIN,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0.5, height: 0.5 },
    shadowOpacity: 0.5,
    shadowRadius: 3,
    overflow: 'hidden',
    alignSelf: 'center',
  },

  image: {
    width: width - IMAGES_MARGIN * 2,
    height: '100%',
    borderRadius: 10,
    zIndex: 999999,
  },
})

export default CarouselItem
