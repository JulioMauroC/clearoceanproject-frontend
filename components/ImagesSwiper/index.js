import React, { useState, useEffect } from 'react'
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  FlatList,
  Animated,
} from 'react-native'
import ImagesSwiperItem from './ImagesSwiperItem'

const { width, heigth } = Dimensions.get('window')

const ImagesSwiper = ({ data }) => {
  console.log('data')
  console.log(data)
  const scrollX = new Animated.Value(0)
  const position = Animated.divide(scrollX, width)
  const [ dataList, setDataList ] = useState(data)

  // const flatListRef = React.useRef()

  useEffect(() => {
    setDataList(data)
    // infiniteScroll(dataList)
  }, [ data ])

  if (data && data.length) {
    return (
      <View>
        <Animated.FlatList
          removeClippedSubviews={ false }
          data={ data }
          // ref={ (flatList) => { flatList = flatList } }
          // ref={ flatListRef }
          keyExtractor={ (item, index) => `key${ index }` }
          horizontal
          pagingEnabled
          scrollEnabled
          snapToAlignment="center"
          scrollEventThrottle={ 32 }
          decelerationRate="fast"
          showsHorizontalScrollIndicator={ false }
          renderItem={ ({ item }) => <ImagesSwiperItem item={ item } /> }
          onScroll={ Animated.event(
            [
              {
                nativeEvent: { contentOffset: { x: scrollX } },
              },
            ],
            { useNativeDriver: false },
          ) }
        />
        {data.length > 1 && (
          <View style={ styles.dotView }>
            {data.map((_, i) => {
              const opacity = position.interpolate({
                inputRange: [ i - 1, i, i + 1 ],
                outputRange: [ 0.3, 1, 0.3 ],
                extrapolate: 'clamp',
              })
              return (
                <Animated.View
                  key={ i }
                  style={ {
                    opacity,
                    height: 10,
                    width: 10,
                    backgroundColor: '#595959',
                    margin: 8,
                    borderRadius: 5,
                  } }
                />
              )
            })}
          </View>
        )}
      </View>
    )
  }

  return null
}

const styles = StyleSheet.create({
  dotView: { flexDirection: 'row', justifyContent: 'center' },
})

export default ImagesSwiper
