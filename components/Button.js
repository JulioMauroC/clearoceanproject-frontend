import React from 'react'
import { Text, StyleSheet, TouchableOpacity } from 'react-native'

import { FONT_SIZES } from 'consts'

const Button = (props) => {
  const {
    action,
    title,
    bgColor,
    textColor,
    height,
    fontSize,
    paddingRight,
    paddingLeft,
    marginTop,
  } = props

  const styles = StyleSheet.create({
    button: {
      alignSelf: 'center',
      justifyContent: 'center',
      backgroundColor: bgColor,
      width: '100%',
      height: height || 40,
      borderRadius: 5,
      marginTop: marginTop || 0,
    },
    text: {
      color: textColor,
      textAlign: 'center',
      alignSelf: 'center',
      fontFamily: 'Lato-Bold',
      fontSize: fontSize || FONT_SIZES.MEDIUM,
      paddingLeft: paddingLeft || 0,
      paddingRight: paddingRight || 0,
    },
  })
  return (
    <TouchableOpacity onPress={ action } style={ styles.button }>
      <Text style={ styles.text }>{title}</Text>
    </TouchableOpacity>
  )
}

export default Button
