import React from 'react'
import styled from 'styled-components/native'

const FormButton = (props) => {
  const { action } = props

  const FormButtonWrapper = styled.TouchableOpacity`
    background-color: ${ (props) => props.bgColor };
    padding: 15px 30px;
    border-radius: 5px;

    /* iconColor: ${ (props) => (props.margin ? margin : 0) }; */
  `

  const TextWrapper = styled.Text`
    color: ${ (props) => props.textColor };
    font-size: 14px;
    font-family: 'Lato-Bold';
    text-align: left;
  `

  return (
    <FormButtonWrapper { ...props } onPress={ action }>
      <TextWrapper { ...props }>{props.text}</TextWrapper>
    </FormButtonWrapper>
  )
}

export default FormButton
