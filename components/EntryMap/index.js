import React from 'react'
import styled from 'styled-components/native'
import { StyleSheet, Platform } from 'react-native'
import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'
import EntryMapAndroid from './EntryMapAndroid'
import EntryMapIOS from './EntryMapIOS'

const EntryMapWrapper = styled.View`
  width: 100%;
  height: ${ heightToDp('20%') };
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
`

const EntryMap = (props) => {
  const isAndroid = Platform.OS === 'android'

  return (
    <EntryMapWrapper>
      {isAndroid ? <EntryMapAndroid { ...props } /> : <EntryMapIOS { ...props } />}
    </EntryMapWrapper>
  )
}

export default EntryMap
