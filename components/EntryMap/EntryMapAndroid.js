import React from 'react'
import styled from 'styled-components/native'
import { StyleSheet } from 'react-native'
import MapView from 'react-native-maps'

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
})

const EntryMapAndroid = (props) => {
  const { latitude, longitude } = props
  console.log(typeof latitude)
  console.log(longitude)

  return (
    <MapView
      style={ styles.map }
      region={ {
        latitude,
        longitude,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      } }
    />
  )
}

export default EntryMapAndroid
