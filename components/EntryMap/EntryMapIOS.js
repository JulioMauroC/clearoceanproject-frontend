import React from 'react'
import styled from 'styled-components/native'
import { StyleSheet } from 'react-native'
import MapView, { Marker } from 'react-native-maps'

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject,
  },
})

const EntryMapIOS = (props) => {
  const {
    latitude, longitude, authorFirstName, authorLastName, types,
  } = props

  const cameraSettings = {
    center: {
      latitude,
      longitude,
    },
    pitch: 0,
    heading: 0,

    // Only on iOS MapKit, in meters. The property is ignored by Google Maps.
    altitude: 10000000,

    // Only when using Google Maps.
    zoom: 16,
  }

  return (
    latitude
    && longitude && (
      <MapView style={ styles.map } camera={ cameraSettings }>
        <Marker
          coordinate={ { latitude, longitude } }
          title={
            authorFirstName && authorLastName
              ? `created by ${ authorFirstName } ${ authorLastName }`
              : 'created by deleted user'
          }
          description={ `${ types.length } types detected at ${ latitude },${ longitude }` }
        />
      </MapView>
    )
  )
}

export default EntryMapIOS
