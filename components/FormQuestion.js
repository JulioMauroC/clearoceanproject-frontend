import React from 'react'
import { StyleSheet, Image, Text } from 'react-native'
import styled from 'styled-components/native'
import { COLORS, FONTS, FONT_SIZES } from 'consts'

const FormQuestionWrapper = styled.View`
flex:1;
background-color:${ COLORS.MID_GRAY };
width:100%;
padding:50px;
border-radius:25px;
align-self:center;
border: 1px solid ${ COLORS.DARK_GRAY };
`

const FormQuestionContent = styled.Text`
font-family:${ FONTS.DEFAULT };
font-size:${ FONT_SIZES.MEDIUM }px;

`

const FormQuestion = (props) => (
  <FormQuestionWrapper>
    <FormQuestionContent>{props.content}</FormQuestionContent>
  </FormQuestionWrapper>
)

export default FormQuestion
