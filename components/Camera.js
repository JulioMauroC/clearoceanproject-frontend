import React, { Component } from 'react'
import { View, Text, Dimensions } from 'react-native'
import styled from 'styled-components/native'
import { Camera } from 'expo-camera'
import * as Permissions from 'expo-permissions'

const { width, height } = Dimensions.get('screen')

const CameraWrapper = styled.View`
  flex: 1;
  height: ${ height }px;
  width: ${ width }px;
  align-items: center;
  justify-content: center;
  position:absolute;
`

class CameraComponent extends Component {
  state = {
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
  };

  async componentWillMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA)
    this.setState({ hasCameraPermission: status === 'granted' })
  }

  render() {
    const { hasCameraPermission } = this.state

    if (hasCameraPermission === null) {
      return <View />
    }
    if (hasCameraPermission === false) {
      return <Text> No access to camera</Text>
    }
    return (
      <CameraWrapper style={ { flex: 1 } }>
        <Camera
          style={ { flex: 1, justifyContent: 'space-between' } }
          type={ this.state.type }
        >
          <View
            style={ {
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingHorizontal: 10,
              marginBottom: 15,
              alignItems: 'flex-end',
            } }
          />
          {/* <Header
              searchBar
              rounded
              style={{
                position: 'absolute',
                backgroundColor: 'transparent',
                left: 0,
                top: 0,
                right: 0,
                zIndex: 100,
                alignItems: 'center',
              }}
            >
              <View style={{ flexDirection: 'row', flex: 4 }}>
                <Icon name="logo-snapchat" style={{ color: 'white' }} />
                <Item style={{ backgroundColor: 'transparent' }}>
                  <Icon
                    name="ios-search"
                    style={{ color: 'white', fontSize: 24, fontWeight: 'bold' }}
                  ></Icon>

                  <Input placeholder="Search" placeholderTextColor="white" />
                </Item>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  flex: 2,
                  justifyContent: 'space-around',
                }}
              >
                <Icon
                  name="ios-flash"
                  style={{ color: 'white', fontWeight: 'bold' }}
                />
                <Icon
                  onPress={() => {
                    this.setState({
                      type:
                        this.state.type === Camera.Constants.Type.back
                          ? Camera.Constants.Type.front
                          : Camera.Constants.Type.back,
                    });
                  }}
                  name="ios-reverse-camera"
                  style={{ color: 'white', fontWeight: 'bold' }}
                />
              </View>
            </Header>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingHorizontal: 10,
                marginBottom: 15,
                alignItems: 'flex-end',
              }}
            >
              <MaterialCommunityIcons
                name="message-reply"
                style={{ color: 'white', fontSize: 36 }}
              ></MaterialCommunityIcons>

              <View style={{ alignItems: 'center' }}>
                <MaterialCommunityIcons
                  name="circle-outline"
                  style={{ color: 'white', fontSize: 100 }}
                ></MaterialCommunityIcons>
                <Icon
                  name="ios-images"
                  style={{ color: 'white', fontSize: 36 }}
                />
              </View>
              <MaterialCommunityIcons
                name="google-circles-communities"
                style={{ color: 'white', fontSize: 36 }}
              ></MaterialCommunityIcons>
            </View> */}
        </Camera>
      </CameraWrapper>
    )
  }
}
export default CameraComponent
