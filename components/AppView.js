import React from 'react'
import {
  SafeAreaView, Platform,
} from 'react-native'
import styled from 'styled-components/native'
import { useSelector } from 'react-redux'
import { createSelector } from 'reselect'
import LoadingLayer from 'components/LoadingLayer'

const AppBg = styled.View`
  background: #ffffff;
  flex: 1;

  ${ (props) => props.forceTopPadding
    && `
        padding-top: 24px;
    ` }
`

const AppView = styled(SafeAreaView)`
  /* background: transparent; */
  flex: 1;
`

export default ({ children }) => {
  // const isAndroid = Platform.OS === 'android'

  const selectIsAppLoading = createSelector(
    (state) => state,
    (state) => Object.keys(state).some((key, index) => state[ key ].loading === true),
  )

  const isAppLoading = useSelector(selectIsAppLoading)

  return (
  // <AppBg { ...{ forceTopPadding: isAndroid } }>
    <AppView>
      {children}
      {isAppLoading && <LoadingLayer />}
    </AppView>
  // </AppBg>
  )
}
