import React from 'react'

import {
  StyleSheet, Text, Platform, Image,
} from 'react-native'

import LoadingLogo from 'assets/logos/loading.gif'

import { BlurView } from 'expo-blur'

const LoadingLayer = (props) => (
  <BlurView
    tint="light"
    intensity={ Platform.OS === 'ios' ? 60 : 90 }
    style={ [ StyleSheet.absoluteFill, { zIndex: 99999, elevation: 99999 } ] }
  >
    <Image
      style={ {
        width: 150,
        height: 150,
        position: 'absolute',
        top: '30%',
        alignSelf: 'center',
        opacity: 0.7,
      } }
      source={ require('assets/logos/loading.gif') }
      resizeMode="contain"
    />
  </BlurView>
)

export default LoadingLayer
