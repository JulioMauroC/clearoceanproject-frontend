import React from 'react'
import {
  View, Text, StyleSheet, Image, Dimensions,
} from 'react-native'

import { COLORS, FONTS, FONT_SIZES } from 'consts'

const { width, height } = Dimensions.get('screen')

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',

  },
  textContainer: {
    alignItems: 'center',
    justifyContent: 'center',

  },
  text: {
    fontFamily: FONTS.DEFAULT,
    fontSize: FONT_SIZES.HEADER,
    textShadowColor: 'rgba(0, 0, 0, 0.15)',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 4,

  },

  image: {
    marginTop: 10,
    width: 85,
    alignSelf: 'center',

  },
})

const FormProgress = (props) => {
  const { header, subheader, iconUrl } = props
  return (
    <View style={ styles.container }>
      <View style={ styles.textContainer }>
        <Text style={ styles.text }>{header}</Text>
        <Text style={ styles.text }>{subheader}</Text>
      </View>
      <Image style={ styles.image } source={ iconUrl } resizeMode="contain" />
    </View>
  )
}

export default FormProgress
