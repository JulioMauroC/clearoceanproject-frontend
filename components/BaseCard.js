import React from 'react'
import { View, StyleSheet } from 'react-native'

const BaseCard = (props) => {
  const styles = StyleSheet.create({
    container: {
      backgroundColor: 'white',
      flex: 1,
      // justifyContent: 'space-around',
    },
    card: {
      // flex: 1,
      backgroundColor: 'white',
      padding: 30,
      // width: props.width,
      // height: props.height,
      alignSelf: 'center',
      borderRadius: 5,
      shadowColor: '#497E93',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.5,
      shadowRadius: 3.84,
    },
  })

  return (
  // <View style={ styles.container }>
  //   <View style={ styles.card }>
  //     {props.children}
  //   </View>

    // </View>
    <View style={ styles.container }>{props.children}</View>
  )
}

export default BaseCard
