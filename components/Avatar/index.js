import React from 'react'
import styled from 'styled-components/native'

const AvatarWrapper = styled.View`
  width: ${ (props) => (props.size ? props.size : props.theme.dimensions.avatarDiameter) };
  height: ${ (props) => (props.size ? props.size : props.theme.dimensions.avatarDiameter) };
  border-radius: ${ (props) => props.theme.dimensions.avatarDiameter / 2 };
  background-color: ${ (props) => props.theme.colors.lightBlue };
  justify-content: center;
  align-items: center;
    border:1.5px solid white;
  
`

const AvatarImage = styled.Image`
  width: ${ (props) => (props.size ? props.size : props.theme.dimensions.avatarDiameter) - 3 };
  height: ${ (props) => (props.size ? props.size : props.theme.dimensions.avatarDiameter) - 3 };
  border-radius: ${ (props) => props.theme.dimensions.avatarDiameter / 2 };
  background-color: transparent;
  /* border:1.5px solid white; */

`

const AvatarInitials = styled.Text`
  font-family: 'Lato-Bold';
  font-size: ${ (props) => props.theme.dimensions.avatarDiameter / 4 };
  color: white;
`

const Avatar = (props) => {
  const {
    imgUrl, firstName, lastName, size,
  } = props
  return (
    <AvatarWrapper size={ size }>
      {imgUrl ? (
        <AvatarImage size={ size } resizeMode="cover" source={ { uri: imgUrl } } />
      ) : (
        <AvatarInitials>{`${ firstName?.charAt(0) } ${ lastName?.charAt(0) }` }</AvatarInitials>
      )}
    </AvatarWrapper>
  )
}

export default Avatar
