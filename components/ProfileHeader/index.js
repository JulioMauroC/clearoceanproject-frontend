import React from 'react'
import Text from 'components/Text/Text'
import styled from 'styled-components/native'
import { useSelector } from 'react-redux'
import store from 'store/store'
import { useRoute } from '@react-navigation/native'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import Icon from 'components/Icon'
import Avatar from 'components/Avatar'

import { TouchableOpacity } from 'react-native-gesture-handler'
import { COLORS } from 'consts'
import AdminBadge from './AdminBadge'

const LogoutButtonWrapper = styled.View`
  margin-left: auto;
  margin-right: 25px;
`

const LogoutButton = (props) => {
  const route = useRoute()

  return (
    route.name === 'Dashboard' && (
      <LogoutButtonWrapper>
        <TouchableOpacity
          onPress={ () => store.dispatch({ type: 'LOGOUT_USER_REQUEST' }) }
        >
          <Icon icon="sign-out-alt" color={ COLORS.DARKEST_GRAY } size={ 22 } />
        </TouchableOpacity>
      </LogoutButtonWrapper>
    )
  )
}

const ProfileHeaderWrapper = styled.ImageBackground`
  width: 100%;
  height: ${ (props) => props.theme.dimensions.yachtImageHeight };
  box-shadow: ${ (props) => props.theme.shadows.light };
  justify-content: flex-start;
  position: absolute;
  top: 0;
  left: 0;
  background: transparent;
`

const ProfileHeaderImage = styled.ImageBackground`
  flex: 1;
  width: 100%;
  height: 100%;
  position: relative;
  background: transparent;
  align-self: flex-start;
`
const ProfileHeaderPlaceholder = styled.View`
  flex: 1;
  width: 100%;
  height: 100%;
  position: relative;
  background: ${ (props) => props.theme.colors.lightBlue };
  align-self: flex-start;
  border-bottom-right-radius: ${ widthToDp('22%') };
  justify-content: center;
  align-items: center;
`
const ProfileUserContent = styled.View`
  position: absolute;
  top: ${ (props) => props.theme.dimensions.yachtImageHeight
    - props.theme.dimensions.avatarDiameter / 2 };
  width: 100%;

  padding-left: ${ (props) => props.theme.dimensions.appHorizontalPadding };
  /* padding-right: ${ (props) => props.theme.dimensions.appHorizontalPadding }; */
`

const ProfileUserTextContent = styled.View`
  position: absolute;
  top: ${ (props) => props.theme.dimensions.avatarDiameter / 2 };
  left: ${ (props) => props.theme.dimensions.avatarDiameter
    + props.theme.dimensions.appHorizontalPadding * 1.2 };
  width: 50%;
  padding-top: ${ (props) => props.theme.dimensions.paddingSmall };
`

const ProfileHeader = (props) => {
  const currUserState = useSelector((state) => state.USERS.currentUserData)
  // const currYachtState = useSelector((state) => state.YACHTS.currentYachtData)
  const currYachtState = useSelector(
    (state) => state.USERS?.currentUserData?.yacht,
  )

  console.log('props')
  console.log(props)

  return (
    <ProfileHeaderWrapper>
      {currYachtState?.yachtImage ? (
        <ProfileHeaderImage
          imageStyle={ { borderBottomRightRadius: widthToDp('22%') } }
          resizeMode="cover"
          source={ { uri: currYachtState.yachtImage } }
          style={ { position: 'relative' } }
        />
      ) : (
        <ProfileHeaderPlaceholder>
          <Icon
            color="rgba(222, 222, 222, 0.9)"
            icon="ship"
            size={ widthToDp('22%') }
          />
        </ProfileHeaderPlaceholder>
      )}

      <ProfileUserContent>
        <Avatar
          imgUrl={ currUserState?.profileImage }
          firstName={ currUserState?.firstName }
          lastName={ currUserState?.lastName }
        />
        {currUserState?.isAdmin && <AdminBadge />}

        <ProfileUserTextContent>
          <Text size={ 22 }>{currYachtState?.name}</Text>
          <Text style={ { paddingTop: 3, color: 'black' } }>
            {`${ currUserState?.firstName } ${ currUserState?.lastName } `}

            <Text size={ 10 }>{`(${ currUserState?.position })`}</Text>
          </Text>
        </ProfileUserTextContent>
      </ProfileUserContent>
      <LogoutButton style={ { position: 'absolute', bottom: 0 } } />
    </ProfileHeaderWrapper>
  )
}

export default ProfileHeader
