import React from 'react'
import styled from 'styled-components/native'
import { COLORS } from 'consts'

import {
  widthPercentageToDP as widthToDp,
  heightPercentageToDP as heightToDp,
} from 'react-native-responsive-screen'

import Icon from 'components/Icon'

const AdminBadgeWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  width: auto;

  position: absolute;

  left: ${ (props) => props.theme.dimensions.avatarDiameter / 3 };
  top: ${ (props) => props.theme.dimensions.avatarDiameter };
  /* padding-top: ${ (props) => props.theme.dimensions.paddingSmall }; */
`

const AdminBadgeText = styled.Text`
margin-left:5px;
font-size:16;
font-family:'Lato-Regular';
color:${ COLORS.PURPLE };
`

const AdminBadge = (props) => (
  <AdminBadgeWrapper>
    <Icon color={ COLORS.PURPLE } icon="star-christmas" size={ widthToDp('4%') } />
    <AdminBadgeText>Admin</AdminBadgeText>
  </AdminBadgeWrapper>
)

export default AdminBadge
