import React from 'react'
import {
  Text,
  View,
  StyleSheet,
  Modal,
  TouchableHighlight,
} from 'react-native'

import { COLORS } from 'consts'

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    // margin: 20,
    backgroundColor: COLORS.GREEN,
    borderColor: COLORS.GREEN,
    borderWidth: 2,
    borderRadius: 20,
    padding: 35,
    color: 'white',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 15,
    },
    shadowOpacity: 1,
    shadowRadius: 5,
    elevation: 5,
    width: '80%',
    height: '40%',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  openButton: {
    backgroundColor: COLORS.GREEN,
    borderRadius: 12,
    paddingVertical: 8,
    paddingHorizontal: 15,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 18,
    opacity: 0.9,
  },
  modalTextBold: {
    color: 'white',
    textAlign: 'center',
    fontSize: 18,
    opacity: 1,
  },
  modalSubText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 14,
    opacity: 0.7,
    marginBottom: 10,
  },
  confirmButton: {
    backgroundColor: '#FFD500',
    borderRadius: 12,
    paddingVertical: 8,
    paddingHorizontal: 15,
    elevation: 2,
  },
  cancelButton: {
    backgroundColor: 'black',
    borderRadius: 12,
    paddingVertical: 8,
    paddingHorizontal: 15,
    elevation: 2,
    borderColor: '#FFD500',
    borderWidth: 1,
  },
  confirmTextStyle: {
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  cancelTextStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
})

const ConfirmationModal = (props) => {
  const { modalVisible, setModalVisible } = props

  return (
    <Modal
      animationType="fade"
      onDismiss={ () => console.log('dismissed') }
      transparent
      visible={ modalVisible }
      onRequestClose={ () => {
        Alert.alert('Modal has been closed.')
      } }
    >
      <View style={ styles.centeredView }>
        <View style={ styles.modalView }>
          <Text style={ styles.modalText }>
            Are you sure you would like to
            {' '}
            <Text style={ styles.modalTextBold }> </Text>
            {' '}
            on
            {' '}
          </Text>

          <TouchableHighlight
            style={ styles.confirmButton }
            onPress={ () => console.log('clicked') }
          >
            <Text style={ styles.confirmTextStyle }>Yes i am sure</Text>
          </TouchableHighlight>

          <TouchableHighlight
            style={ styles.cancelButton }
            onPress={ () => {
              setModalVisible(!modalVisible)
            } }
          >
            <Text style={ styles.cancelTextStyle }>No, please go back</Text>
          </TouchableHighlight>
        </View>
      </View>
    </Modal>
  )
}

export default ConfirmationModal
